<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_payment_method')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('payment');

// LOAD PAYMENTMETHOD MODAL
$paymentmethod_model = $registry->get('loader')->model('paymentmethod');

// validate post data
function validate_request_data($request, $language) 
{
  // validate name
  if (!validateString($request->post['payment_name'])) {
    throw new Exception($language->get('error_payment_method_name'));
  }

  // validate store
  if (!isset($request->post['payment_store']) || empty($request->post['payment_store'])) {
    throw new Exception($language->get('error_store'));
  }

  // validate status
  if (!is_numeric($request->post['status'])) {
    throw new Exception($language->get('error_status'));
  }

  // validate sort order
  if (!is_numeric($request->post['sort_order'])) {
    throw new Exception($language->get('error_sort_order'));
  }
}

// check, if payment method exist or not
function validate_existance($request, $language, $id = 0)
{
  global $db;

  // check, if payment method is exist or not
  $statement = $db->prepare("SELECT * FROM `payments` WHERE `name` = ? AND `payment_id` != ?");
  $statement->execute(array($request->post['payment_name'], $id));
  if ($statement->rowCount() > 0) {
    throw new Exception($language->get('error_payment_method_exist'));
  }
}

// create payment method
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'CREATE')
{
  try {

    // create permission check
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'create_payment_method')) {
      throw new Exception($language->get('error_read_permission'));
    }
    
    // validate post data
    validate_request_data($request, $language);

    // validate existance
    validate_existance($request, $language);

    $Hooks->do_action('Before_Create_PMethod');

    // Insert new payment into database
    $paymentmethod_id = $paymentmethod_model->addPaymentMethod($request->post);

    // get payment method
    $payment_method = $paymentmethod_model->getPaymentMethod($paymentmethod_id);

    $Hooks->do_action('After_Create_PMethod', $payment_method);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_success'), 'id' => $paymentmethod_id, 'payment_method' => $payment_method));
    exit();
  }
  catch(Exception $e) { 
    
    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// update payment method
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'UPDATE')
{
  try {

    // check delete permision
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'update_payment_method')) {
      throw new Exception($language->get('error_update_permission'));
    }

    // validate product id
    if (empty($request->post['payment_id'])) {
      throw new Exception($language->get('error_payment_id'));
    }

    $id = $request->post['payment_id'];

    if (DEMO && $id == 1) {
      throw new Exception($language->get('error_update_permission'));
    }

    // validate post data
    validate_request_data($request, $language);

    // validate existance
    validate_existance($request, $language, $id);

    $Hooks->do_action('Before_Update_PMethod', $request);
    
    // edit payment method
    $pmethod = $paymentmethod_model->editPaymentMethod($id, $request->post);

    $Hooks->do_action('After_Update_PMethod', $pmethod);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_update_success'), 'id' => $id));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 


// delete payment method
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'DELETE') 
{
  try {

    // check delete permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'delete_payment_method')) {
      throw new Exception($language->get('error_delete_permission'));
    }

    // validate payment id
    if (!validateInteger($request->post['payment_id'])) {
      throw new Exception($language->get('error_payment_id'));
    }

    $id = $request->post['payment_id'];
    $new_payment_id = $request->post['new_payment_id'];

    if (DEMO && $id == 1) {
      throw new Exception($language->get('error_delete_permission'));
    }

    if ($request->post['delete_action'] == 'insert_to' && !validateInteger($new_payment_id)) {
      throw new Exception($language->get('error_payment_name'));
    }

    $Hooks->do_action('Before_Delete_PMethod', $request);

    $belongs_stores = $paymentmethod_model->getBelongsStore($id);
    foreach ($belongs_stores as $the_store) {

      // check if relationship exist or not
      $statement = $db->prepare("SELECT * FROM `payment_to_store` WHERE `payment_id` = ? AND `store_id` = ?");
      $statement->execute(array($new_payment_id, $the_store['store_id']));
      if ($statement->rowCount() > 0) continue;

      // create relationship
      $statement = $db->prepare("INSERT INTO `payment_to_store` SET `payment_id` = ?, `store_id` = ?");
      $statement->execute(array($new_payment_id, $the_store['store_id']));
    }

    if ($request->post['delete_action'] == 'insert_to') {
      // update invoice payment method
      $statement = $db->prepare("UPDATE `selling_info` SET `payment_method` = ? WHERE `payment_method` = ?");
      $statement->execute(array($new_payment_id, $id));
    }

    // delete payment method
    $payment_method = $paymentmethod_model->deletePaymentMethod($id);

    $Hooks->do_action('Before_Delete_PMethod', $payment_method);
    
    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_delete_success')));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
}

// payment method edit form
if (isset($request->get['payment_id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'EDIT') {
    // fetch payment method
    $payment = $paymentmethod_model->getPaymentMethod($request->get['payment_id']);
    include 'template/payment_form.php';
    exit();
}

// payment method delete form
if (isset($request->get['payment_id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'DELETE') {
    // fetch payment method
    $payment = $paymentmethod_model->getPaymentMethod($request->get['payment_id']);
    $Hooks->do_action('Before_PMethod_Delete_Form', $payment);
    include 'template/payment_del_form.php';
    $Hooks->do_action('After_PMethod_Delete_Form', $payment);
    exit();
}

/**
 *===================
 * START DATATABLE
 *===================
 */

$Hooks->do_action('Before_Showing_PMethod_List');

$where_query = 'pay2s.store_id = ' . store_id();
 
// DB table to use
$table = "(SELECT payments.*, pay2s.status, pay2s.sort_order FROM payments 
  LEFT JOIN payment_to_store pay2s ON (payments.payment_id = pay2s.payment_id) 
  WHERE $where_query GROUP by payments.payment_id
  ) as payments";
 
// Table's primary key
$primaryKey = 'payment_id';
 
$columns = array(
  array(
      'db' => 'payment_id',
      'dt' => 'DT_RowId',
      'formatter' => function( $d, $row ) {
          return 'row_'.$d;
      }
  ),
  array( 
    'db' => 'payment_id',   
    'dt' => 'select' ,
    'formatter' => function($d, $row) {
        return '<input type="checkbox" name="selected[]" value="' . $row['payment_id'] . '">';
    }
  ),
  array( 'db' => 'payment_id', 'dt' => 'payment_id' ),
  array( 
    'db' => 'name',   
    'dt' => 'name' ,
    'formatter' => function($d, $row) {
        return $row['name'];
    }
  ),
  array( 'db' => 'sort_order', 'dt' => 'sort_order' ),
  array( 'db' => 'details', 'dt' => 'details' ),
  array( 
    'db' => 'status',   
    'dt' => 'status' ,
    'formatter' => function($d, $row) use($language) {
        return $row['status'] == 1 ? '<span class="label label-info">'.$language->get('text_active').'</span>' : '<span class="label label-warning">In-Active</span>';
    }
  ),
  array( 
    'db' => 'status',   
    'dt' => 'btn_edit' ,
    'formatter' => function($d, $row) use($language) {
      if (DEMO && $row['payment_id'] == 1) {          
        return'<button class="btn btn-sm btn-block btn-default" type="button" disabled><i class="fa fa-pencil"></i></button>';
      }
      return '<button id="edit-payment" class="btn btn-sm btn-block btn-primary" type="button" title="'.$language->get('button_edit').'"><i class="fa fa-fw fa-pencil"></i></button>';
    }
  ),
  array( 
    'db' => 'status',   
    'dt' => 'btn_delete' ,
    'formatter' => function($d, $row) use($language) {
      if (DEMO && $row['payment_id'] == 1) {          
        return'<button class="btn btn-sm btn-block btn-default" type="button" disabled><i class="fa fa-trash"></i></button>';
      }
      return '<button id="delete-payment" class="btn btn-sm btn-block btn-danger" type="button" title="'.$language->get('button_delete').'"><i class="fa fa-fw fa-trash"></i></button>';
    }
  )
);

// output for datatable
echo json_encode(
  SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns)
);

$Hooks->do_action('After_Showing_PMethod_List');

/**
 *===================
 * END DATATABLE
 *===================
 */