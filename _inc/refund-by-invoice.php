<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_product')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('management');

// LOAD PRODUCT MODEL
$product_model = $registry->get('loader')->model('product');

// LOAD invoice MODEL
$invoice_model = $registry->get('loader')->model('invoice');

// validate post data
function validate_request_data($request, $language) {

  if (empty($request->post['quantity'])) {
    throw new Exception('Please enter return quantity.');
  }

   // validate product code
  if (!validateInteger($request->post['quantity'])) {
    throw new Exception('Please enter validate quantity');
  }

  if ( !empty($request->post['restock_to_inventory']) && empty($request->post['comments'])) {
    throw new Exception($language->get('error_comments'));
  }

  if ( !empty($request->post['restock_to_inventory']) && empty($request->post['p_image'])) {
    throw new Exception('Please upload image');
  }
}
// update product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'UPDATE')
{



  try {
    
    validate_request_data($request, $language);

    if ($request->post['quantity'] > $request->post['total_qty']) {
      throw new Exception('Enter maximum '. $request->post['total_qty'] .' Quantity.');
    }

    $info_id = $request->post['sell_item_id'];

    if (DEMO && $info_id == 1) {
      throw new Exception($language->get('error_update_permission'));
    }

    $Hooks->do_action('Before_Update_Refund_By_Invoice', $info_id);
    
    // edit product        
   $result = $product_model->editRefundProduct($request->post);
   print_r($result);exit;

    $Hooks->do_action('After_Update_Refund_By_Invoice', $info_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_update_success'), 'id' => $result));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// edit form
if (isset($request->get['customer_id']) AND isset($request->get['sell_item_id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'EDIT') {

  // fetch product info
  // $data = $discbycategory_model->getPromotions($request->get['promotions_type'],$request->get['id']);
    
  $sell_item = $invoice_model->getInvoiceBySellItems($request->get['sell_item_id']);

  $data['customer_id'] = $request->get['customer_id'];
  $data['invoice_id']  = $request->get['invoice_id'];
  $data['sell_item_id']  = $request->get['sell_item_id'];
  $data['item_quantity']  = $sell_item['item_quantity'];
  // $data = $product_model->getProductByInfoId($request->get['info_id'],$request->get['customer_id'],$request->get['invoice_id']);

  $Hooks->do_action('Before_Showing_Refund_By_Invoice_Edit_Form', $data);
  include 'template/refund_by_invoice_edit_form.php';
  $Hooks->do_action('After_Showing_Refund_By_Invoice_Edit_Form', $data);

  exit();
}


/**
 *===================
 * START DATATABLE
 *===================
 */

$Hooks->do_action('Before_Showing_Refund_List');
$customer_id = $request->get['customer_id'];
$invoice_id  = $request->get['invoice_id'];

$where_query = 'selling_info.invoice_id = "'.$invoice_id.'" AND selling_info.customer_id ='.$customer_id;

$table = "(SELECT selling_info.*,
            sell_item.id AS sell_item_id,
            sell_item.returned AS returned,
            sell_item.item_name AS item_name,
            sell_item.item_quantity AS item_quantity,
            sell_item.item_price AS item_price,
            sell_item.item_discount AS item_discount,
            sell_item.item_tax AS item_tax
            FROM selling_info 
            JOIN selling_item AS sell_item ON sell_item.invoice_id = selling_info.invoice_id
            WHERE $where_query
          ) as selling_info";
// Table's primary key
$primaryKey = 'sell_item_id';
$columns = array(
  array(
      'db' => 'sell_item_id',
      'dt' => 'DT_RowId',
      'formatter' => function( $d, $row ) {
          return 'row_'.$row['sell_item_id'];
      }
  ),
  array( 
    'db' => 'sell_item_id',   
    'db' => 'returned',  
    'db' => 'customer_id',
    'dt' => 'select' ,
    'formatter' => function($d, $row) {
      if ($row['returned']) {
        return '';
      }
      else {
        return '<input type="hidden" name="customer_id[]" value="'.$row['customer_id'].'"><input type="checkbox" name="selected[]" value="' . $row['sell_item_id'] . '">';
      }
    }
  ),
  array( 'db' => 'item_quantity',  'dt' => 'item_quantity' ),
  array( 'db' => 'item_name',  'dt' => 'item_name' ),
  array( 'db' => 'item_price',  'dt' => 'item_price' ),
  array( 'db' => 'item_discount',  'dt' => 'item_discount' ),
  array( 'db' => 'item_tax',  'dt' => 'item_tax' ),
  array( 'db' => 'customer_id',  'dt' => 'customer_id' ),
  array( 'db' => 'invoice_id',  'dt' => 'invoice_id' ),
  array( 'db' => 'info_id',  'dt' => 'info_id' ),
  array( 'db' => 'sell_item_id',  'dt' => 'sell_item_id' ),
  array( 'db' => 'return_by',  'dt' => 'return_by' ),
  array( 'db' => 'returned',  'dt' => 'returned',
    'formatter' => function($d, $row) use($language) {
        if ($row['returned']) {
          return'Yes';
        }
        else {
          return'No';
        }
    }),

  
  array( 
    'db' => 'customer_id',   
    'db' => 'returned',   
    'dt' => 'edit_btn' ,
    'formatter' => function($d, $row) use($language) {
        if ($row['returned']) {
          return'<button class="btn btn-sm btn-block btn-primary edit-refuncd-by-invoice" disabled type="button" title="'.$language->get('button_allready_return_product').'"><img src="../assets/wonderpillars/img/imgpsh_fullsize.png"></button>';
        }
        else {
          return'<button class="btn btn-sm btn-block btn-primary edit-refuncd-by-invoice" type="button" title="'.$language->get('button_return_product').'"><img src="../assets/wonderpillars/img/imgpsh_fullsize.png"></button>';
        }
    }
  )
);
 

// output for datatable
echo json_encode(
  SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns, null, $where_query)
);

$Hooks->do_action('After_Showing_Discount_By_Coupons_List');

/**
 *===================
 * END DATATABLE
 *===================
 */