<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'create_invoice')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILES
$language->load('invoice');
$language->load('pos');

// LOAD INVOICE MODEL
$invoice_model = $registry->get('loader')->model('invoice');

// validate customer post data
function validate_customer_request_data($request, $language) 
{
  // validate customer id
  if (!validateInteger($request->post['customer-id'])) {
    throw new Exception($language->get('error_invoice_customer'));
  }
}

// validate invoice items
function validate_invoice_items($invoice_items, $language)
{
  // loop through produdt items for validation checking
  foreach ($invoice_items as $product) 
  {
      // validate product id
      if (!validateInteger($product['item_id'])) {
        throw new Exception($language->get('error_invalid_product_id'));
      }

      // fetch product item
      $the_product = get_the_product($product['item_id'], null, store_id());

      // check, product item exist or not
      if (!$the_product) {
        throw new Exception($language->get('error_product_not_found'));
      }

      // check, product item stock availabel or not
      if ($the_product['quantity_in_stock'] <= 0) {
        throw new Exception($language->get('error_out_of_stock'));
      }

      // chcck, requested quantity is greater than of existing quantity or not
      if ($the_product['quantity_in_stock'] < $product['item_quantity']) {
        throw new Exception($language->get('error_quantity_exceed'));
      }

      // validate product name
      if (!validateString($product['item_name'])) {
        throw new Exception($language->get('error_invoice_product_name'));
      }

      // validate product price
      if (!validateFloat($product['item_price'])) {
        throw new Exception($language->get('error_invoice_product_price'));
      }

      // validate product quantity
      if (!validateInteger($product['item_quantity'])) {
        throw new Exception($language->get('error_invoice_product_quantity'));
      }

      // validate product total price
      if (!validateFloat($product['item_total'])) {
        throw new Exception($language->get('error_invoice_product_total'));
      }
  }
}

if ($request->server['REQUEST_METHOD'] == 'POST')
{
  try {

    $store_id     = store_id();
    $created_at   = date('Y-m-d H:i:s');
    $invoice_id   = isset($request->post['invoice-id']) ? $request->post['invoice-id'] : null;

    validate_customer_request_data($request, $language);

    // validate sub-total
    if (!validateFloat($request->post['sub-total'])) {
      throw new Exception($language->get('error_invoice_sub_total'));
    }

    // validate discount amount
    if (!is_numeric($request->post['discount-amount'])) {
      throw new Exception($language->get('error_invoice_discount_amount'));
    }

    // validate tax amount
    if (!is_numeric($request->post['tax-amount'])) {
      throw new Exception($language->get('error_invoice_tax_amount'));
    }

    // validate previous due
    if (!is_numeric($request->post['previous-due'])) {
      throw new Exception($language->get('error_invoice_previous_due'));
    }

    // validate payable amount
    if (!validateFloat($request->post['payable-amount'])) {
      throw new Exception($language->get('error_invoice_payable_amount'));
    }

    // validate paid amount
    if (!is_numeric($request->post['paid-amount'])) {
      throw new Exception($language->get('error_invoice_paid_amount'));
    }
    
    if ($request->post['paid-amount'] > $request->post['payable-amount']) {
      throw new Exception($language->get('error_invoice_exceed_paid_amount'));
    }

    if ($request->post['paid-amount'] < 0) {
      throw new Exception($language->get('error_invoice_below_paid_amount'));
    }

    // validate payment method id
    if (!validateString($request->post['payment-method-id'])) {
      throw new Exception($language->get('error_invoice_payment_method'));
    }

    // validate present due
    if (!is_numeric($request->post['present-due'])) {
      throw new Exception($language->get('error_invoice_present_due'));
    }

    // validate present due
    if ($request->post['paid-amount'] < 0  || ($request->post['paid-amount'] > $request->post['payable-amount'])) {
      throw new Exception($language->get('error_invoice_paid_amount'));
    }

    // validate invoice product items
    if (!isset($request->post['product-item']) 
      && (isset($request->post['product-item']) || !is_array($request->post['product-item']))) {

      throw new Exception($language->get('error_product_item'));
    }

    $product_items = $request->post['product-item'];
    $total_product = count($request->post['product-item']);
    $invoice_note   = $request->post['invoice-note'];
    $customer_id    = $request->post['customer-id'];
    $currency_code  = $currency->getCode();
    $sub_total      = $request->post['sub-total'];
    $discount_type  = $request->post['discount-type'];
    $discount_amount= $request->post['discount-amount'];
    $tax_amount     = $request->post['tax-amount'];
    $paid_amount    = $request->post['paid-amount'];
    $present_due    = $request->post['present-due'];
    $previous_due   = $request->post['previous-due'];
    $payment_method_id  = $request->post['payment-method-id'];

    $todays_amount = ($sub_total - $discount_amount) + $tax_amount;
    $todays_due = $todays_amount > $paid_amount ? $todays_amount - $paid_amount : 0;
    $due_paid_amount = $paid_amount > $todays_amount ? $paid_amount - $todays_amount : 0;

    $product_discount = $discount_amount / $total_product;
    $product_tax = $tax_amount / $total_product;

    $is_paid = $todays_due ? 0 : 1;
    $is_due = !$is_paid ? 1 : 0;

    // check, if invoice exist or not
    $statement = $db->prepare("SELECT * FROM `selling_info` WHERE `store_id` = ? AND `invoice_id` = ?");
    $statement->execute(array($store_id, $invoice_id));
    $invoice_info = $statement->fetch(PDO::FETCH_ASSOC);

    // check due create permission
    if ($user->getGroupId() != 1 && $todays_due > 0 && !$user->hasPermission('access', 'create_due')) {
      throw new Exception($language->get('error_create_due_permission'));
    }

    // check due paid permission
    if ($user->getGroupId() != 1 && $paid_amount > $todays_amount && !$user->hasPermission('access', 'due_paid')) {
      throw new Exception($language->get('error_due_paid_permission'));
    }

    //===================
    // Edit Invoice
    //===================

    if ($invoice_info) {

      // check invoice update permission
      if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'update_invoice')) {
        throw new Exception($language->get('error_update_permission'));
      }

      // check invoice edit permission
      if (!has_invoice_edit_permission($invoice_info['created_at'], $customer_id, $invoice_id, $store_id)) {
        throw new Exception($language->get('error_edit_duration_expired'));
      }

      $existing_invoice_items = $invoice_model->getInvoiceItems($invoice_id);
      // check item item from invoice permission
      if (!($user->getGroupId() == 1) && !$user->hasPermission('access', 'remove_item_from_invoice')) {
        foreach ($existing_invoice_items as $existing_item) {
          if (!isset($product_items["'".$existing_item['item_id']."'"])) {
            throw new Exception($language->get('error_have_no_item_remove_permission'));
          }
        }
      }  

      // check add item to invoice permission
      if (count($existing_invoice_items) < count($product_items)) {
        if (!($user->getGroupId() == 1) && !$user->hasPermission('access', 'add_item_to_invoice')) {
          throw new Exception($language->get('error_have_no_add_item_to_invoice_permission'));
        }
      }

      // fetch selling invoice item
      $statement = $db->prepare("SELECT * FROM `selling_item` WHERE `store_id` = ? AND `invoice_id` = ?");
      $statement->execute(array($store_id, $invoice_id));
      $invoice_items = $statement->fetchAll(PDO::FETCH_ASSOC);

      // check, invoice item exist or not
      if (!$statement->rowCount()) {
        throw new Exception($language->get('error_invoice_item'));
      }

      foreach ($invoice_items as $product) 
      {
        $product_id = $product['item_id'];
        $product_quantity = $product['item_quantity'];
        $buying_invoice_id = $product['buying_invoice_id'];

        // fetch buying invoice item
        $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `invoice_id` = ? AND `item_id` = ? AND `status` = ?");
        $statement->execute(array($store_id, $buying_invoice_id, $product_id, 'active'));
        $buying_item = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$buying_item) {
          throw new Exception($language->get('error_invoice_closed'));
        }

        // check if the buying invocie is sold out or not if sold out then the item is unable to return
        if ($buying_item['status'] == 'sold' || $buying_item['status'] != 'active') {
          throw new Exception('The product item ' . $buying_item['item_name'] . ' can not be returned. Because the invoice is closed.');
        }

        if ($product_quantity > (int)$buying_item['total_sell']) {
          throw new Exception($product_quantity . ' ' . $buying_item['item_name'] . ' can not be returned. try after decreasing the quantity!');
        }
      }

      foreach ($invoice_items as $product) 
      {
        $product_id = $product['item_id'];
        $product_quantity = $product['item_quantity'];
        $buying_invoice_id = $product['buying_invoice_id'];

        for ($i=0; $i < $product_quantity; $i++) {
          // increment item valude in buying item table
          $statement = $db->prepare("UPDATE `buying_item` SET `total_sell` = `total_sell` - 1 WHERE `store_id` = ? AND `item_id` = ? AND `status` = ?");
          $statement->execute(array($store_id, $product_id, 'active'));
        }
        
        // increment product quantity
        $statement = $db->prepare("UPDATE `product_to_store` SET `quantity_in_stock` = `quantity_in_stock` + $product_quantity WHERE `store_id` = ? AND `product_id` = ?");
        $statement->execute(array($store_id, $product_id));

        // delete selling item from invoice
        $statement = $db->prepare("DELETE FROM `selling_item` WHERE `store_id` = ? AND `item_id` = ? AND `invoice_id` = ?");
        $statement->execute(array($store_id, $product_id, $invoice_id));
      }

      // insert data info selling_price table
      $statement = $db->prepare("UPDATE `selling_price` SET `subtotal` = ?, `discount_type` = ?, `discount_amount` = ?, `tax_amount` = ?, `previous_due` = ?, `payable_amount` = ?, `paid_amount` = ?, `todays_due` = ?, `present_due` = ? WHERE `invoice_id` = ?");
      $statement->execute(array(
        $sub_total,
        $discount_type,
        $discount_amount,
        $tax_amount,
        $request->post['previous-due'],
        $request->post['payable-amount'],
        $paid_amount,
        $todays_due,
        $present_due,
        $invoice_id
      ));

      // update customer balance
      $update_due = $db->prepare("UPDATE `customer_to_store` SET `due_amount` = ?, `currency_code` = ? WHERE `customer_id` = ? AND `store_id` = ?");
      $update_due->execute(array($present_due, $currency_code, $customer_id, $store_id));

      // loop through product items for store infor selling_item table
      foreach ($product_items as $product) 
      {

        $product_id = $product['item_id'];
        $product_name = $product['item_name'];
        $category_id = $product['category_id'];
        $sup_id = $product['sup_id'];
        $product_price   = $product['item_price'];
        $product_quantity   = $product['item_quantity'];
        $product_total = $product['item_total'];

        // buying invoice id
        $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `item_id` = ? AND `status` = ? AND `item_quantity` >= `total_sell`");
        $statement->execute(array($store_id, $product_id, 'active'));
        $buying_item = $statement->fetch(PDO::FETCH_ASSOC);
        $buying_invoice_id = $buying_item['invoice_id'];

        if (!$buying_item) {
          throw new Exception($language->get('error_item_not_found'));
        }

        $Hooks->do_action('Before_Update_Invoice', $invoice_id);

        // buying price adjustment start
        $total_buying_price = 0;
        for ($i=0; $i < $product_quantity; $i++) { 

          // increment item valude in buying item table
          $statement = $db->prepare("UPDATE `buying_item` SET `total_sell` = `total_sell` + 1 WHERE `store_id` = ? AND `item_id` = ? AND `status` = ?");
          $statement->execute(array($store_id, $product_id, 'active'));
          
          // item buying price from buying invoice
          $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `item_id` = ? AND `status` = ?");
          $statement->execute(array($store_id, $product_id, 'active'));
          $buying_item = $statement->fetch(PDO::FETCH_ASSOC);

          $total_buying_price += $buying_item['item_buying_price'];

          if ($buying_item['item_quantity'] <= $buying_item['total_sell']) {

            // increment item valude in buying item table
            $statement = $db->prepare("UPDATE `buying_item` SET `status` = ? WHERE `store_id` = ? AND `item_id` = ? AND `status` = ?");
            $statement->execute(array('sold', $store_id, $product_id, 'active'));

            // fetch another stock item
            $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `item_id` = ? AND `status` = ? ORDER BY `id` ASC LIMIT 1");
            $statement->execute(array($store_id, $product_id, 'stock'));
            $buying_item = $statement->fetch(PDO::FETCH_ASSOC);

            // activate another stock item
            $statement = $db->prepare("UPDATE `buying_item` SET `status` = ? WHERE `id` = ?");
            $statement->execute(array('active', $buying_item['id']));
          } 
        } 

        // insert product int selling_item table
        $statement = $db->prepare("INSERT INTO `selling_item` (invoice_id, store_id, item_id, category_id, sup_id, item_name, total_buying_price, item_price, item_discount, item_tax, item_quantity, item_total, buying_invoice_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $statement->execute(array($invoice_id, $store_id, $product_id, $category_id, $sup_id, $product_name, $total_buying_price, $product_price, $product_discount, $product_tax, $product_quantity, $product_total, $buying_invoice_id));
      
        // decrease product quantity
        $statement = $db->prepare("UPDATE `product_to_store` SET `quantity_in_stock` = `quantity_in_stock` - $product_quantity WHERE `store_id` = ? AND `product_id` = ?");
        $statement->execute(array($store_id, $product_id));
      }

      // increment edit counter
      $statement = $db->prepare("UPDATE `selling_info` SET `edit_counter` = `edit_counter` + 1, `is_paid` = ?, `is_due` = ? WHERE `store_id` = ? AND `invoice_id` = ?");
      $statement->execute(array($is_paid, $is_due, $store_id, $invoice_id));

      $Hooks->do_action('After_Update_Invoice', $invoice_id);
    }

    //====================
    // Create New Invoice
    //====================

    if (!$invoice_info) {

      // check invoice create permission
      if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'create_invoice')) {
        throw new Exception(sprintf($language->get('error_create_permission')));
      }

      validate_invoice_items($product_items, $language);

      // loop through produdt items for validation checking
      foreach ($product_items as $product) 
      {
        $product_id     = $product['item_id'];
        $product_quantity  = $product['item_quantity'];
        for ($i=0; $i < $product_quantity; $i++) { 
          $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `item_id` = ? AND `status` = ? AND `item_quantity` >= `total_sell`");
          $statement->execute(array($store_id, $product_id, 'active'));
          $buying_item = $statement->fetch(PDO::FETCH_ASSOC);
          if (!$buying_item) {
            throw new Exception($language->get('error_item_not_found'));
          }
        }
      }

      $Hooks->do_action('Before_Create_Invoice', $invoice_id);

      // generate unique invoice id
      $invoice_id = generate_invoice_id('sell');

      // insert data info selling_info table
      $statement = $db->prepare("INSERT INTO `selling_info` (invoice_id, store_id, created_at, customer_id, currency_code, payment_method, created_by, invoice_note, is_paid, is_due) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
      $statement->execute(array($invoice_id, $store_id, $created_at, $customer_id, $currency_code, $payment_method_id, $user->getId(), $invoice_note, $is_paid, $is_due));

      // insert data info selling_price table
      $statement = $db->prepare("INSERT INTO `selling_price` (invoice_id, store_id, subtotal, discount_type, discount_amount, tax_amount, previous_due, payable_amount, paid_amount, todays_due, present_due) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
      $statement->execute(array(
        $invoice_id,
        $store_id,
        $sub_total,
        $discount_type,
        $discount_amount,
        $tax_amount,
        $previous_due,
        $request->post['payable-amount'],
        $paid_amount - $due_paid_amount,
        $todays_due,
        $present_due + $due_paid_amount,
      ));

      // update customer balance
      $update_due = $db->prepare("UPDATE `customer_to_store` SET `due_amount` = ?, `currency_code` = ? WHERE `customer_id` = ? AND `store_id` = ?");
      $update_due->execute(array($present_due, $currency_code, $customer_id, $store_id));

      // loop through the invoice product items for storing info. into selling_item table
      // adjustment buying invoice information
      foreach ($product_items as $key => $product) 
      {
        $product_id     = $product['item_id'];
        $category_id     = $product['category_id'];
        $sup_id     = $product['sup_id'];
        $product_name   = $product['item_name'];
        $product_quantity  = $product['item_quantity'];
        $product_price     = $product['item_price'];
        $product_total     = $product['item_total'];

        // buying price adjustment start
        $total_buying_price = 0;
        for ($i=0; $i < $product_quantity; $i++) { 

          $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `item_id` = ? AND `status` = ? AND `item_quantity` > `total_sell`");
          $statement->execute(array($store_id, $product_id, 'active'));
          $buying_item = $statement->fetch(PDO::FETCH_ASSOC);

          $buying_invoice_id = $buying_item['invoice_id'];

          // increment item valude in buying item table
          $statement = $db->prepare("UPDATE `buying_item` SET `total_sell` = `total_sell` + 1 WHERE `store_id` = ? AND `item_id` = ? AND `status` = ?");
          $statement->execute(array($store_id, $product_id, 'active'));

          $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `item_id` = ? AND `status` = ?");
          $statement->execute(array($store_id, $product_id, 'active'));
          $buying_item = $statement->fetch(PDO::FETCH_ASSOC);
          
          // item buying price from buying invoice
          $total_buying_price += $buying_item['item_buying_price'];

          if ($buying_item['item_quantity'] <= $buying_item['total_sell']) {

            // increment item valude in buying item table
            $statement = $db->prepare("UPDATE `buying_item` SET `status` = ? WHERE `store_id` = ? AND `item_id` = ? AND `status` = ?");
            $statement->execute(array('sold', $store_id, $product_id, 'active'));

            // fetch another stock item
            $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `item_id` = ? AND `status` = ? ORDER BY `id` ASC LIMIT 1");
            $statement->execute(array($store_id, $product_id, 'stock'));
            $buying_item = $statement->fetch(PDO::FETCH_ASSOC);

            // activate another stock item
            $statement = $db->prepare("UPDATE `buying_item` SET `status` = ? WHERE `id` = ?");
            $statement->execute(array('active', $buying_item['id']));
          } 
        }
        // buying price adjustment end

        // insert product into selling_item table
        $statement = $db->prepare("INSERT INTO `selling_item` (invoice_id, store_id, item_id, category_id, sup_id, item_name, total_buying_price, item_price, item_discount, item_tax, item_quantity, item_total, buying_invoice_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $statement->execute(array($invoice_id, $store_id, $product_id, $category_id, $sup_id, $product_name, $total_buying_price, $product_price, $product_discount, $product_tax, $product_quantity, $product_total, $buying_invoice_id));
      
        // decrease product quantity
        $statement = $db->prepare("UPDATE `product_to_store` SET `quantity_in_stock` = `quantity_in_stock` - $product_quantity WHERE `store_id` = ? AND `product_id` = ?");
        $statement->execute(array($store_id, $product_id));
      }

      // create due paid invoice
      if ($due_paid_amount > 0) {
        
        $paid_invoice_id = generate_invoice_id('due_paid');

        $datetime = date('Y-m-d H:i:s');
        $present_due = (float)($previous_due - $due_paid_amount);

        $statement = $db->prepare("INSERT INTO `selling_info` (invoice_id, store_id, inv_type, created_at, customer_id, currency_code, payment_method, ref_invoice_id, created_by) VALUES (?,?,?,?,?,?,?,?,?)");
        $statement->execute(array($paid_invoice_id, $store_id, 'due_paid', $datetime, $customer_id, $currency_code, $payment_method_id, $invoice_id, $user->getId()));
        $todays_due = -($due_paid_amount);
        $statement = $db->prepare("INSERT INTO `selling_price` (invoice_id, store_id, previous_due, paid_amount, todays_due, present_due) VALUES (?,?,?,?,?,?)");
        $statement->execute(array($paid_invoice_id, $store_id, $previous_due, $due_paid_amount, $todays_due, $present_due));

        $statement = $db->prepare("INSERT INTO `selling_item` (invoice_id, store_id, item_id, item_name, item_price, item_quantity, item_total) VALUES (?,?,?,?,?,?,?)");
        $statement->execute(array($paid_invoice_id, $store_id, 0, 'Deposit', $due_paid_amount, 1, $due_paid_amount));
      }

      $invoice_info = $invoice_model->getInvoiceInfo($invoice_id);
      $invoice_items = $invoice_model->getInvoiceItems($invoice_id);

      $Hooks->do_action('After_Create_Invoice', $invoice_id);
    }

  	header('Content-Type: application/json');
  	echo json_encode(array('msg' => $language->get('text_success'), 'invoice_id' => $invoice_id, 'invoice_info' => $invoice_info, 'invoice_items' => $invoice_items));
	  exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
}