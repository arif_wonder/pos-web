<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_product')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('management');

// LOAD PRODUCT MODEL
$discbycategory_model = $registry->get('loader')->model('promotions');

// validate post data
function validate_request_data($request, $language) {
  if ( $request->post['promotions_type'] == 'disc_happy_hours') {
    if( !preg_match('/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i',$request->post['time_from']) ) {
      throw new Exception($language->get('error_time_from'));
    }

    if ( !preg_match('/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i',$request->post['time_to']) ) {
      throw new Exception($language->get('error_time_to'));
    }

    //discount
    if (!validateString($request->post['title'])) {
      throw new Exception('Please provide title');
    }

    //discount
    if (!validateString($request->post['discount'])) {
      throw new Exception('Please provide discount');
    }

    //validate minimum purchase
    if (!validateString($request->post['min_purchase_amount'])) {
      throw new Exception('Please provide minimum purchase amount');
    }
    //validate minimum purchase
    if (!validateString($request->post['min_purchase_amount'])) {
      throw new Exception('Please provide minimum purchase amount');
    }

    //validate discount type
    if ($request->post['discount_type'] == 'fixed') {

       if ($request->post['min_purchase_amount'] < $request->post['discount']) {
          throw new Exception('Minimum purchase amount can never be less than discount amount');
        }

        if (!empty($request->post['max_discount']) && 
            $request->post['max_discount'] > $request->post['discount'] ) {
          throw new Exception('Max discount amount can never be greather than discount amount');
        }
      
    }
    

    if ($request->post['discount_type'] == 'precentage') {
      if($request->post['discount'] > 100){
        throw new Exception('Discount precentage must be less than or equal to 100');
      }

       $percentAmount = ($request->post['min_purchase_amount']*$request->post['discount'])/100;
      if($request->post['max_discount'] < round($percentAmount)){
        throw new Exception("Max discount can't be less than ".round($percentAmount));
      }
      
    }

    if (!empty($request->post['max_discount']) && $request->post['max_discount'] > $request->post['min_purchase_amount'] ) {
      throw new Exception('Max discount amount can never be greather than minimum purchase amount');
    }

  }
}

// check product code
function validate_category_id($request, $language)
{
  global $db;
  $statement = $db->prepare("SELECT * FROM `categorys` WHERE `category_id` = ?");
  $statement->execute(array($request->post['category_id']));

  if ($statement->rowCount() <= 0) {
    throw new Exception($language->get('error_category_id_not_exist'));
  }
}

// create product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'CREATE')
{
  try {

    // check create permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'create_product')) {
      throw new Exception($language->get('error_create_permission'));
    }

    // validate post data
    validate_request_data($request, $language);

    $Hooks->do_action('Before_Create_Promotions');
  
    // insert product into database    
    $data = $discbycategory_model->addPromotions($request->post);

    // get box info
    $result = $discbycategory_model->getPromotions($data['promotions_type'],$data['id']);

    $Hooks->do_action('After_Create_Promotions', $result);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_success'), 'id' => $data['id'], 'result' => $result));
    exit();

  } 
  catch (Exception $e) {
    
    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// update product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'UPDATE')
{
  try {
    
    validate_request_data($request, $language);

    $promotions_id = $request->post['promotions_id'];

    if (DEMO && $promotions_id == 1) {
      throw new Exception($language->get('error_update_permission'));
    }

    $Hooks->do_action('Before_Update_Promotions', $promotions_id);
    print_r($request->post);exit;
    // edit product        
    $discbycategory_model->editPromotions($promotions_id, $request->post);

    $Hooks->do_action('After_Update_Promotions', $promotions_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_update_success'), 'id' => $promotions_id));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// delete 
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'DELETE')
{
  try {
    // fetch brand by id
    $promotions_id   = $request->post['promotions_id'];
    $promotions_type = $request->post['promotions_type'];
    $data = $discbycategory_model->getPromotions($promotions_type, $promotions_id);
    // check product exist or not
    if (!isset($data['id'])) {
      throw new Exception($language->get('text_not_found'));
    }

    $Hooks->do_action('Before_Delete_Promotions', $request);

    $action_type = $request->post['action_type'];
    
    $discbycategory_model->deletePromotions($promotions_type, $promotions_id);
    $message = $language->get('text_delete');

    $Hooks->do_action('After_Delete_Promotions', $data);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $message, 'id' => $promotions_id, 'action_type' => $action_type));
    exit();

  } 
  catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
}

// product create form
if (isset($request->get['action_type']) && $request->get['action_type'] == 'CREATE') 
{
  validate_request_data($request, $language);
  $Hooks->do_action('Before_Showing_Promotions_Form');
  include 'template/promotions_create_form.php';
  $Hooks->do_action('After_Showing_Promotions_Form');

  exit();
}

// edit form
if (isset($request->get['id']) AND isset($request->get['promotions_type']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'EDIT') {

  // fetch product info
  $data = $discbycategory_model->getPromotions($request->get['promotions_type'],$request->get['id']);

  $Hooks->do_action('Before_Showing_Promotions_Edit_Form', $data);
  include 'template/promotions_edit_form.php';
  $Hooks->do_action('After_Showing_Promotions_Edit_Form', $data);

  exit();
}

// product delete form
if (isset($request->get['id']) AND isset($request->get['promotions_type']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'DELETE') {
    
  // fetch product info
  $data = $discbycategory_model->getPromotions($request->get['promotions_type'],$request->get['id']);

  $Hooks->do_action('Before_Showing_Promotions_Delete_Form', $data);
  include 'template/promotions_delete_form.php';
  $Hooks->do_action('After_Showing_Promotions_Delete_Form', $data);

  exit();
}

/**
 *===================
 * START DATATABLE
 *===================
 */

$Hooks->do_action('Before_Showing_Promotions_List');

$where_query = '';
 
// DB table to use
$table = "(
            SELECT 
              happy_hours.id AS id, 
              happy_hours.title AS title, 
              happy_hours.description AS description,
              happy_hours.discount AS discount,
              happy_hours.discount_type AS  discount_type,
              happy_hours.min_purchase_amount AS  min_purchase_amount,
              happy_hours.time_from AS  time_from,
              happy_hours.time_to AS  time_to,
              happy_hours.on_days AS on_days,
              'disc_happy_hours' AS promotions_type  
            FROM disc_happy_hours AS happy_hours
          UNION 
            SELECT 
              festival.id AS id, 
              festival.title AS title, 
              festival.description AS description,
              festival.discount AS discount,
              festival.discount_type AS  discount_type,
              festival.min_purchase_amount AS  min_purchase_amount,
              1 AS  time_from,
              1 AS  time_to,
              1 AS on_days,
              'disc_for_festival' AS promotions_type
            FROM offers AS festival
          UNION 
            SELECT 
              anniversary.id AS id,
              anniversary.title AS title, 
              anniversary.description AS description,
              anniversary.discount AS discount,
              anniversary.discount_type AS  discount_type,
              anniversary.min_purchase_amount AS  min_purchase_amount,
              1 AS  time_from,
              1 AS  time_to,
              1 AS on_days ,
              'disc_for_anniversary' AS promotions_type
            FROM disc_for_anniversary AS anniversary
          ) as promotions";
 
// Table's primary key
$primaryKey = 'id';
$columns = array(
  array(
      'db' => 'id',
      'dt' => 'DT_RowId',
      'formatter' => function( $d, $row ) {
          return 'row_'.$d;
      }
  ),
  array( 
    'db' => 'id',
    'db' => 'promotions_type',
    'dt' => 'select' ,
    'formatter' => function($d, $row) {
      $val = $row['promotions_type'].'|||'.$row['id'];
      return '<input type="checkbox" name="selected[]" value="'.$val.'">';
    }
  ),
  array( 'db' => 'id',  'dt' => 'id' ),
  array( 'db' => 'title',  'dt' => 'title' ),
  array( 'db' => 'description',  'dt' => 'description' ),
  array( 'db' => 'promotions_type',  'dt' => 'edit_promotions_type' ),
  array(
    'db' => 'promotions_type',   
    'dt' => 'promotions_type' ,
    'formatter' => function($d, $row) use($language) {
      if ($row['promotions_type']=='disc_for_festival'){
        return '<span class="disc_for_festival">Discount for festival</span>';
      }
      elseif ($row['promotions_type']=='disc_happy_hours') {
       return '<span class="disc_happy_hours">Discount happy hours</span>';
      }
      elseif ($row['promotions_type']=='disc_for_anniversary') {
       return '<span class="disc_for_anniversary">Discount for anniversary</span>';
      }
    } 
  ),
  array( 
    'db' => 'id', 
    'db' => 'promotions_type', 
    'dt' => 'view_btn' ,
    'formatter' => function($d, $row) use($language) {
      return '<a class="btn btn-sm btn-block btn-warning" title="'.$language->get('button_view').'" href="promotions_details.php?id='.$row['id'].'&promotions_type='.$row['promotions_type'].'"><i class="fa fa-eye"></i></a>';
    }
  ),
  array( 
    'db' => 'id',   
    'dt' => 'edit_btn' ,
    'formatter' => function($d, $row) use($language) {
        return'<button class="btn btn-sm btn-block btn-primary edit-discount-by-promotions" type="button" title="'.$language->get('button_edit').'"><i class="fa fa-pencil"></i></button>';
    }
  ),
  array( 
    'db' => 'id',   
    'dt' => 'delete_btn' ,
    'formatter' => function($d, $row) use($language) {
      return'<button class="btn btn-sm btn-block btn-danger promotions-delete" type="button" title="'.$language->get('button_delete').'"><i class="fa fa-trash"></i></button>';
    }
  )
);
 

// output for datatable
echo json_encode(
  SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns, null, $where_query)
);

$Hooks->do_action('After_Showing_Discount_By_Coupons_List');

/**
 *===================
 * END DATATABLE
 *===================
 */