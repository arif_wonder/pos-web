<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_due_collection_report')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('due');

// fetch invoice 
if ($request->server['REQUEST_METHOD'] == 'GET' && isset($request->get['invoice_id']))
{
    try {

        if (empty($request->get['invoice_id'])) {
            throw new Exception($language->get('error_invoice_id'));
        }

        $invoice_id = $request->get['invoice_id'];

        // fetch invoice info
        $statement = $db->prepare("SELECT selling_info.*, selling_price.*, customers.customer_name FROM `selling_info` 
            LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
            LEFT JOIN `customers` ON (`selling_info`.`customer_id` = `customers`.`customer_id`) 
            WHERE `selling_info`.`invoice_id` = ?");
        $statement->execute(array($invoice_id));
        $invoice = $statement->fetch(PDO::FETCH_ASSOC);
        if (empty($invoice)) {
            throw new Exception($language->get('error_selling_not_found'));
        }
        
        // fetch invoice item
        $statement = $db->prepare("SELECT * FROM `selling_item` WHERE invoice_id = ?");
        $statement->execute(array($invoice_id));
        $selling_items = $statement->fetchAll(PDO::FETCH_ASSOC);
        if (empty($selling_items)) {
            throw new Exception($language->get('error_selling_item'));
        }

        $invoice['items'] = $selling_items;

        header('Content-Type: application/json');
        echo json_encode(array('msg' => $language->get('text_success'), 'invoice' => $invoice));
        exit();

    }
    catch(Exception $e) { 

        header('HTTP/1.1 422 Unprocessable Entity');
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode(array('errorMsg' => $e->getMessage()));
        exit();
    }
}

/**
 *===================
 * START DATATABLE
 *===================
 */

$where_query = 'selling_info.store_id = ' . store_id() . ' AND selling_info.inv_type = "due_paid"';

$from = from();
$to = to();
if($from && $to){
    $where_query .= date_range_filter($from, $to);
}


// DB table to use
$table = "(SELECT selling_info.*, 
            cust.customer_name AS customer_name,
            cust.customer_mobile AS customer_mobile,
            selling_price.previous_due, 
            selling_price.paid_amount, 
            selling_price.present_due 
            FROM selling_info 
            LEFT JOIN customers AS cust ON cust.customer_id = selling_info.customer_id
            LEFT JOIN selling_price ON (selling_info.invoice_id = selling_price.invoice_id) 
            WHERE $where_query
        ) as customers";

// Table's primary key
$primaryKey = 'info_id';

$columns = array(
    array( 'db' => 'invoice_id', 'dt' => 'invoice_id' ),
    array( 'db' => 'customer_id', 'dt' => 'customer_id' ),
    array( 
        'db' => 'info_id',   
        'dt' => 'select' ,
        'formatter' => function($d, $row) {
            return '<input type="checkbox" name="selected[]" value="' . $row['info_id'] . '">';
        }
    ),
    array(
        'db'        => 'customer_id',
        'db'        => 'customer_name',
        'dt'        => 'customer_name',
        'formatter' => function( $d, $row) {
            return '<a href="customer_profile.php?customer_id=' . $row['customer_id'] . '">' . $row['customer_name'] . '</a>';
        }
    ),
    array(
        'db'        => 'customer_id',
        'db'        => 'customer_mobile',
        'dt'        => 'customer_mobile',
        'formatter' => function( $d, $row) {
            return ($row['customer_mobile']);
        }
    ),
    array(
        'db'        => 'previous_due',
        'dt'        => 'due_amount',
        'formatter' => function($d, $row) {
            return currency_format($row['previous_due']);
        }
    ),
    array(
        'db'        => 'paid_amount',
        'dt'        => 'paid_amount',
        'formatter' => function($d, $row) {
            return currency_format($row['paid_amount']);
        }
    ),
    array(
        'db'        => 'present_due',
        'dt'        => 'present_due',
        'formatter' => function($d, $row) {
            return currency_format($row['present_due']);
        }
    ),
    array(
        'db'        => 'info_id',
        'dt'        => 'btn_view',
        'formatter' => function($d, $row) {
            return '<a class="btn btn-sm btn-block btn-info" href="view_invoice.php?invoice_id=' . $row['invoice_id'] . '"><i class="fa fa-eye"></i></a>';
        }
    ),
);

// output for datatable
echo json_encode(
    SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns)
);

/**
 *===================
 * END DATATABLE
 *===================
 */