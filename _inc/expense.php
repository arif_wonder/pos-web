<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_expense')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('expense');

// invoice prefix
$invoice_prefix = isset($invoice_init_prefix['expense']) ? $invoice_init_prefix['expense'] : 'EXP';

// validate post data
function validate_request_data($request, $language) 
{
    // validate date
    if (!isItValidDate($request->post['date'])) {
      throw new Exception($language->get('error_date'));
    }

    // validate time
    if (!isItValidTime12($request->post['time'])) {
      throw new Exception($language->get('error_time'));
    }

    // validate title
    if (!validateString($request->post['title'])) {
      throw new Exception($language->get('error_title'));
    }

    // validate price
    if (!validateFloat($request->post['price'])) {
      throw new Exception($language->get('error_price'));
    }
}

// create expence
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'CREATE')
{
  try {

    // check update permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'create_expense')) {
      throw new Exception($language->get('error_create_permission'));
    }

    // validate post data
    validate_request_data($request, $language);

    $Hooks->do_action('Before_Create_Expense');

    $supplier_id = $request->post['sup_id'];
    $invoice_id = $request->post['invoice_id'] ? $invoice_prefix . $request->post['invoice_id'] : $invoice_prefix . unique_id();

    $store_id = store_id();
    $creator = $user->getId();
    $buy_date = $request->post['date'];
    $buy_time = date("H:i", strtotime($request->post['time']));
    $created_at = date('Y-m-d H:i:s');
    $buying_note = $request->post['expense_note'];
    $item_name = $request->post['title'];
    $item_buying_price = (float)$request->post['price'];
    $item_quantity = 1;
    $total_item = $item_quantity;
    $item_total_price = (float)$request->post['price'];
    $paid_amount = (float)$request->post['price'];
    $payment_mode = $request->post['payment_mode'];

    // Check for dublicate
    $statement = $db->prepare("SELECT * FROM `buying_info` WHERE `invoice_id` = ?");
    $statement->execute(array($invoice_id));
    $result = $statement->fetch(PDO::FETCH_ASSOC);
    if ($result) {
        throw new Exception($language->get('error_invoice_exist'));
    }

    // insert into buying info
    $statement = $db->prepare("INSERT INTO `buying_info` (invoice_id, inv_type, store_id, total_item, buy_date, buy_time, sup_id, creator, invoice_note, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $statement->execute(array($invoice_id, 'expense', $store_id, $total_item, $buy_date, $buy_time, $supplier_id, $creator, $buying_note, $created_at));

    // insert into buying item
    $statement = $db->prepare("INSERT INTO `buying_item` (invoice_id, store_id, item_name, item_buying_price, item_quantity, item_total) VALUES (?, ?, ?, ?, ?, ?)");
    $statement->execute(array($invoice_id, $store_id, $item_name, $item_buying_price, $item_quantity, $item_total_price));

    // insert into buying price
    $statement = $db->prepare("INSERT INTO `buying_price` (invoice_id, store_id, paid_amount, payment_mode) VALUES (?, ?, ?, ?)");
    $statement->execute(array($invoice_id, $store_id, $paid_amount,$payment_mode));

    $Hooks->do_action('After_Create_Expense', $invoice_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_success'), 'id' => $invoice_id));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// update expense
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'UPDATE')
{
  try {

    // check update permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'update_expense')) {
      throw new Exception($language->get('error_update_permission'));
    }

    // validate post data
    validate_request_data($request, $language);

    $invoice_id = $request->post['invoice_id'];
    if (empty($invoice_id)) {
        throw new Exception($language->get('error_invoice_id'));
    }

    $store_id = store_id();
    $buy_date = $request->post['date'];
    $buy_time = date("H:i", strtotime($request->post['time']));
    $buying_note = $request->post['expense_note'];
    $item_name = $request->post['title'];
    $item_buying_price = (float)$request->post['price'];
    $item_total_price = (float)$request->post['price'];
    $paid_amount = (float)$request->post['price'];
    $payment_mode = $request->post['payment_mode'];

    // check for dublicate
    $statement = $db->prepare("SELECT * FROM `buying_info` WHERE `invoice_id` = ?");
    $statement->execute(array($invoice_id));
    $invoice = $statement->fetch(PDO::FETCH_ASSOC);

    if (!$invoice) {
        throw new Exception($language->get('error_invoice_not_found'));
    }


    // check invoice edit permission
    $buying_date_time = strtotime($invoice['buy_date'] . ' ' . $invoice['buy_time']);
    // if (time()-(60*60*24) > $buying_date_time) {
    //     throw new Exception($language->get('error_edit_duration_expired'));
    // }
    $Hooks->do_action('Before_Update_Expense', $request);

    // update invoice info
    $statement = $db->prepare("UPDATE  `buying_info` SET `buy_date` = ?, `buy_time` = ?, `invoice_note` = ? WHERE `invoice_id` = ?");
    $statement->execute(array($buy_date, $buy_time, $buying_note, $invoice_id));

    // decrement invoice item, if exist
    $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `invoice_id` = ?");
    $statement->execute(array($invoice_id));
    $invoice_items = $statement->fetchAll(PDO::FETCH_ASSOC);

    if ($statement->rowCount()) {
        foreach ($invoice_items as $item) {
            $statement = $db->prepare("DELETE FROM `buying_item` WHERE `item_id` = ? && `invoice_id` = ?");
            $statement->execute(array($item['item_id'], $invoice_id));
        }
    }

    // insert invoice item into database
    $statement = $db->prepare("INSERT INTO `buying_item` (invoice_id, store_id, item_name, item_buying_price, item_total) VALUES (?, ?, ?, ?, ?)");
    $statement->execute(array($invoice_id, $store_id, $item_name, $item_buying_price, $item_total_price));

    // update paid amount
    $statement = $db->prepare("UPDATE `buying_price` SET `paid_amount` = ?, `payment_mode` = ? WHERE `invoice_id` = ?");
    $statement->execute(array($paid_amount, $payment_mode,$invoice_id));

    $Hooks->do_action('After_Update_Expense', $invoice_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_update_success'), 'id' => $invoice_id));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// delete expense
if($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'DELETE') 
{
  try {

    // check delete permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'delete_expense')) {
      throw new Exception($language->get('error_delete_permission'));
    }

    // validate invoice id
    if (empty($request->post['invoice_id'])) {
      throw new Exception($language->get('error_invoice_id'));
    }

    $Hooks->do_action('Before_Delete_Expense', $request);

    $invoice_id = $request->post['invoice_id'];

    // delete invocie item
    $statement = $db->prepare("DELETE FROM  `buying_item` WHERE `store_id` = ? AND `invoice_id` = ?");
    $statement->execute(array(store_id(), $invoice_id));

    // delete buying price info
    $statement = $db->prepare("DELETE FROM  `buying_price` WHERE `store_id` = ? AND `invoice_id` = ?");
    $statement->execute(array(store_id(), $invoice_id));

    // delete invoice info
    $statement = $db->prepare("DELETE FROM  `buying_info` WHERE `store_id` = ? AND `invoice_id` = ? LIMIT 1");
    $statement->execute(array(store_id(), $invoice_id));

    

    

    $Hooks->do_action('After_Delete_Expense', $invoice_id);
    
    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_delete_success')));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
}

// view expense
if (isset($request->get['invoice_id']) && isset($request->get['action_type']) && $request->get['action_type'] == 'VIEW') 
{
    $invoice_id = $request->get['invoice_id'];
    
    // fetch invoice info
    $statement = $db->prepare("SELECT `buying_info`.*, `buying_price`.`paid_amount`,`buying_price`.`payment_mode` FROM `buying_info` LEFT JOIN buying_price ON (`buying_info`.`invoice_id` = `buying_price`.`invoice_id`) WHERE `buying_info`.`invoice_id` = ?");
    $statement->execute(array($invoice_id));
    $invoice = $statement->fetch(PDO::FETCH_ASSOC);


    $statement = $db->prepare("SELECT `buying_item` .*, `buying_price`.`payment_mode` FROM `buying_item` LEFT JOIN buying_price ON (`buying_item`.`invoice_id` = `buying_price`.`invoice_id`) WHERE `buying_item`.`invoice_id` = ?");

    $statement->execute(array($invoice_id));
    $invoice_items = $statement->fetchAll(PDO::FETCH_ASSOC);

    include 'template/expense_invoice.php';
    exit();
}

// expense invoice edit form
if (isset($request->get['invoice_id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'EDIT') {
    try {

        $invoice_id = $request->get['invoice_id'];
        if (empty($invoice_id)) {
            throw new Exception($language->get('error_invoice_id'));
        }

        // fetch invoice info
        $statement = $db->prepare("SELECT `buying_info`.*, `buying_price`.`paid_amount` FROM `buying_info` LEFT JOIN `buying_price` ON (`buying_info`.`invoice_id` = `buying_price`.`invoice_id`) WHERE `buying_info`.`invoice_id` = ?");
        $statement->execute(array($invoice_id));
        $invoice = $statement->fetch(PDO::FETCH_ASSOC);

        // fetch invoice item
        // $statement = $db->prepare("SELECT * FROM `buying_item` LEFT JOIN `products` ON (`buying_item`.`item_id` = `products`.`p_id`) WHERE `invoice_id` = ?");
        $statement = $db->prepare("SELECT `buying_item` .*, `buying_price`.`payment_mode` FROM `buying_item` LEFT JOIN buying_price ON (`buying_item`.`invoice_id` = `buying_price`.`invoice_id`) WHERE `buying_item`.`invoice_id` = ?");
        $statement->execute(array($invoice_id));
        $invoice_items = $statement->fetchAll(PDO::FETCH_ASSOC);

        include 'template/expense_edit_form.php';
        exit();

    } catch (Exception $e) { 

        header('HTTP/1.1 422 Unprocessable Entity');
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode(array('errorMsg' => $e->getMessage()));
        exit();
      }
}