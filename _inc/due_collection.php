<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'due_collection')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('due');

// LOAD INVOICE MODEL
$invoice_model = $registry->get('loader')->model('invoice');

// fetch invoice 
if ($request->server['REQUEST_METHOD'] == 'GET' && isset($request->get['invoice_id']))
{
    try {

        $invoice_id = $request->get['invoice_id'];

        // validate invoice id
        $invoice = $invoice_model->getInvoiceInfo($invoice_id);
        if (!$invoice) {
            throw new Exception($language->get('error_invoice_id'));
        }

        $Hooks->do_action('Before_Showing_Due_Invoice', $invoice_id);

        // fetch invoice info
        $statement = $db->prepare("SELECT `selling_info`.*, `selling_price`.*, `customers`.`customer_name` FROM `selling_info` 
            LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
            LEFT JOIN `customers` ON (`selling_info`.`customer_id` = `customers`.`customer_id`) 
            WHERE `selling_info`.`invoice_id` = ?");
        $statement->execute(array($invoice_id));
        $invoice = $statement->fetch(PDO::FETCH_ASSOC);
        if (empty($invoice)) {
            throw new Exception($language->get('error_selling_not_found'));
        }
        
        // fetch invoice item
        $statement = $db->prepare("SELECT * FROM `selling_item` WHERE invoice_id = ?");
        $statement->execute(array($invoice_id));
        $selling_items = $statement->fetchAll(PDO::FETCH_ASSOC);
        if (empty($selling_items)) {
            throw new Exception($language->get('error_selling_item'));
        }

        $invoice['items'] = $selling_items;

        $Hooks->do_action('After_Showing_Due_Invoice', $invoice_id);

        header('Content-Type: application/json');
        echo json_encode(array('msg' => $language->get('text_success'), 'invoice' => $invoice));
        exit();

    }
    catch(Exception $e) { 

        header('HTTP/1.1 422 Unprocessable Entity');
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode(array('errorMsg' => $e->getMessage()));
        exit();
    }
}

/**
 *===================
 * START DATATABLE
 *===================
 */

$Hooks->do_action('Before_Showing_Due_Invoice_List');

$where_query = 'c2s.store_id = ' . store_id() . ' AND c2s.due_amount > 0';

// DB table to use
$table = "(SELECT customers.*, c2s.due_amount FROM customers 
  LEFT JOIN customer_to_store AS c2s ON (customers.customer_id = c2s.customer_id) 
  WHERE $where_query) as customers";

// Table's primary key
$primaryKey = 'customer_id';

$columns = array(
    array( 'db' => 'customer_id', 'dt' => 'customer_id' ),
    array( 
    'db' => 'customer_id',   
    'dt' => 'select' ,
    'formatter' => function($d, $row) {
        return '<input type="checkbox" name="selected[]" value="' . $row['customer_id'] . '">';
    }
  ),
    array(
        'db'        => 'customer_id',
        'dt'        => 'customer_name',
        'formatter' => function( $d, $row) {

            return '<a href="customer_profile.php?customer_id=' . $row['customer_id'] . '">' . get_the_customer($row['customer_id'], 'customer_name') . '</a>';
        }
    ),
    array(
        'db'        => 'customer_mobile',
        'dt'        => 'customer_mobile',
        'formatter' => function( $d, $row) {

            return $row['customer_mobile'];
        }
    ),
    array(
        'db'        => 'due_amount',
        'dt'        => 'due_amount',
        'formatter' => function($d, $row) {
            return currency_format($row['due_amount']);
        }
    ),
    array(
        'db'        => 'customer_id',
        'dt'        => 'btn_collection',
        'formatter' => function($d, $row) {
            return '<button id="due-paid" class="btn btn-sm btn-block btn-warning" data-id="' . $row['customer_id'] . '" data-name="' . get_the_customer($row['customer_id'], 'customer_name') . '"><i class="fa fa-money"></i></button>';
        }
    ),
    array(
        'db'        => 'customer_id',
        'dt'        => 'btn_view',
        'formatter' => function($d, $row) {
            return '<a class="btn btn-sm btn-block btn-info" href="customer_profile.php?customer_id=' . $row['customer_id'] . '"><i class="fa fa-eye"></i></a>';
        }
    ),
);

// output for datatable
echo json_encode(
    SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns)
);

$Hooks->do_action('After_Showing_Due_Invoice_List');

/**
 *===================
 * END DATATABLE
 *===================
 */