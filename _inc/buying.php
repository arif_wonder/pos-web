<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return error
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('buy');

$store_id = store_id();
$user_id = $user->getId();

// validate post data
function validate_request_data($request, $language) 
{    
    // validate invoice id
    if (!validateString($request->post['invoice_id'])) {
      throw new Exception($language->get('error_invoice_id'));
    }

    // validate supplier id
    if (!validateInteger($request->post['sup_id'])) {
      throw new Exception($language->get('error_supplier_id'));
    }

    // validate date
    if (!isItValidDate($request->post['date'])) {
      throw new Exception($language->get('error_date'));
    }

    // validate time
    if (!isItValidTime12($request->post['time'])) {
      throw new Exception($language->get('error_time'));
    }

    // validate product
    if (!isset($request->post['product']) || empty($request->post['product'])) {
        throw new Exception($language->get('error_product_item'));
    }
}

// create invoice
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'CREATE')
{
  try {

    $error = '';
    // check create permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'create_buying_invoice')) {
        $error = 'error';
        throw new Exception($language->get('error_create_permission'));
    }

    // validate post data
    validate_request_data($request, $language);

    $Hooks->do_action('Before_Create_Buying_Invoice', $request);

    $supplier_id = $request->post['sup_id'];
    $invoice_id = $request->post['invoice_id'];

    foreach ($request->post['product'] as $id => $product) {

        $statement = $db->prepare("SELECT * FROM `products`
            LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`) 
            WHERE `p2s`.`store_id` = ? AND `p_id` = ?");
        $statement->execute(array($store_id, $id));
        $result = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$result) {
            $error = 'error';
            throw new Exception($language->get('error_product_not_found'));
        }
        else {
            $error = '';
        }        
        // validate quantity    
        if(!validateInteger($product['quantity']) || $product['quantity'] <= 0) {
            $error = 'error';
            throw new Exception($language->get('error_quantity'));
        }
        else {
            $error = '';
        }
        // validate buying cost
        if(!validateFloat($product['cost']) || $product['cost'] <= 0) {
            $error = 'error';
            throw new Exception($language->get('error_buying_price'));
        }
        else {
            $error = '';
        }
        // validate selling price
        if(!validateFloat($product['sell']) || $product['sell'] <= 0) {
            $error = 'error';
            throw new Exception($language->get('error_selling_price'));
        }
        else {
            $error = '';
        }
        // 
        if($product['cost'] >= $product['sell']) {
            $error = 'error';
            throw new Exception($language->get('error_low_selling_price'));
        }
        else {
            $error = '';
        }
    } ///// end of foreach loop

    // validate and update attachment
    if(isset($_FILES["attachment"]["type"]) && $_FILES["attachment"]["type"])
    {
        if (!$_FILES["attachment"]["type"] == "image/jpg" || !$_FILES["attachment"]["type"] == "application/pdf" || $_FILES["attachment"]["size"] > 1048576) {  // 1MB  
            throw new Exception($language->get('error_size_or_type'));
        }

        if ($_FILES["attachment"]["error"] > 0) {
            $error = 'error';
            throw new Exception("Return Code: " . $_FILES["attachment"]["error"]);
        }
        else {
            $error = '';
        }
    }

    $total_item = count($request->post['product']);
    $buy_date = $request->post['date'];
    $buy_time = date("H:i:s", strtotime($request->post['time']));
    $created_at = date('Y-m-d H:i:s', strtotime($buy_date . ' ' . $buy_time));
    $buying_note = $request->post['buying_note'];
    $paid_amount = 0;

    // check for dublicate, if present then update otherwise insert
    $statement = $db->prepare("SELECT * FROM `buying_info` WHERE `invoice_id` = ?");
    $statement->execute(array($invoice_id));
    $result = $statement->fetch(PDO::FETCH_ASSOC);
    if ($result) {
        $error = 'error';
        throw new Exception($language->get('error_invoice_exist'));
    }
    else {
        $error = '';
    }

    if ($error=='') {
        // insert data
        $statement = $db->prepare("INSERT INTO `buying_info` (
                                        invoice_id, 
                                        store_id, 
                                        total_item, 
                                        buy_date, 
                                        buy_time, 
                                        created_at, 
                                        sup_id, 
                                        creator, 
                                        invoice_note) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $statement->execute(array(
                                $invoice_id, 
                                $store_id, 
                                $total_item, 
                                $buy_date, 
                                $buy_time, 
                                $created_at, 
                                $supplier_id, 
                                $user_id, 
                                $buying_note
                            ));
        
        foreach ($request->post['product'] as $id => $product) {  
            $status = 'active';

            // fetch product recent stock amount
            $the_product = get_the_product($id, null, $store_id);
            if (isset($the_product['quantity_in_stock']) && $the_product['quantity_in_stock'] > 0) {
                $status = 'stock';
            }
            $item_name = $product['name'];
            $category_id = $product['category_id'];
            $item_buying_price = $product['cost'];
            $item_selling_price = $product['sell'];
            $item_quantity = $product['quantity'];
            $defected_quantity = $product['defected_quantity'] ? $product['defected_quantity'] :0;
            $item_total = (int)$item_quantity * (float)$item_buying_price;
            $paid_amount += $item_total;
            
            // insert data
            $statement = $db->prepare("INSERT INTO `buying_item` (
                invoice_id, 
                store_id, 
                item_id, 
                category_id, 
                item_name, 
                item_buying_price, 
                item_selling_price, 
                item_quantity,
                defected_quantity,
                item_total, 
                status
            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $statement->execute(array(
                $invoice_id, 
                $store_id, 
                $id, 
                $category_id, 
                $item_name, 
                $item_buying_price, 
                $item_selling_price, 
                $item_quantity, 
                $defected_quantity, 
                $item_total, 
                $status
            ));
            
            // update data
            $statement = $db->prepare("UPDATE `product_to_store` SET `buy_price` = ?, `sell_price` = ?, `quantity_in_stock` = `quantity_in_stock` + ($item_quantity-$defected_quantity) WHERE `product_id` = ? AND `store_id` = ?");
            $statement->execute(array($item_buying_price, $item_selling_price, $id, $store_id));
        }

        // insert data
        $statement = $db->prepare("INSERT INTO `buying_price` (invoice_id, store_id, paid_amount) VALUES (?, ?, ?)");
        $statement->execute(array($invoice_id, $store_id, $paid_amount));

        // upload attachment
        if(isset($_FILES["attachment"]["type"]) && $_FILES["attachment"]["type"])
        {
            $temporary = explode(".", $_FILES["attachment"]["name"]);
            $file_extension = end($temporary);
            $temp = explode(".", $_FILES["attachment"]["name"]);
            $newfilename = $invoice_id . '.' . end($temp);
            $sourcePath = $_FILES["attachment"]["tmp_name"]; // Storing source path of the file in a variable
            $targetFile = DIR_STORAGE . 'buying-invoices/' . $newfilename; // Target path where file is to be stored
            if (file_exists($targetFile) && is_file($targetFile)) {
                if (!isset($request->post['force_upload'])) {
                    throw new Exception($language->get('error_image_exist'));
                } 
                unlink($targetFile);  
            } 

            // update invoice url
            $statement = $db->prepare("UPDATE  `buying_info` SET `invoice_url` = ? WHERE `invoice_id` = ?");
            $statement->execute(array($newfilename, $invoice_id));

            // move uploaded image
            move_uploaded_file($sourcePath, $targetFile);
        }
    }
    else {
        throw new Exception('Something wents wrong!');
    }

    $Hooks->do_action('After_Create_Buying_Invoice', $invoice_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_success'), 'id' => $invoice_id));
    exit();

  } 
  catch (Exception $e) { 
    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
}

// update invoice
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'UPDATE')
{
  try {

    // check update permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'update_buying_invoice')) {
      throw new Exception($language->get('error_update_permission'));
    }

    // validate post data
    validate_request_data($request, $language);

    $Hooks->do_action('Before_Update_Buying_Invoice', $request);

    $supplier_id = $request->post['sup_id'];
    $invoice_id = $request->post['invoice_id'];

    // check, if atleast one invoice item is active or not
    $statement = $db->prepare("SELECT SUM(`total_sell`) as total_sell, status FROM `buying_item` WHERE `invoice_id` = ? GROUP BY `status` DESC");
    $statement->execute(array($invoice_id));
    $buying_item = $statement->fetch(PDO::FETCH_ASSOC);

    if ($buying_item['total_sell'] > 0 
        && (($buying_item['status'] != 'active') && ($buying_item['status'] != 'stock'))) {
       throw new Exception($language->get('error_sold_out'));
    }

    foreach ($request->post['product'] as $id => $product) { 

        $statement = $db->prepare("SELECT * FROM `products`
            LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`) 
            WHERE  `p2s`.`store_id` = ? AND `p_id` = ?");
        $statement->execute(array($store_id, $id));
        $the_product = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$the_product) {
          throw new Exception($language->get('error_item_not_found'));
        }

        // fetch buying item
        $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `invoice_id` = ? AND `item_id` = ?");
        $statement->execute(array($store_id, $invoice_id, $the_product['p_id']));
        $buying_item = $statement->fetch(PDO::FETCH_ASSOC);

        $available_qauantity = $buying_item['item_quantity'] - $buying_item['total_sell'];
        $return_quantity = $buying_item['item_quantity'] - $product['quantity'];

        if (($return_quantity > $available_qauantity)) {
            throw new Exception($language->get('error_invoice_quantity'));
        }

        // validte quantity
        if(!validateInteger($product['quantity']) || ($product['quantity'] <= 0)) {
          throw new Exception($language->get('error_quantity'));
        }

        // validate cost
        if(!validateFloat($product['cost']) || ($product['cost'] <= 0)) {
          throw new Exception($language->get('error_buying_price'));
        }

        // validate selling price
        if(!validateFloat($product['sell']) || ($product['sell'] <= 0)) {
          throw new Exception($language->get('error_selling_price'));
        }

        if($product['cost'] >= $product['sell']) {
          throw new Exception($language->get('error_low_selling_price'));
        }
    }

    // validate and update invoice
    if(isset($_FILES["attachment"]["type"]) && $_FILES["attachment"]["type"])
    {
        if (!$_FILES["attachment"]["type"] == "image/jpg" || !$_FILES["attachment"]["type"] == "application/pdf" || $_FILES["attachment"]["size"] > 1048576) {  // 1MB  
            throw new Exception($language->get('error_size_or_type'));
        }

        if ($_FILES["attachment"]["error"] > 0) {
            throw new Exception("Return Code: " . $_FILES["attachment"]["error"]);
        }
    }

    $total_item = count($request->post['product']);
    $buy_date = $request->post['date'];
    $buy_time = date("H:i:s", strtotime($request->post['time']));
    $buying_note = $request->post['buying_note'];
    $defected_quantity = $request->post['defected_quantity'];
    $paid_amount = 0;

    // check for dublicate, if present or not
    $statement = $db->prepare("SELECT * FROM `buying_info` WHERE `store_id` = ? AND `invoice_id` = ?");
    $statement->execute(array($store_id, $invoice_id));
    $result = $statement->fetch(PDO::FETCH_ASSOC);
    if (!$result) {
        throw new Exception($language->get('error_invoice_not_found'));
    }

    // check invoice edit permission
    $buying_date_time = strtotime($result['buy_date'] . ' ' . $result['buy_time']);
    if (invoice_edit_lifespan() > $buying_date_time) {
        throw new Exception($language->get('error_edit_duration_expired'));
    }

    $statement = $db->prepare("SELECT * FROM `buying_info` WHERE `store_id` = ? AND `sup_id` = ? ORDER BY `info_id` DESC");
    $statement->execute(array($store_id, $supplier_id));
    $invoice = $statement->fetch(PDO::FETCH_ASSOC);
    if ($invoice_id != $invoice['invoice_id']) {
        throw new Exception($language->get('error_edit_invoice'));
    }

    // update buying info
    $statement = $db->prepare("UPDATE `buying_info` SET 
        `total_item` = ?, 
        `invoice_note` = ?,
        `defected_quantity` = ?
        WHERE `store_id` = ? AND `invoice_id` = ?
    ");
    $statement->execute(array($total_item, $buying_note, $defected_quantity, $store_id, $invoice_id));

    // decrement product quantity
    $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `invoice_id` = ?");
    $statement->execute(array($store_id, $invoice_id));
    $invoice_items = $statement->fetchAll(PDO::FETCH_ASSOC);

    $sold_out_products = [];
    $deleted_items = [];

    // delete invoice item
    if ($statement->rowCount()) {

        foreach ($invoice_items as $product) {

            // check, if invoice item is sold out then exit from editing
            if ($product['status'] != 'sold') {

                // decrement product quantity
                $quantity = $product['item_quantity'] - $product['total_sell'];
                $statement = $db->prepare("UPDATE `product_to_store` SET `quantity_in_stock` = `quantity_in_stock` - $quantity WHERE `product_id` = ? AND `store_id` = ?");
                $statement->execute(array($product['item_id'], $store_id));
                $deleted_items[$product['item_id']] = $product;

                // delete invoice item
                $statement = $db->prepare("DELETE FROM `buying_item` WHERE `store_id` = ? AND `item_id` = ? AND `invoice_id` = ?");
                $statement->execute(array($store_id, $product['item_id'], $invoice_id));

            } else {

                $sold_out_products[] = $product['item_id'];
                
            } 
        }
    }

    foreach ($request->post['product'] as $id => $product) { 

        if (!in_array($id, $sold_out_products)) {

            $status = 'active';
            $total_sell = isset($deleted_items[$id]) ? $deleted_items[$id]['total_sell'] : 0;
            $item_quantity = $product['quantity'];
            $quantity_in_stock = $item_quantity - $total_sell;

            // fetch product recent stock amount
            $the_product = get_the_product($id, null, $store_id);
            if (isset($the_product['quantity_in_stock']) && $the_product['quantity_in_stock'] > 0) {
                $status = 'stock';
            }

            if ($total_sell >= $item_quantity ) {
                $status = 'sold';
            }

            $item_name = $product['name'];
            $category_id = $product['category_id'];
            $item_buying_price = $product['cost'];
            $item_selling_price = $product['sell'];
            $item_total = (int)$item_quantity * (float)$item_buying_price;
            $paid_amount += $item_total;
            
            // insert into buying item
            $statement = $db->prepare("INSERT INTO `buying_item` (invoice_id, store_id, item_id, category_id, item_name, item_buying_price, item_selling_price, item_quantity, total_sell, item_total, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $statement->execute(array($invoice_id, $store_id, $id, $category_id, $item_name, $item_buying_price, $item_selling_price, $item_quantity, $total_sell, $item_total, $status));
            
            // update buying and selling price quantity
            $statement = $db->prepare("UPDATE `product_to_store` SET `buy_price` = ?, `sell_price` = ?, `quantity_in_stock` = `quantity_in_stock` + $quantity_in_stock WHERE `product_id` = ? AND `store_id` = ?");
            $statement->execute(array($item_buying_price, $item_selling_price, $id, $store_id));
        }
    }

    // update paid amount
    $statement = $db->prepare("UPDATE `buying_price` SET `store_id` = ?, `paid_amount` = ? WHERE `invoice_id` = ?");
    $statement->execute(array($store_id, $paid_amount, $invoice_id));

    // upload invoice image
    if(isset($_FILES["attachment"]["type"]) && $_FILES["attachment"]["type"])
    {
        $temporary = explode(".", $_FILES["attachment"]["name"]);
        $file_extension = end($temporary);
        $temp = explode(".", $_FILES["attachment"]["name"]);
        $newfilename = $invoice_id . '.' . end($temp);
        $sourcePath = $_FILES["attachment"]["tmp_name"]; // Storing source path of the file in a variable
        $targetFile = DIR_STORAGE . 'buying-invoices/' . $newfilename; // Target path where file is to be stored
        if (file_exists($targetFile) && is_file($targetFile)) {
            if (!isset($request->post['force_upload'])) {
                throw new Exception($language->get('error_image_exist'));
            } 

            // delete existing image
            unlink($targetFile);  
        } 

        // update invoice url
        $statement = $db->prepare("UPDATE  `buying_info` SET `invoice_url` = ? WHERE `store_id` = ? AND `invoice_id` = ?");
        $statement->execute(array($newfilename, $store_id, $invoice_id));

        // move uploaded image
        move_uploaded_file($sourcePath, $targetFile);
    }

    $Hooks->do_action('After_Update_Buying_Invoice', $invoice_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_update_success'), 'id' => $invoice_id));
    exit();

  } catch (Exception $e) {

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();

  }
} 

// delete invoice
if($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'DELETE') 
{
  try {

    // check delete permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'delete_buying_invoice')) {
      throw new Exception($language->get('error_delete_permission'));
    }

    // validate invoice id
    if (empty($request->post['invoice_id'])) {
      throw new Exception($language->get('error_invoice_id'));
    }

    $Hooks->do_action('Before_Delete_Buying_Invoice', $request);

    $invoice_id = $request->post['invoice_id'];

    // check, if invoice exist or not
    $statement = $db->prepare("SELECT * FROM `buying_info` WHERE `store_id` = ? AND `invoice_id` = ?");
    $statement->execute(array($store_id, $invoice_id));
    $invoice_info = $statement->fetch(PDO::FETCH_ASSOC);
    if (!$invoice_info) {
        throw new Exception($language->get('error_invoice_id'));
    }
    $buying_date_time = strtotime($invoice_info['buy_date'] . ' ' . $invoice_info['buy_time']);

    $statement = $db->prepare("SELECT `item_id`, SUM(`item_quantity`) as item_quantity, SUM(`total_sell`) as total_sell, `status` FROM `buying_item` WHERE `store_id` = ? AND `invoice_id` = ? GROUP BY `status` DESC");
    $statement->execute(array($store_id, $invoice_id));
    $buying_item = $statement->fetch(PDO::FETCH_ASSOC);

    if ($buying_item['total_sell'] > 0
      && (($buying_item['status'] == 'active') || ($buying_item['status'] == 'sold'))) {

       throw new Exception($language->get('error_active_or_sold'));
    }

    // check invoice delete duration 
    if (invoice_delete_lifespan() > $buying_date_time) {
       throw new Exception($language->get('error_delete_duration_expired'));
    }

    // increase quantitity of stock
    $return_quantity = $buying_item['item_quantity'] - $buying_item['total_sell'];
    $statement = $db->prepare("UPDATE `product_to_store` SET `quantity_in_stock` = `quantity_in_stock` - $return_quantity WHERE `store_id` = ? AND `product_id` = ?");
    $statement->execute(array($store_id, $buying_item['item_id']));

    // delete invoice info
    $statement = $db->prepare("DELETE FROM  `buying_info` WHERE `store_id` = ? AND `invoice_id` = ? LIMIT 1");
    $statement->execute(array($store_id, $invoice_id));

    // delete invocie item
    $statement = $db->prepare("DELETE FROM  `buying_item` WHERE `store_id` = ? AND `invoice_id` = ?");
    $statement->execute(array($store_id, $invoice_id));

    // delete buying price info
    $statement = $db->prepare("DELETE FROM  `buying_price` WHERE `store_id` = ? AND `invoice_id` = ?");
    $statement->execute(array($store_id, $invoice_id));

    $Hooks->do_action('After_Delete_Buying_Invoice', $invoice_id);
    
    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_delete_success')));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
}

// invoice edit form
if ($request->get['action_type'] == 'EDIT') 
{
    try {

        $invoice_id = $request->get['invoice_id'];
        if (empty($invoice_id)) {
            throw new Exception($language->get('error_invoice_id'));
        }
        
        $supplier_id = (int)$request->get['sup_id'];

        $Hooks->do_action('Before_Buying_Invoice_Edit_Form', $supplier_id);

        if (total_product_of_supplier($supplier_id) <= 0) {
            throw new Exception($language->get('error_product_not_found'));
        }

        // fetch suppliers
        $statement = $db->prepare("SELECT * FROM `suppliers` WHERE `sup_id` = ?");
        $statement->execute(array($supplier_id));
        $supplier = $statement->fetch(PDO::FETCH_ASSOC);
        if (!$statement->rowCount() > 0) {
            throw new Exception($language->get('error_supplier_not_found'));
        }

        // fetch invoice info
        $statement = $db->prepare("SELECT `buying_info`.*, `buying_price`.`paid_amount` FROM `buying_info` 
            LEFT JOIN `buying_price` ON (`buying_info`.`invoice_id` = `buying_price`.`invoice_id`) 
            WHERE `buying_info`.`invoice_id` = ?");
        $statement->execute(array($invoice_id));
        $invoice = $statement->fetch(PDO::FETCH_ASSOC);

        // fetch buying info
        $statement = $db->prepare("SELECT * FROM `buying_item` 
            LEFT JOIN `products` ON (`buying_item`.`item_id` = `products`.`p_id`) 
            LEFT JOIN `product_to_store` p2s ON (`buying_item`.`item_id` = `p2s`.`product_id`) 
            WHERE `p2s`.`store_id` = ? AND `buying_item`.`status` != ? AND `invoice_id` = ?");
        $statement->execute(array($store_id, 'sold', $invoice_id));
        $invoice_items = $statement->fetchAll(PDO::FETCH_ASSOC);
//print_r($invoice_items);exit;
        include 'template/buying_form.php';

        $Hooks->do_action('After_Buying_Invoice_Edit_Form', $supplier_id);

        exit();

    } catch (Exception $e) { 

        header('HTTP/1.1 422 Unprocessable Entity');
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode(array('errorMsg' => $e->getMessage()));
        exit();
      }
}

// invoice create form
if (isset($request->get['sup_id']) && isset($request->get['action_type']) && $request->get['action_type'] == 'CREATE') 
{
    $supplier_id = (int)$request->get['sup_id'];

    $Hooks->do_action('Before_Buying_Invoice_Create_Form', $supplier_id);

    // fetch supplier by id
    $statement = $db->prepare("SELECT * FROM `suppliers` WHERE `sup_id` = ?");
    $statement->execute(array($supplier_id));
    $supplier = $statement->fetch(PDO::FETCH_ASSOC);

    include 'template/buying_form.php';

    $Hooks->do_action('After_Buying_Invoice_Create_Form', $supplier_id);
}

// view invoice
if (isset($request->get['invoice_id']) && isset($request->get['action_type']) && $request->get['action_type'] == 'VIEW') 
{
    $invoice_id = $request->get['invoice_id'];

    $Hooks->do_action('Before_View_Buying_Invoice', $invoice_id);
    
    // fetch invoice info
    $statement = $db->prepare("SELECT buying_info.*, `buying_price`.`paid_amount` FROM `buying_info` LEFT JOIN `buying_price` ON `buying_info`.`invoice_id` = `buying_price`.`invoice_id` WHERE `buying_info`.`invoice_id` = ?");
    $statement->execute(array($invoice_id));
    $invoice = $statement->fetch(PDO::FETCH_ASSOC);

    // fetch invoice items
    $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `invoice_id` = ?");
    $statement->execute(array($invoice_id));
    $invoice_items = $statement->fetchAll(PDO::FETCH_ASSOC);

    include 'template/buying_invoice.php';

    $Hooks->do_action('After_View_Buying_Invoice', $invoice_id);
}