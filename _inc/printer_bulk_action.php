<?php 
ob_start();
session_start();
include ("../_init.php");
// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}
// LOAD LANGUAGE FILE
$language->load('printer');
// LOAD PRODUCT MODEL
$product_brand_model = $registry->get('loader')->model('printer');

if($request->server['REQUEST_METHOD'] == 'POST' && isset($request->get['action'])) {
  try {
    // check permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'delete_printer')) {
      throw new Exception($language->get('error_delete_permission'));
    }
    $action = $request->get['action'];

    // check, if there has selected item or not
    if (!isset($request->post['selected']) || empty($request->post['selected'])) {
      throw new Exception($language->get('error_no_selected'));
    } //// end of the if condition

    $Hooks->do_action('After_Brand_Bulk_Action', $action);

    $ids = $request->post['selected'];
    if (!is_array($ids)) {
      $ids = array($ids);
    }//// end of the if condition
    
    $id_length = count($ids);

    switch ($action) {
      case 'delete':
        // check delete product permission
        if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'delete_printer')) {
          throw new Exception($language->get('error_delete_permission'));
        }

        for ($i=0; $i < $id_length; $i++) { 
          $id = $ids[$i];

          if (DEMO && $id == 1) {
            continue;
          }
          $product_brand_model->deleteprinter($id);
        }
        $success_message = $language->get('success_delete_all');
      break; //// end of delete case
      
      case 'restore':
        // check product restore permission
        if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'restore_all_product')) {
          throw new Exception(sprintf($language->get('error_restore_permission'), $language->get('text_product')));
        } ///// end of the if condition

        for ($i=0; $i < $id_length; $i++) { 
          $id = $ids[$i];
          if (DEMO && $id == 1) {
            continue;
          }
          // update product status
          // $product_brand_model->updateStatus($id, 1, store_id());
        }
        $success_message = $language->get('success_restore_all');
      break; //// end of restore case

      default:
        # code...
      break;
    }

    $Hooks->do_action('After_Brand_Bulk_Action', $action);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $success_message));
    exit();

  } 
  catch (Exception $e) {
    $error_message = $e->getMessage();
    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $error_message));
    exit();
  }
}
