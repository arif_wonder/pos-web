<?php
include ("../_init.php");

// LOAD LANGUAGE FILE
$language->load('product');

// LOAD PRODUCT MODEL
$product_model = $registry->get('loader')->model('product');
$p_id = $request->get['code'];
$product = $product_model->getProduct($p_id);
if (!$product) {
	exit('Product not found');
}

$store_name = store('name');
$req_symbology = $request->get['symbology'];
$limit = (int)($request->get['limit']);
$code = $product['p_code'];
$product_name = $product['p_name'];
$product_price = number_format($product['sell_price'], 2);
$generator = barcode_generator();
$symbology = barcode_symbology($generator, $req_symbology);
$array_limit = [];
for($i=1;$i<=500;$i++){
	array_push($array_limit, $i);
}
$limit_array = $array_limit; 

$Hooks->do_action('Before_Showing_Barcode_List');
?>

<div class="row no-print">
	<div class="col-md-4">
		<!-- <input type="text" name="barcode-limit" id="barcode-limit" placeholder="Enter number" class="generate-barcode"> -->
		<select id="barcode-limit" class="form-control generate-barcode">
			<?php foreach ($limit_array as $limit_number) : ?>
				<option value="<?php echo $limit_number; ?>"<?php echo $limit_number == $limit ? ' selected' : null; ?>>
					<?php echo $limit_number; ?>
				</option>
			<?php endforeach; ?>
		</select> 
	</div>
	<div class="col-md-4">
		<select id="barcode-symbology" class="form-control generate-barcode">
			<option value="code_39"<?php echo $req_symbology == 'code_39' ? ' selected' : null; ?>>CODE 39</option>
			<option value="code_93"<?php echo $req_symbology == 'code_93' ? ' selected' : null; ?>>CODE 93</option>
			<option value="code_128"<?php echo $req_symbology == 'code_128' ? ' selected' : null; ?>>CODE 128</option>
			<option value="ean_2"<?php echo $req_symbology == 'ean_2' ? ' selected' : null; ?>>EAN 2</option>
			<option value="ean_5"<?php echo $req_symbology == 'ean_5' ? ' selected' : null; ?>>EAN 5</option>
		</select>
	</div>
	<div class="col-md-4">
		<div class="print-btn text-right">
			<button class="btn btn-md btn-block btn-warning" onClick="window.print()"><span class="fa fa-print"></span> <?php echo $language->get('button_print'); ?></button>
		</div>
	</div>
</div>

<!-- <script type="text/javascript">
	function setInputFilter(textbox, inputFilter) {
	  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
	    textbox.addEventListener(event, function() {
	      if (inputFilter(this.value)) {
	        this.oldValue = this.value;
	        this.oldSelectionStart = this.selectionStart;
	        this.oldSelectionEnd = this.selectionEnd;
	      } else if (this.hasOwnProperty("oldValue")) {
	        this.value = this.oldValue;
	        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
	      }
	    });
	  });
	};

	setInputFilter(document.getElementById("barcode-limit"), function(value) {
  			return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500);
  	 });
</script> -->

<div class="table-responsive">
	<table id="barcode-list" class="table table-bordered">
	
	
	
<tbody>
	<?php 
	$limit1 = ceil($limit/5);
	$total = 1;
		for ($i=0; $i < $limit1; $i++) : ?>
			<tr>
				<?php
				
				//$limit_td = $limit
				for ($j=0; $j < 5; $j++) : ?>
					<?php
						if($limit >= $total){?>
							<td width="20%">
								<h4 class="shop-name"><?php echo $store_name; ?></h4>
								<p class="product-name"><?php echo $product_name; ?></p>
								<img src="data:image/png;base64,<?php echo base64_encode($generator->getBarcode($code, $symbology, 1)); ?>">
								<p class="code"><?php echo $code; ?></p>
								<p class="price"><?php echo $language->get('text_price'); ?>: <?php echo $currency->getCode();?> <?php echo $product_price; ?></p>
							</td>
						<?php }
						else{?>
							<td width="20%"></td>
						<?php }
							$total++;
						?>
					
				<?php endfor; ?>
				<!-- <td>
					<h4 class="shop-name"><?php echo $store_name; ?></h4>
					<p class="product-name"><?php echo $product_name; ?></p>
					<img src="data:image/png;base64,<?php echo base64_encode($generator->getBarcode($code, $symbology, 1)); ?>">
					<p class="code"><?php echo $code; ?></p>
					<p class="price"><?php echo $language->get('text_price'); ?>: <?php echo $currency->getCode();?> <?php echo $product_price; ?></p>
				</td>
				<td>
					<h4 class="shop-name"><?php echo $store_name; ?></h4>
					<p class="product-name"><?php echo $product_name; ?></p>
					<img src="data:image/png;base64,<?php echo base64_encode($generator->getBarcode($code, $symbology, 1)); ?>">
					<p class="code"><?php echo $code; ?></p>
					<p class="price"><?php echo $language->get('text_price'); ?>: <?php echo $currency->getCode();?> <?php echo $product_price; ?></p>
				</td>
				<td>
					<h4 class="shop-name"><?php echo $store_name; ?></h4>
					<p class="product-name"><?php echo $product_name; ?></p>
					<img src="data:image/png;base64,<?php echo base64_encode($generator->getBarcode($code, $symbology, 1)); ?>">
					<p class="code"><?php echo $code; ?></p>
					<p class="price"><?php echo $language->get('text_price'); ?>: <?php echo $currency->getCode();?> <?php echo $product_price; ?></p>
				</td>
				<td>
					<h4 class="shop-name"><?php echo $store_name; ?></h4>
					<p class="product-name"><?php echo $product_name; ?></p>
					<img src="data:image/png;base64,<?php echo base64_encode($generator->getBarcode($code, $symbology, 1)); ?>">
					<p class="code"><?php echo $code; ?></p>
					<p class="price"><?php echo $language->get('text_price'); ?>: <?php echo $currency->getCode();?> <?php echo $product_price; ?></p>
				</td> -->
			</tr>
	<?php endfor; ?>
		</tbody>
	
	</table>
</div>

<?php $Hooks->do_action('After_Showing_Barcode_List');?>
