<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if your logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_supplier_profile') && !$user->hasPermission('access', 'read_expense')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// supplier id
$sup_id = isset($request->get['sup_id']) ? (int)$request->get['sup_id'] : null;

/**
 *===================
 * START DATATABLE
 *===================
 */

$Hooks->do_action('Before_Showing_Supplier_Invoice_List');

$where_query = 'buying_info.is_visible = 1 AND buying_info.store_id = ' . store_id();
$from = from();
$to = to();
//$where_query .= date_range_filter2($from, $to);

// DB table to use
$table = "(SELECT buying_info.*, buying_price.paid_amount,buying_price.payment_mode, suppliers.sup_name FROM buying_info 
  LEFT JOIN suppliers ON buying_info.sup_id = suppliers.sup_id 
  LEFT JOIN buying_price ON buying_info.invoice_id = buying_price.invoice_id 
  WHERE $where_query) as buying_info";
 
// Table's primary key
$primaryKey = 'info_id';

$columns = array(
  array(
      'db' => 'invoice_id',
      'dt' => 'DT_RowId',
      'formatter' => function( $d, $row ) {
          return 'row_'.$d;
      }
  ),
  array(
      'db' => 'info_id',
      'dt' => 'serial_no',
      'formatter' => function( $d, $row ) {
          return $row['info_id'];
      }
  ),
  array( 
      'db' => 'info_id',   
      'dt' => 'select' ,
      'formatter' => function($d, $row) {
          return '<input type="checkbox" name="selected[]" value="' . $row['info_id'] . '">';
      }
  ),
  array( 
    'db' => 'invoice_id',   
    'dt' => 'item_name' ,
    'formatter' => function($d, $row) use($db) {
        $statement = $db->prepare("SELECT `item_name` FROM `buying_item` WHERE `invoice_id` = ?");
        $statement->execute(array($row['invoice_id']));
        $invoice_item = $statement->fetch(PDO::FETCH_ASSOC);
        return ucfirst($invoice_item['item_name']);
    }
  ),
  array( 'db' => 'invoice_id', 'dt' => 'invoice_id' ),
  array( 'db' => 'payment_mode', 'dt' => 'payment_mode' ),
  array( 'db' => 'sup_id', 'dt' => 'sup_id' ),
  array( 'db' => 'sup_name', 'dt' => 'sup_name' ),
  array( 
    'db' => 'created_at',   
    'dt' => 'datetime' ,
    'formatter' => function($d, $row) {
        return $row['created_at'];
    }
  ),
  array( 'db' => 'paid_amount',	'dt' => 'invoice_amount' ),
  array( 
    'db' => 'invoice_url',   
    'dt' => 'invoice_url' ,
    'formatter' => function($d, $row) {
      if ($row['invoice_url']) {
        return '<a href="../storage/buying-invoices/' . $row['invoice_url'] . '" target="_blink"><span class="fa fa-eye"></span></a>';
      }

      return;
    }
  ),
  array( 
    'db' => 'invoice_note',   
    'dt' => 'note' ,
    'formatter' => function($d, $row) use($language) {
      return limit_char($row['invoice_note'], 30);
    }
  ),
  array( 
    'db' => 'sup_id',   
    'dt' => 'btn_view_invoice' ,
    'formatter' => function($d, $row) use($language) {
      return '<button id="view-invoice-btn" class="btn btn-sm btn-block btn-info" data-id="'.$row['invoice_id'].' title="'.$language->get('button_view').'"><i class="fa fa-fw fa-eye" title="View"></i></button>';
    }
  ),
  array( 
    'db' => 'buy_time',   
    'dt' => 'btn_edit_invoice' ,
    'formatter' => function($d, $row) use($db, $language) {

      $buying_date_time = strtotime($row['created_at']);
      $statement = $db->prepare("SELECT * FROM `buying_info` WHERE `sup_id` = ? ORDER BY `info_id` DESC LIMIT 1");
      $statement->execute(array($row['sup_id']));
      $invoice = $statement->fetch(PDO::FETCH_ASSOC);

      // check, if atleast one invoice item is active or not
      $statement = $db->prepare("SELECT COUNT(*) as total, sum(`total_sell`) as total_sell, status FROM `buying_item` WHERE `invoice_id` = ? GROUP BY `status` DESC");
      $statement->execute(array($row['invoice_id']));
      $buying_item = $statement->fetch(PDO::FETCH_ASSOC);

      if ($buying_item['total_sell'] > 0
          && (($buying_item['status'] != 'active') && ($buying_item['status'] != 'stock')) 
          || (invoice_edit_lifespan() > $buying_date_time)
          || ($row['invoice_id'] != $invoice['invoice_id'])) {

          return '<button class="btn btn-sm btn-block btn-default" disabled><i class="fa fa-fw fa-edit"></i></button>';
      }

      return '<button id="edit-invoice-btn" class="btn btn-sm btn-block btn-success" title="'.$language->get('button_edit').'"><i class="fa fa-fw fa-edit"></i></button>';
    }
  ),
  array( 
    'db' => 'buy_time',   
    'dt' => 'btn_edit_expence' ,
    'formatter' => function($d, $row) use($db, $language) {

      $buying_date_time = strtotime($row['created_at']);
      $statement = $db->prepare("SELECT * FROM `buying_info` WHERE `inv_type` = ? ORDER BY `info_id` DESC");
      $statement->execute(array('expense'));
      $invoice = $statement->fetch(PDO::FETCH_ASSOC);

      // if (invoice_edit_lifespan() > $buying_date_time || $row['invoice_id'] != $invoice['invoice_id']) {
      //     return '<button class="btn btn-sm btn-block btn-default" disabled><i class="fa fa-fw fa-edit"></i></button>';
      // }

      return '<button id="edit-invoice-btn" class="btn btn-sm btn-block btn-success" title="'.$language->get('button_edit').'"><i class="fa fa-fw fa-edit"></i></button>';
    }
  ),
  array( 
    'db' => 'sup_id',   
    'dt' => 'btn_delete_invoice' ,
    'formatter' => function($d, $row) use($db, $language) {

      $buying_date_time = strtotime($row['created_at']);
      $statement = $db->prepare("SELECT * FROM `buying_info` WHERE `sup_id` = ? ORDER BY `info_id` DESC LIMIT 1");
      $statement->execute(array($row['sup_id']));
      $invoice = $statement->fetch(PDO::FETCH_ASSOC);

      // check, if atleast one invoice item is active or not
      $statement = $db->prepare("SELECT COUNT(*) as total, SUM(`total_sell`) as total_sell, status FROM `buying_item` WHERE `invoice_id` = ? GROUP BY `status` DESC");
      $statement->execute(array($row['invoice_id']));
      $buying_item = $statement->fetch(PDO::FETCH_ASSOC);

      // if ($buying_item['total_sell'] > 0
      //     && (($buying_item['status'] == 'active') || ($buying_item['status'] == 'sold')) 
      //     || (invoice_delete_lifespan() > $buying_date_time)
      //     || ($row['invoice_id'] != $invoice['invoice_id'])) {

      //     return '<button class="btn btn-sm btn-block btn-default" disabled><i class="fa fa-fw fa-trash"></i></button>';
      // }

      return '<button id="delete-invoice-btn" class="btn btn-sm btn-block btn-danger" title="'.$language->get('button_delete').'"><i class="fa fa-fw fa-trash"></i></button>';
    }
  )
);

$inv_type = isset($request->get['inv_type']) && in_array($request->get['inv_type'], array('buy', 'expense')) ? $request->get['inv_type'] : 'buy';

$where_query = "inv_type = '" . $inv_type . "'";

if ($sup_id) {
  $where_query .= ' AND sup_id = ' . $sup_id;
}

// output for datatable
echo json_encode(
  SSP::complex( $request->get, $sql_details, $table, $primaryKey, $columns, null, $where_query)
);

$Hooks->do_action('After_Showing_Supplier_Invoice_List');

/**
 *===================
 * END DATATABLE
 *===================
 */