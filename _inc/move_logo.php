<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

function fn_resize($image_resource_id,$width,$height) {
    $target_width = 150;
    $target_height = 100;
    $target_layer=imagecreatetruecolor($target_width,$target_height);
    imagecopyresampled($target_layer,$image_resource_id,0,0,0,0,$target_width,$target_height, $width,$height);
    return $target_layer;
}

if(isset($_FILES["file"]["type"]))
{
	// check permission
	if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'upload_logo')) {
      throw new Exception($language->get('error_upload_logo_permission'));
    }

    

    $Hooks->do_action('Before_Upload_Logo', $request);

   

	$validextensions = array("jpeg", "jpg", "png");
	$temporary = explode(".", $_FILES["file"]["name"]);
	$file_extension = end($temporary);
	
	if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
	) && ($_FILES["file"]["size"] < 2000000)//Approx. 100kb files can be uploaded.
	&& in_array($file_extension, $validextensions)) {
		
		if ($_FILES["file"]["error"] > 0) {
			
			echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";

		} else {

			$randnumber = rand(1111, 9999);
			$temp = explode(".", $_FILES["file"]["name"]);
			$newfilename = $randnumber.'_logo.' . end($temp);
			$sourcePath = $_FILES["file"]["tmp_name"]; // Storing source path of the file in a variable
			//$targetPath = "../assets/wonderpillars/img/logo-favicons/".$newfilename; // Target path where file is to be stored
			$file_new_name = '';
			$file = $_FILES['file']['tmp_name']; 
	        $source_properties = getimagesize($file);
	        $image_type = $source_properties[2]; 
	        if( $image_type == IMAGETYPE_JPEG) {   
	            $image_resource_id = imagecreatefromjpeg($file);  
	            $target_layer = fn_resize($image_resource_id,$source_properties[0],$source_properties[1]);
	            imagejpeg($target_layer,'../assets/wonderpillars/img/logo-favicons/'.$randnumber . '_logo.jpg');
	            $file_new_name = $randnumber . '_logo.jpg';

	        }
	       
	        elseif( $image_type == IMAGETYPE_PNG ) {
	            $image_resource_id = imagecreatefrompng($file); 
	            $target_layer = fn_resize($image_resource_id,$source_properties[0],$source_properties[1]);
	            imagepng($target_layer,'../assets/wonderpillars/img/logo-favicons/'.$randnumber . '_logo.png');
	            $file_new_name = $randnumber . '_logo.png';
	        }

			//if(move_uploaded_file($sourcePath,$targetPath)) {
				//$statement = $db->prepare("UPDATE `stores` SET `logo` = ? WHERE `store_id` = ?");
				//$statement->execute(array($newfilename, $store_id));
			//}; 

			echo "<span class='success'>Logo Moved Successfully...!!</span><br/>";
			echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
			echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
			echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
			echo "<p style='display:none;' id='new_file_name'>".$file_new_name."</b>";
		}

		$Hooks->do_action('After_Upload_Logo', $request);

	} else {

		echo "<span class='invalid'>***Invalid file Size or Type***<span>";
	}
}