<?php
include ("../_init.php");

// return customer due amount
if($request->server['REQUEST_METHOD'] == 'POST' AND isset($request->post['customer_id'])) {
	echo json_encode(array('due_price' => getBalance($request->post['customer_id'])));
}

// return product list
if($request->server['REQUEST_METHOD'] == 'POST' AND !empty($request->post['type'])) {
	$sup_id = isset($request->post['sup_id']) ? $request->post['sup_id'] : null;
	$type = $request->post['type'];
	$name = $request->post['name_starts_with'];

	$query = "SELECT `p_id`, `p_name`, `category_id`, `p2s`.`buy_price`, `p2s`.`sell_price`, `p2s`.`quantity_in_stock` 
		FROM `products` 
		LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`)
		WHERE `p2s`.`store_id` = ? AND `p2s`.`status` = ?";
	
	if ($sup_id) {
		$query .= " AND `p2s`.`sup_id` = ?";
	}
	$query .= " AND UPPER($type) LIKE '" . strtoupper($name) . "%' ORDER BY `p_id` DESC LIMIT 10";
	$statement = $db->prepare($query);
	$statement->execute(array(store_id(), 1, $sup_id));
	$products = $statement->fetchAll(PDO::FETCH_ASSOC);
	
	$data = array();
    foreach ($products as $product) {
    	$buy_price = currency_format($product['buy_price']);
    	$sell_price = currency_format($product['sell_price']);
		$name = $product['p_id'].'|'.$product['p_name'].'|'.$product['category_id'].'|'.$product['quantity_in_stock'].'|'.$buy_price .'|'.$sell_price;
		array_push($data, $name);
    }
	echo json_encode($data);
}