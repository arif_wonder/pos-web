<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if your logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('customer_profile');

// LOAD CUSTOMER MODEL
$customer_model = $registry->get('loader')->model('customer');

if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->get['customer_id']))
{
  try {

    // check permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'due_collection')) {
      throw new Exception($language->get('error_update_permission'));
    }

    // validate customer id
    $customer_id = $request->get['customer_id'];
    $customer = $customer_model->getCustomer($customer_id);
    if (!$customer) {
        throw new Exception($language->get('error_customer_id'));
    }

    $paid_amount = $request->post['paid_amount'];
    if (!validateFloat($paid_amount)) {
      throw new Exception($language->get('error_paid_amount'));
    }

    // fetch customer info
    $previous_due = $customer['due_amount'];
    $present_due = (float)($previous_due - $paid_amount);

    if ($paid_amount < 0 || $previous_due < $paid_amount) {
        throw new Exception($language->get('error_paid_amount_out_of_range'));
    }

    // validate payment method
    if (empty($request->post['payment_method'])) {
      throw new Exception($language->get('error_payment_method'));
    }

    if (!$request->post['paid_amount'] > 0 || $request->post['paid_amount'] > $previous_due) {
      throw new Exception($language->get('error_invalid_paid_amount'));
    }

    $Hooks->do_action('Before_Due_Paid');

    $invoice_id = generate_invoice_id('due_paid');
    $store_id = store_id();
    $datetime = date('Y-m-d H:i:s');
    $paid_amount = $request->post['paid_amount'];
    $currency_code = $currency->getCode();
    $payment_token = $request->post['payment_token'];

    $statement = $db->prepare("INSERT INTO `selling_info` (invoice_id, store_id, inv_type, created_at, customer_id, currency_code, payment_method, created_by) VALUES (?,?,?,?,?,?,?,?)");
    $statement->execute(array($invoice_id, $store_id, 'due_paid', $datetime, $customer_id, $currency_code, $request->post['payment_method'], $user->getId()));
    $todays_due = -($paid_amount);
    $statement = $db->prepare("INSERT INTO `selling_price` (invoice_id, store_id, previous_due, paid_amount, todays_due, present_due, payment_id) VALUES (?,?,?,?,?,?,?)");
    $statement->execute(array($invoice_id, $store_id, $previous_due, $paid_amount, $todays_due, $present_due,$payment_token));

    $statement = $db->prepare("INSERT INTO `selling_item` (invoice_id, store_id, item_id, item_name, item_price, item_quantity, item_total) VALUES (?,?,?,?,?,?,?)");
    $statement->execute(array($invoice_id, $store_id, 0, 'Duepaid', $paid_amount, 1, $paid_amount));

    $update_due = $db->prepare("UPDATE `customer_to_store` SET `due_amount` = ?, `currency_code` = ? WHERE `customer_id` = ? AND `store_id` = ?");
    $update_due->execute(array($present_due, $currency_code, $customer_id, $store_id));

    // fetch customer info
    $customer = $customer_model->getCustomer($customer_id);

    $Hooks->do_action('After_Due_Paid', $invoice_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_success'), 'id' => $invoice_id, 'customer' => $customer));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// view deu paid details
if (isset($request->get['action_type']) AND $request->get['action_type'] == 'DUEPAIDDETAILS') {

    try {

        $created_by = isset($request->get['created_by']) ? $request->get['created_by'] : null;
        if ($created_by) {
            $where_query = "`inv_type` = 'due_paid' AND `created_by` = '$created_by' AND `is_visible` = ?";
        } else {
            $where_query = "`inv_type` = 'due_paid' AND `is_visible` = ?";
        }
        
        $from = from() ? from() : date('Y-m-d');
        $to = to() ? to() : date('Y-m-d');
        $where_query .= date_range_filter($from, $to);

        $statement = $db->prepare("SELECT * FROM `selling_info` 
            LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`)
            WHERE $where_query");
        $statement->execute(array(1));
        $the_invoices = $statement->fetchAll(PDO::FETCH_ASSOC);
        if (!$statement->rowCount() > 0) {
            throw new Exception($language->get('error_not_found'));
        }

        $invoices = array();
        $from = date('Y-m-d H:i:s', strtotime($from.' '.'00:00:00')); 
        $to = date('Y-m-d H:i:s', strtotime($to.' '.'23:59:59'));
        foreach ($the_invoices as $invoice) {
            $ref_invoice = get_the_invoice($invoice['ref_invoice_id']);
            if ((strtotime(date('Y-m-d H:i:s', strtotime($ref_invoice['created_at']))) >= strtotime($from)) && (strtotime(date('Y-m-d H:i:s', strtotime($ref_invoice['created_at']))) <= strtotime($to))) {
                continue;
            }
            $invoices[] = $invoice;
        }

        include('template/due_paid_details.php');
        
    } catch (Exception $e) { 

        header('HTTP/1.1 422 Unprocessable Entity');
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode(array('errorMsg' => $e->getMessage()));
        exit();
    }
}

// view due paid form
if (isset($request->get['action_type']) AND $request->get['action_type'] == 'PAID_FORM' && isset($request->get['customer_id'])) {

    try {

        // validate customer id
        $customer_id = $request->get['customer_id'];
        $customer = $customer_model->getCustomer($customer_id);
        if (!$customer) {
            throw new Exception($language->get('error_customer_id'));
        }
        
        // fetch customer info
        $customer = $customer_model->getCustomer($customer_id);
        if (!$customer) {
            throw new Exception($language->get('error_customer_not_found'));
        }

        include('template/duepaid_form.php');
        
    } catch (Exception $e) { 

        header('HTTP/1.1 422 Unprocessable Entity');
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode(array('errorMsg' => $e->getMessage()));
        exit();
    }
}