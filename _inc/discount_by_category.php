<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_product')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('management');

// LOAD PRODUCT MODEL
$discbycategory_model = $registry->get('loader')->model('discbycategory');

// validate post data
function validate_request_data($request, $language) 
{

  // validate product name
  if (!validateString($request->post['title'])) {
    throw new Exception('please enter title');
  }

  // validate product name
  if (!validateString($request->post['date_from'])) {
    throw new Exception('please select from date');
  }


  // validate product name
  if (!validateString($request->post['category_id'])) {
    throw new Exception($language->get('error_category_id'));
  }

  //discount
  if (!validateString($request->post['discount'])) {
    throw new Exception('Please provide discount');
  }

  //min_purchase_amount
  if (!validateString($request->post['min_purchase_amount'])) {
    throw new Exception('Please provide minimum purchase amount');
  }

    // max discount cannot be null
  if (!validateString($request->post['max_discount'])) {
    throw new Exception('Please provide max discount');
  }

    if($request->post['discount_type'] == 'fixed'){

      if ($request->post['min_purchase_amount'] < $request->post['discount']) {
        throw new Exception('Minimum purchase amount can never be less than discount amount');
      }

      if ($request->post['max_discount'] > $request->post['discount'] ) {
        throw new Exception('Max discount amount can never be greather than discount amount');
      }
     
    }

  if ($request->post['max_discount'] > $request->post['min_purchase_amount'] ) {
      throw new Exception('Max discount amount can never be greather than minimum purchase amount');
  }

  if($request->post['discount_type'] == 'precentage'){

    if ($request->post['discount'] > 100) {
      throw new Exception("Discount can't be greater than 100 percent");
    }

    $percentAmount = ($request->post['min_purchase_amount']*$request->post['discount'])/100;
    if($request->post['max_discount'] < round($percentAmount)){
      throw new Exception("Max discount can't be less than ".round($percentAmount));
    }

    
  }

}

// check product code
function validate_category_id($request, $language)
{
  global $db;
  $statement = $db->prepare("SELECT * FROM `categorys` WHERE `category_id` = ?");
  $statement->execute(array($request->post['category_id']));

  if ($statement->rowCount() <= 0) {
    throw new Exception($language->get('error_category_id_not_exist'));
  }
}

// create product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'CREATE')
{
  try {

    // check create permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'create_product')) {
      throw new Exception($language->get('error_create_permission'));
    }

    // validate post data
    validate_request_data($request, $language);

    // validate existance
    validate_category_id($request, $language);

    $Hooks->do_action('Before_Create_Discount_By_Category');

    // insert product into database    
    $disByCatid = $discbycategory_model->addDiscByCategory($request->post);

    // get box info
    $result = $discbycategory_model->getDiscByCategory($disByCatid);

    $Hooks->do_action('After_Create_Discount_By_Category', $result);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => 'Discount created successfully', 'id' => $disByCatid, 'result' => $result));
    exit();

  } catch (Exception $e) {
    
    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// update product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'UPDATE')
{
  try {
    
    validate_request_data($request, $language);

    $disc_by_category_id = $request->post['disc_by_category_id'];

    if (DEMO && $disc_by_category_id == 1) {
      throw new Exception($language->get('error_update_permission'));
    }

    $Hooks->do_action('Before_Update_Discount_By_Category', $disc_by_category_id);
    
    // edit product        
    $discbycategory_model->editDiscByCategory($disc_by_category_id, $request->post);

    $Hooks->do_action('After_Update_Discount_By_Category', $disc_by_category_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_update_success'), 'id' => $disc_by_category_id));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// delete 
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'DELETE')
{
  try {
    // fetch brand by id
    $disc_by_cat_id = $request->post['disc_by_cat_id'];
    $discbycategory = $discbycategory_model->getDiscByCategory($disc_by_cat_id);

    // check product exist or not
    if (!isset($discbycategory['id'])) {
      throw new Exception($language->get('text_not_found'));
    }

    $Hooks->do_action('Before_Delete_Discount_By_Category', $request);

    $action_type = $request->post['action_type'];
    
    $discbycategory_model->deleteDiscByCategory($disc_by_cat_id); 
    $message = $language->get('text_delete');

    $Hooks->do_action('After_Delete_Discount_By_Category', $discbycategory);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $message, 'id' => $disc_by_cat_id, 'action_type' => $action_type));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
}

// product create form
if (isset($request->get['action_type']) && $request->get['action_type'] == 'CREATE') 
{
  validate_request_data($request, $language);
  $Hooks->do_action('Before_Showing_Discount_By_Category_Form');
  include 'template/disc_by_category_create_form.php';
  $Hooks->do_action('After_Showing_Discount_By_Category_Form');

  exit();
}

// edit form
if (isset($request->get['id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'EDIT') {
    
  // fetch product info
  $data = $discbycategory_model->getDiscByCategory($request->get['id']);

  $Hooks->do_action('Before_Showing_Discount_By_Ctegory_Edit_Form', $data);
  include 'template/disc_by_category_edit_form.php';
  $Hooks->do_action('After_Showing_Discount_By_Ctegory_Edit_Form', $data);

  exit();
}

// product delete form
if (isset($request->get['id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'DELETE') {
    
  // fetch product info
  $disc_by_cat = $discbycategory_model->getDiscByCategory($request->get['id']);

  $Hooks->do_action('Before_Showing_Product_Delete_Form', $disc_by_cat);
  include 'template/discount_by_category_del_form.php';
  $Hooks->do_action('After_Showing_Product_Delete_Form', $disc_by_cat);

  exit();
}

// product view template
if (isset($request->get['p_id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'VIEW') {

  // fetch product info
  $product = $discbycategory_model->getProduct($request->get['p_id']);

  $Hooks->do_action('Before_Showing_Product_View_Form', $product);
  include 'template/disc_by_category_view_form.php';
  $Hooks->do_action('After_Showing_Product_View_Form', $product);

  exit();
}

/**
 *===================
 * START DATATABLE
 *===================
 */

$Hooks->do_action('Before_Showing_Brand_List');

$where_query = '';
 
// DB table to use
$table = "(SELECT * FROM disc_by_category ORDER BY id DESC ) as disc_by_cat";
 
// Table's primary key
$primaryKey = 'id';
$columns = array(
  array(
      'db' => 'id',
      'dt' => 'DT_RowId',
      'formatter' => function( $d, $row ) {
          return 'row_'.$d;
      }
  ),
  array( 
    'db' => 'id',   
    'dt' => 'select' ,
    'formatter' => function($d, $row) {
        return '<input type="checkbox" name="selected[]" value="' . $row['id'] . '">';
    }
  ),
  array( 'db' => 'id',  'dt' => 'id' ),
  array( 'db' => 'title',  'dt' => 'title' ),
  array( 'db' => 'description',  'dt' => 'description' ),
  array( 
    'db' => 'id',   
    'dt' => 'view_btn' ,
    'formatter' => function($d, $row) use($language) {
      return '<a class="btn btn-sm btn-block btn-warning" title="'.$language->get('button_view').'" href="disc_by_category_details.php?id='.$row['id'].'"><i class="fa fa-eye"></i></a>';
    }
  ),
  array( 
    'db' => 'id',   
    'dt' => 'edit_btn' ,
    'formatter' => function($d, $row) use($language) {
        return'<button class="btn btn-sm btn-block btn-primary edit-discount-by-category" type="button" title="'.$language->get('button_edit').'"><i class="fa fa-pencil"></i></button>';
    }
  ),
  array( 
    'db' => 'id',   
    'dt' => 'delete_btn' ,
    'formatter' => function($d, $row) use($language) {
      return'<button class="btn btn-sm btn-block btn-danger discount-by-category-delete" type="button" title="'.$language->get('button_delete').'"><i class="fa fa-trash"></i></button>';
    }
  )
);
 

// output for datatable
echo json_encode(
  SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns, null, $where_query)
);

$Hooks->do_action('After_Showing_Brand_List');

/**
 *===================
 * END DATATABLE
 *===================
 */