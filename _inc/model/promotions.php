<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
class ModelPromotions extends Model {

	public function addPromotions($data) {
		$result = array();
		if ($data['promotions_type']=='disc_for_festival'){
			$insertedId   = $this->disc_for_festival($data);
			$result['id'] = $insertedId;
			$result['promotions_type'] = 'disc_for_festival';
		}
		elseif ($data['promotions_type']=='disc_happy_hours') {
			$insertedId   =	$this->disc_happy_hours($data);
			$result['id'] = $insertedId;
			$result['promotions_type'] = 'disc_for_festival';
		}
		elseif ($data['promotions_type']=='disc_for_anniversary') {
			$insertedId   = $this->disc_for_anniversary($data);
			$result['id'] = $insertedId;
			$result['promotions_type'] = 'disc_for_anniversary';
		}
		return $result;
	}

	public function disc_for_festival($data,$id='') {
		$query_str = $id ? 'UPDATE' : 'INSERT';
    	$wh_id = $id ? 'WHERE id = ?' : '';
		$statement = $this->db->prepare("$query_str 
												offers
											SET 
												title = ?, 
												description = ?,
												discount = ?,
												discount_type = ? ,
												min_purchase_amount = ?,
												form_date = ?,
												to_date = ?
											$wh_id
										");
		$params = array(
    		$data['title'] ? : null, 
    		$data['description'] ? : null,  
    		$data['discount'] ? : null,
    		$data['discount_type'] ? : 'fixed',
    		$data['min_purchase_amount'] ? : null,
    		$data['form_date'] ? date('Y-m-d',strtotime($data['form_date'])) : null,
    		$data['to_date'] ? date('Y-m-d',strtotime($data['to_date'])) : null
    	);

    	if ($id) {
    		$params[] = $id;
    	}
    	$statement->execute($params);
    	if ($id) {
    		$insertedId = $id;
    	}
    	else {
    		$insertedId = $this->db->lastInsertId();
    	}
    	return $insertedId;
	}
	public function disc_happy_hours($data, $id='') {
		$query_str = $id ? 'UPDATE' : 'INSERT';
    	$wh_id = $id ? 'WHERE id = ?' : '';
		$statement = $this->db->prepare("$query_str 
												disc_happy_hours
											SET 
												title = ?, 
												description = ?,
												discount = ?,
												discount_type = ? ,
												min_purchase_amount = ? ,
												max_discount = ?,
												time_from = ? ,
												time_to = ? ,
												on_days = ?
											$wh_id 
										");
    	$ondays = isset($data['on_days'])? implode(',', $data['on_days']) : null;
    	$params = array(
    		$data['title'] ? : null, 
    		$data['description'] ? : null,  
    		$data['discount'] ? : null,
    		$data['discount_type'] ? : 'fixed',
    		$data['min_purchase_amount'] ? : null,
    		$data['max_discount'] ? : null,
    		$data['time_from'] ? date('H:i:s', strtotime($data['time_from'])) : null,
    		$data['time_to'] ? date('H:i:s', strtotime($data['time_to'])) : null,
    		$ondays
    	);

    	if ($id) {
    		$params[] = $id;
    	}
    	$statement->execute($params);
    	if ($id) {
    		$insertedId = $id;
    	}
    	else {
    		$insertedId = $this->db->lastInsertId();
    	}
    	return $insertedId;
	}
	public function disc_for_anniversary($data, $id='') {
		$query_str = $id ? 'UPDATE' : 'INSERT';
    	$wh_id = $id ? 'WHERE id = ?' : '';
		$statement = $this->db->prepare("$query_str 
												disc_for_anniversary
											SET 
												title = ?, 
												description = ?,
												discount = ?,
												discount_type = ? ,
												min_purchase_amount = ? 
											$wh_id
										");
    	
    	$params = array(
    		$data['title'] ? : null, 
    		$data['description'] ? : null,  
    		$data['discount'] ? : null,
    		$data['discount_type'] ? : 'fixed',
    		$data['min_purchase_amount'] ? : null
    	);

    	if ($id) {
    		$params[] = $id;
    	}
    	$statement->execute($params);
    	if ($id) {
    		$insertedId = $id;
    	}
    	else {
    		$insertedId = $this->db->lastInsertId();
    	}
    	return $insertedId;
	}

	public function editPromotions($promotionsId, $data) {
		$result = array();
		if ($data['promotions_type']=='disc_for_festival'){
			$insertedId   = $this->disc_for_festival($data,$promotionsId);
			$result['id'] = $insertedId;
			$result['promotions_type'] = 'disc_for_festival';
		}
		elseif ($data['promotions_type']=='disc_happy_hours') {
			$insertedId   =	$this->disc_happy_hours($data,$promotionsId);
			$result['id'] = $insertedId;
			$result['promotions_type'] = 'disc_happy_hours';
		}
		elseif ($data['promotions_type']=='disc_for_anniversary') {
			$insertedId   = $this->disc_for_anniversary($data,$promotionsId);
			$result['id'] = $insertedId;
			$result['promotions_type'] = 'disc_for_anniversary';
		}
		return $result;
	}

	public function deletePromotions($promotions_type, $promotions_id) {

		if ($promotions_type == 'disc_for_festival'){
			$tableName = 'offers';
		}
		elseif ($promotions_type == 'disc_happy_hours') {
			$tableName = 'disc_happy_hours';
		}
		elseif ($promotions_type == 'disc_for_anniversary') {
			$tableName = 'disc_for_anniversary';
		}

		$statement = $this->db->prepare("DELETE FROM ".$tableName." WHERE `id` = ? LIMIT 1");
        $statement->execute(array($promotions_id));

        return $promotions_id;
	}

	public function getPromotions($promotions_type,$id) {
		$result = '';

		if ($promotions_type == 'disc_for_festival'){
			$statement = $this->db->prepare("SELECT * FROM `offers` WHERE id = ?");
			$statement->execute(array($id));
			$result = $statement->fetch(PDO::FETCH_ASSOC);
			$result['promotions_type'] = 'disc_for_festival';
		}
		elseif ($promotions_type == 'disc_happy_hours') {
		 	$statement = $this->db->prepare("SELECT * FROM `disc_happy_hours` WHERE id = ?");
			$statement->execute(array($id));
			$result = $statement->fetch(PDO::FETCH_ASSOC);
			$result['promotions_type'] = 'disc_happy_hours';
			$result['on_days'] = explode(',', $result['on_days']);

		}
		elseif ($promotions_type == 'disc_for_anniversary') {
			$statement = $this->db->prepare("SELECT * FROM `disc_for_anniversary` WHERE id = ?");
			$statement->execute(array($id));
			$result = $statement->fetch(PDO::FETCH_ASSOC);
			$result['promotions_type'] = 'disc_for_anniversary';
		}
		return $result;
	}
}