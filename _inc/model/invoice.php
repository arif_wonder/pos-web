<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
class ModelInvoice extends Model 
{
	public function getInvoiceInfo($invoice_id, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();
		$statement = $this->db->prepare("SELECT `selling_info`.*, `selling_price`.*, `customers`.`customer_id`, `customers`.`customer_name`, `customers`.`customer_mobile`, `customers`.`customer_email`,`users`.`id`, `users`.`username` FROM `selling_info`
			LEFT JOIN `selling_price` ON `selling_info`.`invoice_id` = `selling_price`.`invoice_id` 
			LEFT JOIN `customers` ON `selling_info`.`customer_id` = `customers`.`customer_id` 
			LEFT JOIN `users` ON `selling_info`.`created_by` = `users`.`id` 
			WHERE `selling_info`.`store_id` = ? AND (`selling_info`.`invoice_id` = ? OR (`selling_info`.`customer_id` = ?) AND `selling_info`.`inv_type` = 'sell') ORDER BY `selling_info`.`invoice_id` DESC");
		$statement->execute(array($store_id, $invoice_id, $invoice_id));
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function getInvoiceItems($invoice_id) 
	{
		$statement = $this->db->prepare("SELECT * FROM `selling_item` WHERE invoice_id = ?");
		$statement->execute(array($invoice_id));
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	//by invoice and sell item id
	public function getInvoiceBySellItems($id) 
	{
		$statement = $this->db->prepare("SELECT * FROM `selling_item` WHERE id = ? LIMIT 1");
		$statement->execute(array($id));
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function getSellingPrice($invoice_id) 
	{
		$statement = $this->db->prepare("SELECT * FROM `selling_price` WHERE invoice_id = ?");
		$statement->execute(array($invoice_id));
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function hasInvoice($invoice_id, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();
		$statement = $this->db->prepare("SELECT * FROM `selling_info` WHERE `selling_info`.`store_id` = ? AND `selling_info`.`invoice_id` = ?");
		$statement->execute(array($store_id, $invoice_id));
		$invoice = $statement->fetch(PDO::FETCH_ASSOC);
		return isset($invoice['invoice_id']);

	}

	public function isLastInvoice($customer_id, $invoice_id, $store_id = null)
	{
		$store_id = $store_id ? $store_id : store_id();
		$statemtnt = $this->db->prepare("SELECT * FROM `selling_info` WHERE `store_id` = ? AND `customer_id` = ? AND `inv_type` = ? ORDER BY `info_id` DESC LIMIT 1");
        $statemtnt->execute(array($store_id, $customer_id, 'sell'));
        $row = $statemtnt->fetch(PDO::FETCH_ASSOC);
        return $row['invoice_id'] == $invoice_id;
	}

	public function getLastInvoice($type = 'sell', $store_id = null)
	{
		$store_id = $store_id ? $store_id : store_id();
		$statemtnt = $this->db->prepare("SELECT * FROM `selling_info` WHERE `store_id` = ? AND `inv_type` = ? ORDER BY `info_id` DESC");
        $statemtnt->execute(array($store_id, $type));
        return $statemtnt->fetch(PDO::FETCH_ASSOC);
	}

	public function getNextInvoice($customer_id, $invoice_id, $type = 'sell', $store_id = null)
	{
		$store_id = $store_id ? $store_id : store_id();
		$statemtnt = $this->db->prepare("SELECT * FROM `selling_info` WHERE `store_id` = ? AND `customer_id` = ? AND `inv_type` = ? ORDER BY `info_id` DESC");
        $statemtnt->execute(array($store_id, $customer_id, $type));
        $rows = $statemtnt->fetchAll(PDO::FETCH_ASSOC);
        $invoice = null;
        foreach ($rows as $r) {
        	if ($r['invoice_id'] == $invoice_id) {
        		break;
        	}
        	$invoice = $r;
        }
        return $invoice;
	}

	public function total($from, $to)
	{
		$where_query = "`store_id` = ? AND `inv_type` = 'sell'";
		$where_query .= date_range_filter($from, $to);
		$statement = $this->db->prepare("SELECT * FROM `selling_info` WHERE $where_query");
		$statement->execute(array(store_id()));
		return $statement->rowCount();
	}

	public function getDuePaid($customer_id, $invoice_id, $store_id = null)
	{
		$total = 0;
		$store_id = $store_id ? $store_id : store_id();
		$where = "`selling_info`.`store_id` = ? AND `selling_info`.`inv_type` = ? AND `customer_id` = $customer_id";

		$invoice_info = $this->getInvoiceInfo($invoice_id);
		$created_at = $invoice_info['created_at'];

		if (!$this->isLastInvoice($customer_id, $invoice_id)) {
			$next_invoice = $this->getNextInvoice($customer_id, $invoice_id);
			$to = $next_invoice['created_at'];
			$where .= " AND (`ref_invoice_id` = '$invoice_id' OR (`created_at` >= '$created_at' AND `created_at` < '$to'))";
		} else {
			$where .= " AND (`ref_invoice_id` = '$invoice_id' OR `created_at` >= '$created_at')";
		}

		$statemtnt = $this->db->prepare("SELECT `selling_info`.*, `selling_price`.`paid_amount` FROM `selling_info` LEFT JOIN `selling_price` ON `selling_info`.`invoice_id` = `selling_price`.`invoice_id` WHERE $where");
        $statemtnt->execute(array($store_id, 'due_paid'));
        $rows = $statemtnt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($rows as $value) {
        	$total += $value['paid_amount'];
        }
        return $total;
	}

	public function getDuePaidRows($customer_id, $invoice_id, $store_id = null)
	{
		$orows = array();
		$store_id = $store_id ? $store_id : store_id();
		$where = "`selling_info`.`store_id` = ? AND `selling_info`.`inv_type` = ? AND `customer_id` = $customer_id";

		$invoice_info = $this->getInvoiceInfo($invoice_id);
		$created_at = $invoice_info['created_at'];

		if (!$this->isLastInvoice($customer_id, $invoice_id)) {
			$next_invoice = $this->getNextInvoice($customer_id, $invoice_id);
			$to = $next_invoice['created_at'];
			$where .= " AND (`ref_invoice_id` = '$invoice_id' OR (`created_at` >= '$created_at' AND `created_at` < '$to'))";
		} else {
			$where .= " AND (`ref_invoice_id` = '$invoice_id' OR `created_at` >= '$created_at')";
		}

		$statemtnt = $this->db->prepare("SELECT `selling_info`.*, `selling_price`.`paid_amount` FROM `selling_info` LEFT JOIN `selling_price` ON `selling_info`.`invoice_id` = `selling_price`.`invoice_id` WHERE $where");
        $statemtnt->execute(array($store_id, 'due_paid'));
        $rows = $statemtnt->fetchAll(PDO::FETCH_ASSOC);
        if ($rows) {
        	$orows = $rows;
        }
        return $orows;
	}
}