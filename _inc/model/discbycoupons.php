<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
class ModelDiscByCoupons extends Model 
{
	public function addDiscByCoupons($data) 
	{
    	$statement = $this->db->prepare("INSERT 
											 disc_coupons
											SET 
												title = ?, 
												description = ?,
												code = ?,
												valid_till = ?,
												discount = ?,
												discount_type = ?,
												max_discount = ?,
												min_purchase_amount = ?,
												limit_per_user = ?
										");
    	
    	$statement->execute(array(
    		$data['title'] ? : null, 
    		$data['description'] ? : null,  
    		$data['code'] ? : null, 
    		$data['valid_till'] ? date('Y-m-d H:i:s',strtotime($data['valid_till'])) : null,
    		$data['discount'] ? : null,
    		$data['discount_type'] ? : 'fixed',
    		$data['max_discount'] ? : null,
    		$data['min_purchase_amount'] ? : null,
    		$data['limit'] ? : null
    	));

    	$insertedId = $this->db->lastInsertId();

    	return $insertedId;
	}

	public function editDiscByCoupons($disc_by_category_id, $data) 
	{
		// update product infomation
    	$statement = $this->db->prepare("UPDATE 
											disc_coupons
											SET 
												title = ?, 
												description = ?,
												code = ?,
												valid_till = ?,
												discount = ?,
												discount_type = ?,
												max_discount = ?,
												min_purchase_amount = ?,
												limit_per_user = ?
								    		WHERE id = ?
							    		");
    	$statement->execute(array(
    		$data['title'] ? : null, 
    		$data['description'] ? : null,  
    		$data['code'] ? : null, 
    		$data['valid_till'] ? date('Y-m-d H:i:s',strtotime($data['valid_till'])) : null,
    		$data['discount'] ? : null,
    		$data['discount_type'] ? : 'fixed',
    		$data['max_discount'] ? : null,
    		$data['min_purchase_amount'] ? : null,
    		$data['limit'] ? : null,
    		$disc_by_category_id,

    	));
    	return $disc_by_category_id;
	}

	public function deleteDiscByCoupons($disc_by_category_id) 
	{
		$statement = $this->db->prepare("DELETE FROM `disc_coupons` WHERE `id` = ? LIMIT 1");
        $statement->execute(array($disc_by_category_id));

        return $disc_by_category_id;
	}

	public function getDiscByCoupons($id) 
	{
		$statement = $this->db->prepare("SELECT * FROM `disc_coupons` 
			WHERE id = ?");
	    $statement->execute(array($id));
	    $result = $statement->fetch(PDO::FETCH_ASSOC);
	    return $result;
	}

	

}