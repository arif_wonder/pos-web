<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
 */
class ModelProduct extends Model {
	public function addProduct($data) {
		$statement = $this->db->prepare("INSERT INTO `products` (
	    		p_name,
	    		p_code,
	    		category_id,
	    		p_image,
	    		description,
	    		p_brand,
				p_quantity,
				p_alert_qty,
				p_unit,
				p_regular_price,
				p_special_price,
				is_gift_item,
				p_gift_item,
				p_bar_code,
				p_purchage_price,
				p_sp_fromdate,
				p_sp_todate,
				p_discount,
				p_discount_type,
				p_taxrate
    		)
    		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?,?,?)");

		$statement->execute(array(
			$data['p_name'] ?: null,
			$data['p_code'] ?: null,
			$data['category_id'] ?: null,
			$data['p_image'] ?: null,
			$data['description'] ?: null,
			$data['p_brand'] ?: null,
			$data['p_quantity'] ?: null,
			$data['p_alert_qty'] ?: null,
			$data['p_unit'] ?: null,
			$data['p_regular_price'] ?: null,
			$data['p_special_price'] ?: null,
			$data['is_gift_item'] ?: 0,
			$data['p_gift_item'] ?: null,
			$data['p_code'] ?: null, ////////p_bar_code
			$data['p_purchage_price'] ?: null,
			$data['p_sp_fromdate'] ? date('Y-m-d', strtotime($data['p_sp_fromdate'])) : null,
			$data['p_sp_todate'] ? date('Y-m-d', strtotime($data['p_sp_todate'])) : null,
			$data['p_discount'] ?: null,
			$data['p_discount_type'] ?: null,
			$data['p_taxrate'] ?: null,
		));

		$product_id = $this->db->lastInsertId();

		if (isset($data['product_store']) && $product_id) {

			$box_id = $data['box_id'] ?: null; ////////box_id
			//$edate = $data['e_date'] ?: null; ////////e_date

			foreach ($data['product_store'] as $store_id) {

				//--- box to store ---//

				$statement = $this->db->prepare("SELECT * FROM `box_to_store` WHERE `store_id` = ? AND `box_id` = ?");
				$statement->execute(array($store_id, $box_id));
				$box = $statement->fetch(PDO::FETCH_ASSOC);
				if (!$box) {
					$statement = $this->db->prepare("INSERT INTO `box_to_store` SET `box_id` = ?, `store_id` = ?");
					$statement->execute(array($box_id, (int) $store_id));
				}

				//--- supplier to store ---//

				$statement = $this->db->prepare("SELECT * FROM `supplier_to_store` WHERE `store_id` = ? AND `sup_id` = ?");
				$statement->execute(array($store_id, $data['sup_id']));
				$supplier = $statement->fetch(PDO::FETCH_ASSOC);
				if (!$supplier) {
					$statement = $this->db->prepare("INSERT INTO `supplier_to_store` SET `sup_id` = ?, `store_id` = ?");
					$statement->execute(array((int) $data['sup_id'], (int) $store_id));
				}

				//--- product to store ---//

				$statement = $this->db->prepare("INSERT INTO `product_to_store` SET `product_id` = ?, `store_id` = ?, `sup_id` = ?, `box_id` = ?, `p_date` = ?");
				$statement->execute(array((int) $product_id, (int) $store_id, $data['sup_id'], $box_id, date('Y-m-d')));

			}
		}

		$this->updateStatus($product_id, $data['status']);
		$this->updateSortOrder($product_id, $data['sort_order']);

		return $product_id;
	}

	public function updateStatus($product_id, $status, $store_id = null) {
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("UPDATE `product_to_store` SET `status` = ? WHERE `store_id` = ? AND `product_id` = ?");
		$statement->execute(array((int) $status, $store_id, (int) $product_id));
	}

	public function updateSortOrder($product_id, $sort_order, $store_id = null) {
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("UPDATE `product_to_store` SET `sort_order` = ? WHERE `store_id` = ? AND `product_id` = ?");
		$statement->execute(array((int) $sort_order, $store_id, (int) $product_id));
	}

	public function editProduct($product_id, $data) {
		// update product infomation
		$statement = $this->db->prepare("UPDATE `products`
								    		SET
												p_name = ?,
												category_id = ?,
												p_image = ?,
												description = ? ,
												p_brand = ? ,
												p_quantity = ? ,
												p_alert_qty = ? ,
												p_unit = ? ,
												p_regular_price = ? ,
												p_special_price = ? ,
												is_gift_item = ? ,
												p_gift_item = ? ,
												p_bar_code = ? ,
												p_purchage_price = ? ,
												p_sp_fromdate = ? ,
												p_sp_todate = ? ,
												p_discount = ? ,
												p_discount_type = ?,
												p_taxrate = ?
								    		WHERE `p_id` = ?"
		);
		$statement->execute(array(
			$data['p_name'],
			$data['category_id'],
			$data['p_image'],
			$data['description'],
			$data['p_brand'] ?: null,
			$data['p_quantity'] ?: null,
			$data['p_alert_qty'] ?: null,
			$data['p_unit'] ?: null,
			$data['p_regular_price'] ?: null,
			$data['p_special_price'] ?: null,
			$data['is_gift_item'] ?: 0,
			$data['p_gift_item'] ?: null,
			$data['p_code'] ?: null, ////////p_bar_code
			$data['p_purchage_price'] ?: null,
			$data['p_sp_fromdate'] ? date('Y-m-d', strtotime($data['p_sp_fromdate'])) : null,
			$data['p_sp_todate'] ? date('Y-m-d', strtotime($data['p_sp_todate'])) : null,
			$data['p_discount'] ?: null,
			$data['p_discount_type'] ?: null,
			$data['p_taxrate'] ?: null,
			$product_id,
		));

		// insert product into store
		if (isset($data['product_store'])) {

			$box_id = $data['box_id'] ?: null; ////////box_id
			//$edate = $data['e_date'] ?: null; ////////e_date
			$store_ids = array();
			//$data['sell_price'] = $data['sell_price'] ? $data['sell_price'] : 0;
			foreach ($data['product_store'] as $store_id) {

				//--- category to store ---//

				$statement = $this->db->prepare("SELECT * FROM `category_to_store` WHERE `store_id` = ? AND `ccategory_id` = ?");
				$statement->execute(array($store_id, $data['category_id']));
				$category = $statement->fetch(PDO::FETCH_ASSOC);
				if (!$category) {
					$statement = $this->db->prepare("INSERT INTO `category_to_store` SET `ccategory_id` = ?, `store_id` = ?");
					$statement->execute(array((int) $data['category_id'], (int) $store_id));
				}

				//--- box to store ---//

				$statement = $this->db->prepare("SELECT * FROM `box_to_store` WHERE `store_id` = ? AND `box_id` = ?");
				$statement->execute(array($store_id, $box_id));
				$box = $statement->fetch(PDO::FETCH_ASSOC);
				if (!$box) {
					$statement = $this->db->prepare("INSERT INTO `box_to_store` SET `box_id` = ?, `store_id` = ?");
					$statement->execute(array($box_id, (int) $store_id));
				}

				//--- supplier to store ---//

				$statement = $this->db->prepare("SELECT * FROM `supplier_to_store` WHERE `store_id` = ? AND `sup_id` = ?");
				$statement->execute(array($store_id, $data['sup_id']));
				$supplier = $statement->fetch(PDO::FETCH_ASSOC);
				if (!$supplier) {
					$statement = $this->db->prepare("INSERT INTO `supplier_to_store` SET `sup_id` = ?, `store_id` = ?");
					$statement->execute(array((int) $data['sup_id'], (int) $store_id));
				}

				//--- product to store ---//

				$statement = $this->db->prepare("SELECT * FROM `product_to_store` WHERE `store_id` = ? AND `product_id` = ?");
				$statement->execute(array($store_id, $product_id));
				$product = $statement->fetch(PDO::FETCH_ASSOC);
				if (!$product) {
					$statement = $this->db->prepare("INSERT INTO `product_to_store` SET `product_id` = ?, `store_id` = ?, `sup_id` = ?, `box_id` = ?, `sell_price` = ?, `p_date` = ?");
					$statement->execute(array((int) $product_id, (int) $store_id, $data['sup_id'], $box_id, $data['sell_price'], date('Y-m-d')));

				} else {

					$statement = $this->db->prepare("UPDATE `product_to_store` SET `sup_id` = ?, `box_id` = ? WHERE `store_id` = ? AND `product_id` = ?");
					$statement->execute(array($data['sup_id'], $box_id, (int) $store_id, (int) $product_id));
				}

				$store_ids[] = $store_id;
			}

			// delete unwanted store
			if (!empty($store_ids)) {

				$unremoved_store_ids = array();

				// get unwanted stores
				$statement = $this->db->prepare("SELECT * FROM `product_to_store` WHERE `store_id` NOT IN (" . implode(',', $store_ids) . ")");
				$statement->execute();
				$unwanted_stores = $statement->fetchAll(PDO::FETCH_ASSOC);
				foreach ($unwanted_stores as $store) {

					$store_id = $store['store_id'];

					// fetch buying invoice id
					$statement = $this->db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `item_id` = ?  AND `status` IN ('stock', 'active') AND `item_quantity` != `total_sell`");
					$statement->execute(array($store_id, $product_id));
					$item_available = $statement->fetch(PDO::FETCH_ASSOC);

					// if item available then store in variable
					if ($item_available) {
						$unremoved_store_ids[$item_available['store_id']] = store_field('name', $item_available['store_id']);
						continue;
					}

					// delete unwanted store link
					$statement = $this->db->prepare("DELETE FROM `product_to_store` WHERE `store_id` = ? AND `product_id` = ?");
					$statement->execute(array($store_id, $product_id));

				}

				if (!empty($unremoved_store_ids)) {

					throw new Exception('The product "' . $item_available['item_name'] . '" can not be removed. Because stock amount available in store ' . implode(', ', $unremoved_store_ids));
				}
			}
		}
		// delete unwanted store

		$this->updateStatus($product_id, $data['status']);
		$this->updateSortOrder($product_id, $data['sort_order']);

		return $product_id;
	}

	public function deleteProduct($product_id) {
		$statement = $this->db->prepare("DELETE FROM `products` WHERE `p_id` = ? LIMIT 1");
		$statement->execute(array($product_id));

		$statement = $this->db->prepare("DELETE FROM `product_to_store` WHERE `product_id` = ?");
		$statement->execute(array($product_id));

		return $product_id;
	}

	public function deleteWithRelatedContent($product_id) {
		// fetch sold out buying invoice id
		$statement = $this->db->prepare("SELECT * FROM `buying_item` WHERE `item_id` = ?");
		$statement->execute(array($product_id));
		$buying_items = $statement->fetchAll(PDO::FETCH_ASSOC);

		// delete buying history
		foreach ($buying_items as $buying_item) {

			if (isset($buying_item['invoice_id'])) {
				$statement = $this->db->prepare("DELETE FROM `buying_info` WHERE `invoice_id` = ?");
				$statement->execute(array($buying_item['invoice_id']));
				$statement = $this->db->prepare("DELETE FROM `buying_price` WHERE `invoice_id` = ?");
				$statement->execute(array($buying_item['invoice_id']));
				$statement = $this->db->prepare("DELETE FROM `buying_item` WHERE `item_id` = ?");
				$statement->execute(array($product_id));
			}
		}

		// fetch selling invoice id
		$statement = $this->db->prepare("SELECT * FROM `selling_item` WHERE `item_id` = ?");
		$statement->execute(array($product_id));
		$selling_items = $statement->fetchAll(PDO::FETCH_ASSOC);

		// delete selling history
		foreach ($selling_items as $selling_item) {

			if (isset($selling_item['invoice_id'])) {
				$statement = $this->db->prepare("DELETE FROM `selling_info` WHERE `invoice_id` = ?");
				$statement->execute(array($selling_item['invoice_id']));
				$statement = $this->db->prepare("DELETE FROM `selling_price` WHERE `invoice_id` = ?");
				$statement->execute(array($selling_item['invoice_id']));
				$statement = $this->db->prepare("DELETE FROM `selling_item` WHERE `item_id` = ?");
				$statement->execute(array($product_id));
			}
		}

		$this->deleteProduct($product_id);
	}

	public function getBelongsStore($p_id) {
		$statement = $this->db->prepare("SELECT * FROM `product_to_store` WHERE `product_id` = ?");
		$statement->execute(array($p_id));

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function isStockAvailable($p_id, $store_id) {
		$statement = $this->db->prepare("SELECT * FROM `buying_item` WHERE `item_id` = ? AND `store_id` = ? AND `status` IN ('active', 'stock')");
		$statement->execute(array($p_id, $store_id));
		return $statement->rowCount();

	}

	public function getProduct($product_id, $store_id = null) {
		$store_id = $store_id ? $store_id : store_id();

		// fetch product
		$statement = $this->db->prepare("SELECT * FROM `products`
			LEFT JOIN `product_to_store` as p2s ON (`products`.`p_id` = `p2s`.`product_id`)
			WHERE `p2s`.`store_id` = ? AND `products`.`p_id` = ?");
		$statement->execute(array($store_id, $product_id));
		$product = $statement->fetch(PDO::FETCH_ASSOC);

		// fetch stores related to products
		$statement = $this->db->prepare("SELECT * FROM `product_to_store` WHERE `product_id` = ?");
		$statement->execute(array($product_id));
		$all_stores = $statement->fetchAll(PDO::FETCH_ASSOC);
		$stores = array();
		foreach ($all_stores as $store) {
			$stores[] = $store['store_id'];
		}

		$product['sup_name'] = get_the_supplier($product['sup_id'], 'sup_name');
		$product['stores'] = $stores;

		return $product;
	}
	public function getProductsTree($productId = '', $giftItems = false) {
		if ($productId) {
			if ($giftItems) {
				$query = $this->db->prepare("SELECT * FROM `products` WHERE `p_id` = ? AND is_gift_item = ?");
				$query->execute(array($productId, 1));
			} else {
				$query = $this->db->prepare("SELECT * FROM `products` WHERE `p_id` = ?");
				$query->execute(array($productId));
			}
		} else {
			if ($giftItems) {
				$query = $this->db->prepare("SELECT * FROM `products` WHERE `is_gift_item` = ?");
				$query->execute(array(1));
			} else {
				$query = $this->db->prepare("SELECT * FROM `products`");
				$query->execute(array());
			}
		}

		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
	public function getProducts($data = array(), $store_id = null) {
		$store_id = $store_id ? $store_id : store_id();

		$sql = "SELECT * FROM `products` p
			LEFT JOIN `product_to_store` p2s ON (`p`.`p_id` = `p2s`.`product_id`)
			LEFT JOIN `suppliers` s ON (`p2s`.`sup_id` = `s`.`sup_id`)
			LEFT JOIN `boxes` bx ON (`p2s`.`box_id` = `bx`.`box_id`)
			WHERE `p2s`.`store_id` = ? AND `p2s`.`status` = ?";

		if (isset($data['filter_name'])) {
			$sql .= " AND `p`.`p_name` LIKE '" . $data['filter_name'] . "%'";
		}

		if (isset($data['filter_buy_price']) && !is_null($data['filter_sell_price'])) {
			$sql .= " AND `p`.`buy_price` LIKE '" . $data['filter_buy_price'] . "%'";
		}

		if (isset($data['filter_sell_price']) && !is_null($data['filter_sell_price'])) {
			$sql .= " AND `p`.`buy_price` LIKE '" . $data['filter_sell_price'] . "%'";
		}

		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND `p`.`quantity_in_stock` = '" . (int) $data['filter_quantity'] . "'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND `p2s`.`status` = '" . (int) $data['filter_status'] . "'";
		}

		$sql .= " GROUP BY p.p_id";

		$sort_data = array(
			'p.p_name',
			'p.buy_price',
			'p.sell_price',
			'p.quantity_in_stock',
			'p2s.status',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY `p`.`p_name`";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
		}

		$statement = $this->db->prepare($sql);
		$statement->execute(array($store_id, 1));

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getSellingPrice($item_id, $from, $to, $store_id = null) {
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`selling_info`.`inv_type` != 'due_paid' AND `selling_item`.`item_id` = ? AND `selling_item`.`store_id` = ?";
		$where_query .= date_range_filter($from, $to);

		$statement = $this->db->prepare("SELECT SUM(`selling_price`.`discount_amount`) as discount, SUM(`selling_price`.`subtotal`) as total FROM `selling_info`
			LEFT JOIN `selling_item` ON (`selling_info`.`invoice_id` = `selling_item`.`invoice_id`)
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`)
			WHERE $where_query");

		$statement->execute(array($item_id, $store_id));
		$invoice = $statement->fetch(PDO::FETCH_ASSOC);

		return (int) ($invoice['total'] - $invoice['discount']);
	}

	public function getBuyingPrice($item_id, $from, $to, $store_id = null) {
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`buying_info`.`inv_type` != 'others' AND `buying_item`.`item_id` = ? AND `buying_item`.`store_id` = ?";
		$where_query .= date_range_filter2($from, $to);

		$statement = $this->db->prepare("SELECT SUM(`buying_price`.`paid_amount`) as total FROM `buying_info`
			LEFT JOIN `buying_item` ON (`buying_info`.`invoice_id` = `buying_item`.`invoice_id`)
			LEFT JOIN `buying_price` ON (`buying_info`.`invoice_id` = `buying_price`.`invoice_id`)
			WHERE $where_query");
		$statement->execute(array($item_id, $store_id));
		$buying_price = $statement->fetch(PDO::FETCH_ASSOC);

		return (int) $buying_price['total'];
	}

	public function getQtyInStock($product_id, $store_id = null) {
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("SELECT SUM(`buying_item`.`item_quantity`) as total, SUM(`buying_item`.`total_sell`) as total_sell FROM `buying_item`
			WHERE `store_id` = ? AND `item_id` = ? AND `status` IN ('stock', 'active')");
		$statement->execute(array($store_id, $product_id));
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result['total'] - $result['total_sell'];
	}

	public function total($store_id = null) {
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("SELECT * FROM `products` LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`) WHERE `p2s`.`store_id` = ? AND `p2s`.`status` = ?");
		$statement->execute(array($store_id, 1));

		return $statement->rowCount();
	}

	public function totalTrash($store_id = null) {
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("SELECT * FROM `products` LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`) WHERE `p2s`.`store_id` = ? AND `p2s`.`status` = ?");
		$statement->execute(array($store_id, 0));

		return $statement->rowCount();
	}

	public function getProductByInfoId($info_id, $customer_id = '', $invoice_id = '') {
		$query = $this->db->prepare("SELECT * FROM `selling_info` WHERE `info_id` = ?");
		$query->execute(array($info_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	public function getProductBySellItemId($sell_item_id, $customer_id = '', $invoice_id = '') {
		$query = $this->db->prepare("SELECT * FROM `selling_item` WHERE `id` = ?");
		$query->execute(array($sell_item_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function editRefundProduct($data) {

		$sell_item_id = $data['sell_item_id'];
		$restock_to_inventory = $data['restock_to_inventory'];

		if ($sell_item_id) {
			$get_selling_info = $this->db->prepare("SELECT * FROM `selling_item` WHERE id = ?");
			$get_selling_info->execute(array($sell_item_id));

			$result = $get_selling_info->fetch(PDO::FETCH_ASSOC);
			$quantity = $data['quantity'];
			$total_amount = $result['item_total'] - $result['item_discount'];
			$item_per_price = $total_amount / $data['total_qty'];

			$credit_amount = $item_per_price * $data['quantity'];

			//$credit_amount = ($result['item_total'])-$result['item_discount'];
			$buying_invoice_id = $result['buying_invoice_id'];

			$data['product_id'] = $result['item_id'];
			$data['invoice_id'] = $result['invoice_id'];
			$store_id = $data['store_id'];

			$this->updateSellingInfo($data['customer_id'],$credit_amount);
			//$this->addRefundInfo($data);
			$this->updateCustomerCredit($data['customer_id'], $credit_amount);

			$this->updateBuyingItem($buying_invoice_id, $restock_to_inventory, $result['item_id'], $store_id, $quantity);

			$statement = $this->db->prepare("UPDATE selling_item SET returned = 1 WHERE id = ? ");
			$statement->execute(array($data['sell_item_id']));

			// $selling_info = $this->db->prepare("DELETE FROM `selling_info` WHERE `invoice_id` = ?");
			// $selling_info->execute(array($invoice_id));

			// $selling_item = $this->db->prepare("DELETE FROM `selling_item` WHERE `invoice_id` = ?");
			// $selling_item->execute(array($invoice_id));

			// $selling_price = $this->db->prepare("DELETE FROM `selling_price` WHERE `invoice_id` = ?");
			// $selling_price->execute(array($invoice_id));

			return $sell_item_id;
		}
		return $sell_item_id;
	}

	public function updateBuyingItem($buying_invoice_id, $restock_to_inventory, $product_id = false, $store_id = false, $quantity = 1) {

		$statement = $this->db->prepare("UPDATE buying_item SET total_sell = total_sell - $quantity 
													WHERE invoice_id = ? ");
		$statement->execute(array($buying_invoice_id));


		if ($restock_to_inventory) {
			$statement = $this->db->prepare("UPDATE buying_item SET
													total_sell = total_sell - $quantity ,
													defected_quantity = defected_quantity + $quantity
													WHERE invoice_id = ? ");
			return $statement->execute(array($buying_invoice_id));
		} else {

			$statement = $this->db->prepare("UPDATE buying_item SET status = 'active'
													WHERE invoice_id = ? ");
			$statement->execute(array($buying_invoice_id));

			if ($product_id) {
				$statement = $this->db->prepare("UPDATE product_to_store SET
														quantity_in_stock = quantity_in_stock + $quantity
														WHERE store_id = ? AND product_id = ?");
				return $statement->execute(array($store_id, $product_id));
			}

			$statement = $this->db->prepare("UPDATE buying_item SET
														total_sell = total_sell - $quantity
														WHERE invoice_id = ? ");
			return $statement->execute(array($buying_invoice_id));
		}
	}

	public function updateCustomerCredit($customer_id, $totalAmt) {
		$totalAmt = $totalAmt;

		$store_id = store_id();
		$get_cus_due = $this->db->prepare("SELECT due_amount FROM customer_to_store WHERE customer_id = ? AND store_id = ?");
		$get_cus_due->execute(array($customer_id, $store_id));
		$customer_due = $get_cus_due->fetch(PDO::FETCH_ASSOC);
		if ($customer_due['due_amount'] > 0) {

			if ($totalAmt >= $customer_due['due_amount']) {
				$due_amount = 0;
				$reamin_amount = $totalAmt - $customer_due['due_amount'];
				$totalAmt = $reamin_amount;
			} else {

				$reamin_amount = $customer_due['due_amount'] - $totalAmt;
				$due_amount = $reamin_amount;
				$totalAmt = 0;
			}

			$statement = $this->db->prepare("UPDATE customer_to_store
								   SET
									due_amount = " . $due_amount . "
									WHERE customer_id = ? AND store_id = ? ");
			$statement->execute(array($customer_id, $store_id));
		}

		$get_cus = $this->db->prepare("SELECT credit_balance FROM customers WHERE customer_id = ?");
		$get_cus->execute(array($customer_id));
		$customer = $get_cus->fetch(PDO::FETCH_ASSOC);

		if ($customer['credit_balance']) {
			$credit_balance = 'credit_balance + ' . $totalAmt;
		} else {
			$credit_balance = $totalAmt;
		}

		$statement = $this->db->prepare("UPDATE customers
								   SET
									credit_balance = " . $credit_balance . "
									WHERE customer_id = ? ");
		return $statement->execute(array($customer_id));
	}

	public function generateInvoiceId($type = 'sell') {
		$invoice_init_prefix = [
			'sell' => '',
			'due_paid' => 'F',
			'expense' => 'E',
		];
		
		$storeId = store_id();
		$invoice_id = $invoice_init_prefix[$type] . $storeId . $_SESSION['id'] . time();

		$query = $this->db->prepare("SELECT invoice_id FROM selling_info WHERE invoice_id = ?");
		$query->execute(array($invoice_id));
		$invId = $query->fetch(PDO::FETCH_ASSOC);

		if ($invId) {
			$this->generateInvoiceId($type);
		}
		return $invoice_id;
	}

	public function updateSellingInfo($customer_id, $duePaidAmount){

		$paidInvId = $this->generateInvoiceId('due_paid');
		$store_id = store_id();
		$get_cus_due = $this->db->prepare("SELECT due_amount FROM customer_to_store WHERE customer_id = ? AND store_id = ?");
		$get_cus_due->execute(array($customer_id, $store_id));
		$customer_due = $get_cus_due->fetch(PDO::FETCH_ASSOC);
		$prevDue = $customer_due['due_amount'];
		$totalAmt = $duePaidAmount;
		if ($totalAmt >= $customer_due['due_amount']) {
				$due_amount = 0;
				$reamin_amount = $totalAmt - $customer_due['due_amount'];
				$totalAmt = $reamin_amount;
			} else {

				$reamin_amount = $customer_due['due_amount'] - $totalAmt;
				$due_amount = $reamin_amount;
				$totalAmt = 0;
			}

		$statement = $this->db->prepare("INSERT INTO `selling_info` (invoice_id, store_id, inv_type, customer_id, currency_code,payment_method,created_by) VALUES (?, ?, ?, ?, ?,?,?)");
    	$statement->execute(array($paidInvId, store_id(), 'due_paid', $customer_id, 'INR', '0', $_SESSION['id']));

    	$statement = $this->db->prepare("INSERT INTO `selling_price` (invoice_id, store_id, previous_due, paid_amount, todays_due,present_due) VALUES (?, ?, ?, ?, ?,?)");
    	$statement->execute(array($paidInvId, store_id(), $prevDue, $duePaidAmount, 0, $due_amount));

    	$statement = $this->db->prepare("INSERT INTO `selling_item` (invoice_id, store_id, item_id, item_name, item_price,item_quantity,item_total) VALUES (?, ?, ?, ?, ?,?,?)");
    	$statement->execute(array($paidInvId, store_id(),0, 'Deposit',$duePaidAmount,1,$duePaidAmount));
	
	}

	public function addRefundInfo($data) {
		/// info_id as selling_item table id
		$data['quantity'] = $data['quantity'];
		$statement = $this->db->prepare("INSERT INTO return_product_info
											SET
												quantity    = ? ,
												invoice_id  = ? ,
												product_id  = ? ,
												info_id     = ? ,
												customer_id = ? ,
												user_id     = ? ,
												store_id    = ? ,
												comments    = ?,
												image    = ?
											");
		return $statement->execute(array(
			$data['quantity'],
			$data['invoice_id'],
			$data['product_id'],
			$data['sell_item_id'],
			$data['customer_id'],
			$_SESSION['id'],
			$_SESSION['store_id'],
			$data['comments'],
			$data['p_image'],
		));
	}
}