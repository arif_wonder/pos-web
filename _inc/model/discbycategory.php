<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
class ModelDiscByCategory extends Model 
{
	public function addDiscByCategory($data) 
	{
    	$statement = $this->db->prepare("INSERT 
											 disc_by_category
											SET 
												title = ?, 
												description = ?,
												category_id = ?,
												date_from = ?,
												date_to = ?,
												discount = ?,
												discount_type = ?,
												min_purchase_amount = ?,
												max_discount = ?
										");
    	
    	$statement->execute(array(
    		$data['title'] ? : null, 
    		$data['description'] ? : null,  
    		$data['category_id'] ? : null, 
    		$data['date_from'] ? date('Y-m-d',strtotime($data['date_from'])) : null, 
    		$data['date_to'] ? date('Y-m-d',strtotime($data['date_to'])) : null, 
    		$data['discount'] ? : null,
    		$data['discount_type'] ? : 'fixed',
    		$data['min_purchase_amount'] ? : null,
    		$data['max_discount'] ? : null,
    	));

    	$insertedId = $this->db->lastInsertId();

    	return $insertedId;
	}

	public function editDiscByCategory($disc_by_category_id, $data) 
	{
		// update product infomation
    	$statement = $this->db->prepare("UPDATE 
											disc_by_category
											SET 
												title = ?, 
												description = ?,
												category_id = ?,
												date_from = ?,
												date_to = ?,
												discount = ?,
												discount_type = ?,
												min_purchase_amount = ?,
												max_discount = ?
								    		WHERE id = ?
							    		");
    	$statement->execute(array(
    		$data['title'] ? : null, 
    		$data['description'] ? : null,  
    		$data['category_id'] ? : null, 
    		$data['date_from'] ? date('Y-m-d',strtotime($data['date_from'])) : null, 
    		$data['date_to'] ? date('Y-m-d',strtotime($data['date_to'])) : null, 
    		$data['discount'] ? : null,
    		$data['discount_type'] ? : 'fixed',
    		$data['min_purchase_amount'] ? : null,
    		$data['max_discount'] ? : null,
    		$disc_by_category_id
    	));
    	return $disc_by_category_id;
	}

	public function deleteDiscByCategory($disc_by_category_id) 
	{
		$statement = $this->db->prepare("DELETE FROM `disc_by_category` WHERE `id` = ? LIMIT 1");
        $statement->execute(array($disc_by_category_id));

        return $disc_by_category_id;
	}

	public function getDiscByCategory($id) 
	{
		$statement = $this->db->prepare("SELECT * FROM `disc_by_category` 
			WHERE id = ?");
	    $statement->execute(array($id));
	    $result = $statement->fetch(PDO::FETCH_ASSOC);
	    return $result;
	}

	

}