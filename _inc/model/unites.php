<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
class ModelUnites extends Model {

	public function addUnites($data) {
		$statement = $this->db->prepare("INSERT unites
											SET 
									    		title = ?, 
									    		description = ?
									    	");
    	
    	$statement->execute(array(
    		$data['title'] ? : null, 
    		$data['description'] ? : null 
    	));

    	$insetedId = $this->db->lastInsertId();

    	return $insetedId;
	}

	public function editUnites($unit_id, $data) {
		$statement = $this->db->prepare("UPDATE unites 
								    		SET 
									    		title = ?, 
									    		description = ?
								    		WHERE unit_id = ?
							    		");
    	$statement->execute(array(
								$data['title'] ? : null, 
								$data['description'] ? : null, 
					    		$unit_id
					    	));
    	return $unit_id;
	}

	public function deleteUnites($unit_id) {

		$statement = $this->db->prepare("DELETE FROM unites WHERE unit_id = ? LIMIT 1");
        $statement->execute(array($unit_id));

        return $unit_id;
	}

	public function getUnites($unit_id) {
		$statement = $this->db->prepare("SELECT * FROM unites WHERE unit_id = ?");
		$statement->execute(array($unit_id));
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result;
	}
	public function getAllUnites(){
		$query = $this->db->prepare("SELECT * FROM `unites` ");
	    $query->execute();

    	$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}