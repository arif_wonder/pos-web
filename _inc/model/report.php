<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
class ModelReport extends Model 
{
	public function getSellingPrice($from, $to, $store_id = null) 
	{
		$invoice_price = 0;
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`selling_info`.`inv_type` != 'due_paid' AND `selling_info`.`store_id` = " . $store_id;
		$where_query .= date_range_filter($from, $to);

		$statement = $this->db->prepare("SELECT SUM(`selling_price`.`previous_due`) as previous_due, SUM(`selling_price`.`payable_amount`) as total FROM `selling_info` 
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
			WHERE $where_query");

		$statement->execute();
		$invoice = $statement->fetch(PDO::FETCH_ASSOC);

		if ($invoice) {
			$invoice_price = $invoice['total'] - $invoice['previous_due'];
		}
		return $invoice_price;
	}

	public function getSellingPriceDaywise($year, $month = null, $day = null, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();
		$where_query = "`selling_info`.`inv_type` != 'due_paid' AND `selling_info`.`store_id` = " . $store_id;
		if ($day) {
		  $where_query .= " AND DAY(`selling_info`.`created_at`) = $day";
		}
		if ($month) {
		  $where_query .= " AND MONTH(`selling_info`.`created_at`) = $month";
		}
		if ($year) {
		  $where_query .= " AND YEAR(`selling_info`.`created_at`) = $year";
		}
		$statement = $this->db->prepare("SELECT SUM(`selling_price`.`discount_amount`) as discount, SUM(`selling_price`.`tax_amount`) as tax, SUM(`selling_price`.`payable_amount`) as total FROM `selling_info` 
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
			WHERE $where_query");
		$statement->execute();
		$invoice = $statement->fetch(PDO::FETCH_ASSOC);

		return $invoice['total'] + $invoice['discount'] + $invoice['tax'];
	}

	public function getNetSellingPriceDaywise($year, $month = null, $day = null, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();
		$where_query = "`selling_info`.`inv_type` != 'due_paid' AND `selling_info`.`store_id` = " . $store_id;
		if ($day) {
		  $where_query .= " AND DAY(`selling_info`.`created_at`) = $day";
		}
		if ($month) {
		  $where_query .= " AND MONTH(`selling_info`.`created_at`) = $month";
		}
		if ($year) {
		  $where_query .= " AND YEAR(`selling_info`.`created_at`) = $year";
		}
		$statement = $this->db->prepare("SELECT SUM(`selling_price`.`discount_amount`) as discount, SUM(`selling_price`.`tax_amount`) as tax, SUM(`selling_price`.`payable_amount`) as total FROM `selling_info` 
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
			WHERE $where_query");
		$statement->execute();
		$invoice = $statement->fetch(PDO::FETCH_ASSOC);

		return $invoice['total'] - $invoice['tax'];
	}

	public function getReceivedAmountDaywise($year, $month = null, $day = null, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`selling_info`.`store_id` = " . $store_id;
		if ($day) {
		  $where_query .= " AND DAY(selling_info.created_at) = $day";
		}
		if ($month) {
		  $where_query .= " AND MONTH(selling_info.created_at) = $month";
		}
		if ($year) {
		  $where_query .= " AND YEAR(selling_info.created_at) = $year";
		}

		$statement = $this->db->prepare("SELECT SUM(`selling_price`.`paid_amount`) as total, SUM(`selling_price`.`tax_amount`) as tax FROM `selling_info` 
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
			WHERE $where_query");
		$statement->execute();
		$paid_amount = $statement->fetch(PDO::FETCH_ASSOC);

		return $paid_amount['total'];
	}

	public function getBuyingPriceOfSellDaywise($year, $month, $day, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`selling_info`.`inv_type` != 'due_paid' AND `selling_info`.`store_id` = " . $store_id;
		if ($day) {
		  $where_query .= " AND DAY(selling_info.created_at) = $day";
		}
		if ($month) {
		  $where_query .= " AND MONTH(selling_info.created_at) = $month";
		}
		if ($year) {
		  $where_query .= " AND YEAR(selling_info.created_at) = $year";
		}

		$statement = $this->db->prepare("SELECT SUM(`selling_item`.`total_buying_price`) as total FROM `selling_info` 
			LEFT JOIN `selling_item` ON `selling_info`.`invoice_id` = `selling_item`.`invoice_id`
			WHERE $where_query");

		$statement->execute();

		return $statement->fetch(PDO::FETCH_ASSOC)['total'];
	}

	public function getProfitAmountDaywise($year, $month, $day) 
	{
		return $this->getNetSellingPriceDaywise($year, $month, $day)  - $this->getBuyingPriceOfSellDaywise($year, $month, $day);
	}

	public function getReceivedAmount($from, $to, $store_id = null) 
	{
		return ($this->getSellingPrice($from, $to, $store_id) - $this->getDueAmount($from, $to, $store_id)) + $this->getDuePaidAmount($from, $to, $store_id);
	}

	public function getProfitAmount($from, $to, $store_id = null) 
	{
		return ($this->getSellingPrice($from, $to, $store_id) - $this->getBuyingPriceOfSell($from, $to, $store_id)) - $this->getTaxAmount($from, $to, $store_id);
	}

	public function getBuyingPriceOfSell($from, $to, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`selling_info`.`inv_type` != 'due_paid' AND `selling_info`.`store_id` = " . $store_id;
		$where_query .= date_range_filter($from, $to);

		$statement = $this->db->prepare("SELECT SUM(`selling_item`.`total_buying_price`) as total FROM `selling_info` 
			LEFT JOIN `selling_item` ON `selling_info`.`invoice_id` = `selling_item`.`invoice_id`
			WHERE $where_query");

		$statement->execute();

		return $statement->fetch(PDO::FETCH_ASSOC)['total'];
	}

	public function getTaxAmount($from, $to, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`selling_info`.`inv_type` != 'due_paid' AND `selling_info`.`store_id` = " . $store_id;
		$where_query .= date_range_filter($from, $to);

		$statement = $this->db->prepare("SELECT SUM(`selling_price`.`tax_amount`) as total FROM `selling_info` 
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
			WHERE $where_query");
		$statement->execute();
		$tax_amount = $statement->fetch(PDO::FETCH_ASSOC);

		return $tax_amount['total'];
	}

	public function getTaxAmountDaywise($year, $month = null, $day = null, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`selling_info`.`inv_type` != 'due_paid' AND `selling_info`.`store_id` = " . $store_id;
		if ($day) {
		  $where_query .= " AND DAY(`selling_info`.`created_at`) = $day";
		}
		if ($month) {
		  $where_query .= " AND MONTH(`selling_info`.`created_at`) = $month";
		}
		if ($year) {
		  $where_query .= " AND YEAR(`selling_info`.`created_at`) = $year";
		}

		$statement = $this->db->prepare("SELECT SUM(`selling_price`.`tax_amount`) as total FROM `selling_info` 
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
			WHERE $where_query");
		$statement->execute();
		$tax_amount = $statement->fetch(PDO::FETCH_ASSOC);

		return $tax_amount['total'];
	}

	public function getDiscountAmount($from, $to, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`selling_info`.`inv_type` != 'due_paid' AND `selling_info`.`store_id` = " . $store_id;
		$where_query .= date_range_filter($from, $to);

		$statement = $this->db->prepare("SELECT SUM(`selling_price`.`discount_amount`) as total FROM `selling_info` 
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
			WHERE $where_query");
		$statement->execute();
		$discount_amount = $statement->fetch(PDO::FETCH_ASSOC);

		return $discount_amount['total'];
	}

	public function getDueAmount($from, $to, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`selling_info`.`inv_type` != 'due_paid' AND `selling_info`.`store_id` = " . $store_id;
		$where_query .= date_range_filter($from, $to);
		$statement = $this->db->prepare("SELECT SUM(`selling_price`.`todays_due`) as due FROM `selling_info` 
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`)
			WHERE $where_query");
		$statement->execute();
		$invoice = $statement->fetch(PDO::FETCH_ASSOC);

		return $invoice['due'];
	}

	public function getBuyingPrice($from, $to, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`buying_info`.`inv_type` != 'expense' AND `buying_info`.`store_id` = " . $store_id;
		$where_query .= date_range_filter2($from, $to);

		$statement = $this->db->prepare("SELECT SUM(`buying_price`.`paid_amount`) as total FROM `buying_info` 
			LEFT JOIN `buying_price` ON (`buying_info`.`invoice_id` = `buying_price`.`invoice_id`) 
			WHERE $where_query");
		$statement->execute();
		$buying_price = $statement->fetch(PDO::FETCH_ASSOC);

		return $buying_price['total'];
	}

	public function getExpenseAmount($from, $to, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`buying_info`.`inv_type` = 'expense' AND `buying_info`.`store_id` = " . $store_id;
		$where_query .= date_range_filter2($from, $to);

		$statement = $this->db->prepare("SELECT SUM(`buying_price`.`paid_amount`) as total FROM `buying_info` 
			LEFT JOIN `buying_price` ON (`buying_info`.`invoice_id` = `buying_price`.`invoice_id`) 
			WHERE $where_query");
		$statement->execute();
		$buying_price = $statement->fetch(PDO::FETCH_ASSOC);

		return $buying_price['total'];
	}

	public function getDuePaidAmount($from, $to, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();
		$where_query = "`selling_info`.`inv_type` = 'due_paid' AND `selling_info`.`store_id` = " . $store_id;
		$where_query .= date_range_filter($from, $to);
		$statement = $this->db->prepare("SELECT SUM(`selling_price`.`paid_amount`) as due_paid FROM `selling_info` 
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`)
			WHERE $where_query");
		$statement->execute();
		$invoice = $statement->fetch(PDO::FETCH_ASSOC);
		return $invoice['due_paid'];
	}

	public function getTopProduct($from, $to, $limit = 3, $store_id = null)
	{
		$store_id = $store_id ? $store_id : store_id();

		$where_query = "`selling_info`.`inv_type` != 'due_paid' AND `selling_info`.`store_id` = " . $store_id;
		$where_query .= date_range_filter($from, $to);

		$statement = $this->db->prepare("SELECT `selling_info`.*, `selling_item`.`item_name`, SUM(`selling_item`.`item_quantity`) as quantity FROM `selling_info` 
			LEFT JOIN `selling_item` ON (`selling_info`.`invoice_id` = `selling_item`.`invoice_id`)
			WHERE $where_query
			GROUP BY `selling_item`.`item_id` ORDER BY `quantity` 
			DESC LIMIT $limit");
		$statement->execute(array());

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function totalOutOfStock($min = 0, $store_id = null)
	{
		$store_id = $store_id ? $store_id : store_id();

		$statement =  $this->db->prepare("SELECT * FROM `products` 
			LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`) 
			WHERE `p2s`.`store_id` = ? AND `p2s`.`quantity_in_stock` <= ? AND `p2s`.`status` = 1");
		$statement->execute(array($store_id, $min));
		
		return $statement->rowCount();
	}

	public function totalExpired($store_id = null) {
		$store_id = $store_id ? $store_id : store_id();

		$statement =  $this->db->prepare("SELECT * FROM `products` LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`) WHERE `p2s`.`store_id` = ? AND `e_date` <= CURDATE() AND `p2s`.`status` = 1");
		$statement->execute(array($store_id));
		
		return $statement->rowCount();
	}

	public function totalWillBeExpiredProduct($store_id = null) {
		$store_id = $store_id ? $store_id : store_id();

		$statement =  $this->db->prepare("SELECT
											    p2s.*
											FROM
											    product_to_store AS p2s
											JOIN products AS pro
											ON
											    pro.p_id = p2s.product_id
											WHERE
											    p2s.store_id = ? AND p2s.quantity_in_stock <=(
											    SELECT
											        sub_pro.p_alert_qty
											    FROM
											        products AS sub_pro
											    WHERE
											        sub_pro.p_alert_qty != '' AND sub_pro.p_id = pro.p_id
											)
										");
		$statement->execute(array($store_id));
		
		return $statement->rowCount();
	}

	public function getTotalCashReceivedBy($type, $type_id, $from = null, $to = null, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();
		$where_query = "`selling_info`.`store_id` = ? AND `selling_info`.`is_visible` = 1";
		$where_query .= date_range_filter($from, $to);
		switch ($type) {
			case 'userwise':
				$where_query .= " AND `selling_info`.`inv_type` = 'sell' AND `selling_info`.`created_by` = $type_id";
				$statement = $this->db->prepare("SELECT SUM(`selling_price`.`paid_amount`) as total FROM `selling_info` 
					LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
					WHERE $where_query");
				$statement->execute(array($store_id));
				$invoice = $statement->fetch(PDO::FETCH_ASSOC);
				$total = isset($invoice['total']) ? (float)$invoice['total'] : 0;
				break;
			case 'invoicewise':
				$where_query .= " AND `selling_info`.`inv_type` = 'sell' AND `selling_info`.`invoice_id` = '$type_id'";
				$statement = $this->db->prepare("SELECT SUM(`selling_price`.`paid_amount`) as total FROM `selling_info` 
					LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
					WHERE $where_query");
				$statement->execute(array($store_id));
				$invoice = $statement->fetch(PDO::FETCH_ASSOC);
				$total = isset($invoice['total']) ? (float)$invoice['total'] : 0;
				break;
		}	
		return $total;
	}

	public function getTotalDueAmountBy($type, $type_id, $from = null, $to = null, $store_id = null) 
	{
		$edited_invoice_amount = 0;
		$store_id = $store_id ? $store_id : store_id();
		$where_query = "`selling_info`.`store_id` = ? AND `selling_info`.`is_visible` = 1";
		$where_query .= date_range_filter($from, $to);
		if ($this->current_state) {
			$edited_invoice_amount = $this->getTotalEditedDueAmountBy($type, $type_id, $from, $to, $store_id);
			$where_query .= " AND `edit_count` = 0";
		}
		switch ($type) {
			case 'invoicewise':
				$where_query .= " AND `selling_info`.`invoice_id` = '$type_id'";
				$statement = $this->db->prepare("SELECT SUM(`selling_price`.`present_due`) as total FROM `selling_info` 
					LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
					WHERE $where_query");
				$statement->execute(array($store_id));
				$invoice = $statement->fetch(PDO::FETCH_ASSOC);
				$total = isset($invoice['total']) ? (float)$invoice['total'] : 0;
				break;
			case 'userwise':
				$where_query .= " AND `inv_type` = 'sell' AND `selling_info`.`created_by` = $type_id";
				$statement = $this->db->prepare("SELECT SUM(`selling_price`.`present_due`) as total FROM `selling_info` 
					LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
					WHERE $where_query");
				$statement->execute(array($store_id));
				$invoice = $statement->fetch(PDO::FETCH_ASSOC);
				$total = isset($invoice['total']) ? (float)$invoice['total'] : 0;
				break;
		}	
		return $total + $edited_invoice_amount;
	}

	public function getTotalDueCollectionBy($user_id, $from = null, $to = null) 
	{
		$total = 0;
		$from = $from ? $from : date('Y-m-d');
		$to = $to ? $to : date('Y-m-d');
		$where_query = "`selling_info`.`inv_type`='due_paid' AND `selling_info`.`created_by` = ? AND `selling_info`.`is_visible` = ?";
		$where_query .= date_range_filter($from, $to);
		$statement = $this->db->prepare("SELECT SUM(`selling_price`.`paid_amount`) as total, `created_at` FROM `selling_info` 
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
			WHERE $where_query");
		$statement->execute(array($user_id, 1));
		$invoice = $statement->fetch(PDO::FETCH_ASSOC);
		return $invoice['total'];
	}

	public function getTotalTaxAmountBy($type, $type_id, $from = null, $to = null, $store_id = null) 
	{
		$total = 0;
		$store_id = $store_id ? $store_id : store_id();
		$where_query = "`selling_info`.`inv_type` = 'sell' AND `selling_info`.`store_id` = ? AND `selling_info`.`is_visible` = 1";
		$where_query .= date_range_filter($from, $to);
		switch ($type) {
			case 'invoicewise':
				$where_query .= " AND `selling_info`.`invoice_id` = '$type_id'";
				$statement = $this->db->prepare("SELECT `selling_info`.*, `selling_price`.`tax_amount` as total 
				FROM `selling_info` 
				LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
				WHERE $where_query");
				break;
			case 'userwise':
				$where_query .= " AND `selling_info`.`created_by` = $type_id";
				$statement = $this->db->prepare("SELECT `selling_info`.*, `selling_price`.`tax_amount` as total 
				FROM `selling_info` 
				LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
				WHERE $where_query");
				break;		
		}
		$statement->execute(array($store_id));
		$invoices = $statement->fetchAll(PDO::FETCH_ASSOC);
		foreach ($invoices as $inv) {
			$total += $inv['total'];
		}
		return $total;
	}

	public function getTotalDiscountAmountBy($type, $type_id, $from = null, $to = null, $store_id = null) 
	{
		$total = 0;
		$store_id = $store_id ? $store_id : store_id();
		$where_query = "`selling_info`.`inv_type` = 'sell' AND `selling_info`.`store_id` = ? AND `selling_info`.`is_visible` = 1";
		$where_query .= date_range_filter($from, $to);
		switch ($type) {
			case 'invoicewise':
				$where_query .= " AND `selling_info`.`invoice_id` = '$type_id'";
				$statement = $this->db->prepare("SELECT `selling_info`.*, `selling_price`.`discount_amount` as total 
				FROM `selling_info` 
				LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
				WHERE $where_query");
				break;
			case 'userwise':
				$where_query .= " AND `selling_info`.`created_by` = $type_id";
				$statement = $this->db->prepare("SELECT `selling_info`.*, `selling_price`.`discount_amount` as total 
				FROM `selling_info` 
				LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
				WHERE $where_query");
				break;			
		}
		$statement->execute(array($store_id));
		$invoices = $statement->fetchAll(PDO::FETCH_ASSOC);
		foreach ($invoices as $inv) {
			$total += $inv['total'];
		}
		return $total;
	}

	public function getTotalInvoiceAmountBy($type, $type_id, $from = null, $to = null, $store_id = null) 
	{
		$total = 0;
		$store_id = $store_id ? $store_id : store_id();
		$where_query = "`selling_info`.`store_id` = ? AND `selling_info`.`is_visible` = 1";
		if($from && $to){
			$where_query .= date_range_filter($from, $to);
		}
		
		switch ($type) {
			case 'invoicewise':
				$where_query .= " AND `inv_type` = 'sell' AND `selling_info`.`invoice_id` = '$type_id'";
				$statement = $this->db->prepare("SELECT SUM(`selling_price`.`subtotal`) as total FROM `selling_info` 
					LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
					WHERE $where_query");
				$statement->execute(array($store_id));
				$invoice = $statement->fetch(PDO::FETCH_ASSOC);
				$total = isset($invoice['total']) ? (float)$invoice['total'] : 0;
				break;
			case 'userwise':
				$where_query .= " AND `inv_type` = 'sell' AND `selling_info`.`created_by` = $type_id";
				$statement = $this->db->prepare("SELECT SUM(`selling_price`.`subtotal`) as total FROM `selling_info` 
					LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
					WHERE $where_query");
				$statement->execute(array($store_id));
				$invoice = $statement->fetch(PDO::FETCH_ASSOC);
				$total = isset($invoice['total']) ? (float)$invoice['total'] : 0;
				break;
		}	
		return $total;
	}

	public function userTotalInvoiceCount($user_id = null, $from = null, $to = null, $store_id = null)
	{
		$store_id = $store_id ? $store_id : store_id();
		$where_query = "`selling_info`.`store_id` = ? AND `selling_info`.`is_visible` = 1 AND `created_by` = $user_id AND `inv_type` = 'sell'";
		$where_query .= date_range_filter($from, $to);
		$statement = $this->db->prepare("SELECT * FROM `selling_info` WHERE $where_query");
		$statement->execute(array($store_id));
		return $statement->rowCount();
	}
}