<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
class ModelTaxRates extends Model {

	public function addTaxRates($data) {
		$statement = $this->db->prepare("INSERT tax_rates
											SET 
									    		title = ?, 
									    		description = ?, 
									    		rate = ?
									    	");
    	
    	$statement->execute(array(
    		$data['title'] ? : null, 
    		$data['description'] ? : null, 
    		$data['rate'] ? : 0.00 
    	));

    	$product_id = $this->db->lastInsertId();

    	return $product_id;
	}

	public function editTaxRates($tax_rate_id, $data) {
		$statement = $this->db->prepare("UPDATE tax_rates 
								    		SET 
									    		title = ?, 
									    		description = ?, 
									    		rate = ?
								    		WHERE tax_rate_id = ?
							    		");
    	$statement->execute(array(
								$data['title'] ? : null, 
								$data['description'] ? : null, 
								$data['rate'] ? : null,  
					    		$tax_rate_id
					    	));
    	return $tax_rate_id;
	}

	public function deleteTaxRates($tax_rate_id) {

		$statement = $this->db->prepare("DELETE FROM tax_rates WHERE tax_rate_id = ? LIMIT 1");
        $statement->execute(array($tax_rate_id));

        return $tax_rate_id;
	}

	public function getTaxRates($tax_rate_id) {
		$statement = $this->db->prepare("SELECT * FROM tax_rates WHERE tax_rate_id = ?");
		$statement->execute(array($tax_rate_id));
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $result;
	}
	public function getAllTaxRates(){
		$query = $this->db->prepare("SELECT * FROM `tax_rates` ");
	    $query->execute();

    	$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}