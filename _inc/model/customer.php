<?php
/* 
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
class ModelCustomer extends Model 
{
	public function addCustomer($data) 
	{
    	$statement = $this->db->prepare("INSERT INTO `customers` (
    		customer_name, 
    		customer_email,
    		mobile_cc,
    		customer_mobile, 
    		customer_sex, 
    		customer_age,  
    		created_at,
			anniversary_date,
			occupation,
			dob,
			marital_status,
			alt_contact_number,
			passport_number,
			dl_number,
			pan_number,
			aadhar_number
    	) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)");

    	$data['anniversary_date'] = $data['anniversary_date'] ? date('Y-m-d',strtotime($data['anniversary_date'])) : '';
    	$data['dob'] = '';
    	if($data['date']){
    		$data['dob'] = $data['date'];
    	}
    	if($data['month']){
    		$data['dob'] = $data['dob'].':'.$data['month'];
    	}
    	if($data['year']){
    		$data['dob'] = $data['dob'].':'.$data['year'];
    	}
    	

    	$statement->execute(array(
    		$data['customer_name'] ? : null, 
    		$data['customer_email'] ? : null,
    		$data['mobile_cc'] ? : null,
    		$data['customer_mobile'] ? : null, 
    		$data['customer_sex'] ? : null, 
    		$data['customer_age'] ? : null, 
    		date_time(),
    		$data['anniversary_date']? : null,
    		$data['occupation']? : null,
    		$data['dob']? : null,
    		$data['marital_status']? : null,
    		$data['alt_contact_number']? : null,
    		$data['passport_number']? : null,
    		$data['dl_number']? : null,
    		$data['pan_number']? : null,
    		$data['aadhar_number']? : null
    	));

    	$customer_id = $this->db->lastInsertId();
    	$user_type = 3;
    	// insert bank account details
    	$users_bank = $this->db->prepare("INSERT INTO `users_bank` (
					    		bank_name, 
					    		bank_acc_number, 
					    		ifsc_code, 
					    		user_id, 
					    		user_type
				    		) VALUES (?, ?, ?, ?, ?)"
			    		);
    	$users_bank->execute(array(
			    		$data['bank_name']? : null, 
			    		$data['bank_acc_number']? : null, 
			    		$data['ifsc_code']? : null, 
			    		$customer_id, 
			    		$user_type
			    	));

		//foreach (get_stores() as $the_store) {
			$statement = $this->db->prepare("INSERT INTO `customer_to_store` SET `customer_id` = ?, `store_id` = ?");
			$statement->execute(array((int)$customer_id, (int)store_id()));
		//}

		$this->updateStatus($customer_id, $data['status']);
		//$this->updateSortOrder($customer_id, $data['sort_order']);


		// user details
    	$user_details = $this->db->prepare("INSERT INTO `user_details`(
										user_id, 
										user_type, 
										vehicle_number, 
										vehicle_model, 
										number_of_vehicles, 
										busi_name, 
										busi_address, 
										busi_contact_number, 
										busi_annual_turnover, 
										busi_trans_to_office, 
										ser_man_designation, 
										ser_man_comp_name, 
										ser_man_comp_address, 
										ser_man_comp_cont_number, 
										ser_man_trans_to_office, 
										perma_address, 
										perma_city, 
										perma_state, 
										perma_zip_code, 
										cor_address, 
										cor_city, 
										cor_state, 
										cor_zip_code
				    	) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    	$user_details->execute(array(
						$customer_id, 
						$user_type, 
						$data['vehicle_number']? : null,
						$data['vehicle_model']? : null,
						$data['number_of_vehicles']? : null,
						$data['busi_name']? : null,
						$data['busi_address']? : null,
						$data['busi_contact_number']? : null,
						$data['busi_annual_turnover']? : null,
						$data['busi_trans_to_office']? : null,
						$data['ser_man_designation']? : null,
						$data['ser_man_comp_name']? : null,
						$data['ser_man_comp_address']? : null,
						$data['ser_man_comp_cont_number']? : null,
						$data['ser_man_trans_to_office']? : null,
						$data['perma_address']? : null,
						$data['perma_city']? : null,
						$data['perma_state']? : null,
						$data['perma_zip_code']? : null,
						$data['cor_address']? : null,
						$data['cor_city']? : null,
						$data['cor_state']? : null,
						$data['cor_zip_code']? : null
			    	));
    	$this->family_members($customer_id,3,$data);
    	return $customer_id;    
	}


	public function updateStatus($customer_id, $status, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("UPDATE `customer_to_store` SET `status` = ? WHERE `store_id` = ? AND `customer_id` = ?");
		$statement->execute(array((int)$status, $store_id, (int)$customer_id));
	}

	public function updateSortOrder($customer_id, $sort_order, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("UPDATE `customer_to_store` SET `sort_order` = ? WHERE `store_id` = ? AND `customer_id` = ?");
		$statement->execute(array((int)$sort_order, $store_id, (int)$customer_id));
	}

	public function editCustomer($customer_id, $data) 
	{

		$dob = '';
    	if($data['date']){
    		$dob = $data['date'];
    	}
    	if($data['month']){
    		$dob = $dob.':'.$data['month'];
    	}
    	if($data['year']){
    		$dob = $dob.':'.$data['year'];
    	}

    	$statement = $this->db->prepare("UPDATE `customers` 
								    		SET 
												customer_name = ?, 
												customer_email = ?, 
												mobile_cc = ?,
												customer_mobile = ?, 
												customer_sex = ?, 
												customer_age = ?, 
												anniversary_date = ?,
												occupation = ?,
												dob = ?,
												marital_status = ?,
												alt_contact_number = ?,
												passport_number = ?,
												dl_number = ?,
												pan_number = ?,
												aadhar_number = ?
												WHERE `customer_id` = ? 
								    		");
    	$params = array(
    		$data['customer_name'] ? : null, 
    		$data['customer_email'] ? : null,
    		$data['mobile_cc'] ? : null, 
    		$data['customer_mobile'] ? : null, 
    		$data['customer_sex'] ? : null, 
    		$data['customer_age'] ? : null, 
    		$data['anniversary_date'] ? date('Y-m-d', strtotime($data['anniversary_date'])) : null,
    		$data['occupation'],
			$dob,
    		$data['marital_status'],
    		$data['alt_contact_number'] ? : null,
    		$data['passport_number'] ? : null,
    		$data['dl_number'] ? : null,
    		$data['pan_number'] ? : null,
    		$data['aadhar_number'] ? : null,
    		$customer_id
    	);
    	$statement->execute($params);
    	// print_r($params);

    	$this->updateStatus($customer_id, $data['status']);
    	//$this->updateSortOrder($customer_id, $data['sort_order']);
    	$this->users_bank($customer_id,3,$data,$data['bank_id']);
    	$this->user_details($customer_id,3,$data,$data['user_details_id']);
    	$this->family_members($customer_id,3,$data);
    	return $customer_id;
    
	}
	
	public function family_members($customer_id,$user_type,$data) {
		$member = sizeof( ($data['member_name']) ) ;
		if ($member) {
			$statement = $this->db->prepare("DELETE FROM `family_members` WHERE `user_id` = ? AND  `user_type` = 3");
	    	$statement->execute(array($customer_id));

			for ($i=0; $i < $member; $i++) { 
				if ($data['member_name'][$i]) {
		    		$users_bank = $this->db->prepare("INSERT INTO `family_members` (
							    		name, 
							    		relationship, 
							    		dob, 
							    		user_id, 
							    		user_type
						    		) VALUES (?, ?, ?, ?, ?)"
					    		);
					$users_bank->execute(array(
					    		$data['member_name'][$i]  ? : null, 
					    		$data['member_relationship'][$i]  ? : null, 
					    		$data['member_dob'][$i] ? date('Y-m-d',strtotime($data['member_dob'][$i])) : null, 
					    		$customer_id, 
					    		$user_type
					    	));
				}
	    	}
    	}
    	return true;
	}
	public function replaceWith($new_id, $id)
	{
		$statement = $this->db->prepare("UPDATE `customer_to_store` SET `customer_id` = ? WHERE `customer_id` = ?");
      	$statement->execute(array($new_id, $id));

      	$statement = $this->db->prepare("UPDATE `selling_info` SET `customer_id` = ? WHERE `customer_id` = ?");
      	$statement->execute(array($new_id, $id));
	}

	public function deleteCustomer($customer_id) 
	{
        $statement = $this->db->prepare("DELETE FROM `customers` WHERE `customer_id` = ? LIMIT 1");
    	$statement->execute(array($customer_id));

    	$statement = $this->db->prepare("DELETE FROM `customer_to_store` WHERE `customer_id` = ?");
    	$statement->execute(array($customer_id));

        return $customer_id;
	}

	public function getCustomer($customer_id, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

	    $statement = $this->db->prepare(
	    	"SELECT * ,
	    	u_bank.*,
	    	u_details.*
	    	FROM `customers` 
	    	LEFT JOIN user_details AS u_details ON u_details.user_id = customers.customer_id AND u_details.user_type = 3
	    	LEFT JOIN users_bank AS u_bank ON u_bank.user_id = customers.customer_id AND u_bank.user_type = 3
	    	LEFT JOIN `customer_to_store` c2s ON (`customers`.`customer_id` = `c2s`.`customer_id`)
	    	WHERE `customers`.`customer_id` = ? AND `store_id` = ?");

		$statement->execute(array($customer_id, $store_id));
		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function getCustomers($data = array(), $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$sql = "SELECT * FROM `customers` c LEFT JOIN `customer_to_store` c2s ON (`c`.`customer_id` = `c2s`.`customer_id`) WHERE `c2s`.`store_id` = ? AND `status` = ?";

		if (isset($data['filter_name'])) {
			$sql .= " AND `c`.`customer_name` LIKE '" . $data['filter_name'] . "%'";
		}

		if (isset($data['filter_email'])) {
			$sql .= " AND `c`.`customer_email` LIKE '" . $data['filter_email'] . "%'";
		}

		if (isset($data['filter_mobile'])) {
			$sql .= " AND `c`.`customer_mobile` LIKE '" . $data['filter_mobile'] . "%'";
		}

		$sql .= " GROUP BY `c`.`customer_id`";

		$sort_data = array(
			'customer_name',
			'customer_email',
			'customer_mobile'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY `c`.`customer_id`";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$statement = $this->db->prepare($sql);
		$statement->execute(array($store_id, 1));
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function total($store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();
		$where_query = '`c2s`.`store_id` = ' . $store_id;
		$statement = $this->db->prepare("SELECT * FROM `customers` LEFT JOIN `customer_to_store` c2s ON (`customers`.`customer_id` = `c2s`.`customer_id`) WHERE  {$where_query}");
		$statement->execute(array($store_id));

		return $statement->rowCount();
	}

	public function getBestCustomer($field, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("SELECT `selling_info`.*, `selling_price`.*, `customers`.*, SUM(`selling_price`.`payable_amount`) as total 
			FROM `selling_info` 
			LEFT JOIN `customers` ON (`selling_info`.`customer_id` = `customers`.`customer_id`)
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
			WHERE `selling_info`.`store_id` = ?
			GROUP BY `selling_info`.`customer_id` ORDER BY `total` DESC");
		$statement->execute(array($store_id));
		$customer = $statement->fetch(PDO::FETCH_ASSOC);

		return isset($customer[$field]) ? $customer[$field] : null;
	}

	public function getRecentCustomers($limit, $store_id = null)
	{
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("SELECT customers.* FROM `selling_info` 
			LEFT JOIN `customers` ON (`selling_info`.`customer_id` = `customers`.`customer_id`) 
			LEFT JOIN `customer_to_store` as c2s ON (`selling_info`.`customer_id` = `c2s`.`customer_id`)
			where `selling_info`.`store_id` = ? AND `c2s`.`status` = ?
			GROUP BY `selling_info`.`customer_id`
			ORDER BY `selling_info`.`info_id` DESC 
			LIMIT $limit"
			);
	    $statement->execute(array($store_id, 1));
	    return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getTotalBuyingAmount($customer_id, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("SELECT `selling_info`.*, `selling_price`.*, `customers`.*, SUM(`selling_price`.`payable_amount`) as total FROM `selling_info` 
			LEFT JOIN `customers` ON (`selling_info`.`customer_id` = `customers`.`customer_id`)
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`)
			where `customers`.`customer_id` = ? AND `selling_info`.`store_id` = ? 
			ORDER BY `total` DESC");
		$statement->execute(array($customer_id, $store_id));
		$customer = $statement->fetch(PDO::FETCH_ASSOC);

		return isset($customer['total']) ? $customer['total'] : '0';
	}

	public function getTotalInvoiceNumber($customer_id = null, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		if ($customer_id) {
			$statement = $this->db->prepare("SELECT * FROM `selling_info` 
				WHERE `customer_id` = ? AND `store_id` = ?");
			$statement->execute(array($customer_id, store_id()));
		}
		else {
			$statement = $this->db->prepare("SELECT * FROM `selling_info` WHERE `store_id` = ?");
			$statement->execute(array($store_id));
		}

		return $statement->rowCount();
	}

	public function getBalance($customer_id, $index = null, $store_id = null) 
	{	
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("SELECT `selling_info`.*, `selling_price`.`present_due` FROM `selling_info` 
			LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`) 
			WHERE `selling_info`.`customer_id` = ? AND `selling_info`.`store_id` = ?
			ORDER by `selling_info`.`invoice_id` DESC LIMIT 1");
		$statement->execute(array($customer_id, $store_id));

		$invoice = $statement->fetch(PDO::FETCH_ASSOC);
		
		if ($index) {
			return isset($invoice[$index]) ? $invoice[$index] : null;
		}

		return isset($invoice['present_due']) ? $invoice['present_due'] : 0;
	}

	public function getDueAmount($customer_id, $store_id = null, $index = 'due_amount') 
	{
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("SELECT * FROM `customer_to_store` WHERE `customer_id` = ? AND `store_id` = ?");
		$statement->execute(array($customer_id, $store_id));

		$row = $statement->fetch(PDO::FETCH_ASSOC);

		return isset($row[$index]) ? $row[$index] : null;
	}

	public function getCreditAmount($customer_id, $store_id = null, $index = 'credit_balance') 
	{
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("SELECT * FROM `customers` WHERE `customer_id` = ?");
		$statement->execute(array($customer_id));

		$row = $statement->fetch(PDO::FETCH_ASSOC);

		return isset($row[$index]) ? $row[$index] : null;
	}

	public function getBelongsStore($customer_id)
	{
		$statement = $this->db->prepare("SELECT * FROM `customer_to_store` WHERE `customer_id` = ?");
		$statement->execute(array($customer_id));

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getFamilyMembers($customer_id)
	{
		$statement = $this->db->prepare("SELECT * FROM `family_members` WHERE `user_id` = ? AND `user_type` = 3 ");
		$statement->execute(array($customer_id));

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getCustomerAvatar($sex)
	{
		switch ($sex) {
			case 1:
				$avatar = 'avatar';
				break;
			case 2:
				$avatar = 'avatar-female';
				break;
			default:
				$avatar = 'avatar-others';
				break;
		}
		
		return $avatar;
	}
}