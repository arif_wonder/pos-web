<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
class ModelPaymentMethod extends Model 
{
	public function addPaymentMethod($data) 
	{
    	$statement = $this->db->prepare("INSERT INTO `payments` (name, details, created_at) VALUES (?, ?, ?)");
    	$statement->execute(array($data['payment_name'], $data['payment_details'], date('Y-m-d H:i:s')));

    	$payment_id = $this->db->lastInsertId();

    	if (isset($data['payment_store'])) {
			foreach ($data['payment_store'] as $store_id) {
				$statement = $this->db->prepare("INSERT INTO `payment_to_store` SET `payment_id` = ?, `store_id` = ?");
				$statement->execute(array((int)$payment_id, (int)$store_id));
			}
		}

		$this->updateStatus($payment_id, $data['status']);
		$this->updateSortOrder($payment_id, $data['sort_order']);

    	return $payment_id;
	}

	public function updateStatus($payment_id, $status, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("UPDATE `payment_to_store` SET `status` = ? WHERE `store_id` = ? AND `payment_id` = ?");
		$statement->execute(array((int)$status, $store_id, (int)$payment_id));
	}

	public function updateSortOrder($payment_id, $sort_order, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$statement = $this->db->prepare("UPDATE `payment_to_store` SET `sort_order` = ? WHERE `store_id` = ? AND `payment_id` = ?");
		$statement->execute(array((int)$sort_order, $store_id, (int)$payment_id));
	}

	public function editPaymentMethod($payment_id, $data) 
	{    	
    	$statement = $this->db->prepare("UPDATE `payments` SET `name` = ?, `details` = ? WHERE `payment_id` = ? ");
    	$statement->execute(array($data['payment_name'], $data['payment_details'], $payment_id));

    	// delete store data balongs to the payment
    	$statement = $this->db->prepare("DELETE FROM `payment_to_store` WHERE `payment_id` = ?");
    	$statement->execute(array($payment_id));
		
		// insert payment into store
    	if (isset($data['payment_store'])) {
			foreach ($data['payment_store'] as $store_id) {
				$statement = $this->db->prepare("INSERT INTO `payment_to_store` SET `payment_id` = ?, `store_id` = ?");
				$statement->execute(array((int)$payment_id, (int)$store_id));
			}
		}

		$this->updateStatus($payment_id, $data['status']);
		$this->updateSortOrder($payment_id, $data['sort_order']);

    	return $payment_id;
	}

	public function deletePaymentMethod($payment_id) 
	{    	
    	$statement = $this->db->prepare("DELETE FROM `payments` WHERE `payment_id` = ? LIMIT 1");
    	$statement->execute(array($payment_id));	

    	$statement = $this->db->prepare("DELETE FROM `payment_to_store` WHERE `payment_id` = ?");
    	$statement->execute(array($payment_id));

        return $payment_id;
	}

	public function getPaymentMethod($payment_id, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

	    $statement = $this->db->prepare("SELECT `payments`.*, `pay2s`.`status`, `pay2s`.`sort_order` 
	    	FROM `payments` 
	    	LEFT JOIN `payment_to_store` as pay2s ON (`payments`.`payment_id` = `pay2s`.`payment_id`)  
	    	WHERE `pay2s`.`store_id` = ? AND `payments`.`payment_id` = ?");
	    $statement->execute(array($store_id, $payment_id));
	    $payment = $statement->fetch(PDO::FETCH_ASSOC);

	    // fetch stores related to payments
	    $statement = $this->db->prepare("SELECT `store_id` FROM `payment_to_store` WHERE `payment_id` = ?");
	    $statement->execute(array($payment_id));
	    $all_stores = $statement->fetchAll(PDO::FETCH_ASSOC);
	    $stores = array();
	    foreach ($all_stores as $store) {
	    	$stores[] = $store['store_id'];
	    }

	    $payment['stores'] = $stores;

	    return $payment;
	}

	public function getPaymentMethods($data = array(), $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		$sql = "SELECT * FROM `payments` LEFT JOIN `payment_to_store` pay2s ON (`payments`.`payment_id` = `pay2s`.`payment_id`) WHERE `pay2s`.`store_id` = ? AND `pay2s`.`status` = ?";

		if (isset($data['filter_name'])) {
			$sql .= " AND `name` LIKE '" . $data['filter_name'] . "%'";
		}

		$sql .= " GROUP BY `payments`.`payment_id`";

		$sort_data = array(
			'name'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY `pay2s`.`sort_order`";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$statement = $this->db->prepare($sql);
		$statement->execute(array($store_id, 1));

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getBelongsStore($payment_id)
	{
		$statement = $this->db->prepare("SELECT * FROM `payment_to_store` WHERE `payment_id` = ?");
		$statement->execute(array($payment_id));

		return $statement->fetchAll(PDO::FETCH_ASSOC);

	}
}