<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
class ModelStore extends Model 
{

	public function callAdminApi($token, $type){
		$token = 'Bearer '.$token;
	    $request_headers = array(
	                    "Authorization: ".$token);

	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_URL, ADMIN_API.'/update-store-limit?action='.$type);
	    curl_setopt($ch, CURLOPT_HEADER, true);
	    //curl_setopt($ch, CURLOPT_NOBODY, true);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);

	    $season_data = curl_exec($ch);

	    if (curl_errno($ch)) {
	        print "Error: " . curl_error($ch);
	        exit();
	    }

	    // Show me the result
	    curl_close($ch);
	    $json = $season_data;
	    //$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    $a = $season_data;
	    return $season_data;
	}

	public function addStore($data) 
	{



    $token = 'Bearer '.$data['token'];
    $request_headers = array(
                    "Authorization: ".$token,
                    
                );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, ADMIN_API.'/update-store-limit?action=increase');
    curl_setopt($ch, CURLOPT_HEADER, true);
    //curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);

    $season_data = curl_exec($ch);

    if (curl_errno($ch)) {
        print "Error: " . curl_error($ch);
        exit();
    }

    // Show me the result
    curl_close($ch);
    $json = $season_data;
    //$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $a = $season_data;
    

		if (strpos($season_data, 'true') !== false) {
		    $statement = $this->db->prepare("INSERT INTO `stores` (name, mobile, country, vat_reg_no, zip_code, address, area, city,state, logo, favicon, sound_effect, status, sort_order, receipt_printer, remote_printing, auto_print, created_at,gst_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)");
			$statement->execute(array($data['name'], $data['mobile'], $data['country'], $data['vat_reg_no'], $data['zip_code'], $data['address'],$data['area'],$data['city'],$data['state'], $data['logo'], $data['favicon'], $data['sound_effect'], $data['status'], $data['sort_order'], $data['receipt_printer'], $data['remote_printing'], $data['auto_print'], date_time(),$data['gst_type']));

			return $this->db->lastInsertId();    
		}
		else{
			return false;
		}
		
	}

	public function editStore($store_id, $data) 
	{

    	$statement = $this->db->prepare("UPDATE `stores` SET 
								    		`name` = ?, 
								    		`mobile` = ?, 
								    		`country` = ?, 
								    		`zip_code` = ?, 
								    		`vat_reg_no` = ?, 
								    		`address` = ?, 
								    		`area` = ?, 
								    		`city` = ?, 
								    		`state` = ?, 
								    		`gst_type` = ?,			    		
								    		`sound_effect` = ?, 
								    		`status` = ?, 
								    		`sort_order` = ?, 
								    		`receipt_printer` = ?, 
								    		`remote_printing` = ?, 
								    		`auto_print` = ? 
								    		WHERE `store_id` = ? 
    									");
    	$statement->execute(array(
				    		$data['name'], 
				    		$data['mobile'], 
				    		$data['country'],
				    		$data['zip_code'],  
				    		$data['vat_reg_no'], 
				    		$data['address'], 
				    		$data['area'], 
				    		$data['city'], 
				    		$data['state'], 
				    		$data['gst_type'], 
				    		$data['sound_effect'], 
				    		$data['status'], 
				    		$data['sort_order'], 
				    		$data['receipt_printer'], 
				    		$data['remote_printing'], 
				    		$data['auto_print'], 
				    		$store_id
				    	));

    	return $store_id;
	}

	public function editPreference($store_id, $preference = array())
	{

		if (empty($preference)) {
			$preference = array();
		}
		
		// Update timezone in _init.php

		// $timezone = $preference['timezone'];
		// $index_path = ROOT . '/_init.php';
		// @chmod($index_path, 0777);
		// if (is_writable($index_path) === false) {
		// 	//throw new Exception($language->get('timezone_not_change_in_init.php'));
		// 	return false;
		// } else {
		// 	$file = $index_path;
		// 	$filecontent = "$" . "timezone = '". $timezone ."';";
		// 	$fileArray = array(3 => $filecontent);
		// 	replace_lines($file, $fileArray);
		// 	@chmod($index_path, 0644);
		// }

		$statement = $this->db->prepare("UPDATE `stores` SET `preference` = ? WHERE `store_id` = ?");
    	$statement->execute(array(serialize($preference), $store_id));

    	return $store_id;
	}

	public function deleteStore($store_id, $token) 
	{


		$apiResponce = $this->callAdminApi($token, 'decrease');
		if (strpos($apiResponce, 'true') !== false) {
			
			$statement = $this->db->prepare("DELETE FROM `stores` WHERE `store_id` = ? LIMIT 1");
	    	$statement->execute(array($store_id));

	        return $store_id;
		}

    	
	}

	public function getStore($store_id) 
	{
		$statement = $this->db->prepare("SELECT * FROM `stores` WHERE `store_id` = ?");
	  	$statement->execute(array($store_id));

	  	return $statement->fetch(PDO::FETCH_ASSOC);
	}

	public function getStores($data = array()) 
	{
		$sql = "SELECT * FROM `stores`";

		if (isset($data['filter_name'])) {
			$sql .= " AND `name` LIKE '" . $data['filter_name'] . "%'";
		}

		$sql .= " GROUP BY `store_id`";

		$sort_data = array(
			'name'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY `store_id`";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$statement = $this->db->prepare($sql);
		$statement->execute();

		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function total() 
	{
		$statement = $this->db->prepare("SELECT * FROM `stores` WHERE `status` = ?");
		$statement->execute(array(1));

		return $statement->rowCount();
	}

	public function getCashiers($store_id) 
	{
		$statement = $this->db->prepare("SELECT * FROM `users`
		LEFT JOIN `user_to_store` as u2s ON (`users`.`id` = `u2s`.`user_id`) 
		LEFT JOIN `user_group` as ug ON (`users`.`group_id` = `ug`.`group_id`) 
		 WHERE `store_id` = ? AND `ug`.`slug` = ?");
	  	$statement->execute(array($store_id, 'cashier'));

	  	return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getStates() 
	{
		$statement = $this->db->prepare("SELECT * FROM `states`");
	  	$statement->execute();

	  	return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
}