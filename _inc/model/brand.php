<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	Modern POS - Point of Sale with Stock Management System
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
class ModelBrand extends Model 
{
	public function addBrand($data) 
	{
    	$statement = $this->db->prepare("INSERT INTO `product_brands` (
	    		brand_name, 
	    		brand_description
    		) 
    		VALUES (?, ?)");
    	
    	$statement->execute(array(
    		$data['brand_name'], 
    		$data['brand_description']
    	));

    	$product_id = $this->db->lastInsertId();

    	return $product_id;
	}

	public function editBrand($brand_id, $data) 
	{
		// update product infomation
    	$statement = $this->db->prepare("UPDATE `product_brands` 
				    		SET 
					    		`brand_name` = ?, 
					    		`brand_description` = ? 
				    		WHERE `brand_id` = ?
			    		");
    	$statement->execute(array($data['brand_name'], $data['brand_description'], $brand_id));
    	return $brand_id;
	}

	public function deleteBrand($brand_id) 
	{
		$statement = $this->db->prepare("DELETE FROM `product_brands` WHERE `brand_id` = ? LIMIT 1");
        $statement->execute(array($brand_id));

        return $brand_id;
	}

	public function getBrand($brand_id, $store_id = null) 
	{
		$store_id = $store_id ? $store_id : store_id();

		// fetch product
		$statement = $this->db->prepare("SELECT * FROM `product_brands` 
			WHERE brand_id = ?");
	    $statement->execute(array($brand_id));
	    $brand = $statement->fetch(PDO::FETCH_ASSOC);

	    return $brand;
	}

	public function getBrands() 
	{
		$query = $this->db->prepare("SELECT * FROM `product_brands` ");
	    $query->execute();

    	$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

}