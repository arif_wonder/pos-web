<?php
function get_payment_methods() 
{
	global $registry;

	$model = $registry->get('loader')->model('paymentmethod');

	return $model->getPaymentMethods();
}

function get_the_payment_method($id, $field = null)
{
	global $registry;

	$model = $registry->get('loader')->model('paymentmethod');

	$payment_methods = $model->getPaymentMethod($id);

	if ($field && isset($payment_methods[$field])) {
		return $payment_methods[$field];
	} elseif ($field) {
		return;
	}

	return '';
}