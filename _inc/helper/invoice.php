<?php
function get_the_invoice($invoice_id)
{
    global $registry;
    
    $invoice_model = $registry->get('loader')->model('invoice');
    return $invoice_model->getInvoiceInfo($invoice_id);
}

function get_invoice_due_amount($invoice_id)
{
	global $registry;
	
	$invoice_model = $registry->get('loader')->model('invoice');
	return $invoice_model->getDueAmount($invoice_id);
}

function get_invoice_last_edited_versiont_id($invoice_id)
{
	global $registry;
	
	$invoice_model = $registry->get('loader')->model('invoice');
	return $invoice_model->getInvoiceLastEditedVersionId($invoice_id);
}

function get_invoice_due_paid_amount($invoice_id)
{
	global $registry;
	
	$invoice_model = $registry->get('loader')->model('invoice');
	return $invoice_model->getDuePaidAmount($invoice_id);
}

function get_invoice_due_paid_discount_amount($invoice_id)
{
	global $registry;
	
	$invoice_model = $registry->get('loader')->model('invoice');
	return $invoice_model->getDuePaidDiscountAmount($invoice_id);
}

function get_invoice_due_paid_amount_rows($invoice_id)
{
	global $registry;
	
	$invoice_model = $registry->get('loader')->model('invoice');
	return $invoice_model->getDuePaidAmountRows($invoice_id);
}

function total_invoice($from, $to)
{
	global $registry;
	
	$invoice_model = $registry->get('loader')->model('invoice');
	return $invoice_model->total($from, $to);

}

function unique_invoice_id()
{
    global $db;
    
    $statement = $db->prepare("SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE `table_name` = 'selling_info'");
    $statement->execute(array());
    $invoice_id = $statement->fetch(PDO::FETCH_ASSOC)["auto_increment"];
    $invoice_id += 100000; 
    return $invoice_id;
}

function generate_invoice_id($type = 'sell', $invoice_id = null)
{
    global $registry;
    global $invoice_init_prefix;
    
    $invoice_model = $registry->get('loader')->model('invoice');
    if (!$invoice_id) {
        $last_invoice = $invoice_model->getLastInvoice($type);
        $invoice_id = isset($last_invoice['invoice_id']) ? $last_invoice['invoice_id'] : $invoice_init_prefix[$type].date('y').date('m').date('d').'1';
    }

    if ($invoice_model->hasInvoice($invoice_id)) {
        $invoice_id = str_replace(array('A','B','C','D','E','F','G','H','I','G','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'), '', $invoice_id);
        $invoice_id = (int)substr($invoice_id,-4) +1;
        $temp_invoice_id = $invoice_init_prefix[$type].date('y').date('m').date('d').$invoice_id;
        $zero_length = 11 - strlen($temp_invoice_id);
        $zeros = '';
        for ($i=0; $i < $zero_length; $i++) { 
            $zeros .= '0';
        }
        $invoice_id = $invoice_init_prefix[$type].date('y').date('m').date('d').store_id().$zeros.$invoice_id;
        generate_invoice_id($type, $invoice_id);
    } else {
        $zero_length = 11 - strlen($invoice_id);
        $zeros = '';
        for ($i=0; $i < $zero_length; $i++) { 
            $zeros .= '0';
        }
        $invoice_id = $invoice_init_prefix[$type].date('y').date('m').date('d').store_id().$zeros.'1';
    }
    return $invoice_id;
}

function total_trash_invoice($from, $to)
{
    global $registry;

    $invoice_model = $registry->get('loader')->model('invoice');

    return $invoice_model->totalTrash($from, $to);
}

function invoice_edit_lifespan()
{
    $lifespan = get_preference('invoice_edit_lifespan');
    $lifespan_unit = get_preference('invoice_edit_lifespan_unit');

    switch ($lifespan_unit) {
        case 'minute':
            
            $lifespan =  time() - ($lifespan * 60);

            break;

        case 'second':
                $lifespan =  time() - $lifespan;
            break;
        
        default:
            $lifespan = time()-(60*60*24);
            break;
    }

    return $lifespan;
}

function has_invoice_edit_permission($invoice_date_time, $customer_id = null, $invoice_id = null, $store_id = null) {

    global $db;
                
    $passed = true;
    $store_id = $store_id ? $store_id : store_id();
    $selling_date_time = strtotime($invoice_date_time);

    if ($customer_id && $invoice_id) {
        $statement = $db->prepare("SELECT * FROM `selling_info` WHERE `store_id` = ? AND `customer_id` = ? AND `inv_type` = ? ORDER BY `info_id` DESC LIMIT 1");
        $statement->execute(array($store_id, $customer_id, 'sell'));
        $invoice = $statement->fetch(PDO::FETCH_ASSOC);

        $passed = $invoice_id == $invoice['invoice_id'];
    }
    
    return (invoice_edit_lifespan() < $selling_date_time) && $passed;
}

function invoice_delete_lifespan()
{
    $lifespan = get_preference('invoice_delete_lifespan');
    $lifespan_unit = get_preference('invoice_delete_lifespan_unit');

    switch ($lifespan_unit) {
        case 'minute':
            
            $lifespan =  time() - ($lifespan * 60);

            break;

        case 'second':
                $lifespan =  time() - $lifespan;
            break;
        
        default:
            $lifespan = time()-(60*60*24);
            break;
    }

    return $lifespan;
}