<?php $language->load('management'); ?>

<h4 class="sub-title">
  <?php echo $language->get('text_delete_title'); ?>
</h4>

<form class="form-horizontal" id="promotions-delete-form" action="promotions.php" method="post">

  <input type="hidden" id="action_type" name="action_type" value="DELETE">
  <input type="hidden" id="promotions_id" name="promotions_id" value="<?php echo $data['id']; ?>">
  <input type="hidden" id="promotions_type" name="promotions_type" value="<?php echo $data['promotions_type']; ?>">
  
  <h4 class="box-title text-center">
    <?php echo $language->get('text_delete_instruction'); ?>
  </h4>
  <div class="box-body">
    <div class="form-group">
      <div class="col-sm-8 col-sm-offset-2">
        <button id="promotions-delete-submit" data-form="#promotions-delete-form" data-datatable="#promotions-list" class="btn btn-danger" name="submit" data-loading-text="Deleting...">
          <span class="fa fa-fw fa-trash"></span>
          <?php echo $language->get('button_delete'); ?>
        </button>
      </div>
    </div>
  </div>
</form>