<div class="form-horizontal">
  <div class="box-body">

    <div class="form-group">
      <label for="p_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_name'),null); ?>
      </label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="brand_name" value="<?php echo $product['brand_name']; ?>" name="brand_name" readonly>
      </div>
    </div>


    <div class="form-group">
      <label for="e_date" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-6">
        <textarea class="form-control" id="brand_description" name="brand_description" rows="3" readonly><?php echo $product['brand_description']; ?></textarea>
      </div>
    </div>

  </div>
</div>