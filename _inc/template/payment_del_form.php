<?php $language->load('payment'); ?>

<h4 class="sub-title">
  <?php echo $language->get('text_delete_title'); ?>
</h4>

<form class="form-horizontal" id="payment-del-form" action="payment.php" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="DELETE">
  <input type="hidden" id="payment_id" name="payment_id" value="<?php echo $payment['payment_id']; ?>">
  
  <h4 class="box-title text-center">
    <?php echo $language->get('text_delete_instruction'); ?>
  </h4>

  <div class="box-body">
    
    <div class="form-group">
      <label for="insert_to" class="col-sm-4 control-label">
        <?php echo $language->get('label_invoice_to'); ?>
      </label>
      <div class="col-sm-6">
        <div class="radio">
            <input type="radio" id="insert_to" value="insert_to" name="delete_action" checked="checked">
            <select name="new_payment_id" class="form-control">
                <option value="">
                  <?php echo $language->get('text_select'); ?>
                 </option>
              <?php foreach (get_payment_methods() as $the_payment) : ?>
                <?php if($the_payment['payment_id'] == $payment['payment_id']) continue ?>
                <option value="<?php echo $the_payment['payment_id']; ?>"> 
                  <?php echo $the_payment['name']; ?>
                </option>
              <?php endforeach; ?>
            </select> 
        </div>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-4 control-label"></label>
      <div class="col-sm-6">
        <button id="payment-delete" data-form="#payment-del-form" data-datatable="#payment-payment-list" class="btn btn-danger" name="btn_edit_box" data-loading-text="Deleting...">
          <span class="fa fa-fw fa-trash"></span>
          <?php echo $language->get('button_delete'); ?>
        </button>
      </div>
    </div>
    
  </div>
</form>