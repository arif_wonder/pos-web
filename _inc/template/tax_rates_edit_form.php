<?php $language->load('management'); ?>
<h4 class="sub-title">
  <?php echo $language->get('text_update_title'); ?>
</h4>
<form id="tax_rates_edit_form" class="form-horizontal" action="tax_rates.php?box_state=open" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <input type="hidden" id="tax_rate_id" name="tax_rate_id" value="<?php echo $data['tax_rate_id']; ?>">

  <div class="box-body">

    <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="title" value="<?php echo $data['title']; ?>" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo $data['description']; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="rate" class="col-sm-3 control-label">
        <?php echo $language->get('label_tax_rate'); ?> % 
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="rate" value="<?php echo $data['rate'] ? $data['rate'] : '0.00'; ?>" name="rate" required>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-8">
        <button class="btn btn-info" id="tax-rates-update-submit" name="form_update" data-form="#tax_rates_edit_form" data-loading-text="Updating...">
          <i class="fa fa-fw fa-pencil"></i> 
          <?php echo $language->get('button_update'); ?>
        </button>
      </div>
    </div>
  </div>
  </div>
</form>