<?php $language->load('management'); ?>
<h4 class="sub-title">
  <?php echo $language->get('text_update_title'); ?>
</h4>
<form id="promotions_edit_form" class="form-horizontal" action="promotions.php?box_state=open" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <input type="hidden" id="promotions_id" name="promotions_id" value="<?php echo $data['id']; ?>">
  <input type="hidden" id="promotions_type" name="promotions_type" value="<?php echo $data['promotions_type']; ?>">

  <div class="box-body">
    <div class="form-group">
      <label for="promotions_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control" 
        value="<?php echo array_key_exists($data['promotions_type'], $language->get('label_promotion_type')) ? $language->get('label_promotion_type')[$data['promotions_type']]: '';?>">
      </div>
    </div>

    <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="title" value="<?php echo $data['title'];?>" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo $data['title'];?></textarea>
      </div>
    </div>

    <!-- start of from date and to date -->
    <div class="form-group disc_for_festival">
      <label for="form_date" class="col-sm-3 control-label">
        <?php echo $language->get('label_from_date'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control" id="form_date" value="<?php echo $data['form_date'] ? date('d M Y', strtotime($data['form_date'])) : '';?>" name="form_date">
      </div>
    </div>

    <div class="form-group disc_for_festival">
      <label for="to_date" class="col-sm-3 control-label">
        <?php echo $language->get('label_to_date'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control" id="to_date" value="<?php echo $data['to_date'] ? date('d M Y', strtotime($data['to_date'])) : '';?>" name="to_date">
      </div>
    </div>
    <!-- end dof from date and to date -->

    <div class="form-group">
      <label for="discount_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="discount_type" id="discount_type" required>
          <option <?php echo $data['discount_type'] == 'fixed' ? 'selected="selected"' : '';?> value="fixed">Fixed</option>
          <option <?php echo $data['discount_type'] == 'precentage' ? 'selected="selected"' : '';?> value="precentage">Precentage</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="discount" value="<?php echo $data['discount'];?>" name="discount">
      </div>
    </div>

    <div class="form-group">
      <label for="min_purchase_amount" class="col-sm-3 control-label">
        <?php echo $language->get('label_min_purchase_amount'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="min_purchase_amount" value="<?php echo $data['min_purchase_amount'];?>" name="min_purchase_amount">
      </div>
    </div>

    <div class="form-group">
      <label for="max_discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_max_discount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="max_discount" value="<?php echo $data['max_discount'];?>" name="max_discount">
      </div>
    </div>

    <div class="form-group disc_happy_hours">
      <label for="time_from" class="col-sm-3 control-label">
        <?php echo $language->get('label_time_from'); ?>
      </label>
      <div class="col-sm-7 input-group bootstrap-timepicker timepicker" style="padding: 0px 14px 0 14px;">
        <input type="text" class="form-control" id="time_from" value="<?php echo $data['time_from'];?>" name="time_from" style="cursor: pointer;">
      </div>
    </div>

    <div class="form-group disc_happy_hours">
      <label for="time_to" class="col-sm-3 control-label">
        <?php echo $language->get('label_time_to'); ?>
      </label>
      <div class="col-sm-7 input-group bootstrap-timepicker timepicker" style="padding: 0px 14px 0 14px;">
        <input type="text" class="form-control" id="time_to" value="<?php echo $data['time_to'];?>" name="time_to" style="cursor: pointer;">
      </div>
    </div>

    <div class="form-group disc_happy_hours">
      <label for="on_days" class="col-sm-3 control-label">
        <?php echo $language->get('label_on_days'); ?>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="on_days[]" id="on_days" multiple="multiple">
          <option <?php echo in_array('mon', $data['on_days']) ? 'selected="selected"' : '';?> value="mon">Monday</option>
          <option <?php echo in_array('tue', $data['on_days']) ? 'selected="selected"' : '';?> value="tue">Tuesday</option>
          <option <?php echo in_array('wed', $data['on_days']) ? 'selected="selected"' : '';?> value="wed">Wednesday</option>
          <option <?php echo in_array('thu', $data['on_days']) ? 'selected="selected"' : '';?> value="thu">Thursday</option>
          <option <?php echo in_array('fri', $data['on_days']) ? 'selected="selected"' : '';?> value="fri">Friday</option>
          <option <?php echo in_array('sat', $data['on_days']) ? 'selected="selected"' : '';?> value="sat">Saturday</option>
          <option <?php echo in_array('sun', $data['on_days']) ? 'selected="selected"' : '';?> value="sun">Sunday</option>
       </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-8">
        <button class="btn btn-info" id="promotions-submit" name="form_update" data-form="#promotions_edit_form" data-loading-text="Updating...">
          <i class="fa fa-fw fa-pencil"></i> 
          <?php echo $language->get('button_update'); ?>
        </button>
      </div>
    </div>
  </div>
  </div>
</form>
<script type="text/javascript">
  $('#time_from').timepicker();
  $('#time_to').timepicker();
  $(document).ready(function(){
    promotions_type('<?php echo $data['promotions_type']; ?>');
  })
  $(document).on('change','#promotions_type',function(){
    console.log($(this).val());
    promotions_type($(this).val());
  })
  function promotions_type(promotionsType) {
    if (promotionsType=='disc_happy_hours') {
      $('.disc_happy_hours').show();
      $('.disc_for_festival').hide();
    }
    else if (promotionsType=='disc_for_festival') {
      $('.disc_for_festival').show();
      $('.disc_happy_hours').hide();
    }
    else {
      $('.disc_happy_hours').hide();
      $('.disc_for_festival').hide();
    }
  }

  $(function(){
  var year = new Date().getFullYear();
  var mindate = new Date();

  var maxdate = new Date();

  $("#form_date").datepicker({ 
    format: 'd M yyyy', 
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,
    changeMonth: true,
    startDate : new Date(),
  }).on('changeDate', function (ev) {
    var minDate = new Date(ev.date.valueOf());
    $('#to_date').datepicker('setStartDate', minDate);
  });

  $("#to_date").datepicker({ 
    format: 'd M yyyy', 
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,   
    changeMonth: true
  }).on('changeDate', function (ev) {
    var minDate = new Date(ev.date.valueOf());
    $('#form_date').datepicker('setEndDate', minDate);
  });
});
</script>