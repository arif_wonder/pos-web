<div class="form-horizontal">
  <div class="box-body">
    <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control" id="title" value="<?php echo $data['title']; ?>" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" readonly="true" id="description" name="description" rows="3"><?php echo $data['description']; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="category_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_category'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" readonly="true" name="category_id" id="category_id">
          <option value="">
            <?php echo $language->get('text_select'); ?>
          </option>
          <?php 
          foreach (get_categories_tree() as $key => $category) { 
            $slc = $category['category_id']==$data['category_id'] ? "selected='selected'" : '';
            ?>
            <option <?php echo $slc;?> value="<?php echo $category['category_id'] ; ?>"><?php echo $category['category_name'] ; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="date_from" class="col-sm-3 control-label">
        <?php echo $language->get('label_from_date'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control pick_date" id="date_from" 
        value="<?php echo $data['date_from'] ? date('d M Y', strtotime($data['date_from'])) : ''; ?>" 
        name="date_from">
      </div>
    </div>

    <div class="form-group">
      <label for="date_to" class="col-sm-3 control-label">
        <?php echo $language->get('label_to_date'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control pick_date" id="date_to" value="<?php echo $data['date_to'] ? date('d M Y', strtotime($data['date_to'])) : ''; ?>" name="date_to">
      </div>
    </div>

    <div class="form-group">
      <label for="discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control number" id="discount" value="<?php echo $data['discount']; ?>" name="discount">
      </div>
    </div>

    <div class="form-group">
      <label for="discount_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control number" id="discount_type" value="<?php echo $data['category_id']=='fixed' ? 'Fixed' : 'Precentage' ; ?>" name="discount_type">
      </div>
    </div>

    <div class="form-group">
      <label for="min_purchase_amount" class="col-sm-3 control-label">
        <?php echo $language->get('label_min_purchase_amount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control number" id="min_purchase_amount" value="<?php echo $data['min_purchase_amount']; ?>" name="min_purchase_amount">
      </div>
    </div>
  </div>
</div>