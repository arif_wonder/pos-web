<?php $language->load('supplier'); ?>
<form id="create-supplier-form" class="form-horizontal" action="supplier.php" method="post" enctype="multipart/form-data">
  
  <input type="hidden" id="action_type" name="action_type" value="CREATE">
  
  <div class="box-body">
    
    <div class="form-group">
      <label for="sup_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_name'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_name" name="sup_name" value="<?php echo isset($request->post['sup_name']) ? $request->post['sup_name'] : null; ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_email" class="col-sm-3 control-label">
        <?php echo $language->get('label_email'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="email" class="form-control" id="sup_email" name="sup_email" value="<?php echo isset($request->post['sup_email']) ? $request->post['sup_email'] : null; ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_mobile" class="col-sm-3 control-label">
        <?php echo $language->get('label_mobile'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_mobile" name="sup_mobile" value="<?php echo isset($request->post['sup_mobile']) ? $request->post['sup_mobile'] : null; ?>" required maxlength="10">
      </div>
    </div>

    <div class="form-group">
      <label for="sup_alternate_mobile" class="col-sm-3 control-label">
        <?php echo $language->get('label_alternate_mobile'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_alternate_mobile" name="sup_alternate_mobile" value="<?php echo isset($request->post['sup_alternate_mobile']) ? $request->post['sup_alternate_mobile'] : null; ?>">
      </div>
    </div>
<!-- ----------------- -->
    <div class="form-group">
      <label for="sup_aadhar_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_aadhar_number'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_aadhar_number" name="sup_aadhar_number" value="<?php echo isset($request->post['sup_aadhar_number']) ? $request->post['sup_aadhar_number'] : null; ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_pan_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_pan'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_pan_number" name="sup_pan_number" value="<?php echo isset($request->post['sup_pan_number']) ? $request->post['sup_pan_number'] : null; ?>" required maxlength="10">
      </div>
    </div>


    <div class="form-group">
      <label for="sup_driving_license" class="col-sm-3 control-label">
        <?php echo $language->get('label_driving_license'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_driving_license" name="sup_driving_license" value="<?php echo isset($request->post['sup_driving_license']) ? $request->post['sup_driving_license'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="sup_passport_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_passport_number'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_passport_number" name="sup_passport_number" value="<?php echo isset($request->post['sup_passport_number']) ? $request->post['sup_passport_number'] : null; ?>">
      </div>
    </div>
    <!-- ------------ -->

    <div class="form-group">
      <label for="sup_address" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_address'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="sup_address"  name="sup_address" value="<?php echo isset($request->post['sup_address']) ? $request->post['sup_address'] : null; ?>" required></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_city" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_city'), null); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_city" name="sup_city" value="<?php echo isset($request->post['sup_city']) ? $request->post['sup_city'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="sup_state" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_state'), null); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_state" name="sup_state" value="<?php echo isset($request->post['sup_state']) ? $request->post['sup_state'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="sup_zipcode" class="col-sm-3 control-label">
        <?php echo 'Pin Code'; ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_zipcode" name="sup_zipcode" value="<?php echo isset($request->post['sup_zipcode']) ? $request->post['sup_zipcode'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label">
        <?php echo $language->get('label_store'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7 store-selector">
        <div class="checkbox selector">
          <label>
            <input type="checkbox" onclick="$('input[name*=\'supplier_store\']').prop('checked', this.checked);"> Select / Deselect
          </label>
        </div>
        <div class="filter-searchbox">
          <input ng-model="search_store" class="form-control" type="text" id="search_store" placeholder="<?php echo $language->get('search'); ?>">
        </div>
        <div class="well well-sm store-well"> 
          <div filter-list="search_store">
            <?php foreach(get_stores() as $the_store) : ?>                    
              <div class="checkbox">
                <label>                         
                  <input type="checkbox" name="supplier_store[]" value="<?php echo $the_store['store_id']; ?>" <?php echo $the_store['store_id'] == store_id() ? 'checked' : null; ?>>
                  <?php echo ucfirst($the_store['name']).' ('.ucfirst($the_store['area']).') '; ?>
                </label>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>

      <div class="form-group">
      <label class="col-sm-3 control-label">
        <?php echo 'Category'; ?><i class="required">*</i>
      </label>
      <div class="col-sm-7 category-selector">
        <div class="checkbox selector">
          <label>
            <input type="checkbox" onclick="$('input[name*=\'supplier_category\']').prop('checked', this.checked);"> Select / Deselect
          </label>
        </div>
        <div class="filter-searchbox">
          <input ng-model="search_category" class="form-control" type="text" id="search_category" placeholder="<?php echo $language->get('search'); ?>">
        </div>
        <div class="well well-sm category-well"> 
          <div filter-list="search_category">
            <?php foreach(get_categorys() as $the_category) : ?>                    
              <div class="checkbox">
                <label>                         
                  <input type="checkbox" name="supplier_category[]" value="<?php echo $the_category['category_id']; ?>">
                  <?php echo ucfirst($the_category['category_name']); ?>
                </label>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_details" class="col-sm-3 control-label">
        <?php echo $language->get('label_details'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="sup_details"  name="sup_details" value="<?php echo isset($request->post['sup_details']) ? $request->post['sup_details'] : null; ?>" required></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="status" class="col-sm-3 control-label">
        <?php echo $language->get('label_status'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select id="status" class="form-control" name="status" >
          <option <?php echo isset($request->post['status']) && $request->post['status'] == '1' ? 'selected' : null; ?> value="1">
            <?php echo $language->get('text_active'); ?>
          </option>
          <option <?php echo isset($request->post['status']) && $request->post['status'] == '0' ? 'selected' : null; ?> value="0">
            <?php echo $language->get('text_in_active'); ?>
          </option>
        </select>
      </div>
    </div>


<input type="hidden" value="0" name="sort_order">

<!--     <div class="form-group d-none">
      <label for="sort_order" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_sort_order'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="number" class="form-control" id="sort_order" value="<?php echo isset($request->post['sort_order']) ? $request->post['sort_order'] : 0; ?>" name="sort_order" required>
      </div>
    </div> -->
    <!-- ****************************** bank details ****************************** -->
    <div  class="form-group" style="display: none;">
      <label for="sup_zipcode" class="col-sm-3 control-label">
      </label>
      <div class="col-sm-7">
       <?php echo sprintf($language->get('label_bank_details'), null); ?>
      </div>
    </div>

    <div class="form-group">
      <label for="bank_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_bank_name'), null); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="bank_name" name="bank_name" value="<?php echo isset($request->post['bank_name']) ? $request->post['bank_name'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="bank_acc_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_acc_number'), null); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="bank_acc_number" name="bank_acc_number" value="<?php echo isset($request->post['bank_acc_number']) ? $request->post['bank_acc_number'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="ifsc_code" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_ifsc_code'), null); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="ifsc_code" name="ifsc_code" value="<?php echo isset($request->post['ifsc_code']) ? $request->post['ifsc_code'] : null; ?>">
      </div>
    </div>

    <!-- Company Details -->
    <div class="clearfix"></div>

    <div class="form-group col-sm-12 com-det-for-list">
      <!-- <label for="label_company_details" class="col-sm-3 control-label">
      </label> -->
      <div class="col-sm-12">
       <?php echo sprintf($language->get('label_company_details'), null); ?>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_company_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_company_name'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_company_name" name="sup_company_name" value="<?php echo isset($request->post['sup_company_name']) ? $request->post['sup_company_name'] : null; ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_company_address" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_address'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_company_address" name="sup_company_address" value="<?php echo isset($request->post['sup_company_address']) ? $request->post['sup_company_address'] : null; ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_company_contact_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_contact_number'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_company_contact_number" name="sup_company_contact_number" value="<?php echo isset($request->post['sup_company_contact_number']) ? $request->post['sup_company_contact_number'] : null; ?>" required maxlength="10">
      </div>
    </div>

    <div class="form-group">
      <label for="sup_company_gst_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_gst_number'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_company_gst_number" name="sup_company_gst_number" value="<?php echo isset($request->post['sup_company_gst_number']) ? $request->post['sup_company_gst_number'] : null; ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_company_tin_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_tin_number'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_company_tin_number" name="sup_company_tin_number" value="<?php echo isset($request->post['sup_company_tin_number']) ? $request->post['sup_company_tin_number'] : null; ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_company_tan_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_tan_number'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_company_tan_number" name="sup_company_tan_number" value="<?php echo isset($request->post['sup_company_tan_number']) ? $request->post['sup_company_tan_number'] : null; ?>" required>
      </div>
    </div>

    <!-- end of the company details-->

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7 res-sav-for">
        <button class="btn btn-info" id="create-supplier-submit" type="submit" name="create-supplier-submit" data-form="#create-supplier-form" data-loading-text="Saving...">
          <span class="fa fa-fw fa-save"></span>
          <?php echo $language->get('button_save'); ?>
        </button>
        <button type="reset" class="btn btn-danger" id="reset" name="reset"><span class="fa fa-fw fa-circle-o"></span>
          <?php echo $language->get('button_reset'); ?>
        </button>
      </div>
    </div>
    
  </div>
</form>