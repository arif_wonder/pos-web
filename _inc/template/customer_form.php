<?php 
  $language->load('customer'); 
?>
<h4 class="sub-title">
  <?php echo $language->get('text_update_title'); 
  // echo "<pre>";
  // print_r($customer);
  ?>
</h4>

<form class="form-horizontal mod-comm-cla" id="customer-form" action="customer.php" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <input type="hidden" id="customer_id" name="customer_id" value="<?php echo $customer['customer_id']; ?>">
  <input type="hidden" id="bank_id" name="bank_id" value="<?php echo $customer['bank_id']; ?>">
  <input type="hidden" id="user_details_id" name="user_details_id" value="<?php echo $customer['user_details_id']; ?>">
  
  <div class="box-body">
    
    <div class="form-group">
      <label for="customer_name" class="col-sm-4 control-label">
        <?php echo sprintf($language->get('label_name'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="customer_name" value="<?php echo $customer['customer_name']; ?>" name="customer_name" required>
      </div>
    </div>

    <div class="form-group">
      <label for="customer_email" class="col-sm-4 control-label">
        <?php echo sprintf($language->get('label_email'), null); ?>
      </label>
      <div class="col-sm-7">
        <input type="email" class="form-control" id="customer_email" value="<?php echo $customer['customer_email']; ?>" name="customer_email">
      </div>
    </div>

    <div class="form-group">
      <label for="customer_mobile" class="col-sm-3 control-label">
        <?php echo 'Country Code'; ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
       
          <select  id="mobile_cc" class="form-control" name="mobile_cc" >
            <option value="">Country Code</option>
            <?php
              foreach($country_codes as $code){?>
                <option value="<?php echo $code['code'] ?>" 
                  <?php if($code['code'] == $customer['mobile_cc']) { echo 'selected';} ?>>
                  <?php echo $code['name'] ?>(<?php echo $code['code'] ?>)<?php $customer['mobile_cc']; ?>
                </option>
              <?php }
            ?>
        </select>
       </div>
    </div>

    <div class="form-group">
      <label for="customer_mobile" class="col-sm-4 control-label">
        <?php echo $language->get('label_phone'); ?><i class="required">*</i>
      </label>

      <div class="col-sm-7">
        <input type="text" class="form-control number" id="customer_mobile" value="<?php echo $customer['customer_mobile']; ?>" name="customer_mobile" maxlength="10">
      </div>
    </div>

     <div class="form-group">
      <label for="alt_contact_number" class="col-sm-4 control-label">
        <?php echo $language->get('label_Alternate Contact No.'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control number" id="alt_contact_number" value="<?php echo $customer['alt_contact_number']; ?>" name="alt_contact_number">
      </div>
    </div>

    <div class="form-group">
      <label for="customer_sex" class="col-sm-4 control-label">
        <?php echo 'Gender'; ?>
      </label>
      <div class="col-sm-7">
        <select name="customer_sex" class="form-control" required>
          <option <?php echo $customer['customer_sex'] == 1 ? 'selected' : null; ?> value="1">
            <?php echo $language->get('text_male'); ?>
          </option>
          <option <?php echo $customer['customer_sex'] == 2 ? 'selected' : null; ?> value="2">
            <?php echo $language->get('text_female'); ?>
          </option>
           <option <?php echo $customer['customer_sex'] == 3 ? 'selected' : null; ?> value="3">
            <?php echo $language->get('label_others'); ?>
          </option>
        </select>
      </div>
    </div>

     <div class="form-group">
      <label for="customer_age" class="col-sm-4 control-label">
        <?php echo sprintf($language->get('label_age'), null); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control number" id="customer_age" value="<?php echo $customer['customer_age']; ?>" name="customer_age" onKeyUp="if(this.value>140){this.value='140';}else if(this.value<0){this.value='0';}">
      </div>
    </div>
    <?php
      $dob = $customer['dob'];
      $explodeDob = explode(':', $dob);
      $date = '';
      $month = '';
      $year = '';
      if(isset($explodeDob[0])){
        $date = $explodeDob[0];
      }
      if(isset($explodeDob[1])){
        $selectedmonth = $explodeDob[1];
      }
      if(isset($explodeDob[2])){
        $selectedmonthyear = $explodeDob[2];
      }
    ?>
    <div class="form-group">
      <label for="dob" class="col-sm-4 control-label">
        <?php echo $language->get('label_Birth Date'); ?>
      </label>
      <div class="col-sm-7">
        <select name="date">
          <option value="">Date</option>
          <?php
            for($i=1;$i<=31;$i++){
              if($i <=9){
                $i = '0'.$i;
              } ?>
              <option
              <?php
              if($date == $i){
                echo 'selected';
              }
              ?>
               value="<?php echo $i;?>"><?php echo $i; ?></option>
            <?php } ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="dob" class="col-sm-4 control-label">
        <?php echo 'Month'; ?>
      </label>
      <div class="col-sm-7">
        <select name="month">
          <option value="">Month</option>
          <?php
          $months = ['January', 'February', 'March', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            foreach($months as $key=> $month){
              $key = $key+1;
              if($key <=9){
                $key = '0'.$key;
              }?>
              <option 
              <?php
              if($selectedmonth == $key){
                echo 'selected';
              }
              ?>
              value="<?php echo $key; ?>"><?php echo $month; ?></option>
            <?php }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="dob" class="col-sm-4 control-label">
        <?php echo 'Year'; ?>
      </label>
      <div class="col-sm-7">
         <select name="year">
          <option value="">Year</option>
          <?php
          $year = date('Y');
            for($i=1970;$i<=$year;$i++){?>
              <option 
              <?php
              if($selectedmonthyear == $i){
                echo 'selected';
              }
              ?>
              value="<?php echo $i;?>"><?php echo $i; ?></option>
            <?php }
          ?>
        </select>
      </div>
    </div>

     <div style="display:none;" class="form-group">
      <label for="anniversary_date" class="col-sm-4 control-label">
        <?php echo $language->get('label_Anniversary Date'); ?>
      </label>
      <div class="col-sm-7">
        <input type="Date" class="form-control" id="anniversary_date" value="<?php echo $customer['anniversary_date']; ?>" name="anniversary_date">
      </div>
    </div>



        <!-- =================================== -->

    <div style="display:none;" class="form-group">
      <label for="" class="col-sm-3 control-label"></label>
      <div class="col-sm-7"><?php echo $language->get('label_permanent_address'); ?></div>
    </div>
  
    <div class="form-group">
      <label for="Address" class="col-sm-4 control-label">
        <?php echo $language->get('label_Address'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="perma_address" value="<?php echo $customer['perma_address']; ?>" name="perma_address">
      </div>
    </div>

    <div class="form-group">
      <label for="city" class="col-sm-4 control-label">
        <?php echo $language->get('label_City'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="perma_city" value="<?php echo $customer['perma_city']; ?>" name="perma_city">
      </div>
    </div>

    <div class="form-group">
      <label for="state" class="col-sm-4 control-label">
        <?php echo 'State'; ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="perma_state" value="<?php echo $customer['perma_state']; ?>" name="perma_state">
      </div>
    </div>

    <div class="form-group">
      <label for="zip code" class="col-sm-4 control-label">
        <?php echo 'Pin Code'; ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control number" id="perma_zip_code" value="<?php echo $customer['perma_zip_code']; ?>" name="perma_zip_code">
      </div>
    </div>


    <hr></hr>

    <!-- <div class="form-group">
      <label for="customer_address" class="col-sm-4 control-label">
        <?php echo sprintf($language->get('label_address'), null); ?>
      </label>
      <div class="col-sm-6">
        <textarea class="form-control" id="customer_address" name="customer_address"><?php echo $customer['customer_address']; ?></textarea>
      </div>
    </div> -->
<input type="hidden" name="status" value="1">
    <!-- <div class="form-group">
      <label for="status" class="col-sm-4 control-label">
        <?php echo $language->get('label_status'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-6">
        <select id="status" class="form-control" name="status" >
          <option <?php echo isset($customer['status']) && $customer['status'] == '1' ? 'selected' : null; ?> value="1">
            <?php echo $language->get('text_active'); ?>
          </option>
          <option <?php echo isset($customer['status']) && $customer['status'] == '0' ? 'selected' : null; ?> value="0">
            <?php echo $language->get('text_inactive'); ?>
          </option>
        </select>
      </div>
    </div> -->

    <input type="hidden" id="sort_order" value="0" name="sort_order">

    <!-- <div class="form-group">
      <label for="sort_order" class="col-sm-4 control-label">
        <?php echo sprintf($language->get('label_sort_order'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-6">
        <input type="number" class="form-control" id="sort_order" value="<?php echo $customer['sort_order']; ?>" name="sort_order">
      </div>
    </div> -->


   

    <div class="form-group">
      <label for="aadhar_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_aadhar_number'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" maxlength="12" class="form-control number" id="aadhar_number" name="aadhar_number" value="<?php echo $customer['aadhar_number']; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="pan_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_pan'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="pan_number" name="pan_number" value="<?php echo $customer['pan_number']; ?>">
      </div>
    </div>


    <div class="form-group">
      <label for="dl_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_driving_license'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="dl_number" name="dl_number" value="<?php echo $customer['dl_number']; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="passport_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_passport_number'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="passport_number" name="passport_number" value="<?php echo $customer['passport_number']; ?>">
      </div>
    </div>


    <h4 style="display:none;" style="margin-left: 100px; font: bold; ">Correspondence Address</h4>

    <div style="display:none;" class="form-group">
      <label for="Address" class="col-sm-4 control-label">
        <?php echo $language->get('label_Address'); ?>
      </label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="cor_address" value="<?php echo $customer['cor_address']; ?>" name="cor_address">
      </div>
    </div>

    <div style="display:none;" class="form-group">
      <label for="cor_city" class="col-sm-4 control-label">
        <?php echo $language->get('label_City'); ?>
      </label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="cor_city" value="<?php echo $customer['cor_city']; ?>" name="cor_city">
      </div>
    </div>

    <div style="display:none;" class="form-group">
      <label for="cor_state" class="col-sm-4 control-label">
        <?php echo $language->get('label_State'); ?>
      </label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="cor_state" value="<?php echo $customer['cor_state']; ?>" name="cor_state">
      </div>
    </div>

    <div style="display:none;" class="form-group">
      <label for="zip code" class="col-sm-4 control-label">
        <?php echo $language->get('label_Zip Code'); ?>
      </label>
      <div class="col-sm-6">
        <input type="text" class="form-control number" id="cor_zip_code" value="<?php echo $customer['cor_zip_code']; ?>" name="cor_zip_code">
      </div>
    </div>


    <div class="form-group">
      <label for="marital_status" class="col-sm-4 control-label">
        <?php echo $language->get('label_Marital Status'); ?>
      </label>
      <div class="col-sm-7">
        <select id="marital_status" name="marital_status" class="form-control" required>
          <option value="1"<?php echo $customer['marital_status'] == '1' ? ' selected' : null; ?>>
            <?php echo $language->get('label_Married'); ?>
          </option>
          <option value="2"<?php echo $customer['marital_status'] == '2' ? ' selected' : null; ?>>
            <?php echo $language->get('label_Widowed'); ?>
          </option>
          <option value="3"<?php echo $customer['marital_status'] == '3' ? ' selected' : null; ?>>
            <?php echo $language->get('label_Divorced'); ?>
          </option>
          <option value="4"<?php echo $customer['marital_status'] == '4' ? ' selected' : null; ?>>
            <?php echo $language->get('label_UnMarried'); ?>
          </option>
          <option value="5"<?php echo $customer['marital_status'] == '5' ? ' selected' : null; ?>>
            <?php echo $language->get('label_others'); ?>
          </option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="occupation" class="col-sm-3 control-label">
        <?php echo $language->get('label_Occupation'); ?>
      </label>
      <div class="col-sm-7">
        <select id="occupation" name="occupation" class="form-control">
          <option value="1"<?php echo $customer['occupation'] == '1' ? ' selected' : null; ?>>
            <?php echo $language->get('label_Businessman'); ?>
          </option>
          <option value="2"<?php echo $customer['occupation'] == '2' ? ' selected' : null; ?>>
            <?php echo $language->get('label_ServiceMan'); ?>
          </option>
        </select>
      </div>
    </div>

    <!-- Businessman -->
    <div id="businessman">
      <div class="form-group">
        <label for="busi_name" class="col-sm-3 control-label">
          <?php echo $language->get('label_NameofBusiness'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="busi_name" name="busi_name" value="<?php echo $customer['busi_name']; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="busi_address" class="col-sm-3 control-label">
          <?php echo $language->get('label_address'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="busi_address" name="busi_address" value="<?php echo $customer['busi_address']; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="busi_contact_number" class="col-sm-3 control-label">
          <?php echo $language->get('label_contact_number'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" maxlength="10" class="form-control number" id="busi_contact_number" name="busi_contact_number" value="<?php echo $customer['busi_contact_number']; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="busi_annual_turnover" class="col-sm-3 control-label">
          <?php echo $language->get('label_AnnualTurnover'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control decimal" id="busi_annual_turnover" name="busi_annual_turnover" value="<?php echo $customer['busi_annual_turnover']; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="busi_trans_to_office" class="col-sm-3 control-label">
          <?php echo $language->get('label_Modeoftransport'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="busi_trans_to_office" name="busi_trans_to_office" value="<?php echo $customer['busi_trans_to_office']; ?>">
        </div>
      </div>
    </div>
    <!-- Service Man -->
    <div id="serviceman">
      <div class="form-group">
        <label for="ser_man_designation" class="col-sm-3 control-label">
          <?php echo $language->get('label_Designation'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="ser_man_designation" name="ser_man_designation" value="<?php echo $customer['ser_man_designation']; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="ser_man_comp_name" class="col-sm-3 control-label">
          <?php echo $language->get('label_company_name'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="ser_man_comp_name" name="ser_man_comp_name" value="<?php echo $customer['ser_man_comp_name']; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="ser_man_comp_address" class="col-sm-3 control-label">
          <?php echo $language->get('label_CompanyAddress'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="ser_man_comp_address" name="ser_man_comp_address" value="<?php echo $customer['ser_man_comp_address']; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="ser_man_comp_cont_number" class="col-sm-3 control-label">
          <?php echo $language->get('label_Companycontactnumber'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" maxlength="10" class="form-control number" id="ser_man_comp_cont_number" name="ser_man_comp_cont_number" value="<?php echo $customer['ser_man_comp_cont_number']; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="ser_man_trans_to_office" class="col-sm-3 control-label">
          <?php echo $language->get('label_Modeoftransport'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="ser_man_trans_to_office" name="ser_man_trans_to_office" value="<?php echo $customer['ser_man_trans_to_office']; ?>">
        </div>
      </div>
    </div>

    <!-- ========= Bank Details ======== -->
    <!-- <div class="form-group">
      <label for="label_bank_details" class="col-sm-3 control-label"></label>
      <div class="col-sm-7"><?php echo $language->get('label_bank_details'); ?></div>
    </div> -->
    
    <div class="form-group">
      <label for="bank_name" class="col-sm-3 control-label">
        <?php echo $language->get('label_bank_name'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="bank_name" name="bank_name" value="<?php echo $customer['bank_name']; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="bank_acc_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_acc_number'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" maxlength="20" class="form-control" id="bank_acc_number" name="bank_acc_number" value="<?php echo $customer['bank_acc_number']; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="ifsc_code" class="col-sm-3 control-label">
        <?php echo $language->get('label_ifsc_code'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" maxlength="10" class="form-control" id="ifsc_code" name="ifsc_code" value="<?php echo $customer['ifsc_code']; ?>">
      </div>
    </div>
    <!-- ========== Vehicle Details ========== -->
    <div style="display:none;" class="form-group">
      <label for="label_VehicleDetails" class="col-sm-3 control-label"></label>
      <div class="col-sm-7"><?php echo $language->get('label_VehicleDetails'); ?></div>
    </div>

     <div style="display:none;" class="form-group">
      <label for="number_of_vehicles" class="col-sm-3 control-label">
        <?php echo $language->get('label_Noofvehiclesowned'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="number_of_vehicles" name="number_of_vehicles" value="<?php echo $customer['number_of_vehicles']; ?>">
      </div>
    </div>

    <div style="display:none;" class="form-group">
      <label for="vehicle_model" class="col-sm-3 control-label">
        <?php echo $language->get('label_VehicleModel'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="vehicle_model" name="vehicle_model" value="<?php echo $customer['vehicle_model']; ?>">
      </div>
    </div>

    <div style="display:none;" class="form-group">
      <label for="vehicle_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_VehicleNumber'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="vehicle_number" name="vehicle_number" value="<?php echo $customer['vehicle_number']; ?>">
      </div>
    </div>
    <!-- Family Members details (tabular Format) -->
    <div class="form-group">
      <table class="table table-bordered" id="edit-customer-table">
        <thead>
          <tr>
            <th><?php echo $language->get('label_box_name'); ?></th>
            <th><?php echo $language->get('label_DOB'); ?></th>
            <th><?php echo $language->get('label_Relationshipwithcustomer'); ?></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (sizeof($customer['family'])) {
            for ($i=0; $i < sizeof($customer['family']) ; $i++) {  ?>
          <tr class="<?php echo 't-row rem-row-'.$i; ?>">
            <td><input type="text" class="form-control" id="member_name" name="member_name[]" 
                value="<?php echo $customer['family'][$i]['name']; ?>" ></td>
            <td> <input type="text" class="form-control pick_date" autocomplete="off" readonly="true" id="member_dob" name="member_dob[]" value="<?php echo date('d M Y',strtotime($customer['family'][$i]['dob'])); ?>"></td>
            <td>
              <input type="text" class="form-control" id="member_relationship" name="member_relationship[]" value="<?php echo $customer['family'][$i]['relationship']; ?>">
            </td>
            <td>
              <button type="button" class="editremoveBtn"><i class="fa fa-fw fa-trash"></i></button>
              <button type="button" class="editaddBtn"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </td>
          </tr>
          <?php
            } /// end of the for loop
          } /// end of the if condition
          else{ ?>
            <tr class="t-row rem-row-0">
            <td><input type="text" class="form-control" id="member_name" name="member_name[]" 
                value="" ></td>
            <td> 
              <input type="text" autocomplete="off" readonly="true" class="form-control pick_date" id="member_dob" name="member_dob[]" value="">
            </td>
            <td>
              <input type="text" class="form-control" id="member_relationship" name="member_relationship[]" value="">
            </td>
            <td>
              <button type="button" class="editremoveBtn"><i class="fa fa-fw fa-trash"></i></</button>
              <button type="button" class="editaddBtn"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </td>
          </tr>
        <?php 
          } ?>
        </tbody>
        
      </table>
    </div>
    <div class="form-group">
      <label for="customer_address" class="col-sm-4 control-label"></label>
      <div class="col-sm-6">
        <button id="customer-update" data-form="#customer-form" data-datatable="#customer-customer-list" class="btn btn-info" name="btn_edit_customer" data-loading-text="Updating...">
          <span class="fa fa-fw fa-pencil"></span>
          <?php echo $language->get('button_update'); ?>
        </button>
      </div>
    </div>

  </div>
</form>
<script type="text/javascript">
$('body').on('focus',".pick_date", function(){
  var todayDate = new Date().getFullYear();
  var endD= new Date(new Date().setDate(todayDate - 15));
  var currDate = new Date();
  $(this).datepicker({
    autoclose: true,
    todayHighlight: true,
    orientation: "top auto",
    format: "dd M yyyy",
  });
});
  $(document).on('change', '#occupation', function() {
    customer_occupation($(this).val());
  });
$( document ).ready(function() {
  var occ= "<?php echo $customer['occupation'];?>";
  customer_occupation(occ);
});
function customer_occupation(occupation){
  if ( occupation ==1 ) {
    $('#businessman').show();
    $('#serviceman').hide();
  }
  else {
    $('#serviceman').show();
    $('#businessman').hide();
  }
}
$(document).on('click', '.editaddBtn', function(){
  var existRows = $('#edit-customer-table > tbody > tr.t-row');
  existRows = existRows.length;
  var newRow = `
              <tr class="t-row rem-row-`+existRows+`">
          <td><input type="text" class="form-control" id="member_name" name="member_name[]" 
              value="" ></td>
          <td> <input type="text" class="form-control pick_date" autocomplete="off" readonly="true" id="member_dob" name="member_dob[]" value=""></td>
          <td>
            <input type="text" class="form-control" id="member_relationship" name="member_relationship[]" value="">
          </td>
          <td>
            <button type="button" class="editremoveBtn"><i class="fa fa-fw fa-trash"></i></button>
            <button type="button" class="editaddBtn"><i class="fa fa-plus" aria-hidden="true"></i></button>
          </td>
        </tr>
                `;
    $('#edit-customer-table > tbody').append(newRow);
});
  $(document).on('click', '.editremoveBtn', function(){
    var numRows = $('#edit-customer-table > tbody > tr.t-row').length;
    if (numRows>1) {
      $(this).parent('td').parent('tr').remove();  
    }
    else{
      $(this).parent('td').parent('tr').find('input, textarea, select').val('');
    }
  })
</script>