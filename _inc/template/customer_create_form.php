<?php $language->load('customer'); 
 $str = file_get_contents(DOMAIN_PATH.'/_inc/template/countries.json');
 $country_codes = json_decode($str, true);
?>

<form id="create-customer-form" class="form-horizontal" action="customer.php" method="post" enctype="multipart/form-data">
  
  <input type="hidden" id="action_type" name="action_type" value="CREATE">
  
  <div class="box-body">
    
    <div class="form-group">
      <label for="customer_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_name'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="customer_name" value="<?php echo isset($request->post['customer_name']) ? $request->post['customer_name'] : null; ?>" name="customer_name" required>
      </div>
    </div>

    <div class="form-group">
      <label for="customer_email" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_email'), null); ?><!-- <i class="required">*</i> -->
      </label>
      <div class="col-sm-7">
        <input type="email" class="form-control" id="customer_email" value="<?php echo isset($request->post['customer_email']) ? $request->post['customer_email'] : null; ?>" name="customer_email">
      </div>
    </div>

    <div class="form-group">
      <label for="customer_mobile" class="col-sm-3 control-label">
        <?php echo 'Country Code'; ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
       
          <select  id="mobile_cc" class="form-control" name="mobile_cc" >
            <option value="">Country Code</option>
            <?php
              foreach($country_codes as $code){?>
                <option value="<?php echo $code['code'] ?>" 
                  <?php if($code['code'] == '91') { echo 'selected';} ?>>
                  <?php echo $code['name'] ?>(<?php echo $code['code'] ?>)
                </option>
              <?php }
            ?>
        </select>
       </div>
    </div>

    <div class="form-group">
      <label for="customer_mobile" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_phone'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" maxlength="10" class="form-control" id="customer_mobile" value="<?php echo isset($request->post['customer_mobile']) ? $request->post['customer_mobile'] : null; ?>" name="customer_mobile">
      </div>
    </div>

     <div class="form-group">
      <label for="alt_contact_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_AlternateNumber'), null); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" maxlength="10" class="form-control number" id="alt_contact_number" value="<?php echo isset($request->post['alt_contact_number']) ? $request->post['alt_contact_number'] : null; ?>" name="alt_contact_number">
      </div>
    </div>

    <div class="form-group">
      <label for="customer_sex" class="col-sm-3 control-label">
        <?php echo 'Gender'; ?>
      </label>
      <div class="col-sm-7">
        <select id="customer_sex" name="customer_sex" class="form-control" required>
          <option value="">--- Please select Gender ---</option>
          <option value="1"<?php echo isset($request->post['customer_sex']) && $request->post['customer_sex'] == '1' ? ' selected' : null; ?>>
            <?php echo $language->get('label_male'); ?>
          </option>
          <option value="2"<?php echo isset($request->post['customer_sex']) && $request->post['customer_sex'] == '2' ? ' selected' : null; ?>>
            <?php echo $language->get('label_female'); ?>
          </option>
          <option value="3"<?php echo isset($request->post['customer_sex']) && $request->post['customer_sex'] == '3' ? ' selected' : null; ?>>
            <?php echo $language->get('label_others'); ?>
          </option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="customer_age" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_age'), null); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" maxlength="3" class="form-control number" id="customer_age" value="<?php echo isset($request->post['customer_age']) ? $request->post['customer_age'] : null; ?>" name="customer_age" onKeyUp="if(this.value>140){this.value='140';}else if(this.value<0){this.value='0';}">
      </div>
    </div>

    <div class="form-group birt-custo">
      <label for="date" class="col-sm-3 control-label bir-inn-label">
        <?php echo $language->get('label_BirthDate'); ?>
      </label>
      <div class="col-sm-7">
      <div class="bir-inn2">
        <select name="date" id="date_birth">
          <option value="">Date</option>
          <?php
            for($i=1;$i<=31;$i++){
              if($i <=9){
                $i = '0'.$i;
              }
              echo '<option value="'.$i.'">'.$i.'</option>';
            }
          ?>
        </select>
      </div>

      <!-- <label for="month" class="col-sm-2 control-label bir-inn-label1">
        <//?php echo 'Month'; ?><i class="required">*</i>
      </label> -->
      <div class="bir-inn2">
       <select name="month" id="date_month">
          <option value="">Month</option>
          <?php
          $months = ['January', 'February', 'March', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            foreach($months as $key=> $month){
              $key = $key+1;
              if($key <=9){
                $key = '0'.$key;
              }
              echo '<option value="'.$key.'">'.$month.'</option>';
            }
          ?>
        </select>
      </div>

      <!-- <label for="year" class="col-sm-2 control-label bir-inn-label1">
        <//?php echo 'Year'; ?>
      </label> -->
      <div class="bir-inn2">
       <select name="year" id="date_year">
          <option value="">Year</option>
          <?php
          $year = date('Y');
            for($i=1970;$i<=$year;$i++){
              echo '<option value="'.$i.'">'.$i.'</option>';
            }
          ?>
        </select>
      </div>
    </div>
    </div>

     <div style="display: none;" class="form-group">
      <label for="anniversary_date" class="col-sm-3 control-label">
        <?php echo $language->get('label_AnniversaryDate'); ?>
      </label>
      <div class="col-sm-7">
        <!-- <input type="text" class="form-control" id="anniversary_date" name="anniversary_date" value="<?php //echo isset($request->post['anniversary_date']) ? $request->post['anniversary_date'] : null; ?>"> -->
        <input type="date" class="form-control" id="anniversary_date" value="<?php echo isset($request->post['anniversary_date']) ? $request->post['e_date'] : null; ?>" name="anniversary_date" autocomplete="off">
      </div>
    </div>

        <!-- permanent_address -->
   <!--  <div class="form-group">
      <label for="" class="col-sm-3 control-label"></label>
      <div class="col-sm-7"><?php echo $language->get('label_permanent_address'); ?></div>
    </div> -->

    <div class="form-group">
      <label for="perma_address" class="col-sm-3 control-label">
        <?php echo $language->get('label_address'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="perma_address" name="perma_address" value="<?php echo isset($request->post['perma_address']) ? $request->post['perma_address'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="perma_city" class="col-sm-3 control-label">
        <?php echo $language->get('label_city'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="perma_city" name="perma_city" value="<?php echo isset($request->post['perma_city']) ? $request->post['perma_city'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="perma_state" class="col-sm-3 control-label">
        <?php echo $language->get('label_state'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="perma_state" name="perma_state" value="<?php echo isset($request->post['perma_state']) ? $request->post['perma_state'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="perma_zip_code" class="col-sm-3 control-label">
        <?php echo 'Pin Code'; ?>
      </label>
      <div class="col-sm-7">
        <input type="text" maxlength="6" class="form-control number" id="perma_zip_code" name="perma_zip_code" value="<?php echo isset($request->post['perma_zip_code']) ? $request->post['perma_zip_code'] : null; ?>">
      </div>
    </div>

    

        <!-- label_correspondence_address -->
    <div style="display: none;" class="form-group">
      <label for="label_correspondence_address" class="col-sm-3 control-label">
        <input type="checkbox" id="same_as_perm_addr_chk">Same as <?php echo $language->get('label_permanent_address'); ?>
      </label>
      <div class="col-sm-7"><?php echo $language->get('label_correspondence_address'); ?></div>
    </div>

    <div style="display: none;"  class="form-group">
      <label for="cor_address" class="col-sm-3 control-label">
        <?php echo $language->get('label_address'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="cor_address" name="cor_address" value="<?php echo isset($request->post['cor_address']) ? $request->post['cor_address'] : null; ?>">
      </div>
    </div>

    <div style="display: none;"  class="form-group">
      <label for="cor_city" class="col-sm-3 control-label">
        <?php echo $language->get('label_city'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="cor_city" name="cor_city" value="<?php echo isset($request->post['cor_city']) ? $request->post['cor_city'] : null; ?>">
      </div>
    </div>

    <div style="display: none;"  class="form-group">
      <label for="cor_state" class="col-sm-3 control-label">
        <?php echo $language->get('label_state'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="cor_state" name="cor_state" value="<?php echo isset($request->post['cor_state']) ? $request->post['cor_state'] : null; ?>">
      </div>
    </div>

    <div style="display: none;"  class="form-group">
      <label for="cor_zip_code" class="col-sm-3 control-label">
        <?php echo $language->get('label_zip_code'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" maxlength="6" class="form-control number" id="cor_zip_code" name="cor_zip_code" value="<?php echo isset($request->post['cor_zip_code']) ? $request->post['cor_zip_code'] : null; ?>">
      </div>
    </div>
<hr></hr>
    <!-- =============== -->

   <!--  <div class="form-group">
      <label for="customer_address" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_address'), null); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="customer_address" name="customer_address" value="<?php echo isset($request->post['customer_address']) ? $request->post['customer_address'] : null; ?>"></textarea>
      </div>
    </div> -->
<input type="hidden" name="status" value="1">
    <!-- <div class="form-group">
      <label for="status" class="col-sm-3 control-label">
        <?php echo $language->get('label_status'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select id="status" class="form-control" name="status" >
          <option <?php echo isset($request->post['status']) && $request->post['status'] == '1' ? 'selected' : null; ?> value="1">
            <?php echo $language->get('text_active'); ?>
          </option>
          <option <?php echo isset($request->post['status']) && $request->post['status'] == '0' ? 'selected' : null; ?> value="0">
            <?php echo $language->get('text_inactive'); ?>
          </option>
        </select>
      </div>
    </div> -->

   <!--  <div class="form-group">
      <label for="sort_order" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_sort_order'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="number" class="form-control" id="sort_order" value="<?php echo isset($request->post['sort_order']) ? $request->post['sort_order'] : 0; ?>" name="sort_order" required>
      </div>
    </div> -->
    <!-- ================================================ -->
   
    <div class="form-group">
      <label for="aadhar_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_aadhar_number'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" maxlength="12" class="form-control number" id="aadhar_number" name="aadhar_number" value="<?php echo isset($request->post['aadhar_number']) ? $request->post['aadhar_number'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="pan_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_pan'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="pan_number" name="pan_number" value="<?php echo isset($request->post['pan_number']) ? $request->post['pan_number'] : null; ?>">
      </div>
    </div>


    <div class="form-group">
      <label for="dl_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_driving_license'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="dl_number" name="dl_number" value="<?php echo isset($request->post['dl_number']) ? $request->post['dl_number'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="passport_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_passport_number'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="passport_number" name="passport_number" value="<?php echo isset($request->post['passport_number']) ? $request->post['passport_number'] : null; ?>">
      </div>
    </div>


    <div class="form-group">
      <label for="marital_status" class="col-sm-3 control-label">
        <?php echo $language->get('label_MaritalStatus'); ?>
      </label>
      <div class="col-sm-7">
        <select id="marital_status" name="marital_status" class="form-control" required>
          <option value="">--- Please select Marital Status ---</option>
          <option value="1"<?php echo isset($request->post['marital_status']) && $request->post['marital_status'] == '1' ? ' selected' : null; ?>>
            <?php echo $language->get('label_Married'); ?>
          </option>
          <option value="2"<?php echo isset($request->post['marital_status']) && $request->post['marital_status'] == '2' ? ' selected' : null; ?>>
            <?php echo $language->get('label_Widowed'); ?>
          </option>
          <option value="3"<?php echo isset($request->post['marital_status']) && $request->post['marital_status'] == '3' ? ' selected' : null; ?>>
            <?php echo $language->get('label_Divorced'); ?>
          </option>
          <option value="4"<?php echo isset($request->post['marital_status']) && $request->post['marital_status'] == '4' ? ' selected' : null; ?>>
            <?php echo $language->get('label_UnMarried'); ?>
          </option>
          <option value="5"<?php echo isset($request->post['marital_status']) && $request->post['marital_status'] == '5' ? ' selected' : null; ?>>
            <?php echo $language->get('label_others'); ?>
          </option>
        </select>
      </div>
    </div>

    

   

    <div class="form-group">
      <label for="occupation" class="col-sm-3 control-label">
        <?php echo $language->get('label_Occupation'); ?>
      </label>
      <div class="col-sm-7">
        <select id="occupation" name="occupation" class="form-control">
          <option value="">--- Please select Occupation ---</option>
          <option value="1"<?php echo isset($request->post['occupation']) && $request->post['occupation'] == '1' ? ' selected' : null; ?>>
            <?php echo $language->get('label_Businessman'); ?>
          </option>
          <option value="2"<?php echo isset($request->post['occupation']) && $request->post['occupation'] == '2' ? ' selected' : null; ?>>
            <?php echo $language->get('label_ServiceMan'); ?>
          </option>
        </select>
      </div>
    </div>
    <!-- Businessman -->
      <div class="form-group">
        <label for="busi_name" class="col-sm-3 control-label">
          <?php echo $language->get('label_NameofBusiness'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="busi_name" name="busi_name" value="<?php echo isset($request->post['busi_name']) ? $request->post['busi_name'] : null; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="busi_address" class="col-sm-3 control-label">
          <?php echo $language->get('label_address'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="busi_address" name="busi_address" value="<?php echo isset($request->post['busi_address']) ? $request->post['busi_address'] : null; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="busi_contact_number" class="col-sm-3 control-label">
          <?php echo $language->get('label_contact_number'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" maxlength="10" class="form-control number" id="busi_contact_number" name="busi_contact_number" value="<?php echo isset($request->post['busi_contact_number']) ? $request->post['busi_contact_number'] : null; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="busi_annual_turnover" class="col-sm-3 control-label">
          <?php echo $language->get('label_AnnualTurnover'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control decimal" id="busi_annual_turnover" name="busi_annual_turnover" value="<?php echo isset($request->post['busi_annual_turnover']) ? $request->post['busi_annual_turnover'] : null; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="busi_trans_to_office" class="col-sm-3 control-label">
          <?php echo $language->get('label_Modeoftransport'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="busi_trans_to_office" name="busi_trans_to_office" value="<?php echo isset($request->post['busi_trans_to_office']) ? $request->post['busi_trans_to_office'] : null; ?>">
        </div>
      </div>
    <!-- Service Man -->
    <div id="serviceman">
      <div class="form-group">
        <label for="ser_man_designation" class="col-sm-3 control-label">
          <?php echo $language->get('label_Designation'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="ser_man_designation" name="ser_man_designation" value="<?php echo isset($request->post['ser_man_designation']) ? $request->post['ser_man_designation'] : null; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="ser_man_comp_name" class="col-sm-3 control-label">
          <?php echo $language->get('label_company_name'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="ser_man_comp_name" name="ser_man_comp_name" value="<?php echo isset($request->post['ser_man_comp_name']) ? $request->post['ser_man_comp_name'] : null; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="ser_man_comp_address" class="col-sm-3 control-label">
          <?php echo $language->get('label_CompanyAddress'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="ser_man_comp_address" name="ser_man_comp_address" value="<?php echo isset($request->post['ser_man_comp_address']) ? $request->post['ser_man_comp_address'] : null; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="ser_man_comp_cont_number" class="col-sm-3 control-label">
          <?php echo $language->get('label_Companycontactnumber'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" maxlength="10" class="form-control number" id="ser_man_comp_cont_number" name="ser_man_comp_cont_number" value="<?php echo isset($request->post['ser_man_comp_cont_number']) ? $request->post['ser_man_comp_cont_number'] : null; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="ser_man_trans_to_office" class="col-sm-3 control-label">
          <?php echo $language->get('label_Modeoftransport'); ?>
        </label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="ser_man_trans_to_office" name="ser_man_trans_to_office" value="<?php echo isset($request->post['ser_man_trans_to_office']) ? $request->post['ser_man_trans_to_office'] : null; ?>">
        </div>
      </div>
    </div>
    <!-- ========= Bank Details ======== -->
    <div class="form-group" style="display: none;">
      <label for="label_bank_details" class="col-sm-3 control-label"></label>
      <div class="col-sm-7"><?php echo $language->get('label_bank_details'); ?></div>
    </div>

    <div class="form-group">
      <label for="bank_name" class="col-sm-3 control-label">
        <?php echo $language->get('label_bank_name'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="bank_name" name="bank_name" value="<?php echo isset($request->post['bank_name']) ? $request->post['bank_name'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="bank_acc_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_acc_number'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" maxlength="20" class="form-control" id="bank_acc_number" name="bank_acc_number" value="<?php echo isset($request->post['bank_acc_number']) ? $request->post['bank_acc_number'] : null; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="ifsc_code" class="col-sm-3 control-label">
        <?php echo $language->get('label_ifsc_code'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" maxlength="10" class="form-control" id="ifsc_code" name="ifsc_code" value="<?php echo isset($request->post['ifsc_code']) ? $request->post['ifsc_code'] : null; ?>">
      </div>
    </div>
    <!-- ========== Vehicle Details ========== -->
    <div style="display:none;" class="form-group">
      <label for="label_VehicleDetails" class="col-sm-3 control-label"></label>
      <div class="col-sm-7"><?php echo $language->get('label_VehicleDetails'); ?></div>
    </div>

     <div style="display:none;" class="form-group">
      <label for="number_of_vehicles" class="col-sm-3 control-label">
        <?php echo $language->get('label_Noofvehiclesowned'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="number_of_vehicles" name="number_of_vehicles" value="<?php echo isset($request->post['number_of_vehicles']) ? $request->post['number_of_vehicles'] : null; ?>">
      </div>
    </div>

    <div style="display:none;" class="form-group">
      <label for="vehicle_model" class="col-sm-3 control-label">
        <?php echo $language->get('label_VehicleModel'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="vehicle_model" name="vehicle_model" value="<?php echo isset($request->post['vehicle_model']) ? $request->post['vehicle_model'] : null; ?>">
      </div>
    </div>

    <div style="display:none;" class="form-group">
      <label for="vehicle_number" class="col-sm-3 control-label">
        <?php echo $language->get('label_VehicleNumber'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="vehicle_number" name="vehicle_number" value="<?php echo isset($request->post['vehicle_number']) ? $request->post['vehicle_number'] : null; ?>">
      </div>
    </div>
    <!-- ================================================ -->
    <!-- Family Members details (tabular Format) -->
    <div class="form-group family-cust">
      <label for="vehicle_number" class="col-sm-3 control-label">Family Members</label>
      <div class="col-sm-7">
        <table class="table table-bordered" id="add-customer-table">
          <thead>
            <tr>
              <th><?php echo $language->get('label_box_name'); ?></th>
              <th><?php echo $language->get('label_DOB'); ?></th>
              <th><?php echo $language->get('label_Relationshipwithcustomer'); ?></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr class="t-row rem-row-0">
              <td><input type="text" class="form-control" id="member_name" name="member_name[]" 
                  value="" ></td>
              <td> 
                <input type="text" autocomplete="off" readonly="true" class="form-control pick_date" name="member_dob[]" value="">
              </td>
              <td>
                <input type="text" class="form-control" id="member_relationship" name="member_relationship[]" value="">
              </td>
              <td class="btn-cus-det">
                <button type="button" class="btn btn-sm btn-block btn-danger addremoveBtn"><i class="fa fa-fw fa-trash"></i></button>
                <button type="button" class="addBtn btn-add-cus-fam"><i class="fa fa-plus" aria-hidden="true"></i></button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7">
        <button class="btn btn-info" id="create-customer-submit" type="submit" name="create-customer-submit" data-form="#create-customer-form" data-loading-text="Saving...">
          <span class="fa fa-fw fa-save"></span>
          <?php echo $language->get('button_save'); ?>
        </button> 
        <button type="reset" class="btn btn-danger" id="reset" name="reset"><span class="fa fa-fw fa-circle-o"></span>
          <?php echo $language->get('button_reset'); ?>
        </button>
      </div>
    </div>

  </div>
</form>
<script type="text/javascript">
  $(document).on('change', '#same_as_perm_addr_chk', function(){
    if($(this).prop("checked") == true){
      $('#cor_address').val($('#perma_address').val());
      $('#cor_city').val($('#perma_city').val());
      $('#cor_state').val($('#perma_state').val());
      $('#cor_zip_code').val($('#perma_zip_code').val());

    }
    else if($(this).prop("checked") == false){
      $('#cor_address').val('');
      $('#cor_city').val('');
      $('#cor_state').val('');
      $('#cor_zip_code').val('');
    }
  });

  $(document).on('change', '#occupation', function() {
    if ( $(this).val()==1 ) {
      $('#businessman').show();
      $('#serviceman').hide();
    }
    else {
      $('#serviceman').show();
      $('#businessman').hide();
    }
  });
  $(document).on('click', '.addBtn', function(){
    var existRows = $('#add-customer-table > tbody > tr.t-row');
  existRows = existRows.length;
  var newRow = `
              <tr class="t-row rem-row-`+existRows+`">
          <td><input type="text" class="form-control" id="member_name" name="member_name[]" 
              value="" ></td>
          <td> <input type="text" class="form-control pick_date" autocomplete="off" readonly="true" name="member_dob[]" value=""></td>
          <td>
            <input type="text" class="form-control" id="member_relationship" name="member_relationship[]" value="">
          </td>
          <td class="btn-cus-det">
            <button type="button" class="btn-sm btn-block btn-danger addremoveBtn"><i class="fa fa-fw fa-trash"></i></button>
            <button type="button" class="addBtn btn-add-cus-fam"><i class="fa fa-plus" aria-hidden="true"></i></button>
          </td>
        </tr>
                `;
    $('#add-customer-table > tbody').append(newRow);
  });
  $(document).on('click', '.addremoveBtn', function(){
    var numRows = $('#add-customer-table > tbody > tr.t-row').length;
    if (numRows>1) {
      $(this).parent('td').parent('tr').remove();  
    }
    else{
      $(this).parent('td').parent('tr').find('input, textarea, select').val('');
    }
  })
  
$( document ).ready(function() {
  $('#businessman').show();
  $('#serviceman').hide();
});
$('body').on('focus',".pick_date", function(){
  var todayDate = new Date().getFullYear();
  var endD= new Date(new Date().setDate(todayDate - 15));
  var currDate = new Date();
  $(this).datepicker({
    autoclose: true,
    todayHighlight: true,
    orientation: "top auto",
    format: "dd M yyyy",
  });
});
</script>