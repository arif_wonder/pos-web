<?php include ("../../_init.php");

$language->load('pos'); ?>

<!-- <form class="form-inline" id="pay-form" action="pos_processing.php"> -->

<input type="hidden" name="invoice-id" value="{{ invoiceId }}">
<input type="hidden" name="customer-id" value="{{ customerId }}">

<textarea class="hidden" name="invoice-note">{{ invoiceNote }}</textarea>

	<div class="row">
		<div class="col-sm-6">
			<div  class="panel panel-default line min-height">
				
				<div class="panel-heading">
					<h4 class="panel-title">
						<span>
							<?php echo $language->get('text_summary'); ?>
							
						</span>
					</h4>
				</div>

				<div class="panel-body">

					<table class="table table-bordered">
						
						<tbody>
							<tr>
								<td class="text-right">Sl no.</td>
								<td>Particulars(Qty)</td>
								<td class="text-right">Price</td>
								<td class="text-right">Net Amount</td>
							</tr>
							<tr ng-repeat="items in itemArray">
								<td class="text-right w-ten">
									<input type="hidden" name="product-item['{{ items.id }}'][item_id]" value="{{ items.id }}">
									<input type="hidden" name="product-item['{{ items.id }}'][category_id]" value="{{ items.categoryId }}">
									<input type="hidden" name="product-item['{{ items.id }}'][sup_id]" value="{{ items.supId }}">
									<input type="hidden" name="product-item['{{ items.id }}'][item_name]" value="{{ items.name }}">
									<input type="hidden" name="product-item['{{ items.id }}'][item_price]" value="{{ items.price  | formatDecimal:2 }}">
									<input type="hidden" name="product-item['{{ items.id }}'][item_quantity]" value="{{ items.quantity }}">
									<input type="hidden" name="product-item['{{ items.id }}'][item_total]" value="{{ items.subTotal  | formatDecimal:2 }}">
									{{ $index+1 }}
								</td>
								<td class="w-seventy">{{ items.product.buying_item.item_name }}
								({{ items.quantity }})</td>
								<td class="w-seventy text-right">{{ items.product.buying_item.item_selling_price | formatDecimal:2 }}</td>

								<td class="text-right w-twenty">
									{{ items.price_after_discount | formatDecimal:2 }}
								</td>
							</tr>
						</tbody>

						<tfoot>
							<tr>
								<th class="text-right w-sixty" colspan="3">
									<?php echo $language->get('label_subtotal'); ?>
								</th>
								<input type="hidden" name="sub-total" value="{{ totalAmount }}">
								<td class="text-right w-fourty">
								{{ subTotalWithoutTax(totalPrice,totalTax)  | formatDecimal:2 }}</td>
							</tr>
							<tr>
								<th class="text-right w-sixty"  colspan="3">
									<?php echo 'Total '.$language->get('label_discount'); ?> 
								</th>
								<input type="hidden" name="discount-amount" value="{{ discountType  == 'percentage' ? _percentage(totalAmount, discountAmount) : discountAmount }}">
								<input type="hidden" name="discount-type" value="{{ discountType }}">
								<td class="text-right w-fourty" >
								{{ totalDiscount(productDiscount,coupan_code_discount) | formatDecimal:2 }}
							</td>
							</tr>
							<tr>
								<th class="text-right w-sixty" colspan="3">
									<?php echo 'Tax'; ?>
								</th>
								<input type="hidden" name="tax-amount" value="{{ taxAmount }}">
								<td class="text-right w-fourty">{{ totalTax | formatDecimal:2 }}</td>
							</tr>
							<tr>
								<th class="text-right w-sixty" colspan="3">
									<?php echo $language->get('label_previous_due'); ?>
								</th>
								<input type="hidden" name="previous-due" value="{{ dueAmount }}">
								<td class="text-right w-fourty">{{ dueAmount  | formatDecimal:2 }}</td>
							</tr>
							<tr>
								<th class="text-right w-sixty" colspan="3">
									<?php echo $language->get('label_payable_amount'); ?>
									<small>({{ totalItem }} items)</small>
								</th>
								<input type="hidden" name="payable-amount" value="{{ totalPayable }}">
								<td class="text-right w-fourty">{{ dueAmount+(totalPrice-coupan_code_discount)| formatDecimal:2}}</td>
							</tr>
							<tr ng-show="done">
								<th class="text-right w-sixty" colspan="3">
									<?php echo $language->get('label_paid_amount'); ?>
								</th>
								<input type="hidden" name="paid-amount" value="{{ paidAmount }}">
								<td class="text-right w-fourty">
								{{ invoiceInfo.paid_amount  | formatDecimal:2 }}
							</td>
							</tr>
							<tr ng-show="done">
								<th class="text-right w-sixty" colspan="3">
									<?php echo $language->get('label_due'); ?>
								</th>
								<td class="text-right w-fourty">
								{{ invoiceInfo.present_due  | formatDecimal:2 }}</td>
							</tr>
							<tr colspan="3"><td>&nbsp;</td></tr>
							<tr ng-show="invoiceNote" class="active">
								<td colspan="3">
									<b><?php echo $language->get('label_note'); ?>:</b> {{ invoiceNote }}
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<!-- col end -->

		<div class="col-sm-6">
			<div class="panel panel-default">
				<div class="panel-body text-center">

					<h2 ng-show="done" class="done-meseage title">
						<i class="fa fa-check"></i>
						<?php echo $language->get('label_payment_received'); ?> 
						<div class="invoice-id">
							<?php echo $language->get('label_invoice_id'); ?> &rarr; 
							<a href="invoice-print.php?invoice_id={{ invoiceId }}" target="_blink">{{ invoiceId }}
							</a>
						</div>
					</h2>
					
					<div ng-show="!done" class="form-group form-group-lg mb-20">
						<div class="col-sm-12">
							<h2 class="title">
								<?php echo 'Total Payable Amount'; ?>
							</h2>
						</div>
						<input type="hidden" id="payable-amount_hidden" value="{{ dueAmount+(totalPrice)| formatDecimal:2 }}">

						<div class="col-sm-12">
							<input type="text" class="form-control text-center paid-bg" id="payable-amount" value="{{ dueAmount+(totalPrice-coupan_code_discount)| formatDecimal:2 }}" autocomplete="off" autofocus ng-disabled="notificationSent">
							<div ng-if="creditAmount > 0" class="paybywallet">
								<input ng-click="checkBoxChange()" type="checkbox" name="pay_by_credit" id="pay_by_credit" ng-disabled="notificationSent">
								<label>
									Use Credit({{ creditAmount | formatDecimal:2 }})
								</label>
							</div>
						</div>
					</div>

					<div ng-show="!done && !notificationSent" class="form-group mt-20">
						<?php foreach(get_payment_methods() as $payment) : ?>
							<button ng-click="pay(<?php echo $payment['payment_id']; ?>, '<?php echo $payment['name']; ?>');" onClick="return false;" id="payment-method-<?php echo $payment['payment_id']; ?>" class="btn btn-warning" type="button">
								<span class="fa fa-fw fa-money"></span> <?php echo $payment['name']; ?>
							</button>
						<?php endforeach; ?>
					</div>
					<div ng-show="!done && notificationSent" class="form-group mt-20">
						
							<button ng-click="pay(paymentNameLater,paymentName, reference_id, paymentidLater);" onClick="return false;" id="payment-method" class="btn btn-warning" type="button">
								<span class="fa fa-fw fa-money"></span>
								Confirm Payment
							</button>
						
					</div>

				</div>
			</div>
<!-- //ng-show="done" -->
			<div ng-show="done" class="panel panel-default">
				<div class="panel-body text-center">
					<div>
						<div class="btn-group">

							<a style="background-color: green;border: 1px solid #408104;" class="btn btn-sm btn-warning" ng-click="whatsapp(invoiceId)" onClick="return false;" href="#"><span class="fa fa-fw fa-envelope"></span>
								<?php echo 'WhatsApp'; ?>
							</a>
							<a class="btn btn-sm btn-warning" ng-click="sendInvoiceViaSms(invoiceId)" onClick="return false;" href="#"><span class="fa fa-fw fa-envelope"></span>
								<?php echo 'SMS'; ?>
							</a>

							<a class="btn btn-sm btn-info" href="invoice-print.php?invoice_id={{invoiceId}}&print=true" target="_blink">
								<span class="fa fa-fw fa-print"></span>
								<?php echo $language->get('label_print_receipt'); ?>
							</a> 
							
						</div>
					</div>
					<div class="text-center pos-payment-done">
						<button ng-click="closePayNowModal();" id="done" class="btn btn-block btn-lg btn-success" type="button">
							<span class="fa fa-fw fa-check"></span>
							<?php echo $language->get('button_done'); ?>
						</button>
					</div>
				</div>
			</div>
		</div>
		<!-- col end -->
	</div>
<!-- </form> -->

<script type="text/javascript">
$(function() {
	$("#pay-form").on("keyup keypress", function(e) {
	    var keyCode = e.keyCode || e.which;
	    if (keyCode == 13) {
	        e.preventDefault();
	    }
	});
});
</script>