<?php $language->load('management'); ?>
<h4 class="sub-title">
  <?php echo $language->get('text_update_title'); ?>
</h4>
<form id="disc_by_coupons_edit_form" class="form-horizontal" action="discount_by_coupons.php?box_state=open" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <input type="hidden" id="coupons_id" name="coupons_id" value="<?php echo $data['id']; ?>">

  <div class="box-body">
    <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="title" value="<?php echo $data['title']; ?>" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo $data['description']; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="code" class="col-sm-3 control-label">
        <?php echo $language->get('label_code'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="code" value="<?php echo $data['code']; ?>" name="code">
      </div>
    </div>

    <div class="form-group">
      <label for="valid_till" class="col-sm-3 control-label">
        <?php echo $language->get('label_valid_till'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <div class='input-group date' id='valid_till'>
          <input type='text' class="form-control" name="valid_till" value="<?php echo date('d M Y H:i:s', strtotime($data['valid_till'])); ?>" style="cursor: pointer;"/>
          <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="discount" value="<?php echo $data['discount']; ?>" name="discount">
      </div>
    </div>

    <div class="form-group">
      <label for="discount_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="discount_type" id="discount_type">
          <option <?php echo $data['discount_type']=='fixed' ? "selected='selected'" : '';?> value="fixed">Fixed</option>
          <option <?php echo $data['discount_type']=='precentage' ? "selected='selected'" : '';?> value="precentage">Precentage</option>
       </select>
      </div>
    </div>

   <!--  <div class="form-group">
      <label for="discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="discount" value="<?php echo $data['discount']; ?>" name="discount">
      </div>
    </div> -->

     <div class="form-group">
      <label for="min_purchase_amount" class="col-sm-3 control-label">
        <?php echo $language->get('label_min_purchase_amount'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control number" id="min_purchase_amount" value="<?php echo $data['min_purchase_amount']; ?>" name="min_purchase_amount">
      </div>
    </div>

    <div class="form-group">
      <label for="max_discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_max_discount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="max_discount" value="<?php echo $data['max_discount']; ?>" name="max_discount">
      </div>
    </div>

   

    <div class="form-group">
      <label for="max_discount" class="col-sm-3 control-label">
        <?php echo 'Usage Limit'; ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="limit" value="<?php echo $data['limit_per_user']; ?>" name="limit">
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7">
        <button class="btn btn-info" id="discbycoupons-update-submit" type="button" name="create-product-brand-submit" data-form="#disc_by_coupons_edit_form" data-datatable="product-brand-list" data-loading-text="Saving...">
          <span class="fa fa-fw fa-save"></span>
          <?php echo 'Update'; ?>
        </button>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(function () {
  $('#valid_till').datetimepicker({
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,   
    changeMonth: true,
    startDate : new Date(),
  });
});
</script>