<?php $language->load('brand'); ?>
<h4 class="sub-title">
  <?php echo $language->get('text_update_title'); ?>
</h4>
<form id="update-product-brand-form" class="form-horizontal" action="brand.php?box_state=open" method="post">
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <input type="hidden" id="brand_id" name="brand_id" value="<?php echo $brand['brand_id']; ?>">

  <div class="box-body">

    <div class="form-group">
      <label for="brand_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_brand'),$language->get('text_product')); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="brand_name" value="<?php echo $brand['brand_name']; ?>" name="brand_name">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="brand_description" name="brand_description" rows="3"><?php echo $brand['brand_description']; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7">
        <button id="product-brand-update-submit" data-form="#update-product-brand-form" data-datatable="#product-brad-list" class="btn btn-info" name="product-brand-update-submit" data-loading-text="Updating...">
            <span class="fa fa-fw fa-pencil"></span>
            <?php echo $language->get('button_update'); ?>
          </button>
      </div>
    </div>
    
  </div>
</form>