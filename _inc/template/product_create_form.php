<?php $language->load('product');?>

<form id="create-product-form" class="form-horizontal" action="product.php?box_state=open" method="post">
  <input type="hidden" id="action_type" name="action_type" value="CREATE">

  <div class="box-body">

    <div class="form-group">
      <label for="p_image" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_image'), null); ?>
      </label>
      <div class="col-sm-7">
        <div class="preview-thumbnail">
          <a ng-click="POSFilemanagerModal({target:'p_image',thumb:'p_thumb'})" onClick="return false;" href="#" data-toggle="image" id="p_thumb">
            <img src="../assets/wonderpillars/img/noimage.jpg" alt="">
          </a>
          <input type="hidden" name="p_image" id="p_image" value="">
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="p_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_name'), $language->get('text_product')); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="p_name" value="<?php echo isset($request->post['p_name']) ? $request->post['p_name'] : null; ?>" name="p_name" required>
      </div>
    </div>
    <div class="form-group">
      <label for="is_gift_item" class="col-sm-3 control-label">
        <?php echo 'Sell As Gift Item'; ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <label><input type="radio" class="is_gift_item" id="is_gift_item_no" <?php echo ((isset($request->post['is_gift_item']) && $request->post['is_gift_item'] == 0) || !isset($request->post['is_gift_item'])) ? 'checked' : ''; ?> name="is_gift_item" value="0" /> No</label>
        <label><input type="radio" class="is_gift_item" id="is_gift_item_yes" <?php echo (isset($request->post['is_gift_item']) && ($request->post['is_gift_item'] == 1)) ? 'checked' : ''; ?> name="is_gift_item" value="1" /> Yes</label>
      </div>
    </div>
    <!-- *********************************************** -->

    <div class="form-group">
      <label for="p_discount_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="p_discount_type" id="p_discount_type">
          <option value="fixed">Fixed</option>
          <option value="precentage">Percentage</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="p_discount" class="col-sm-3 control-label">
        <?php echo 'Discount amount/percentage'; ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="p_discount" value="<?php echo isset($request->post['p_discount']) ? $request->post['p_discount'] : null; ?>" name="p_discount">
      </div>
    </div>

    

    <div class="form-group">
      <label for="p_brand" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_brand'), $language->get('text_product')); ?> <i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <!-- <input type="text" class="form-control" id="p_brand" value="<?php //echo isset($request->post['p_brand']) ? $request->post['p_brand'] : null; ?>" name="p_brand"> -->
        <div class="{{ !hideSupAddBtn ? 'input-group' : null }}">
          <select id="p_brand" class="form-control select2" name="p_brand" required>
            <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php foreach (get_brand_tree() as $key => $brand) {?>
              <option value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']; ?></option>
            <?php }?>
          </select>
          <a class="input-group-addon" href="brand.php" target="_blank">
            <i class="fa fa-plus"></i>
          </a>
        </div>
      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_quantity" class="col-sm-3 control-label">
        <?php echo $language->get('label_quantity'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="p_quantity" value="<?php echo isset($request->post['p_quantity']) ? $request->post['p_quantity'] : null; ?>" name="p_quantity" required>
      </div>
    </div>

    <div class="form-group">
      <label for="p_unit" class="col-sm-3 control-label">
        <?php echo $language->get('label_unit'); ?> <i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <!-- <input type="text" class="form-control" id="p_unit" value="<?php //echo isset($request->post['p_unit']) ? $request->post['p_unit'] : null; ?>" name="p_unit"> -->

        <div class="{{ !hideSupAddBtn ? 'input-group' : null }}">
          <select id="p_unit" class="form-control select2" name="p_unit" required>
            <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php foreach (get_unites_tree() as $key => $unites) {?>
              <option value="<?php echo $unites['title']; ?>"><?php echo $unites['title']; ?></option>
            <?php }?>
          </select>
          <a class="input-group-addon" href="unites.php" target="_blank">
            <i class="fa fa-plus"></i>
          </a>
        </div>

      </div>
    </div>



    <div class="form-group">
      <label for="p_tax_rate" class="col-sm-3 control-label">
        <?php echo $language->get('label_tax_rates'); ?> <i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <!-- <input type="text" class="form-control" id="p_unit" value="<?php //echo isset($request->post['p_unit']) ? $request->post['p_unit'] : null; ?>" name="p_unit"> -->

        <div class="">
          <select id="p_tax_rate" class="form-control select2" name="p_taxrate" required>
            <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php foreach (get_taxrates_tree() as $key => $taxrates) {?>
              <option value="<?php echo $taxrates['tax_rate_id']; ?>"><?php echo $taxrates['rate'] . ' %'; ?></option>
            <?php }?>
          </select>
         
        </div>

      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_regular_price" class="col-sm-3 control-label">
        <?php echo $language->get('label_regular_price'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="p_regular_price" value="<?php echo isset($request->post['p_regular_price']) ? $request->post['p_regular_price'] : null; ?>" name="p_regular_price" required>
      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_special_price" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_special_price'), $language->get('text_product')); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="p_special_price" value="<?php echo isset($request->post['p_special_price']) ? $request->post['p_special_price'] : null; ?>" name="p_special_price">
      </div>
    </div>

    <div class="form-group">
      <label for="p_alert_qty" class="col-sm-3 control-label">
        <?php echo 'Stock Alert Quantity'; ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="number form-control" id="p_alert_qty" value="<?php echo isset($request->post['p_alert_qty']) ? $request->post['p_alert_qty'] : null; ?>" name="p_alert_qty">
      </div>
    </div>

<!--  -->
    <div class="form-group d-none">
      <label for="p_sp_fromdate" class="col-sm-3 control-label">
        Special Price From Date
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control pick_date" id="p_sp_fromdate" value="<?php echo isset($request->post['p_sp_fromdate']) ? $request->post['p_sp_fromdate'] : null; ?>" name="p_sp_fromdate">
      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_sp_todate" class="col-sm-3 control-label">
        Special Price To Date
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control pick_date" id="p_sp_todate" value="<?php echo isset($request->post['p_sp_todate']) ? $request->post['p_sp_todate'] : null; ?>" name="p_sp_todate">
      </div>
    </div>
    <!--  -->

    <div class="form-group">
      <label for="p_gift_item" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_gift_item'), $language->get('text_product')); ?>
      </label>
      <div class="col-sm-7">
        <select id="p_gift_item" class="form-control select2" name="p_gift_item">
          <option value=""><?php echo $language->get('text_select'); ?></option>
          <?php
foreach (get_gift_item_tree('') as $key => $product) {?>
          <option value="<?php echo $product['p_id']; ?>">
            <?php echo $product['p_name'] . ' (' . $product['p_code'] . ')'; ?>
          </option>
          <?php }?>
        </select>
        <!-- <input type="text" class="form-control" id="p_gift_item" value="<?php// echo isset($request->post['p_gift_item']) ? $request->post['p_gift_item'] : null; ?>" name="p_gift_item"> -->
      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_bar_code" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_bar_code'), $language->get('text_product')); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="p_bar_code" value="<?php echo isset($request->post['p_bar_code']) ? $request->post['p_bar_code'] : null; ?>" name="p_bar_code">
      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_purchage_price" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_purchage_price'), $language->get('text_product')); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="p_purchage_price" value="<?php echo isset($request->post['p_purchage_price']) ? $request->post['p_purchage_price'] : null; ?>" name="p_purchage_price">
      </div>
    </div>
    <!-- ==================== -->

    <div class="form-group">
      <label for="category_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_category'); ?> <i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <div class="{{ !hideSupAddBtn ? 'input-group' : null }}">
          <select id="category_id" class="form-control select2" name="category_id" required>
            <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php foreach (get_category_tree(array('filter_fetch_all' => true)) as $category_id => $category_name) {?>
              <option value="<?php echo $category_id; ?>"><?php echo $category_name; ?></option>
            <?php }?>
          </select>
          <a ng-hide="hideCategoryAddBtn" class="input-group-addon" ng-click="createNewCategory();" onClick="return false;" href="category.php?box_state=open">
            <i class="fa fa-plus"></i>
          </a>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_supplier'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <div class="{{ !hideSupAddBtn ? 'input-group' : null }}">
          <select id="sup_id" class="form-control" name="sup_id" required>
            <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php foreach (get_suppliers() as $supplier): ?>
              <option value="<?php echo $supplier['sup_id']; ?>">
                <?php echo $supplier['sup_name']; ?>
              </option>
            <?php endforeach;?>
          </select>
          <a ng-hide="hideSupAddBtn" class="input-group-addon" ng-click="createNewSupplier();" onClick="return false;" href="supplier.php?box_state=open">
            <i class="fa fa-plus"></i>
          </a>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="box_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_box'); ?>
       <!--  <i class="required">*</i> -->
      </label>
      <div class="col-sm-7">
        <div class="{{ !hideBoxAddBtn ? 'input-group' : null }}">
          <select id="box_id" class="form-control" name="box_id" required>
            <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php foreach (get_boxes() as $box_row): ?>
              <option value="<?php echo $box_row['box_id']; ?>">
                <?php echo $box_row['box_name']; ?>
              </option>
            <?php endforeach;?>
          </select>
          <a ng-hide="hideBoxAddBtn" class="input-group-addon" ng-click="createNewBox();" onClick="return false;" href="box.php?box_state=open">
            <i class="fa fa-plus"></i>
          </a>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="e_date" class="col-sm-3 control-label">
        <?php echo 'Expiry date'; ?>
      </label>
      <div class="col-sm-7">
        <input type="date" class="form-control" id="e_date" value="<?php echo isset($request->post['e_date']) ? $request->post['e_date'] : null; ?>" name="e_date" autocomplete="off" required>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label">
        <?php echo $language->get('label_store'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7 store-selector">
        <div class="checkbox selector">
          <label>
            <input type="checkbox" onclick="$('input[name*=\'product_store\']').prop('checked', this.checked);"> Select / Deselect
          </label>
        </div>
        <div class="filter-searchbox">
            <input ng-model="search_store" class="form-control" type="text" id="search_store" placeholder="<?php echo $language->get('search'); ?>">
        </div>
        <div class="well well-sm store-well">
          <div filter-list="search_store">
            <?php foreach (get_stores() as $the_store): ?>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="product_store[]" value="<?php echo $the_store['store_id']; ?>" <?php echo $the_store['store_id'] == store_id() ? 'checked' : null; ?>>
                  <?php echo ucfirst($the_store['name']) . ' (' . ucfirst($the_store['area']) . ') '; ?>
                </label>
              </div>
            <?php endforeach;?>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo isset($request->post['description']) ? $request->post['description'] : null; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="status" class="col-sm-3 control-label">
        <?php echo $language->get('label_status'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select id="status" class="form-control" name="status" >
          <option <?php echo isset($request->post['status']) && $request->post['status'] == '1' ? 'selected' : null; ?> value="1">
            <?php echo $language->get('text_active'); ?>
          </option>
          <option <?php echo isset($request->post['status']) && $request->post['status'] == '0' ? 'selected' : null; ?> value="0">
            <?php echo $language->get('text_inactive'); ?>
          </option>
        </select>
      </div>
    </div>

    <div class="form-group all">
      <label for="p_code" class="col-sm-3 control-label">
        <?php echo $language->get('label_bar_code'); ?> <i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <div class="input-group">
          <input type="text" name="p_code" id="p_code" class="form-control" autocomplete="off" required>
          <span id="random_num" class="input-group-addon pointer random_num" title="Generate new Barcode">
              <i class="fa fa-random"></i>
          </span>
        </div>
      </div>
    </div>

<input type="hidden" id="sort_order" value="0" name="sort_order">

   <!--  <div class="form-group">
      <label for="sort_order" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_sort_order'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="number" class="form-control" id="sort_order" value="<?php echo isset($request->post['sort_order']) ? $request->post['sort_order'] : 0; ?>" name="sort_order" required>
      </div>
    </div> -->

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7">
        <button class="btn btn-info" id="create-product-submit" type="submit" name="create-product-submit" data-form="#create-product-form" data-datatable="product-product-list" data-loading-text="Saving...">
          <span class="fa fa-fw fa-save"></span>
          <?php echo $language->get('button_save'); ?>
        </button>
        <button type="reset" class="btn btn-danger" id="reset" name="reset">
          <span class="fa fa-circle-o"></span>
         <?php echo $language->get('button_reset'); ?></button>
      </div>
    </div>

  </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
  setTimeout(function() {
    //$("#random_num").trigger("click");
  }, 1000);
})
$('body').on('focus',".pick_date", function(){
  var todayDate = new Date().getFullYear();
  var endD= new Date(new Date().setDate(todayDate - 15));
  var currDate = new Date();
  $(this).datepicker({
    autoclose: true,
    todayHighlight: true,
    orientation: "top auto",
    format: "dd M yyyy",
  });
});


  var mindate = new Date()
  $("#e_date").datepicker({
    format: 'yyyy-mm-d',
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,
    changeMonth: true,
    startDate : mindate,
  });

</script>