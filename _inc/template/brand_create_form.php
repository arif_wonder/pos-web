<?php $language->load('brand'); ?>

<form id="create-product-brand-form" class="form-horizontal" action="brand.php?box_state=open" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="CREATE">
  
  <div class="box-body">

    <div class="form-group">
      <label for="brand_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_brand'),$language->get('text_product')); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="brand_name" value="<?php echo isset($request->post['brand_name']) ? $request->post['brand_name'] : null; ?>" name="brand_name">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="brand_description" name="brand_description" rows="3"><?php echo isset($request->post['brand_description']) ? $request->post['brand_description'] : null; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7">
        <button class="btn btn-info" id="create-product-brand-submit" type="submit" name="create-product-brand-submit" data-form="#create-product-brand-form" data-datatable="product-brand-list" data-loading-text="Saving...">
          <span class="fa fa-fw fa-save"></span>
          <?php echo $language->get('button_save'); ?>
        </button>
        <button type="reset" class="btn btn-danger" id="reset" name="reset">
          <span class="fa fa-circle-o"></span>
         <?php echo $language->get('button_reset'); ?></button>
      </div>
    </div>
    
  </div>
</form>