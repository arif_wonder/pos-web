<?php $language->load('management'); ?>

<form id="disc_by_category_create_form" class="form-horizontal" action="discount_by_category.php?box_state=open" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="CREATE">
  
  <div class="box-body">

    <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="title" value="<?php echo isset($request->post['title']) ? $request->post['title'] : null; ?>" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo isset($request->post['description']) ? $request->post['description'] : null; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="category_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_category'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="category_id" id="category_id">
           <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php foreach (get_categories_tree() as $key => $category) { ?>
              <option value="<?php echo $category['category_id'] ; ?>"><?php echo $category['category_name'] ; ?></option>
            <?php } ?>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="date_from" class="col-sm-3 control-label">
        <?php echo $language->get('label_from_date'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control pick_date" id="date_from" value="<?php echo isset($request->post['date_from']) ? $request->post['date_from'] : null; ?>" name="date_from" style="cursor: pointer;">
      </div>
    </div>

    <div class="form-group">
      <label for="date_to" class="col-sm-3 control-label">
        <?php echo $language->get('label_to_date'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control pick_date" id="date_to" value="<?php echo isset($request->post['date_to']) ? $request->post['date_to'] : null; ?>" name="date_to" style="cursor: pointer;">
      </div>
    </div>

    <div class="form-group">
      <label for="discount_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="discount_type" id="discount_type">
          <option value="fixed">Fixed</option>
          <option value="precentage">Percentage</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="discount" value="<?php echo isset($request->post['discount']) ? $request->post['discount'] : null; ?>" name="discount">
      </div>
    </div>

     <div class="form-group">
      <label for="min_purchase_amount" class="col-sm-3 control-label">
        <?php echo $language->get('label_min_purchase_amount'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control number" id="min_purchase_amount" value="<?php echo isset($request->post['min_purchase_amount']) ? $request->post['min_purchase_amount'] : null; ?>" name="min_purchase_amount">
      </div>
    </div>

    <div class="form-group">
      <label for="max_discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_max_discount'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="max_discount" value="<?php echo isset($request->post['max_discount']) ? $request->post['max_discount'] : null; ?>" name="max_discount">
      </div>
    </div>

    
   

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7">
        <button class="btn btn-info" id="discbycategory-submit" type="button" name="create-product-brand-submit" data-form="#disc_by_category_create_form" data-datatable="product-brand-list" data-loading-text="Saving...">
          <span class="fa fa-fw fa-save"></span>
          <?php echo $language->get('button_save'); ?>
        </button>
        <button type="reset" class="btn btn-danger" id="reset" name="reset">
          <span class="fa fa-circle-o"></span>
         <?php echo $language->get('button_reset'); ?></button>
      </div>
    </div>
    
  </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
  setTimeout(function() {
    $("#random_num").trigger("click");
  }, 1000);
});

$(function(){
  var year = new Date().getFullYear();
  var mindate = new Date();

  var maxdate = new Date();

  $("#date_from").datepicker({ 
    format: 'd M yyyy', 
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,
    changeMonth: true,
    startDate : mindate,
  }).on('changeDate', function (ev) {
    var minDate = new Date(ev.date.valueOf());
    $('#date_to').datepicker('setStartDate', minDate);
  });

  $("#date_to").datepicker({ 
    format: 'd M yyyy', 
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,   
    changeMonth: true
  }).on('changeDate', function (ev) {
    var minDate = new Date(ev.date.valueOf());
    $('#date_from').datepicker('setEndDate', minDate);
  });
});
</script>