<div class="form-horizontal">
  <div class="box-body">
    
    <div class="form-group">
      <label for="p_image" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_image'),null); ?>
      </label>
      <div class="col-sm-1">
        <div class="preview-thumbnail">
          <a onClick="return false;" id="p_thumb" href="#">
            <?php if (is_file(DIR_STORAGE . 'products/' . $product['p_image']) && file_exists(DIR_STORAGE . 'products/' . $product['p_image'])) : ?>
              <img src="<?php echo root_url(); ?>/storage/products/<?php echo $product['p_image']; ?>">
            <?php else : ?>
              <img src="../assets/wonderpillars/img/noimage.jpg">
            <?php endif; ?>
          </a>
        </div>

      </div>
    </div>

    <div class="form-group">
      <label for="p_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_name'),null); ?>
      </label>
      <div class="col-sm-6">
        <input type="text" class="form-control" id="p_name" value="<?php echo $product['p_name']; ?>" name="p_name" readonly>
      </div>
    </div>

    <div class="form-group">
      <label for="category_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_category'); ?>
      </label>
      <div class="col-sm-6">
        <select class="form-control select2" name="category_id" disabled>
          <option value="">
            <?php echo $language->get('text_select'); ?>
          </option>
          <?php foreach (get_category_tree(array('filter_fetch_all' => true)) as $category_id => $category_name) { ?>
              <?php if($product['category_id'] == $category_id) : ?>
                <option value="<?php echo $category_id; ?>" selected><?php echo $category_name ; ?></option>
              <?php else: ?>
                <option value="<?php echo $category_id; ?>"><?php echo $category_name ; ?></option>
              <?php endif; ?>
          <?php } ?>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_supplier'); ?>
      </label>
      <div class="col-sm-6">
        <select class="form-control" name="sup_id" readonly disabled>
          <option value="">
            <?php echo $language->get('label_select'); ?>
          </option>
          <?php foreach(get_suppliers() as $supplier) {
              if($supplier['sup_id'] == $product['sup_id']) { ?>
                <option value="<?php echo $supplier['sup_id']; ?>" selected><?php echo $supplier['sup_name']; ?></option><?php
              } else { ?>
                <option value="<?php echo $supplier['sup_id']; ?>"><?php echo $supplier['sup_name']; ?></option><?php
              }
            }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="box_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_box'); ?>
      </label>
      <div class="col-sm-6">
        <select class="form-control" name="box_id" readonly disabled>
           <?php foreach(get_boxes() as $box) {
                if($box['box_id'] == $product['box_id']) { ?>
                  <option value="<?php echo $box['box_id']; ?>" selected>
                    <?php echo $box['box_name']; ?>
                  </option>
                <?php
                } else { ?>
                  <option value="<?php echo $box['box_id']; ?>">
                    <?php echo $box['box_name']; ?>
                  </option>
                  <?php
                }
              }
            ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="quantity_in_stock" class="col-sm-3 control-label">
        <?php echo $language->get('label_stock'); ?>
       </label>
      <div class="col-sm-6">
        <input type="number" class="form-control" id="quantity_in_stock" value="<?php echo $product['quantity_in_stock']; ?>" name="quantity_in_stock" readonly>
      </div>
    </div>

    <div class="form-group">
      <label for="buy_price"  class="col-sm-3 control-label">
        <?php echo $language->get('label_buy_price'); ?>
      </label>
      <div class="col-sm-6">
        <input type="number" step="0.01" class="form-control" id="buy_price" value="<?php echo $product['buy_price']; ?>" name="buy_price" readonly>
      </div>
    </div>

    <div class="form-group">
      <label for="sell_price" class="col-sm-3 control-label">
        <?php echo $language->get('label_sell_price'); ?>
      </label>
      <div class="col-sm-6">
        <input type="number" step="0.01" class="form-control" id="sell_price" value="<?php echo $product['sell_price']; ?>" name="sell_price" readonly>
      </div>
    </div>

    <div class="form-group">
      <label for="e_date" class="col-sm-3 control-label">
        <?php echo $language->get('label_expired_date'); ?>
       </label>
      <div class="col-sm-6">
        <input type="date" class="form-control" id="e_date" value="<?php echo $product['e_date']; ?>" name="e_date" readonly disabled>
      </div>
    </div>

    <div class="form-group">
      <label for="e_date" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-6">
        <textarea class="form-control" id="description" name="description" rows="3" readonly><?php echo $product['description']; ?></textarea>
      </div>
    </div>

  </div>
</div>