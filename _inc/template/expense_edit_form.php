<?php $language->load('expense'); ?>

<form id="form-expense" class="form-horizontal" action="expense.php" method="post" enctype="multipart/form-data">
  
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <?php $invoice_id = isset($invoice['invoice_id']) ? $invoice['invoice_id'] : null; ?>
  <input type="hidden" id="invoice_id" name="invoice_id" value="<?php echo $invoice_id; ?>">
  
  <div class="box-body">
    
    <div class="form-group">
      <div class="col-sm-5">
        <label for="date" class="control-label">
          <?php echo $language->get('label_date'); ?><i class="required">*</i>
        </label>
        <br>
        <input type="date" class="form-control" id="date" name="date" value="<?php echo isset($invoice['buy_date']) ? $invoice['buy_date'] : date('Y-m-d'); ?>">
      </div>
      <div class="col-sm-5">
        <label for="time" class="control-label">
          <?php echo $language->get('label_time'); ?><i class="required">*</i>
        </label><br>
        <div class="input-group bootstrap-timepicker timepicker">
            <input type="text" class="form-control input-small showtimepicker" id="time" name="time" value="<?php echo isset($invoice['expense_time']) ? to_am_pm($invoice['expense_time']) : to_am_pm(current_time()); ?>">
        </div>
      </div>
    </div>

    <?php foreach ($invoice_items as $item) : ?>

      <div class="form-group">
        <label for="title" class="col-sm-2 control-label">
          <?php echo $language->get('label_title'); ?><i class="required">*</i>
        </label>
        <div class="col-sm-10">
          <input type="text" id="title" class="form-control" name="title" value="<?php echo $item['item_name']; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="price" class="col-sm-2 control-label">
          <?php echo $language->get('label_price'); ?><i class="required">*</i>
        </label>
        <div class="col-sm-10">
          <input type="number" id="price" class="form-control" name="price" value="<?php echo $item['item_buying_price']; ?>" onclick="this.select();">
        </div>
      </div>

      <div class="form-group">
        <label for="expense_note" class="col-sm-2 control-label">
          <?php echo $language->get('label_details'); ?>
        </label>
        <div class="col-sm-10">
          <textarea name="expense_note" id="expense_note" class="form-control"><?php echo $invoice['invoice_note']; ?></textarea>
        </div>
      </div>

       <div class="form-group">
      <label for="p_discount_type" class="col-sm-2 control-label">
        <?php echo 'Payment Mode'; ?>
      </label>
      <div class="col-sm-9">
        <select class="form-control select2" name="payment_mode" id="payment_mode">
          <option 
          <?php if($item['payment_mode'] == 'Cash'){
            echo 'selected';
          } ?> 
          value="Cash">Cash</option>
          <option
          <?php if($item['payment_mode'] == 'Card'){
            echo 'selected';
          } ?> 
           value="Card">Card</option>
          <option
          <?php if($item['payment_mode'] == 'Net Banking'){
            echo 'selected';
          } ?> 
           value="Net Banking">Net Banking</option>
       </select>
      </div>
    </div>

    <?php endforeach; ?>

    <div class="form-group">
      <label class="col-sm-2 control-label"></label>
      <div class="col-sm-9">            
        <button id="edit-invoice-update" class="btn btn-info" data-form="#form-expense" data-datatable="#invoice-invoice-list" name="submit" data-loading-text="Updating...">
          <i class="fa fa-fw fa-pencil"></i>
          <?php echo $language->get('button_save'); ?>
        </button>
      </div>
    </div>
    
  </div>
</form>