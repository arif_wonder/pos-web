<?php $language->load('management'); ?>

<h4 class="sub-title">
  <?php echo $language->get('text_delete_title'); ?>
</h4>

<form class="form-horizontal" id="disc-bycat-delete-form" action="discount_by_category.php" method="post">

  <input type="hidden" id="action_type" name="action_type" value="DELETE">
  <input type="hidden" id="disc_by_cat_id" name="disc_by_cat_id" value="<?php echo $disc_by_cat['id']; ?>">
  
  <h4 class="box-title text-center">
    <?php echo $language->get('text_delete_instruction'); ?>
  </h4>

  <div class="box-body">

    <div class="form-group">
      <div class="col-sm-8 col-sm-offset-2">
        <button id="disc-bycat-delete-submit" data-form="#disc-bycat-delete-form" data-datatable="#discount-by-category-list" class="btn btn-danger" name="submit" data-loading-text="Deleting...">
          <span class="fa fa-fw fa-trash"></span>
          <?php echo $language->get('button_delete'); ?>
        </button>
      </div>
    </div>
    
  </div>
</form>