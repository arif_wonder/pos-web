<?php $language->load('expense'); ?>

<form id="form-expense" class="form-horizontal" action="expense.php" method="post" enctype="multipart/form-data">
  
  <input type="hidden" id="action_type" name="action_type" value="CREATE">
  <input type="hidden" id="sup_id" name="sup_id" value="1">
  
  <div class="box-body">
    
    <div class="form-group">
      
      <div class="col-sm-3 col-sm-offset-2">
        <?php $invoice_id = isset($invoice['invoice_id']) ? $invoice['invoice_id'] : null; ?>
        <label for="invoice_id" class="control-label">
            <?php echo $language->get('label_invoice_id'); ?>
            <span data-toggle="tooltip" title="" data-original-title="<?php echo $language->get('hint_invoice_id'); ?>"></span>
        </label><br>
        <input type="text" class="form-control" id="invoice_id" value="<?php echo $invoice_id; ?>" name="invoice_id" autofocus <?php echo $invoice_id ? 'readonly' : null; ?> autocomplete="off">
      </div>

      <div class="col-sm-3">
        <label for="date" class="control-label">
          <?php echo $language->get('label_date'); ?><i class="required">*</i>
        </label><br>
        <input type="date" class="form-control" id="date" name="date" value="<?php echo isset($invoice['expense_date']) ? $invoice['expense_date'] : date('Y-m-d'); ?>">
      </div>

      <div class="col-sm-3">
        <label for="time" class="control-label">
          <?php echo $language->get('label_time'); ?><i class="required">*</i>
        </label><br>
        <div class="input-group bootstrap-timepicker timepicker">
            <input type="text" class="form-control input-small showtimepicker" id="time" name="time" value="<?php echo isset($invoice['expense_time']) ? to_am_pm($invoice['expense_time']) : to_am_pm(current_time()); ?>">
        </div>
      </div>
      
    </div>

    <div class="form-group">
      <label for="title" class="col-sm-2 control-label">
        <?php echo $language->get('label_title'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-9">
        <input type="text" id="title" class="form-control" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="price" class="col-sm-2 control-label">
        <?php echo $language->get('label_price'); ?><i class="required">*</i>
       </label>
      <div class="col-sm-9">
        <input type="number" id="price" class="form-control" name="price" onclick="this.select();">
      </div>
    </div>

    <div class="form-group">
      <label for="expense_note" class="col-sm-2 control-label">
        <?php echo $language->get('label_details'); ?>
      </label>
      <div class="col-sm-9">
        <textarea name="expense_note" id="expense_note" class="form-control"><?php echo isset($invoice) ? $invoice['invoice_note'] : null; ?></textarea>
      </div>
    </div>

     <div class="form-group">
      <label for="p_discount_type" class="col-sm-2 control-label">
        <?php echo 'Payment Mode'; ?><i class="required">*</i>
      </label>
      <div class="col-sm-9">
        <select class="form-control select2" name="payment_mode" id="payment_mode">
          <option value="Cash">Cash</option>
          <option value="Card">Card</option>
          <option value="Net Banking">Net Banking</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label"></label>
      <div class="col-sm-9">            
        <button id="create-expense-submit" class="btn btn-info" data-form="#form-expense" data-datatable="#invoice-invoice-list" name="submit" data-loading-text="Saving...">
          <i class="fa fa-fw fa-pencil"></i>
          <?php echo $language->get('button_save'); ?>
        </button>
        <button type="reset" class="btn btn-danger" id="reset" name="reset">
          <span class="fa fa-fw fa-circle-o"></span>
          <?php echo $language->get('button_reset'); ?>
        </button>
      </div>
    </div>
    
  </div>
</form>