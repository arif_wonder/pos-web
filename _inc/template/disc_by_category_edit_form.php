<?php $language->load('management'); ?>
<h4 class="sub-title">
  <?php echo $language->get('text_update_title'); ?>
</h4>
<form id="disc_by_category_edit_form" class="form-horizontal" action="discount_by_category.php?box_state=open" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <input type="hidden" id="disc_by_category_id" name="disc_by_category_id" value="<?php echo $data['id']; ?>">
  <div class="box-body">

    <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="title" value="<?php echo $data['title']; ?>" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo $data['description']; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="category_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_category'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="category_id" id="category_id">
           <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php 
            foreach (get_categories_tree() as $key => $category) { 
              $slc = $category['category_id']==$data['category_id'] ? "selected='selected'" : '';
              ?>
              <option <?php echo $slc;?> value="<?php echo $category['category_id'] ; ?>"><?php echo $category['category_name'] ; ?></option>
            <?php } ?>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="date_from" class="col-sm-3 control-label">
        <?php echo $language->get('label_from_date'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control pick_date" id="date_from" 
        value="<?php echo $data['date_from'] ? date('d M Y', strtotime($data['date_from'])) : ''; ?>" 
        name="date_from" style="cursor: pointer;">
      </div>
    </div>

    <div class="form-group">
      <label for="date_to" class="col-sm-3 control-label">
        <?php echo $language->get('label_to_date'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control pick_date" id="date_to" value="<?php echo $data['date_to'] ? date('d M Y', strtotime($data['date_to'])) : ''; ?>" name="date_to" style="cursor: pointer;">
      </div>
    </div>

    <div class="form-group">
      <label for="discount_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="discount_type" id="discount_type">
          <option <?php echo $data['discount_type']=='fixed' ? "selected='selected'" : ''; ?> value="fixed">Fixed</option>
          <option <?php echo $data['discount_type']=='precentage' ? "selected='selected'" : ''; ?> value="precentage">Percentage</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control number" id="discount" value="<?php echo $data['discount']; ?>" name="discount">
      </div>
    </div>

    <div class="form-group">
      <label for="min_purchase_amount" class="col-sm-3 control-label">
        <?php echo $language->get('label_min_purchase_amount'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control number" id="min_purchase_amount" value="<?php echo $data['min_purchase_amount']; ?>" name="min_purchase_amount">
      </div>
    </div>

    <div class="form-group">
      <label for="max_discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_max_discount'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control number" id="max_discount" value="<?php echo $data['max_discount']; ?>" name="max_discount">
      </div>
    </div>

    

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7">
        <button class="btn btn-info" id="discbycategory-update-submit" type="button" name="create-product-brand-submit" data-form="#disc_by_category_edit_form" data-datatable="product-brand-list" data-loading-text="Saving...">
          <span class="fa fa-fw fa-save"></span>
          <?php echo 'Update'; ?>
        </button>
      </div>
    </div>
    
  </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
  setTimeout(function() {
    $("#random_num").trigger("click");
  }, 1000);
});

$(function(){
  var year = new Date().getFullYear();
  var mindate = new Date();

  var maxdate = new Date();

  $("#date_from").datepicker({ 
    format: 'd M yyyy', 
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,
    changeMonth: true,
    startDate : mindate,
  }).on('changeDate', function (ev) {
    var minDate = new Date(ev.date.valueOf());
    $('#date_to').datepicker('setStartDate', minDate);
  });

  $("#date_to").datepicker({ 
    format: 'd M yyyy', 
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,   
    changeMonth: true
  }).on('changeDate', function (ev) {
    var minDate = new Date(ev.date.valueOf());
    $('#date_from').datepicker('setEndDate', minDate);
  });
});
</script>