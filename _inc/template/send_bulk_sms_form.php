<?php $language->load('management'); ?>

<form  class="form-horizontal" action="" method="post">
  

  
  <div class="box-body" style="display: block !important;">

     <div class="form-group">
      <label class="col-sm-3 control-label">
        <?php echo 'Select Customers'; ?><i class="required">*</i>
      </label>
      <div class="col-sm-7 customer-selector">
        <div class="checkbox selector">
          <label>
            <input type="checkbox" onclick="$('input[name*=\'customers\']').prop('checked', this.checked);"> Select / Deselect
          </label>
        </div>
        <div class="filter-searchbox">
          <input ng-model="search_customer" class="form-control" type="text" id="search_customer" placeholder="<?php echo $language->get('search'); ?>">
        </div>
        <div class="well well-sm store-well"> 
          <div filter-list="search_customer">
            <?php foreach(get_customers() as $customer) : ?>                    
              <div class="checkbox">
                <label>                         
                  <input type="checkbox" class="selected_customer" name="customers[]" value="<?php echo $customer['customer_mobile']; ?>">
                  <?php echo ucfirst($customer['customer_name']); ?>
                </label>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>



     <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo 'Message'; ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" 
        name="description" 
        placeholder="maximum 160 character only" 
        maxlength="160" 
        onKeyDown="textCounter(this,q17length,160);"
        onKeyUp="textCounter(this,'q17length' ,160)"
        rows="3"></textarea>
        Maximum of 160 characters - <span id="q17length">160</span> characters left
       <!--  <input style="color:red;font-size:12pt;font-style:italic;" readonly type="text" id='q17length' name="q17length" size="3" maxlength="3" value="160">  -->
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7">
        <button class="btn btn-info" id="form-submit" type="button" name="create-product-brand-submit">
          <span class="fa fa-fw fa-save"></span>
          <?php echo 'Send Sms'; ?>
        </button>
       
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  function textCounter(field,cnt, maxlimit) {         
    var cntfield = document.getElementById(cnt);
       if (field.value.length > maxlimit){
          // if too long...trim it!
        field.value = field.value.substring(0, maxlimit);
        // otherwise, update 'characters left' counter
       } 
      else{
        if(cntfield !== null){
          cntfield.innerHTML = maxlimit - field.value.length;
        }
      }
  }
</script>




