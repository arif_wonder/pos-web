<?php $language->load('management'); ?>
<h4 class="sub-title">
  <?php echo $language->get('text_update_title'); ?>
</h4>
<form id="refund_by_invoice_edit_form" class="form-horizontal" action="refund-by-invoice.php?box_state=open" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <input type="hidden" id="customer_id" name="customer_id" value="<?php echo $data['customer_id']; ?>">
  <input type="hidden" id="invoice_id" name="invoice_id" value="<?php echo $data['invoice_id']; ?>">
  <input type="hidden" id="sell_item_id" name="sell_item_id" value="<?php echo $data['sell_item_id']; ?>">
  <input type="hidden" id="restock_to_inventory" name="restock_to_inventory" value="">
  <input type="hidden" id="store_id" name="store_id" value="<?php echo store('store_id'); ?>">

  <input type="hidden" id="total_qty" name="total_qty" value="<?php echo $data['item_quantity']; ?>">

  <div class="box-body">

    <div class="form-group">
      <label for="promotions_type" class="col-sm-3 control-label">
        Quantity
      </label>
      <div class="col-sm-7">
        <p><input type="text" id="quantity" name="quantity"></p>
      </div>
    </div>

    <span id="error_message" style="display:none;color:red;">
        Please enter only numeric value.
    </span>

    <span id="error_message_max_qty" style="display:none;color:red;">
        
    </span>



    <div class="form-group">
      <label for="promotions_type" class="col-sm-3 control-label">
        
      </label>
      <div class="col-sm-7">
        <p><input type="checkbox" id="restock_to_nventory">Damaged</p>
      </div>
    </div>

    <div class="form-group comments_div">
      <label for="p_image" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_image'),null); ?>
        <i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <div class="preview-thumbnail">
          <a ng-click="POSFilemanagerModal({target:'p_image',thumb:'p_thumb'})" onClick="return false;" href="#" data-toggle="image" id="p_thumb">
            <img src="../assets/wonderpillars/img/noimage.jpg" alt="">
          </a>
          <input type="hidden" name="p_image" id="p_image" value="">
        </div>
      </div>
    </div>

    <div class="form-group comments_div">
      <label for="promotions_type" class="col-sm-4 control-label">
        <?php echo $language->get('label_comments'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-6">
        <textarea class="form-control" id="comments" name="comments" rows="3"></textarea>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-8">
        <button class="btn btn-info" id="refund_by_invoice_submit" name="form_update" data-form="#refund_by_invoice_edit_form" data-loading-text="Updating...">
          <i class="fa fa-fw fa-pencil"></i> 
          <?php echo $language->get('button_update'); ?>
        </button>
      </div>
    </div>
  </div>
  </div>
</form>
<script type="text/javascript">
  $(document).ready(function(){
    $('.comments_div').hide();
  })
  $(document).on('change','#restock_to_nventory', function(){
    if ($(this).is(':checked')) {
      $('.comments_div').show();
      $('#restock_to_inventory').val(1);
    }
    else {
      $('.comments_div').hide();
      $('#restock_to_inventory').val('');
    }
  });

  $("#quantity").keyup(function(event){
    if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
      console.log('stop character from entering input');
      event.preventDefault();
      $('#error_message').show();
      return false;
    }
    $('#error_message').hide();
    if($('#quantity').val() > $('#total_qty').val()){
      $('#error_message_max_qty').html('Enter maximum '+ $('#total_qty').val()+' Quantity.').show();
      return false;
    }
     $('#error_message_max_qty').hide();
  });

</script>