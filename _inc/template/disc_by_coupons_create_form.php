<?php $language->load('management'); ?>

<form id="disc_by_category_create_form" class="form-horizontal" action="discount_by_coupons.php?box_state=open" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="CREATE">
  
  <div class="box-body">

    <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="title" value="<?php echo isset($request->post['title']) ? $request->post['title'] : null; ?>" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo isset($request->post['description']) ? $request->post['description'] : null; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="code" class="col-sm-3 control-label">
        <?php echo $language->get('label_code'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="code" value="<?php echo isset($request->post['code']) ? $request->post['code'] : null; ?>" name="code" required>
      </div>
    </div>

    <div class="form-group">
      <label for="valid_till" class="col-sm-3 control-label">
        <?php echo $language->get('label_valid_till'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <!-- <input type="text" readonly="true" class="form-control" id="valid_till" value="<?php //echo isset($request->post['valid_till']) ? $request->post['valid_till'] : null; ?>" name="date_to"> -->

        <div class='input-group date' id='valid_till'>
          <input type='text' class="form-control" name="valid_till" value="<?php echo isset($request->post['valid_till']) ? $request->post['valid_till'] : null; ?>" required style="cursor: pointer;"/>
          <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="discount_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="discount_type" id="discount_type" required>
          <option value="fixed">Fixed</option>
          <option value="precentage">Percentage</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="discount" value="<?php echo isset($request->post['discount']) ? $request->post['discount'] : null; ?>" name="discount">
      </div>
    </div>

    <div class="form-group">
      <label for="min_purchase_amount" class="col-sm-3 control-label">
        <?php echo $language->get('label_min_purchase_amount'); ?>
        <i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control number" id="min_purchase_amount" 
        value="<?php echo isset($request->post['min_purchase_amount']) ? $request->post['min_purchase_amount'] : null; ?>"
        name="min_purchase_amount">
      </div>
    </div>

    <div class="form-group">
      <label for="max_discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_max_discount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="max_discount" value="<?php echo isset($request->post['max_discount']) ? $request->post['max_discount'] : null; ?>" name="max_discount">
      </div>
    </div>

    

    <div class="form-group">
      <label for="max_discount" class="col-sm-3 control-label">
        <?php echo 'Usage Limit'; ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="limit" value="<?php echo isset($request->post['limit']) ? $request->post['limit'] : null; ?>" name="limit">
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7">
        <button class="btn btn-info" id="discbycoupons-submit" type="button" name="create-product-brand-submit" data-form="#disc_by_category_create_form" data-datatable="discount-by-coupons-list" data-loading-text="Saving...">
          <span class="fa fa-fw fa-save"></span>
          <?php echo $language->get('button_save'); ?>
        </button>
        <button type="reset" class="btn btn-danger" id="reset" name="reset">
          <span class="fa fa-circle-o"></span>
         <?php echo $language->get('button_reset'); ?></button>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(function () {
  $('#valid_till').datetimepicker({
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,   
    changeMonth: true,
    startDate : new Date(),
  });
});
</script>
