<?php $language->load('usergroup'); ?>

<?php
$permissions = array(
  'report' => array(
    'read_overview_report' => 'Read Overview Report',
    'read_sell_report' => 'Read Sell Report', 
    'read_buy_report' => 'Read Buy Report',
    'read_due_collection_report' => 'Read Due Collection Report',
    'read_payment_report' => 'Read Payment Report',
    'read_stock_report' => 'Read Stock Report',
    'read_refund_report' => 'Read Refund Report',
    'read_damage_report' => 'Read Damage Report',
    'read_defect_report' => 'Read Defect Report',
    //'read_collection_report' => 'Read Collec. Report', 
    //'read_full_collection_report' => 'Read Full Collec. Report',
    //'read_analytics' => 'Read Analytics', 
    //'send_report_via_email' => 'Send Report via Email',
    //'read_overview_report' => 'Read Overview Report',
   
    //'send_report_email' => 'Send Report Email',
  ),
  'invoice' => array(
    'create_invoice' => 'Create Invoice', 
    'read_invoice_list' => 'Read Invoice List',
    'view_invoice' => 'View Invoice',
    'update_invoice' => 'Update Invoice',
    //'add_item_to_invoice' => 'Add Item To Invoice', 
    //'remove_item_from_invoice' => 'Remove Item From Invoice',
    //'delete_invoice' => 'Delete Invoice',
    //'email_invoice' => 'Send Invoice via Email', 
  ),
  'due' => array(
    'create_due' => 'Create Due',
    'due_collection' => 'Due Collection',
  ),
  'product' => array(
    'read_product' => 'Read Product List',
    'create_product' => 'Create Product', 
    'update_product' => 'Update Product', 
    'delete_product' => 'Delete Product',
    'import_product' => 'Import Product',
    'product_bulk_action' => 'Product Bulk Action',
    'delete_all_product' => 'Delete All Product',
    'read_category' => 'Read Category List',
    'create_category' => 'Create Category', 
    'update_category' => 'Update Category', 
    'delete_category' => 'Delete Category',
    'read_stock_alert' => 'Read Stock Alert',
    'read_expired_product' => 'Read Expired Product List',
    'print_barcode' => 'Print Barcode',
    'read_tax' => 'Read Tax List',
    'create_tax' => 'Create Tax', 
    'update_tax' => 'Update Tax', 
    'delete_tax' => 'Delete Tax',

    'read_unit' => 'Read Unit List',
    'create_unit' => 'Create Unit', 
    'update_unit' => 'Update Unit', 
    'delete_unit' => 'Delete Unit',
    //'restore_all_product' => 'Restore All Product',
  ),
  'supplier' => array(
    'read_supplier' => 'Read Supplier List',
    'create_supplier' => 'Create Supplier',
    'update_supplier' => 'Update Supplier',
    'delete_supplier' => 'Delete Supplier',
    'read_supplier_profile' => 'Read Supplier Profile',
  ),
  'Storebox' => array(
    'read_box' => 'Read Box',
    'create_box' => 'Create Box',
    'update_box' => 'Update Box',
    'delete_box' => 'Delete Box',
  ),
  'buying' => array(
    'create_buying_invoice' => 'Create Invoice',
    'update_buying_invoice' => 'Update Invoice',
    'delete_buying_invoice' => 'Delete Invoice',
    'product_return' => 'Product Return',
  ),
  'expenditure' => array(
    'read_expense' => 'Read Expense',
    'create_expense' => 'Create Expense',
    'update_expense' => 'Update Expense',
    'delete_expense' => 'Delete Expense',
  ),
  'customer' => array(
    'read_customer' => 'Read Customer List',
    'read_customer_profile' => 'Read Customer Profile',
    'create_customer' => 'Create Customer', 
    'update_customer' => 'Update Customer', 
    'delete_customer' => 'Delete Customer', 
  ),
  'user' => array(
    'read_user' => 'Read User List',
    'create_user' => 'Create User', 
    'update_user' => 'Update User', 
    'delete_user' => 'Delete User', 
    'change_password' => 'Change Password',
  ),
  'usergroup' => array(
    'read_usergroup' => 'Read Usergroup List',
    'create_usergroup' => 'Create Usergroup', 
    'update_usergroup' => 'Update Usergroup', 
    'delete_usergroup' => 'Delete Usergroup', 
  ),
  // 'currency' => array(
  //   'read_currency' => 'Read Currency',
  //   'create_currency' => 'Add Currency',
  //   'update_currency' => 'Update Currency',
  //   'change_currency' => 'Change Currency',
  //   'delete_currency' => 'Delete Currency',
  // ),
  // 'filemanager' => array(
  //   'read_filemanager' => 'Read Filemanager',
  // ),
  'payment_method' => array(
    'read_payment_method' => 'Read Payment Method List',
    'create_payment_method' => 'Create Payment Method',
    'update_payment_method' => 'Update Payment Method',
    'delete_payment_method' => 'Delete Payment Method',
  ),
  'store' => array(
    'read_store' => 'Read Store List',
    'create_store' => 'Create Store',
    'update_store' => 'Update Store',
    'delete_store' => 'Delete Store',
    'activate_store' => 'Active Store',
    'upload_favicon' => 'Upload Favicon',
    'upload_logo' => 'Upload Logo',
  ),
  // 'printer' => array(
  //   'create_printer' => 'Add Printer',
  //   'update_printer' => 'Update Printer',
  //   'delete_printer' => 'Delete Printer',
  // ),
  // 'settings' => array(
  //   'read_user_preference' => 'Read User Preference',
  //   'update_user_preference' => 'Update User Preference',
  //   'filtering' => 'Filtering',
  //   'read_keyboard_shortcut' => 'Keyboard Shortcut',
  //   //'backup' => 'Database Backup',
  //   //'restore' => 'Database Restore',
  // ),
  'pos' => array(
    'manage_pos' => 'Manage Pos',
  ),
  'brand' => array(
    'read_brand' => 'Read Brand List',
    'create_brand' => 'Create Brand',
    'update_brand' => 'Update Brand',
    'delete_brand' => 'Delete Brand',
  ),
  'marketing' => array(
    'read_category_by_discount' => 'Read Category By Discount List',
    'create_category_by_discount' => 'Create Category By Discount',
    'update_category_by_discount' => 'Update Category By Discount',
    'delete_category_by_discount' => 'Delete Category By Discount',

    'read_coupons' => 'Read Coupon List',
    'create_coupons' => 'Create Coupon',
    'update_coupons' => 'Update Coupon',
    'delete_coupons' => 'Delete Coupon',

    'read_promotions' => 'Read Promotion List',
    'create_promotions' => 'Create Promotion',
    'update_promotions' => 'Update Promotion',
    'delete_promotions' => 'Delete Promotion',

    'send_bulk_sms' => 'Send Bulk Sms',
    'manage_whatsapp_link' => 'Manage whatsapp Link',
  ),
);


if($usergroup['group_id'] == 1){
  unset($permissions['pos']);
}


//print_r($usergroup['permission']);
?>

<h4 class="sub-title">
  <?php echo $language->get('text_update_title'); ?>
</h4>

<form class="form-horizontal" id="user-group-form" action="user_group.php" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <input type="hidden" id="group_id" name="group_id" value="<?php echo $usergroup['group_id']; ?>">
  
  <div class="box-body">
    
    <div class="form-group">
      <label for="name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_name'), null); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="name" ng-model="usergroupName" ng-init="usergroupName='<?php echo $usergroup['name']; ?>'" value="<?php echo $usergroup['name']; ?>" name="name" required>
      </div>
    </div>

    <div class="form-group">
      <label for="slug" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_slug'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="slug" value="{{ usergroupName | strReplace:' ':'_' | lowercase }}" name="slug" required readonly>
      </div>
    </div>

    <hr>

    <div class="form-group mb-0">
      <div class="col-sm-12">
        <h4 class="pull-left">
          <?php echo $language->get('text_permission'); ?>
        </h4>
        <button data-form="#user-group-form" data-datatable="#user-group-list" class="btn btn-info btn-sm pull-right user-group-update" name="btn_edit_user" data-loading-text="Updating...">
          <span class="fa fa-fw fa-pencil"></span>
          <?php echo $language->get('button_update'); ?>
        </button>
      </div>
    </div>

    <hr>

    <?php $the_permissions = unserialize($usergroup['permission']); ?>

    <div class="form-group permission-list">

      <?php foreach ($permissions as $type => $lists) : ?>
       
      <!-- Permission Start -->
      <div class="col-sm-3">
        <h4>
          <input type="checkbox" id="<?php echo $type; ?>_action" onclick="$('.<?php echo $type; ?>').prop('checked', this.checked);">
          <label for="<?php echo $type; ?>_action">
            <?php echo ucfirst(str_replace('_', ' ', $type)); ?>
          </label>
        </h4>
        <div class="filter-searchbox">
            <input ng-model="search_<?php echo $type; ?>" class="form-control" type="text" placeholder="<?php echo $language->get('search'); ?>">
        </div>
        <div class="well well-sm permission-well">
          <div filter-list="search_<?php echo $type; ?>">

            <?php foreach ($lists as $key => $name) : ?>
              <?php
              //echo isset($the_permissions['access'][$key]) ? ' checked' : 'uncheck';
              ?>
              <div>
                <input type="checkbox" class="<?php echo $type; ?>" id="<?php echo $key; ?>" value="true" name="access[<?php echo $key; ?>]"<?php echo isset($the_permissions['access'][$key]) ? ' checked' : null; ?>>
                <label for="<?php echo $key; ?>"><?php echo ucfirst($name); ?></label>
              </div>
            <?php endforeach; ?>

          </div>
        </div>
      </div>
      <!-- Permission End -->
      <?php endforeach; ?>

    </div>

    <hr>
    <br>

    <div class="form-group">
      <div class="col-sm-12 text-center">
        <button data-form="#user-group-form" data-datatable="#user-group-list" class="btn btn-info user-group-update" name="btn_edit_user" data-loading-text="Updating...">
          <span class="fa fa-fw fa-pencil"></span>
          <?php echo $language->get('button_update'); ?>
        </button>
      </div>
    </div>
    
  </div>
</form>