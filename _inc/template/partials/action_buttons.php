<div id="action-button" class="row">
  <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_pos')) : ?>
   <!--  <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_pos">
      <div class="panel panel-app">
        <div class="panel-body">
          
          <a class="panel-app-link" href="javascript:void(0)">
            <h2>
              <span class="icon">
               
                <img src="../assets/wonderpillars/img/icon/pos.png" alt="" />
              </span>
            </h2>
            <div class="small small2">
              <?php echo $language->get('button_pos'); ?>
            </div>
          </a>
        </div>
      </div>
    </div> -->
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_invoice')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_invoice">
        <div class="panel panel-app">
          <div class="panel-body">
            <a class="panel-app-link" href="invoice.php">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-invoice"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/invoice.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_invoice'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_product')) : ?>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_product">
      <div class="panel panel-app">
        <div class="panel-body">
          <a ng-click="createNewProduct();" onClick="return false;" class="panel-app-link" href="product.php?box_state=open">
            <h2>
              <span class="icon">
                <!--<svg class="svg-icon"><use href="#icon-btn-product"></svg>-->
                <img src="../assets/wonderpillars/img/icon/add-product.png" alt="" />
              </span>
            </h2>
            <div class="small small2">
              <?php echo $language->get('button_add_product'); ?>
            </div>
          </a>
        </div>
      </div>
    </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_box')) : ?>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_box">
      <div class="panel panel-app">
        <div class="panel-body">
          <a ng-click="createNewBox();" onClick="return false;" class="panel-app-link" href="box.php?box_state=open">
            <h2>
              <span class="icon">
                <!--<svg class="svg-icon"><use href="#icon-btn-box"></svg>-->
                <img src="../assets/wonderpillars/img/icon/add-box.png" alt="" />
              </span>
            </h2>
            <div class="small small2">
              <?php echo $language->get('button_add_box'); ?>
            </div>
          </a>
        </div>
      </div>
    </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_supplier')) : ?>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_supplier">
      <div class="panel panel-app">
        <div class="panel-body">
          <a ng-click="createNewSupplier();" onClick="return false;" class="panel-app-link" href="supplier.php?box_state=open">
            <h2>
              <span class="icon">
                <!--<svg class="svg-icon"><use href="#icon-btn-add-supplier"></svg>-->
                <img src="../assets/wonderpillars/img/icon/add-supplier.png" alt="" />
              </span>
            </h2>
            <div class="small small2">
              <?php echo $language->get('button_add_supplier'); ?>
            </div>
          </a>
        </div>
      </div>
    </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_customer')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_customer">
        <div class="panel panel-app">
          <div class="panel-body">
            <a ng-click="createNewCustomer();" onClick="return false;" class="panel-app-link" href="customer.php?box_state=open">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-add-customer"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/add-customer.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_add_customer'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_user')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_user">
        <div class="panel panel-app">
          <div class="panel-body">
            <a ng-click="createNewUser();" onClick="return false;" class="panel-app-link" href="user.php?box_state=open">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-add-user"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/add-user.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_add_user'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_usergroup')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_usergroup">
        <div class="panel panel-app">
          <div class="panel-body">
            <a ng-click="createNewUsergroup();" onClick="return false;" class="panel-app-link" href="user_group.php?box_state=open">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-add-usergroup"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/add-users.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_add_usergroup'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_overview_report')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_overview_report">
        <div class="panel panel-app">
          <div class="panel-body">
            <!-- report_overview.php -->
            <a class="panel-app-link" href="report_overview.php">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-overview-report"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/overview-report.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_overview_report'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_sell_report')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_sell">
        <div class="panel panel-app">
          <div class="panel-body">
            <!-- report_sell_itemwise.php -->
            <a class="panel-app-link" href="report_sell_itemwise.php">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-sell-report"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/sell-report.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_sell_report'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_buy_report')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_buy_report">
          <div class="panel panel-app">
            <div class="panel-body">
              <!-- report_buy_itemwise.php -->
              <a class="panel-app-link" href="report_buy_itemwise.php">
                <h2>
                  <span class="icon">
                    <!--<svg class="svg-icon"><use href="#icon-btn-buy-report"></svg>-->
                    <img src="../assets/wonderpillars/img/icon/buy-report.png" alt="" />
                  </span>
                </h2>
                <div class="small small2">
                  <?php echo $language->get('button_buy_report'); ?>
                </div>
              </a>
            </div>
          </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_stock_alert')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_stock_alert">
        <div class="panel panel-app">
          <div class="panel-body">
            <a class="panel-app-link" href="stock_alert.php">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-stock-alert"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/stock-alert.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_stock_alert'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_expired_product')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_expired_product">
        <div class="panel panel-app">
          <div class="panel-body">
            <a class="panel-app-link" href="expired.php">
              <h2>
                <span class="icon">
                 
                  <img src="../assets/wonderpillars/img/icon/expired-product.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_expired_alert'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_analytics')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_analytics">
        <div class="panel panel-app">
          <div class="panel-body">
            <!-- analytics.php -->
            <a class="panel-app-link" href="analytics.php">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-analytics"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/analytics.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_analytics'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_product_import')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_import">
        <div class="panel panel-app">
          <div class="panel-body">
            <!-- import_product.php -->
            <a class="panel-app-link" href="import_product.php">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-import"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/import.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_import'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_backup_restore')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_backup_restore">
        <div class="panel panel-app">
          <div class="panel-body">
            <!-- backup_restore.php -->
            <a class="panel-app-link" href="javascript:void(0)">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-backup-restore"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/backup-restore.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_backup_restore'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_store')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_settings">
        <div class="panel panel-app">
          <div class="panel-body">
            <a class="panel-app-link" href="store.php">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-stores"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/store.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_stores'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_password')) : ?>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 tour-item" id="button_password">
        <div class="panel panel-app">
          <div class="panel-body">
            <a class="panel-app-link" href="password.php">
              <h2>
                <span class="icon">
                  <!--<svg class="svg-icon"><use href="#icon-btn-password"></svg>-->
                  <img src="../assets/wonderpillars/img/icon/password.png" alt="" />
                </span>
              </h2>
              <div class="small small2">
                <?php echo $language->get('button_password_change'); ?>
              </div>
            </a>
          </div>
        </div>
      </div>
    <?php endif; ?>
</div>
