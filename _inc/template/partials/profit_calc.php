<table class="table table-bordered profit_calc">
  <tbody>
    <tr>
      <td class="text-right bg-yellow">
        <strong>
          <?php echo $language->get('text_selling_price'); ?>
        </strong>
      </td>
      <td class="text-right bg-green">
        <?php 
          $total_tax_amount = tax_amount(from(), to());
          $totalSellingPrice = selling_price(from(), to()) - $total_tax_amount;
          echo currency_format($totalSellingPrice);
        ?>
      </td>
    </tr>
    <tr>
      <td class="text-right bg-yellow">
        <strong>
          <?php echo $language->get('text_tax_amount'); ?>
         </strong>
        </td>
      <td class="text-right bg-green">
        <?php
          echo currency_format($total_tax_amount);
        ?>
      </td>
    </tr>
    <tr>
      <td class="text-right bg-yellow">
        <strong>
          <?php echo $language->get('text_buying_price'); ?>
         </strong>
        </td>
      <td class="text-right bg-green">
        <?php
          $totalPurchasePrice = sell_buying_price(from(), to());
          echo currency_format($totalPurchasePrice);
        ?>
      </td>
    </tr>
    <tr>
      <td class="text-right bg-blue">
        <strong>
          <?php echo $language->get('text_profit'); ?>
         </strong>
        </td>
      <td class="text-right bg-blue">
        <?php echo currency_format($totalSellingPrice - $totalPurchasePrice); ?>
      </td>
    </tr>
    <tr class="info">
      <td class="text-center bg-gray">&nbsp;</td>
      <td class="text-center bg-red">
        <strong>
          <?php echo $language->get('text_due_amount'); ?>: 
        </strong>
        <?php echo currency_format(due_amount(from(), to())); ?>
      </td>
    </tr>
  </tbody>
</table>