<div class="table-responsive">
  <table class="table table-striped table-condenced">
    <tbody>
      <tr>
        <td class="text-center bg-info">
          <h4><?php echo $language->get('text_net_amount'); ?></h4>
          <h2 class="price">
              <?php 
              $total_selling_price = selling_price(from(), to());
              echo currency_format($total_selling_price); ?>
          </h2>
          <p class="description">
            <?php echo $language->get('text_buying_price'); ?>
            <label class="control-label">
              <span data-toggle="tooltip" title="" data-original-title="<?php echo $language->get('hint_buy_price_of_sell'); ?>"></span>
            </label> : 
            <?php echo currency_format(sell_buying_price(from(), to())); ?>
          </p>
          <br>
          <a href="report_sell_itemwise.php" target="_blink">
            <?php echo $language->get('button_details'); ?> &rarr;
          </a>
        </td>
        <td class="text-center bg-success">
          <h4><?php echo $language->get('text_discount_amount'); ?></h4>
          <h2 class="price">
            <?php echo currency_format(discount_amount(from(), to())); ?>
          </h2>
        </td>
        <td class="text-center bg-danger">
          <h4><?php echo $language->get('text_due_given'); ?></h4>
          <h2 class="price">
            <?php 
              $total_due_amount = due_amount(from(), to());
              echo currency_format($total_due_amount);?>
          </h2>
          <br>
          <a href="report_due_collection.php" target="_blink">
            <?php echo $language->get('button_details'); ?> &rarr;
          </a>
        </td>
      </tr>
      <tr>
        <td class="text-center bg-success">
          <h4><?php echo $language->get('text_due_collection'); ?></h4>
          <h2 class="price">
            <?php $due_paid_amount = due_paid_amount(from(), to());
            echo currency_format($due_paid_amount); ?>
          </h2>
          <br>
          <a href="report_due_collection.php" target="_blink">
            <?php echo $language->get('button_details'); ?> &rarr;
          </a>
        </td>
        <td class="text-center bg-warning">
          <h4><?php echo $language->get('text_cash_received'); ?></h4>
          <h2 class="price">
            <?php 
              $total_received_amount = received_amount(from(), to());
              echo currency_format($total_received_amount); ?>
          </h2>
          <br>
          <a href="report_payment.php" target="_blink">
            <?php echo $language->get('button_details'); ?> &rarr;
          </a>
        </td>
        <td class="text-center bg-success">
          <?php 
            $profit_amount = profit_amount(from(), to());
            $is_profit = $profit_amount >= 0 ? 1 : 0;
          ?>
          <h4><?php echo $is_profit ? $language->get('text_profit') : $language->get('text_loss'); ?></h4>
          <h2 class="price">
            <?php echo currency_format($profit_amount); ?>
          </h2>
        </td>
      </tr>
    </tbody>
  </table>
  <table id="report_final" class="table table-striped table-condenced">
    <tbody>  
      <tr>
        <td class="text-center bg-info">
          <h4><?php echo $language->get('text_buying_price'); ?></h4>
          <h2 class="price">
            <?php $buying_price = buying_price(from(), to());
            echo currency_format($buying_price); ?>
          </h2>
          <br>
          <a href="report_buy_itemwise.php" target="_blink">
            <?php echo $language->get('button_details'); ?> &rarr;
          </a>
        </td>
        <td class="text-center bg-warning">
          <h4><?php echo $language->get('text_tax_amount'); ?></h4>
          <h2 class="price">
            <?php 
            $total_tax_amount = tax_amount(from(), to());
            echo currency_format($total_tax_amount); ?>
          </h2>
        </td>
        <td class="text-center bg-info">
          <h4><?php echo $language->get('text_expense_amount'); ?></h4>
          <h2 class="price">
            <?php 
              $expense_amount = expense_amount(from(), to());
              echo currency_format($expense_amount); ?>
          </h2>
          <br>
          <a href="expense.php" target="_blink">
            <?php echo $language->get('button_details'); ?> &rarr;
          </a>
        </td>
      </tr>
    </tbody>
  </table>
</div>