<?php $language->load('supplier'); ?>

<h4 class="sub-title">
  <?php echo $language->get('text_update_title'); ?>
</h4>

<form class="form-horizontal mod-comm-cla" id="supplier-form" action="supplier.php" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <input type="hidden" id="sup_id" name="sup_id" value="<?php echo $supplier['sup_id']; ?>">
  <input type="hidden" id="bank_id" name="bank_id" value="<?php echo $supplier['bank_id']; ?>">
  
  <div class="box-body">
    
    <div class="form-group">
      <label for="sup_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_name'), null); ?><i class="required">*</i>
     </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_name" value="<?php echo $supplier['sup_name']; ?>" name="sup_name">
      </div>
    </div>

    <div class="form-group">
      <label for="sup_email" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_email'), null); ?><i class="required">*</i>
     </label>
      <div class="col-sm-8">
        <input type="email" class="form-control" id="sup_email" value="<?php echo $supplier['sup_email']; ?>" name="sup_email">
      </div>
    </div>

    <div class="form-group">
      <label for="sup_mobile" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_mobile'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="number" class="form-control" id="sup_mobile" value="<?php echo $supplier['sup_mobile']; ?>" name="sup_mobile">
      </div>
    </div>

    

<!-- 
    <div class="form-group">
      <label for="sup_contact" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Contact Number'), null); ?><i class="required">*</i>
     </label>
      <div class="col-sm-8">
        <input type="conatct_number" class="form-control" id="sup_contact" value="<?php echo $supplier['sup_company_contact_number']; ?>" name="sup_company_contact_number">
      </div>
    </div> -->



<div class="form-group">
      <label for="sup_alternate_mobile" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Alternate Contact Number'), null); ?>
     </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_alternate_mobile" value="<?php echo $supplier['sup_alternate_mobile']; ?>" name="sup_alternate_mobile">
      </div>
    </div>

     <div class="form-group">
      <label for="sup_aadhar_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Aadhar Number'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_aadhar_number" name="sup_aadhar_number" value="<?php echo $supplier['sup_aadhar_number']; ?>">
      </div>
    </div>

 <div class="form-group">
      <label for="sup_pan" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Pan'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_Pan" name="sup_pan_number" value="<?php echo $supplier['sup_pan_number']; ?>">
      </div>
    </div>

 <div class="form-group">
      <label for="sup_driving_license_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Driving License Number'), null); ?>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_driving_license_number" name="sup_driving_license_number" value="<?php echo $supplier['sup_driving_license']; ?>">
      </div>
    </div>

     <div class="form-group">
      <label for="sup_passport_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Passport Number'), null); ?>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_passport_number" name="sup_passport_number" value="<?php echo $supplier['sup_passport_number']; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="sup_address" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_address'), null); ?><i class="required">*</i>
     </label>
      <div class="col-sm-8">
        <textarea class="form-control" id="sup_address" name="sup_address"><?php echo $supplier['sup_address']; ?></textarea>
      </div>
    </div>

      <div class="form-group">
        <label for="sup_city" class="col-sm-3 control-label">
          <?php echo sprintf($language->get('label_city'), null); ?>
        </label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="sup_city" name="sup_city" 
          value="<?php echo $supplier['sup_city'] ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="sup_state" class="col-sm-3 control-label">
          <?php echo sprintf($language->get('label_state'), null); ?>
        </label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="sup_state" name="sup_state" value="<?php echo $supplier['sup_state'] ?>">
        </div>
      </div>

      <div class="form-group">
      <label for="sup_zipcode" class="col-sm-3 control-label">
        <?php echo 'Pin Code'; ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="sup_zipcode" name="sup_zipcode" value="<?php echo $supplier['sup_zipcode'] ?>">
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label">
        <?php echo $language->get('label_store'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8 store-selector">
        <div class="checkbox selector">
          <label>
            <input type="checkbox" onclick="$('input[name*=\'supplier_store\']').prop('checked', this.checked);"> Select / Deselect
          </label>
        </div>
        <div class="filter-searchbox">
          <input ng-model="search_store" class="form-control" type="text" id="search_store" placeholder="<?php echo $language->get('search'); ?>">
        </div>
        <div class="well well-sm store-well">
          <div filter-list="search_store">
            <?php foreach(get_stores() as $the_store) : ?>                    
              <div class="checkbox">
                <label>                         
                  <input type="checkbox" name="supplier_store[]" value="<?php echo $the_store['store_id']; ?>" <?php echo in_array($the_store['store_id'], $supplier['stores']) ? 'checked' : null; ?>>
                  <?php echo ucfirst($the_store['name']).' ('.ucfirst($the_store['area']).') '; ?>
                </label>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>


     <div class="form-group">
      <label class="col-sm-3 control-label">
        <?php echo 'Category'; ?><i class="required">*</i>
      </label>
      <div class="col-sm-7 category-selector">
        <div class="checkbox selector">
          <label>
            <input type="checkbox" onclick="$('input[name*=\'supplier_category\']').prop('checked', this.checked);"> Select / Deselect
          </label>
        </div>
        <div class="filter-searchbox">
          <input ng-model="search_category" class="form-control" type="text" id="search_category" placeholder="<?php echo $language->get('search'); ?>">
        </div>
        <div class="well well-sm category-well"> 
          <div filter-list="search_category">
            <?php foreach(get_categorys() as $the_category) : ?>                    
              <div class="checkbox">
                <label>                         
                  <input type="checkbox" name="supplier_category[]" value="<?php echo $the_category['category_id']; ?>" <?php echo in_array($the_category['category_id'], $supplier['category_ids']) ? 'checked' : null; ?>>
                  <?php echo ucfirst($the_category['category_name']); ?>
                </label>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>



    <div class="form-group">
      <label for="sup_details" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_details'), null); ?>
      </label>
      <div class="col-sm-8">
        <textarea class="form-control" id="sup_details" name="sup_details"><?php echo $supplier['sup_details']; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="status" class="col-sm-3 control-label">
        <?php echo $language->get('label_status'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <select id="status" class="form-control" name="status" >
          <option <?php echo isset($supplier['status']) && $supplier['status'] == '1' ? 'selected' : null; ?> value="1"><?php echo $language->get('text_active'); ?></option>
          <option <?php echo isset($supplier['status']) && $supplier['status'] == '0' ? 'selected' : null; ?> value="0"><?php echo $language->get('text_in_active'); ?></option>
        </select>
      </div>
    </div>


    <div class="form-group">
      <label for="bank_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Bank Name'), null); ?>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="bank_name" name="bank_name" value="<?php echo $supplier['bank_name']; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="bank_acc_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Account number'), null); ?>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="bank_acc_number" name="bank_acc_number" value="<?php echo $supplier['bank_acc_number']; ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="ifsc_code" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_IFSC Code'), null); ?>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="ifsc_code" name="ifsc_code" value="<?php echo $supplier['ifsc_code']; ?>">
      </div>
    </div>



   

<div class="form-group">
      <label for="sup_company_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Company Name'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_company_name" name="sup_company_name" value="<?php echo $supplier['sup_company_name']; ?>">
      </div>
    </div>


<div class="form-group">
      <label for="sup_address1" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Address'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_address1" name="sup_company_address" value="<?php echo $supplier['sup_company_address']; ?>">
      </div>
    </div>

<div class="form-group">
      <label for="sup_contact_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Contact Number'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_contact_number" name="sup_company_contact_number" value="<?php echo $supplier['sup_company_contact_number']; ?>">
      </div>
    </div>



<div class="form-group">
      <label for="sup_gst_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_GST No.'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_gst_no" name="sup_company_gst_number" value="<?php echo $supplier['sup_company_gst_number']; ?>">
      </div>
    </div>



<div class="form-group">
      <label for="sup_tin_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Tin No.'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_tin_number" name="sup_company_tin_number" value="<?php echo $supplier['sup_company_tin_number']; ?>">
      </div>
    </div>


    <div class="form-group">
      <label for="sup_tan_number" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_Tan No.'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="sup_tan_number" name="sup_company_tan_number" value="<?php echo $supplier['sup_company_tan_number']; ?>">
      </div>
    </div>


    

     <input type="hidden"  id="sort_order" value="0" name="sort_order">

   <!--  <div class="form-group">
      <label for="sort_order" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_sort_order'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="number" class="form-control" id="sort_order" value="<?php echo $supplier['sort_order']; ?>" name="sort_order">
      </div>
    </div> -->

    <div class="form-group">
      <label for="supplier_address" class="col-sm-3 control-label"></label>
      <div class="col-sm-8">
        <button id="supplier-update" data-form="#supplier-form" data-datatable="#supplier-supplier-list" class="btn btn-info" name="btn_edit_supplier" data-loading-text="Updating...">
          <span class="fa fa-fw fa-pencil"></span>
          <?php echo sprintf($language->get('button_update'), null); ?>
        </button>
      </div>
    </div>
    
  </div>
</form>