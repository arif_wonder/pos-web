<div class="form-horizontal">
   <div class="box-body">
    <div class="form-group">
      <label for="promotions_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="promotions_type" id="promotions_type">
          <option <?php echo $data['promotions_type']=='disc_for_festival' ? 'selected="selected"' : '';?> value="disc_for_festival">Discount for festival</option>
          <option <?php echo $data['promotions_type']=='disc_happy_hours' ? 'selected="selected"' : '';?> value="disc_happy_hours">Discount for happy hours</option>
          <option <?php echo $data['promotions_type']=='disc_for_anniversary' ? 'selected="selected"' : '';?> value="disc_for_anniversary">Discount for anniversary</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control" id="title" value="<?php echo $data['title'];?>" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" readonly="true" id="description" name="description" rows="3"><?php echo $data['description'];?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="discount_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" readonly="true" name="discount_type" id="discount_type" required>
          <option value="fixed">Fixed</option>
          <option value="precentage">Precentage</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control decimal" id="discount" value="<?php echo $data['discount'];?>" name="discount">
      </div>
    </div>

    <div class="form-group">
      <label for="min_purchase_amount" class="col-sm-3 control-label">
        <?php echo $language->get('label_min_purchase_amount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="min_purchase_amount" value="<?php echo $data['min_purchase_amount'];?>" name="min_purchase_amount">
      </div>
    </div>

    <div class="form-group disc_happy_hours">
      <label for="time_from" class="col-sm-3 control-label">
        <?php echo $language->get('label_time_from'); ?>
      </label>
      <div class="col-sm-7 input-group bootstrap-timepicker timepicker" style="padding: 0px 14px 0 14px;">
        <input type="text" readonly="true" class="form-control" id="time_from" value="<?php echo $data['time_from'];?>" name="time_from">
      </div>
    </div>

    <div class="form-group disc_happy_hours">
      <label for="time_to" class="col-sm-3 control-label">
        <?php echo $language->get('label_time_to'); ?>
      </label>
      <div class="col-sm-7 input-group bootstrap-timepicker timepicker" style="padding: 0px 14px 0 14px;">
        <input type="text" readonly="true" class="form-control" id="time_to" value="<?php echo $data['time_to'];?>" name="time_to">
      </div>
    </div>

    <div class="form-group disc_happy_hours">
      <label for="on_days" class="col-sm-3 control-label">
        <?php echo $language->get('label_on_days'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" readonly="true" name="on_days[]" id="on_days" multiple="multiple">
          <option <?php echo in_array('mon', $data['on_days']) ? 'selected="selected"' : '';?> value="mon">Monday</option>
          <option <?php echo in_array('tue', $data['on_days']) ? 'selected="selected"' : '';?> value="tue">Tuesday</option>
          <option <?php echo in_array('wed', $data['on_days']) ? 'selected="selected"' : '';?> value="wed">Wednesday</option>
          <option <?php echo in_array('thu', $data['on_days']) ? 'selected="selected"' : '';?> value="thu">Thursday</option>
          <option <?php echo in_array('fri', $data['on_days']) ? 'selected="selected"' : '';?> value="fri">Friday</option>
          <option <?php echo in_array('sat', $data['on_days']) ? 'selected="selected"' : '';?> value="sat">Saturday</option>
          <option <?php echo in_array('sun', $data['on_days']) ? 'selected="selected"' : '';?> value="sun">Sunday</option>
       </select>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    promotions_type('<?php echo $data['promotions_type']; ?>');
  })
  function promotions_type(promotionsType) {
    if (promotionsType=='disc_happy_hours') {
      $('.disc_happy_hours').show();
    }
    else {
      $('.disc_happy_hours').hide();
    }
  }
</script>