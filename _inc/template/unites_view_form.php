<div class="form-horizontal">
   <div class="box-body">
      <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="title" value="<?php echo $data['title']; ?>" name="title" readonly="true">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" name="description" rows="3" readonly="true"><?php echo $data['description']; ?></textarea>
      </div>
    </div>
  </div>
</div>