<?php $language->load('management'); ?>

<h4 class="sub-title">
  <?php 
    echo 'Delete tax rate';
    //$language->get('text_taxrates_delete_title'); 
  ?>
</h4>

<form class="form-horizontal" id="tax-rates-delete-form" action="tax_rates.php" method="post">

  <input type="hidden" id="action_type" name="action_type" value="DELETE">
  <input type="hidden" id="tax_rate_id" name="tax_rate_id" value="<?php echo $data['tax_rate_id']; ?>">
  <h4 class="box-title text-center">
    <?php echo $language->get('text_delete_instruction'); ?>
  </h4>
  <div class="box-body">
    <div class="form-group">
      <div class="col-sm-8 col-sm-offset-2">
        <button id="tax-rates-delete-submit" data-form="#tax-rates-delete-form" data-datatable="#tax-rates-list" class="btn btn-danger" name="submit" data-loading-text="Deleting...">
          <span class="fa fa-fw fa-trash"></span>
          <?php echo $language->get('button_delete'); ?>
        </button>
      </div>
    </div>
  </div>
</form>