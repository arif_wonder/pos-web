<h4 class="sub-title">
  <?php echo $language->get('text_update_title'); ?>
</h4>

<form id="product-update-form" class="form-horizontal" action="product.php" method="post">
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <input type="hidden" id="p_id" name="p_id" value="<?php echo $product['p_id']; ?>">

  <div class="box-body">
    <div class="form-group">
      <label class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_image'), null); ?>
      </label>
      <div class="col-sm-2">
        <div class="preview-thumbnail">
          <a ng-click="POSFilemanagerModal({target:'product_image',thumb:'product_thumbnail'})" onClick="return false;" href="" data-toggle="image" id="product_thumbnail">
            <?php if (isset($product['p_image']) && ((FILEMANAGERPATH && is_file(FILEMANAGERPATH . $product['p_image']) && file_exists(FILEMANAGERPATH . $product['p_image'])) || (is_file(DIR_STORAGE . 'products' . $product['p_image']) && file_exists(DIR_STORAGE . 'products' . $product['p_image'])))): ?>
              <img  src="<?php echo FILEMANAGERURL ? FILEMANAGERURL : root_url() . '/storage/products'; ?>/<?php echo $product['p_image']; ?>">
            <?php else: ?>
              <img src="../assets/wonderpillars/img/noimage.jpg">
            <?php endif;?>
          </a>
          <input type="hidden" name="p_image" id="product_image" value="<?php echo isset($product['p_image']) ? $product['p_image'] : null; ?>">
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="p_name" class="col-sm-3 control-label">
        <?php echo 'Product'. sprintf($language->get('label_name'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="p_name" name="p_name" value="<?php echo $product['p_name']; ?>" required>
      </div>
    </div>

    <div class="form-group">
      <label for="is_gift_item" class="col-sm-3 control-label">
        <?php echo 'Sell As Gift Item'; ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <label><input type="radio" class="is_gift_item" id="is_gift_item_no" <?php echo ($product['is_gift_item'] == 0) ? 'checked' : ''; ?> name="is_gift_item" value="0" /> No</label>
        <label><input type="radio" class="is_gift_item" id="is_gift_item_yes" <?php echo ($product['is_gift_item'] == 1) ? 'checked' : ''; ?> name="is_gift_item" value="1" /> Yes</label>
      </div>
    </div>

    <div class="form-group" <?php echo ($product['is_gift_item'] == 1) ? 'style="display:none;"' : ''; ?>>
      <label for="p_discount" class="col-sm-3 control-label">
        <?php echo 'Discount amount/percentage'; ?>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control decimal" id="p_discount" value="<?php echo $product['p_discount']; ?>" name="p_discount">
      </div>
    </div>

    <div class="form-group" <?php echo ($product['is_gift_item'] == 1) ? 'style="display:none;"' : ''; ?>>
      <label for="p_discount_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?>
      </label>
      <div class="col-sm-8">
        <select class="form-control select2" name="p_discount_type" id="p_discount_type">
          <option <?php echo $product['p_discount_type'] == 'fixed' ? 'selected="selected"' : ''; ?> value="fixed">Fixed</option>
          <option <?php echo $product['p_discount_type'] == 'precentage' ? 'selected="selected"' : ''; ?> value="precentage">Percentage</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="p_brand" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_brand'), $language->get('text_product')); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <!-- <input type="text" class="form-control" id="p_brand" value="<?php //echo $product['p_brand']; ?>" name="p_brand"> -->
        <div class="{{ !hideSupAddBtn ? 'input-group' : null }}">
          <select id="p_brand" class="form-control select2" name="p_brand" required>
            <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php
foreach (get_brand_tree() as $key => $brand) {
	$slc = $brand['brand_id'] == $product['p_brand'] ? "selected='selected'" : '';
	?>
              <option <?php echo $slc; ?> value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']; ?></option>
            <?php }?>
          </select>
          <a class="input-group-addon" href="brand.php" target="_blank">
            <i class="fa fa-plus"></i>
          </a>
        </div>
      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_quantity" class="col-sm-3 control-label">
        <?php echo $language->get('label_quantity'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="p_quantity" value="<?php echo $product['p_quantity']; ?>" name="p_quantity" required>
      </div>
    </div>

    <div class="form-group">
      <label for="p_unit" class="col-sm-3 control-label">
        <?php echo $language->get('label_unit'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <!-- <input type="text" class="form-control" id="p_unit" value="<?php //echo $product['p_unit']; ?>" name="p_unit"> -->

        <div class="{{ !hideSupAddBtn ? 'input-group' : null }}">
          <select id="p_unit" class="form-control select2" name="p_unit" required>
            <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php foreach (get_unites_tree() as $key => $unites) {

	$slc = $product['p_unit'] == $unites['title'] ? 'selected="selected"' : '';

	?>
              <option <?php echo $slc; ?> value="<?php echo $unites['title']; ?>"><?php echo $unites['title']; ?></option>
            <?php }?>
          </select>
          <a class="input-group-addon" href="unites.php" target="_blank">
            <i class="fa fa-plus"></i>
          </a>
        </div>

      </div>
    </div>

     <div class="form-group">
      <label for="p_unit" class="col-sm-3 control-label">
        <?php echo $language->get('label_tax_rates'); ?> <i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <!-- <input type="text" class="form-control" id="p_unit" value="<?php //echo isset($request->post['p_unit']) ? $request->post['p_unit'] : null; ?>" name="p_unit"> -->

        <div class="{{ !hideSupAddBtn ? 'input-group' : null }}">
          <select id="p_taxrate" class="form-control select2" name="p_taxrate" required>
            <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php foreach (get_taxrates_tree() as $key => $taxrates) {
	$slc = $product['p_taxrate'] == $taxrates['tax_rate_id'] ? 'selected="selected"' : '';
	?>
              <option <?php echo $slc; ?> value="<?php echo $taxrates['tax_rate_id']; ?>"><?php echo $taxrates['rate'] . ' %'; ?></option>
            <?php }?>
          </select>
          <a class="input-group-addon" href="tax_rates.php" target="_blank">
            <i class="fa fa-plus"></i>
          </a>
        </div>

      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_regular_price" class="col-sm-3 control-label">
        <?php echo $language->get('label_regular_price'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="p_regular_price" value="<?php echo $product['p_regular_price']; ?>" name="p_regular_price" required>
      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_special_price" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_special_price'), $language->get('text_product')); ?>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="p_special_price" value="<?php echo $product['p_special_price']; ?>" name="p_special_price">
      </div>
    </div>

    <div class="form-group">
      <label for="p_alert_qty" class="col-sm-3 control-label">
        <?php echo 'Stock Alert Quantity'; ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="number form-control" id="p_alert_qty" value="<?php echo $product['p_alert_qty']; ?>" name="p_alert_qty">
      </div>
    </div>
    <!--  -->
    <div class="form-group d-none">
      <label for="p_sp_fromdate" class="col-sm-3 control-label">
        Special Price From Date
      </label>
      <div class="col-sm-8">
        <input type="text" readonly="true" class="form-control pick_date" id="p_sp_fromdate" value="<?php echo $product['p_sp_fromdate']; ?>" name="p_sp_fromdate">
      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_sp_todate" class="col-sm-3 control-label">
        Special Price To Date
      </label>
      <div class="col-sm-8">
        <input type="text" readonly="true" class="form-control pick_date" id="p_sp_todate" value="<?php echo $product['p_sp_todate']; ?>" name="p_sp_todate">
      </div>
    </div>
    <!--  -->

    <div class="form-group" <?php echo ($product['is_gift_item'] == 1) ? 'style="display:none;"' : ''; ?>>
      <label for="p_gift_item" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_gift_item'), $language->get('text_product')); ?>
      </label>
      <div class="col-sm-8">
        <select id="p_gift_item" class="form-control select2" name="p_gift_item">
          <option value=""><?php echo $language->get('text_select'); ?></option>
          <?php
foreach (get_gift_item_tree() as $key => $val) {
	$slc = $val['p_id'] == $product['p_gift_item'] ? "selected='selected'" : '';
	?>
          <option <?php echo $slc; ?> value="<?php echo $val['p_id']; ?>">
            <?php echo $val['p_name'] . ' (' . $val['p_code'] . ')'; ?>
          </option>
          <?php }?>
        </select>
        <!-- <input type="text" class="form-control" id="p_gift_item" value="<?php //echo $product['p_gift_item']; ?>" name="p_gift_item"> -->
      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_bar_code" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_bar_code'), $language->get('text_product')); ?>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="p_bar_code" value="<?php echo $product['p_bar_code']; ?>" name="p_bar_code">
      </div>
    </div>

    <div class="form-group d-none">
      <label for="p_purchage_price" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_purchage_price'), $language->get('text_product')); ?>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="p_purchage_price" value="<?php echo $product['p_purchage_price']; ?>" name="p_purchage_price">
      </div>
    </div>
    <!-- ================== -->

    <div class="form-group">
      <label for="category_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_category'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <select class="form-control select2" name="category_id" required>
          <option value="">
            <?php echo $language->get('text_select'); ?>
          </option>
          <?php foreach (get_category_tree(array('filter_fetch_all' => true)) as $category_id => $category_name) {?>
              <?php if ($product['category_id'] == $category_id): ?>
                <option value="<?php echo $category_id; ?>" selected><?php echo $category_name; ?></option>
              <?php else: ?>
                <option value="<?php echo $category_id; ?>"><?php echo $category_name; ?></option>
              <?php endif;?>
          <?php }?>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="sup_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_supplier'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <select class="form-control" name="sup_id" required>
          <option value="">
            <?php echo $language->get('text_select'); ?>
          </option>
          <?php foreach (get_suppliers() as $supplier) {
	if ($supplier['sup_id'] == $product['sup_id']) {?>
                <option value="<?php echo $supplier['sup_id']; ?>" selected>
                  <?php echo $supplier['sup_name']; ?>
                </option>
              <?php
} else {?>
                <option value="<?php echo $supplier['sup_id']; ?>">
                  <?php echo $supplier['sup_name']; ?>
                </option>
              <?php
}
}
?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="box_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_box'); ?>
      </label>
      <div class="col-sm-8">
        <select class="form-control" name="box_id" required>
            <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php foreach (get_boxes() as $box_row) {
	if ($box_row['box_id'] == $product['box_id']) {?>
                  <option value="<?php echo $box_row['box_id']; ?>" selected><?php echo $box_row['box_name']; ?></option><?php
} else {
		?>
                  <option value="<?php echo $box_row['box_id']; ?>">
                    <?php echo $box_row['box_name']; ?>
                  </option>
                <?php
}
}
?>
        </select>
      </div>
    </div>

    <input type="hidden" name="sell_price" value="0">
    <!-- <div class="form-group">
      <label for="sell_price" class="col-sm-3 control-label">
        <?php echo $language->get('label_selling_price'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="number" step="0.01" class="form-control" id="sell_price" value="<?php echo $product['sell_price']; ?>" name="sell_price" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" onKeyUp="if(this.value<0){this.value='1';}" required>
      </div>
    </div> -->

    <div class="form-group">
      <label for="e_date" class="col-sm-3 control-label">
        <?php echo $language->get('label_expired_date'); ?>
      </label>
      <div class="col-sm-8">
        <input type="date" class="form-control date" id="e_date" value="<?php echo $product['e_date']; ?>" name="e_date" autocomplete="off" required>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label">
        <?php echo $language->get('label_store'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8 store-selector">
        <div class="checkbox selector">
          <label>
            <input type="checkbox" onclick="$('input[name*=\'product_store\']').prop('checked', this.checked);"> Select / Deselect
          </label>
        </div>
        <div class="filter-searchbox">
          <input ng-model="search_store" class="form-control" type="text" id="search_store" placeholder="<?php echo $language->get('search'); ?>">
        </div>
        <div class="well well-sm store-well">
          <div filter-list="search_store">
            <?php foreach (get_stores() as $the_store): ?>
              <div class="checkbox">
                <label <?php echo in_array($the_store['store_id'], $product['stores']) ? 'class="disabled"' : null; ?>>
                  <input type="checkbox" name="product_store[]" value="<?php echo $the_store['store_id']; ?>" <?php echo in_array($the_store['store_id'], $product['stores']) ? 'checked' : null; ?>  <?php echo in_array($the_store['store_id'], $product['stores']) ? 'class="disabled"' : null; ?>>
                  <?php echo ucfirst($the_store['name']) . ' (' . ucfirst($the_store['area']) . ') '; ?>
                </label>
              </div>
            <?php endforeach;?>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="e_date" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-8">
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo $product['description']; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="status" class="col-sm-3 control-label">
        <?php echo $language->get('label_status'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <select id="status" class="form-control" name="status" >
          <option <?php echo isset($product['status']) && $product['status'] == '1' ? 'selected' : null; ?> value="1">
            <?php echo $language->get('text_active'); ?>
          </option>
          <option <?php echo isset($product['status']) && $product['status'] == '0' ? 'selected' : null; ?> value="0">
            <?php echo $language->get('text_inactive'); ?>
          </option>
        </select>
      </div>
    </div>

     <div class="form-group all">
      <label for="p_code" class="col-sm-3 control-label">
        <?php echo $language->get('label_pcode'); ?> <i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" name="p_code" value="<?php echo $product['p_code']; ?>" class="form-control" id="xp_code" required readonly>
      </div>
    </div>

     <input type="hidden" class="form-control" id="sort_order" value="0" name="sort_order">

   <!--  <div class="form-group">
      <label for="sort_order" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_sort_order'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="number" class="form-control" id="sort_order" value="<?php echo $product['sort_order']; ?>" name="sort_order">
      </div>
    </div> -->

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-8">
        <button class="btn btn-info" id="product-update-submit" name="form_update" data-form="#product-update-form" data-loading-text="Updating...">
          <i class="fa fa-fw fa-pencil"></i>
          <?php echo $language->get('button_update'); ?>
        </button>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
  setTimeout(function() {
    $("#random_num").trigger("click");
  }, 1000);
})
$('body').on('focus',".pick_date", function(){
  var todayDate = new Date().getFullYear();
  var endD= new Date(new Date().setDate(todayDate - 15));
  var currDate = new Date();
  $(this).datepicker({
    autoclose: true,
    todayHighlight: true,
    orientation: "top auto",
    format: "dd M yyyy",
  });
});
</script>