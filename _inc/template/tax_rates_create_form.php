<?php $language->load('management'); ?>

<form id="tax_rates_create_form" class="form-horizontal" action="tax_rates.php?box_state=open" method="post">
  <input type="hidden" id="action_type" name="action_type" value="CREATE">
  <div class="box-body">
    <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="title" value="<?php echo isset($request->post['title']) ? $request->post['title'] : null; ?>" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo isset($request->post['description']) ? $request->post['description'] : null; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="rate" class="col-sm-3 control-label">
        <?php echo $language->get('label_tax_rate'); ?> % 
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="rate" value="<?php echo isset($request->post['rate']) ? $request->post['rate'] : '0.00'; ?>" name="rate" required>
      </div>
    </div>


    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7">
        <button class="btn btn-info" id="taxrates-submit" type="button" name="create-product-brand-submit" data-form="#tax_rates_create_form" data-datatable="discount-by-coupons-list" data-loading-text="Saving...">
          <span class="fa fa-fw fa-save"></span>
          <?php echo $language->get('button_save'); ?>
        </button>
        <button type="reset" class="btn btn-danger" id="reset" name="reset">
          <span class="fa fa-circle-o"></span>
         <?php echo $language->get('button_reset'); ?></button>
      </div>
    </div>
  </div>
</form>
