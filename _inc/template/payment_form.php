<?php $language->load('payment'); ?>

<h4 class="sub-title">
  <?php echo $language->get('text_update_title'); ?>
</h4>

<form class="form-horizontal" id="payment-form" action="payment.php" method="post">
  
  <input type="hidden" id="action_type" name="action_type" value="UPDATE">
  <input type="hidden" id="payment_id" name="payment_id" value="<?php echo $payment['payment_id']; ?>">
  
  <div class="box-body">
    
    <div class="form-group">
      <label for="payment_name" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_name'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="payment_name" value="<?php echo $payment['name']; ?>" name="payment_name">
      </div>
    </div>

    <div class="form-group">
      <label for="payment_name" class="col-sm-3 control-label">
        <?php echo $language->get('label_details'); ?>
      </label>
      <div class="col-sm-8">
        <textarea class="form-control" id="payment_details" name="payment_details"><?php echo isset($payment['details']) ? $payment['details'] : null; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label">
        <?php echo $language->get('label_store'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8 store-selector">
        <div class="checkbox selector">
          <label>
            <input type="checkbox" onclick="$('input[name*=\'payment_store\']').prop('checked', this.checked);"> Select / Deselect
          </label>
        </div>

        <div class="filter-searchbox">
          <input ng-model="search_store" class="form-control" type="text" id="search_store" placeholder="<?php echo $language->get('search'); ?>">
        </div>
        
        <div class="well well-sm store-well">
          <div filter-list="search_store">
            <?php foreach(get_stores() as $the_store) : ?>                    
              <div class="checkbox">
                <label>                         
                  <input type="checkbox" name="payment_store[]" value="<?php echo $the_store['store_id']; ?>" <?php echo in_array($the_store['store_id'], $payment['stores']) ? 'checked' : null; ?>>
                  <?php echo ucfirst($the_store['name']).' ('.ucfirst($the_store['area']).') '; ?>
                </label>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="status" class="col-sm-3 control-label">
        <?php echo $language->get('label_status'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <select id="status" class="form-control" name="status" >
          <option <?php echo isset($payment['status']) && $payment['status'] == '1' ? 'selected' : null; ?> value="1"><?php echo $language->get('text_active'); ?></option>
          <option <?php echo isset($payment['status']) && $payment['status'] == '0' ? 'selected' : null; ?> value="0"><?php echo $language->get('text_in_active'); ?></option>
        </select>
      </div>
    </div>

     <input type="hidden" id="sort_order" value="0" name="sort_order">

   <!--  <div class="form-group">
      <label for="sort_order" class="col-sm-3 control-label">
        <?php echo sprintf($language->get('label_sort_order'), null); ?><i class="required">*</i>
      </label>
      <div class="col-sm-8">
        <input type="number" class="form-control" id="sort_order" value="<?php echo $payment['sort_order']; ?>" name="sort_order">
      </div>
    </div> -->

    <div class="form-group">
      <label for="box_address" class="col-sm-3 control-label"></label>
      <div class="col-sm-8">            
        <button id="payment-update" class="btn btn-info"  data-form="#payment-form" data-datatable="#payment-payment-list" name="btn_edit_customer" data-loading-text="Updating...">
          <i class="fa fa-fw fa-pencil"></i>
          <?php echo $language->get('button_update'); ?>
        </button>
      </div>
    </div>
    
  </div>
</form>