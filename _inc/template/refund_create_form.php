<?php $language->load('management'); ?>

<form id="disc_by_category_create_form" class="form-horizontal" action="promotions.php?box_state=open" method="post">
  <input type="hidden" id="action_type" name="action_type" value="CREATE">
  <div class="box-body">
    <div class="form-group">
      <label for="promotions_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="promotions_type" id="promotions_type" required>
          <option value="disc_for_festival">Discount for festival</option>
          <option value="disc_happy_hours">Discount for happy hours</option>
          <option value="disc_for_anniversary">Discount for anniversary</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="title" value="<?php echo isset($request->post['title']) ? $request->post['title'] : null; ?>" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo isset($request->post['description']) ? $request->post['description'] : null; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="discount_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="discount_type" id="discount_type" required>
          <option value="fixed">Fixed</option>
          <option value="precentage">Precentage</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="discount" value="<?php echo isset($request->post['discount']) ? $request->post['discount'] : null; ?>" name="discount">
      </div>
    </div>

    <div class="form-group">
      <label for="min_purchase_amount" class="col-sm-3 control-label">
        <?php echo $language->get('label_min_purchase_amount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="min_purchase_amount" value="<?php echo isset($request->post['min_purchase_amount']) ? $request->post['min_purchase_amount'] : null; ?>" name="min_purchase_amount">
      </div>
    </div>

    <div class="form-group disc_happy_hours">
      <label for="time_from" class="col-sm-3 control-label">
        <?php echo $language->get('label_time_from'); ?>
      </label>
      <div class="col-sm-7 input-group bootstrap-timepicker timepicker" style="padding: 0px 14px 0 14px;">
        <input type="text" class="form-control" id="time_from" value="<?php echo isset($request->post['time_from']) ? $request->post['time_from'] : null; ?>" name="time_from">
      </div>
    </div>

    <div class="form-group disc_happy_hours">
      <label for="time_to" class="col-sm-3 control-label">
        <?php echo $language->get('label_time_to'); ?>
      </label>
      <div class="col-sm-7 input-group bootstrap-timepicker timepicker" style="padding: 0px 14px 0 14px;">
        <input type="text" class="form-control" id="time_to" value="<?php echo isset($request->post['time_to']) ? $request->post['time_to'] : null; ?>" name="time_to">
      </div>
    </div>

    <div class="form-group disc_happy_hours">
      <label for="on_days" class="col-sm-3 control-label">
        <?php echo $language->get('label_on_days'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="on_days[]" id="on_days" multiple="multiple" required>
          <option value="mon">Monday</option>
          <option value="tue">Tuesday</option>
          <option value="wed">Wednesday</option>
          <option value="thu">Thursday</option>
          <option value="fri">Friday</option>
          <option value="sat">Saturday</option>
          <option value="sun">Sunday</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label"></label>
      <div class="col-sm-7">
        <button class="btn btn-info" id="discbycoupons-submit" type="button" name="create-product-brand-submit" data-form="#disc_by_category_create_form" data-datatable="discount-by-coupons-list" data-loading-text="Saving...">
          <span class="fa fa-fw fa-save"></span>
          <?php echo $language->get('button_save'); ?>
        </button>
        <button type="reset" class="btn btn-danger" id="reset" name="reset">
          <span class="fa fa-circle-o"></span>
         <?php echo $language->get('button_reset'); ?></button>
      </div>
    </div>
  </div>
</form>
<style type="text/css">
  .bootstrap-timepicker-widget.dropdown-menu.ps.ps--theme_default.open {
    width: 180px;
    margin: 0 0 0 14px;
  }
</style>
<script type="text/javascript">
  $('#time_from').timepicker();
  $('#time_to').timepicker();
  $(document).ready(function(){
    promotions_type('');
  })
  $(document).on('change','#promotions_type',function(){
    console.log($(this).val());
    promotions_type($(this).val());
  })
  function promotions_type(promotionsType) {
    if (promotionsType=='disc_happy_hours') {
      $('.disc_happy_hours').show();
    }
    else {
      $('.disc_happy_hours').hide();
    }
  }
$(function () {
  $('#valid_till').datetimepicker({
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,   
    changeMonth: true
  });
});
</script>
