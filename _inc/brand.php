<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_brand')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('brand');

// LOAD PRODUCT MODEL
$brand_model = $registry->get('loader')->model('brand');

// validate post data
function validate_request_data($request, $language) 
{
  // validate product name
  if (!validateString($request->post['brand_name'])) {
    throw new Exception($language->get('error_product_name'));
  }
}

// check product existance by id
function validate_existance($request, $language, $p_id = 0)
{
  global $db;

  $statement = $db->prepare("SELECT * FROM `products` WHERE `p_name` = ? AND `p_id` != ?");
  $statement->execute(array($request->post['p_name'], $p_id));
  if ($statement->rowCount() > 0) {
    throw new Exception($language->get('error_product_exist'));
  }
}

// check product code
function validate_product_code($request, $language, $p_id = NULL)
{
  global $db;

  if ($p_id) {
    $statement = $db->prepare("SELECT * FROM `products` WHERE `p_code` = ? AND `p_id` != ?");
    $statement->execute(array($request->post['p_code'], $p_id));
  } else {
    $statement = $db->prepare("SELECT * FROM `products` WHERE `p_code` = ?");
    $statement->execute(array($request->post['p_code']));
  }
  if ($statement->rowCount() > 0) {
    throw new Exception($language->get('error_product_code_exist'));
  }
}

// create product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'CREATE')
{
  try {

    // check create permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'create_product')) {
      throw new Exception($language->get('error_create_permission'));
    }

    // validate post data
    validate_request_data($request, $language);

    // validate existance
    // validate_existance($request, $language);

    $Hooks->do_action('Before_Create_Product');
  
    // insert product into database    
    $product_id = $brand_model->addBrand($request->post);

    // get box info
    $product = $brand_model->getBrand($product_id);

    $Hooks->do_action('After_Create_Product', $product);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_success'), 'id' => $product_id, 'product' => $product));
    exit();

  } catch (Exception $e) {
    
    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// update product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'UPDATE')
{
  try {
    
    validate_request_data($request, $language);

    $brand_id = $request->post['brand_id'];

    if (DEMO && $brand_id == 1) {
      throw new Exception($language->get('error_update_permission'));
    }

    $Hooks->do_action('Before_Update_Product', $brand_id);
    
    // edit product        
    $brand_model->editBrand($brand_id, $request->post);

    $Hooks->do_action('After_Update_Product', $brand_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_update_success'), 'id' => $brand_id));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// delete product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'DELETE')
{
  try {
    // fetch brand by id
    $brand_id = $request->post['brand_id'];
    $product = $brand_model->getBrand($brand_id);

    // check product exist or not
    if (!isset($product['brand_id'])) {
      throw new Exception($language->get('text_not_found'));
    }

    $Hooks->do_action('Before_Delete_Brand', $request);

    $action_type = $request->post['delete_action'];
    
    $brand_model->deleteBrand($brand_id); 
    $message = $language->get('text_delete');

    $Hooks->do_action('After_Delete_Brand', $product);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $message, 'brand_id' => $brand_id, 'action_type' => $action_type));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
}

// product create form
if (isset($request->get['action_type']) && $request->get['action_type'] == 'CREATE') 
{
  validate_request_data($request, $language);
  $Hooks->do_action('Before_Showing_Product_Form');
  include 'template/product_create_form.php';
  $Hooks->do_action('After_Showing_Product_Form');

  exit();
}

// product edit form
if (isset($request->get['brand_id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'EDIT') {
    
  // fetch product info
  $brand = $brand_model->getBrand($request->get['brand_id']);

  $Hooks->do_action('Before_Showing_Product_Brand_Edit_Form', $brand);
  include 'template/brand_edit_form.php';
  $Hooks->do_action('After_Showing_Product_Brand_Edit_Form', $brand);

  exit();
}

// product delete form
if (isset($request->get['brand_id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'DELETE') {
    
  // fetch product info
  $product = $brand_model->getBrand($request->get['brand_id']);

  $Hooks->do_action('Before_Showing_Product_Delete_Form', $product);
  include 'template/product_brand_del_form.php';
  $Hooks->do_action('After_Showing_Product_Delete_Form', $product);

  exit();
}

// product view template
if (isset($request->get['p_id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'VIEW') {

  // fetch product info
  $product = $brand_model->getProduct($request->get['p_id']);

  $Hooks->do_action('Before_Showing_Product_View_Form', $product);
  include 'template/product_view_form.php';
  $Hooks->do_action('After_Showing_Product_View_Form', $product);

  exit();
}

/**
 *===================
 * START DATATABLE
 *===================
 */

$Hooks->do_action('Before_Showing_Brand_List');

$where_query = '';
 
// DB table to use
$table = "(SELECT * FROM product_brands ORDER BY brand_id DESC ) as product_brands";
 
// Table's primary key
$primaryKey = 'brand_id';
$columns = array(
  array(
      'db' => 'brand_id',
      'dt' => 'DT_RowId',
      'formatter' => function( $d, $row ) {
          return 'row_'.$d;
      }
  ),
  array( 
    'db' => 'brand_id',   
    'dt' => 'select' ,
    'formatter' => function($d, $row) {
        return '<input type="checkbox" name="selected[]" value="' . $row['brand_id'] . '">';
    }
  ),
  array( 'db' => 'brand_id',  'dt' => 'brand_id' ),
  array( 'db' => 'brand_name',  'dt' => 'brand_name' ),
  array( 'db' => 'brand_description',  'dt' => 'brand_description' ),
  array( 
    'db' => 'brand_id',   
    'dt' => 'view_btn' ,
    'formatter' => function($d, $row) use($language) {
      return '<a class="btn btn-sm btn-block btn-warning" title="'.$language->get('button_view').'" href="brand_details.php?brand_id='.$row['brand_id'].'"><i class="fa fa-eye"></i></a>';
    }
  ),
  array( 
    'db' => 'brand_id',   
    'dt' => 'edit_btn' ,
    'formatter' => function($d, $row) use($language) {
        return'<button class="btn btn-sm btn-block btn-primary edit-product-brand" type="button" title="'.$language->get('button_edit').'"><i class="fa fa-pencil"></i></button>';
    }
  ),
  array( 
    'db' => 'brand_id',   
    'dt' => 'delete_btn' ,
    'formatter' => function($d, $row) use($language) {
      return'<button class="btn btn-sm btn-block btn-danger product-brand-delete" type="button" title="'.$language->get('button_delete').'"><i class="fa fa-trash"></i></button>';
    }
  )
);
 

// output for datatable
echo json_encode(
  SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns, null, $where_query)
);

$Hooks->do_action('After_Showing_Brand_List');

/**
 *===================
 * END DATATABLE
 *===================
 */