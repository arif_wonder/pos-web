<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if your logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_store')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('store');

// LOAD STORE MODEL
$store_model = $registry->get('loader')->model('store');

// validate post data
function validate_request_data($request, $language) 
{

  // validate store name
  if (!validateString($request->post['name'])) {
    throw new Exception($language->get('error_name'));
  }



  // validate store mobile number 
  if (empty($request->post['mobile']) || !valdateMobilePhone($request->post['mobile'])) {
    throw new Exception($language->get('error_mobile'));
  }

  if (!empty($request->post['mobile'])) {
     // validate customer mobile
    if (!valdateMobilePhone($request->post['mobile'])) {
     throw new Exception($language->get('You must enter valid Mobile number'));
    }

    // validate customer mobile
    if (strlen($request->post['mobile']) <10){
      throw new Exception($language->get('You must enter minimum ten number in Mobile field'));
    }

    // validate customer mobile
    if (strlen($request->post['mobile']) > 10){
      throw new Exception($language->get("You can't enter greater than ten number in Mobile field"));
    }
  }

    // validate store country
  if (!validateString($request->post['country'])) {
    throw new Exception($language->get('error_country'));
  }

    // validate store zip code
  // if (empty($request->post['zip_code'])) {
  //   throw new Exception('Please provide Pin code');
  // }

    // validate store cashiar name
  // if (!validateInteger($request->post['cashier_id'])) {
  //   throw new Exception($language->get('error_cashier_name'));
  // }

    // validate store address
  if (!validateString($request->post['address'])) {
    throw new Exception($language->get('error_addreess'));
  }

  // validate area
  if (!validateString($request->post['area'])) {
    throw new Exception('Please provide area');
  }

  // validate city
  if (!validateString($request->post['city'])) {
    throw new Exception('Please provide city');
  }

  // validate state
  if (!validateString($request->post['state'])) {
    throw new Exception('Please select state');
  }

  // validate state
  if (!validateString($request->post['vat_reg_no'])) {
    throw new Exception('Please enter GST No');
  }


  if(empty($request->post['vat_reg_no'])) {
    throw new Exception($language->get('Please enter GST No.'));
  }

  // if (!preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $request->post['vat_reg_no'])) {
  //    throw new Exception($language->get('You must enter alphanumeric in Gst No.'));
  // }

  if (strlen($request->post['vat_reg_no']) <15){
    throw new Exception($language->get('You must enter minimum fifteen number in Gst'));
  }

 
  // if (strlen($request->post['sup_company_gst_number']) > 15){
  //   throw new Exception($language->get("You can't enter greater than fifteen number in Gst"));
  // }

  // validate state
  if (!validateString($request->post['gst_type'])) {
    throw new Exception('Please select GST Type');
  }

  // validate store sort_order
  // if (is_null($request->post['sort_order'])) {
  //   throw new Exception($language->get('error_position'));
  // }

  // validate timezone
  // if (!isset($request->post['preference']['timezone']) || !validateString($request->post['preference']['timezone'])) {
  //   throw new Exception($language->get('error_preference_timezone'));
  // }

  // validate receipt printer
  // if ($request->post['remote_printing'] == 1 && !$request->post['receipt_printer']) {
  //   throw new Exception($language->get('error_receipt_printer'));
  // }

  // validate invoice edit lifespan
  // if ($request->post['preference']['invoice_edit_lifespan'] < 0 || !is_numeric($request->post['preference']['invoice_edit_lifespan'])) {
  //   throw new Exception($language->get('error_preference_invoice_edit_lifespan'));
  // }

  // check, if lifespan more than 24 hourse or not
  // if ($request->post['preference']['invoice_edit_lifespan'] > 1440) {
  //   throw new Exception($language->get('error_preference_invoice_edit_lifespan_exceed'));
  // }

  // validate invoice delete lifespan
  // if ($request->post['preference']['invoice_delete_lifespan'] < 0 || !is_numeric($request->post['preference']['invoice_delete_lifespan'])) {
  //   throw new Exception($language->get('error_preference_invoice_delete_lifespan'));
  // }

  // check, if lifespan more than 24 hourse or not
  // if ($request->post['preference']['invoice_delete_lifespan'] > 1440) {
  //   throw new Exception($language->get('error_preference_invoice_delete_lifespan_exceed'));
  // }

  // validate invoice edit lifespan unit
  // if (!isset($request->post['preference']['invoice_edit_lifespan_unit']) || !validateString($request->post['preference']['invoice_edit_lifespan_unit'])) {
  //   throw new Exception($language->get('error_preference_invoice_edit_lifespan_unit'));
  // }

  // validate after sell page
  // if (!validateString($request->post['preference']['after_sell_page'])) {
  //   throw new Exception($language->get('error_preference_after_sell_page'));
  // }

  // validate tax
  // if (!is_numeric($request->post['preference']['tax']) || $request->post['preference']['tax'] < 0) {
  //   throw new Exception($language->get('error_preference_tax'));
  // }

  // validate datatable item limit
  // if (!is_numeric($request->post['preference']['datatable_item_limit']) || $request->post['preference']['datatable_item_limit'] < 0) {
  //   throw new Exception($language->get('error_preference_datatable_item_limit'));
  // }

  // validate stock alert quantity
  // if (!is_numeric($request->post['preference']['stock_alert_quantity']) || $request->post['preference']['stock_alert_quantity'] < 0) {
  //   throw new Exception($language->get('error_preference_stock_alert_quantity'));
  // }

  // validate email from
  // if (!validateString($request->post['preference']['email_from'])) {
  //   throw new Exception($language->get('error_preference_email_from'));
  // }

  // // validate email address
  // if (!validateString($request->post['preference']['email_address'])) {
  //   throw new Exception($language->get('error_preference_email_address'));
  // }

  // // validate email driver
  // if (!validateString($request->post['preference']['email_driver']) || !in_array($request->post['preference']['email_driver'], array('mail_function', 'send_mail', 'smtp_server'))) {
  //   throw new Exception($language->get('error_preference_email_driver'));
  // }

  // // validate sendmail path
  // if ($request->post['preference']['email_driver'] == 'send_mail' && !validateString($request->post['preference']['send_mail_path'])) {
  //   throw new Exception($language->get('error_preference_send_mail_path'));
  // }

  // // validate smtp host
  // if ($request->post['preference']['email_driver'] == 'smtp_server' && !validateString($request->post['preference']['smtp_host'])) {
  //   throw new Exception($language->get('error_preference_smtp_host'));
  // }

  // validate smtp username
  // if ($request->post['preference']['email_driver'] == 'smtp_server' && !validateEmail($request->post['preference']['smtp_username'])) {
  //   throw new Exception($language->get('error_preference_smtp_username'));
  // }

  // validate smtp password
  // if ($request->post['preference']['email_driver'] == 'smtp_server' && !validateString($request->post['preference']['smtp_password'])) {
  //   throw new Exception($language->get('error_preference_smtp_password'));
  // }

  // validate smtp port
  // if ($request->post['preference']['email_driver'] == 'smtp_server' && !validateString($request->post['preference']['smtp_port'])) {
  //   throw new Exception($language->get('error_preference_smtp_port'));
  // }

  // // validate smtp ssl_tls
  // if ($request->post['preference']['email_driver'] == 'smtp_server' && (!validateString($request->post['preference']['ssl_tls']) || !in_array($request->post['preference']['ssl_tls'], array('tls', 'ssl')))) {
  //   throw new Exception($language->get('error_preference_ssl_tls'));
  // }

  // // validate ftp hostname
  // if (!validateString($request->post['preference']['ftp_hostname'])) {
  //   throw new Exception($language->get('error_preference_ftp_hostname'));
  // }

  // // validate ftp username
  // if (!validateString($request->post['preference']['ftp_username'])) {
  //   throw new Exception($language->get('error_preference_ftp_username'));
  // }
}

// check store existance by id
function validate_existance($request, $language, $id = 0)
{
  global $db;

  // check, if store name exist or not
  $statement = $db->prepare("SELECT * FROM `stores` WHERE `name` = ? AND `store_id` != ?");
  $statement->execute(array($request->post['name'], $id));
  if ($statement->rowCount() > 0) {
    throw new Exception($language->get('error_store_exist'));
  }

 
}

function validatelimit($request){
    $request_headers = array("Authorization: Bearer ".$request->post['token']);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, ADMIN_API.'/update-store-limit?action=increase');
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);

    $season_data = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    

    // Show me the result
    curl_close($ch);
    $json = $season_data;
   // print_r($json);exit;
    if ($httpcode != 200 ) {
    throw new Exception($httpcode);
  }
}

// create store
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'CREATE')
{

  try {

    // check create permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'create_store')) {
      throw new Exception($language->get('error_read_permission'));
    }
    
    //validate limit 
    //validatelimit($request);
    // validate post data
    validate_request_data($request, $language);

    // validate logo url
    if (!isset($request->post['logo']) || !validateString($request->post['logo'])) {
      $request->post['logo'] = null;
    }

    // // validate favicon url
    if (!isset($request->post['favicon']) || !validateString($request->post['favicon'])) {
      $request->post['favicon'] = null;
    }

    // validate currency
    // if (empty($request->post['currency'])) {
    //   throw new Exception($language->get('error_currency'));
    // }

    // validate payment method
    if (empty($request->post['payment_method'])) {
      throw new Exception($language->get('error_payment_method'));
    }

    // validate existance
    validate_existance($request, $language);

  


    $Hooks->do_action('Before_Create_Store');

    $store_id = $store_model->addStore($request->post);
    
    if(!empty($request->post['file_name'])){
      $statement = $db->prepare("UPDATE `stores` SET `logo` = ? WHERE `store_id` = ?");
      $statement->execute(array($request->post['file_name'], $store_id));
    }
    

    if($store_id){
      $store_model->editPreference($store_id, $request->post['preference']);
    
    

    // add product to store
    if (!empty($request->post['product'])) {
      foreach ($request->post['product'] as $product_id) {

        // fetch product info
        $product_info = get_the_product($product_id);

        //--- category to store ---//

          $statement = $db->prepare("SELECT * FROM `category_to_store` WHERE `store_id` = ? AND `ccategory_id` = ?");
          $statement->execute(array($store_id, $product_info['category_id']));
          $category = $statement->fetch(PDO::FETCH_ASSOC);
          if (!$category) {
             $statement = $db->prepare("INSERT INTO `category_to_store` SET `ccategory_id` = ?, `store_id` = ?");
              $statement->execute(array((int)$product_info['category_id'], (int)$store_id));
          } 

        //--- box to store ---//

          $statement = $db->prepare("SELECT * FROM `box_to_store` WHERE `store_id` = ? AND `box_id` = ?");
          $statement->execute(array($store_id, $product_info['box_id']));
          $box = $statement->fetch(PDO::FETCH_ASSOC);
          if (!$box) {
             $statement = $db->prepare("INSERT INTO `box_to_store` SET `box_id` = ?, `store_id` = ?");
              $statement->execute(array((int)$product_info['box_id'], (int)$store_id));
          } 

      //--- supplier to store ---//

          $statement = $db->prepare("SELECT * FROM `supplier_to_store` WHERE `store_id` = ? AND `sup_id` = ?");
          $statement->execute(array($store_id, $product_info['sup_id']));
          $supplier = $statement->fetch(PDO::FETCH_ASSOC);
          if (!$supplier) {
            $statement = $db->prepare("INSERT INTO `supplier_to_store` SET `sup_id` = ?, `store_id` = ?");
            $statement->execute(array((int)$product_info['sup_id'], (int)$store_id));
          }

        //--- create product link ---//

          $statement = $db->prepare("INSERT INTO `product_to_store` SET `product_id` = ?, `store_id` = ?, `sup_id` = ?, `box_id` = ?, `e_date` = ?, `p_date` = ?");
          $statement->execute(array((int)$product_id, (int)$store_id, (int)$product_info['sup_id'], (int)$product_info['box_id'], $product_info['e_date'], date('Y-m-d')));

      }
    }

    // add currency to store
    //foreach ($request->post['currency'] as $currency_id) {
      $statement = $db->prepare("INSERT INTO `currency_to_store` SET `currency_id` = ?, `store_id` = ?");
      $statement->execute(array(5, (int)$store_id));
    //}

    // add payment method to store
    foreach ($request->post['payment_method'] as $payment_id) {
      $statement = $db->prepare("INSERT INTO `payment_to_store` SET `payment_id` = ?, `store_id` = ?");
      $statement->execute(array((int)$payment_id, (int)$store_id));
    }

    // add walking customer to the store
    //$statement = $db->prepare("INSERT INTO `customer_to_store` SET `customer_id` = ?, `store_id` = ?");
    //$statement->execute(array(1, $store_id));

    // add cashier to the store
    //$statement = $db->prepare("INSERT INTO `user_to_store` SET `user_id` = ?, `store_id` = ?");
    //$statement->execute(array($request->post['cashier_id'], $store_id));

    // add admin to the store
    $statement = $db->prepare("INSERT INTO `user_to_store` SET `user_id` = ?, `store_id` = ?");
    $statement->execute(array(1, $store_id));

    // add current user to the store
    $statement = $db->prepare("INSERT INTO `user_to_store` SET `user_id` = ?, `store_id` = ?");
    $statement->execute(array(user_id(), $store_id));

    $Hooks->do_action('After_Create_Store');
    $_SESSION['success_store_create'] = 'Store created successfully';
    header('location: store.php');
    exit;


    //header('Content-Type: application/json');
    //echo json_encode(array('msg' => 'Store created successfully', 'id' => $store_id));
    //exit();
  }
  else{
    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => 'You have reached the limit for your maximum number of stores.'));
    exit();
  }

  } catch(Exception $e) {
    
    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// update store
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'UPDATE')
{
  try {

    // check update permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'update_store')) {
      throw new Exception($language->get('error_update_permission'));
    }

    // validate store id
    if (!validateInteger($request->post['store_id'])) {
      throw new Exception($language->get('error_store_id'));
    }

    $id = $request->post['store_id'];

    // if (DEMO && $id == 1) {
    //   throw new Exception($language->get('error_update_permission'));
    // }

    // validate post data
    validate_request_data($request, $language);

    // validate existance
    validate_existance($request, $language, $id);

    $Hooks->do_action('Before_Update_Store', $request);
    
    // edit store
    $store_model->editStore($id, $request->post);
    $the_store = $store_model->editPreference($id, $request->post['preference']);
    //print_r($the_store);exit;
    $Hooks->do_action('After_Update_Store', $the_store);

    // header('Content-Type: application/json');
    // echo json_encode(array('msg' => $language->get('text_update_success'), 'id' => $id));
    $_SESSION['success_store_update'] = 'Store updated successfully';
    header('location: store.php');
   // exit;
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// delete store
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'DELETE') 
{
  try {

    // check delete permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'delete_store') || DEMO) {
      throw new Exception($language->get('error_delete_permission'));
    }

    // validate store id
    if (!validateInteger($request->post['store_id'])) {
      throw new Exception($language->get('error_store_id'));
    }

    $id = $request->post['store_id'];
    $new_store_id = $request->post['new_store_id'];

    if (DEMO && $id == 1) {
      throw new Exception($language->get('error_delete_permission'));
    }

    // store id 1 can not be deleted
    if ($id == 1) {
      throw new Exception($language->get('error_store_delete'));
    }

    // active store can not be deleted
    if (store_id() == $id) {
      throw new Exception($language->get('error_active_store_delete'));
    }

    // validate delete action
    if ($request->post['delete_action'] == 'insert_to' && !validateInteger($new_store_id)) {
      throw new Exception($language->get('error_store_name'));
    }

    $Hooks->do_action('Before_Delete_Store', $request);

    $action_type = $request->post['delete_action'];

    switch ($action_type) {
      case 'delete':

        // delete store info
        $statement = $db->prepare("DELETE FROM `buying_info` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `buying_item` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `buying_price` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `selling_info` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `selling_item` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `selling_price` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `category_to_store` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `box_to_store` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `currency_to_store` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `customer_to_store` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `payment_to_store` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `product_to_store` WHERE `store_id` = ?");
        $statement->execute(array($id));

        $statement = $db->prepare("DELETE FROM `supplier_to_store` WHERE `store_id` = ?");
        $statement->execute(array($id));

      case 'insert_to':

        // update store id

        $statement = $db->prepare("UPDATE `buying_info` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `buying_item` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `buying_price` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `selling_info` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `selling_item` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `selling_price` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `category_to_store` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `box_to_store` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `currency_to_store` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `customer_to_store` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `payment_to_store` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `product_to_store` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        $statement = $db->prepare("UPDATE `supplier_to_store` SET `store_id` = ? WHERE `store_id` = ?");
        $statement->execute(array($new_store_id, $id));

        break;
    }

    // delete store
    $the_store = $store_model->deleteStore($id,$request->post['token']);

    $Hooks->do_action('After_Delete_Store', $the_store);
    
    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_delete_success')));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
}

// store delete form
if (isset($request->get['store_id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'DELETE') {
    // fetch store
    $store_info = $store_model->getStore($request->get['store_id']);
    $Hooks->do_action('Before_Store_Delete_Form', $store_info);
    include 'template/store_del_form.php';
    $Hooks->do_action('After_Store_Delete_Form', $store_info);
    exit();
}

/**
 *===================
 * START DATATABLE
 *===================
 */
 
 $Hooks->do_action('Before_Showing_Store_List');

// DB table to use
$where_query = '1=1';

if (!is_admin()) {
  $where_query = 'u2s.user_id = ' . user_id();
}
 
// DB table to use
$table = "(SELECT stores.* FROM stores 
  LEFT JOIN user_to_store u2s ON (stores.store_id = u2s.store_id) 
  WHERE $where_query GROUP by stores.store_id
  ) as stores";
 
// Table's primary key
$primaryKey = 'store_id';
 
$columns = array(
  array(
      'db' => 'store_id',
      'dt' => 'DT_RowId',
      'formatter' => function( $d, $row ) {
        return 'row_'.$d;
      }
  ),
  array( 
    'db' => 'store_id',   
    'dt' => 'select' ,
    'formatter' => function($d, $row) {
        return '<input type="checkbox" name="selected[]" value="' . $row['store_id'] . '">';
    }
  ),
  array( 'db' => 'store_id', 'dt' => 'store_id' ),
  array( 'db' => 'mobile', 'dt' => 'mobile' ),
  array( 
    'db' => 'name',   
    'dt' => 'name' ,
    'formatter' => function($d, $row) {
        return $row['name'];
    }
  ),
  array( 'db' => 'country', 'dt' => 'country' ),
  array( 'db' => 'address', 'dt' => 'address' ),
  array( 'db' => 'sort_order', 'dt' => 'sort_order' ),
  array( 'db' => 'created_at', 'dt' => 'created_at' ),
  array( 
    'db' => 'created_at',   
    'dt' => 'created_at' ,
    'formatter' => function($d, $row) {
        return $row['created_at'];
    }
  ),
  array( 
    'db' => 'status',   
    'dt' => 'status' ,
    'formatter' => function($d, $row) use($language) {
      if ($row['status'] == 1) {
        return  '<span class="label label-info">'.$language->get('text_active').'</span>';
      }
      return '<span class="label label-warning">'.$language->get('text_inactivate').'</span>';
    }
  ),
  array(
    'db' => 'status',   
    'dt' => 'btn_edit' ,
    'formatter' => function($d, $row) use($language) {
      if (DEMO && $row['store_id'] == 1) {          
        return'<button class="btn btn-sm btn-block btn-default" type="button" disabled><i class="fa fa-pencil"></i></button>';
      }
      return '<a id="edit-store" class="btn btn-sm btn-block btn-primary" href="store_single.php?store_id='.$row['store_id'].'" title="'.$language->get('button_edit').'"><i class="fa fa-fw fa-pencil"></i></a>';
    }
  ),
  array( 
    'db' => 'status',   
    'dt' => 'btn_delete' ,
    'formatter' => function($d, $row) use($language) {

      if ((DEMO && $row['store_id'] == 2) || $row['store_id'] == 1 || store_id() == $row['store_id']) {
        return '<button class="btn btn-sm btn-block btn-default" type="button" title="'.$language->get('button_delete').'" disabled><i class="fa fa-fw fa-trash"></i></button>';
      }

      return '<button id="delete-store" class="btn btn-sm btn-block btn-danger" type="button" title="'.$language->get('button_delete').'"><i class="fa fa-fw fa-trash"></i></button>';
    }
  ),
  array( 
    'db' => 'status',   
    'dt' => 'btn_action' ,
    'formatter' => function($d, $row) use($language) {
      $store_id = $row['store_id'];
      if (store_id() ==  $store_id) {
        return '<button class="btn btn-sm btn-block btn-success" type="button" title="'.$language->get('button_activated').'" disabled><i class="fa fa-fw fa-check"></i></button>';
      } else {
        return '<a class="btn btn-sm btn-block btn-info activate-store" href="store.php?active_store_id='.$store_id.'" title="Switch"><i class="fa fa-fw fa-check"></i> Switch</button>';
      }
    }
  )
);

// output for datatable
echo json_encode(
  SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns)
);

$Hooks->do_action('After_Showing_Store_List');

/**
 *===================
 * END DATATABLE
 *===================
 */