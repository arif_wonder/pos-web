<?php 
ob_start();
session_start();
include ("../_init.php");



// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_product')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('management');

// LOAD PRODUCT MODEL
$discbycategory_model = $registry->get('loader')->model('promotions');

// validate post data
function validate_request_data($request, $language) {
  if ( $request->post['promotions_type'] == 'disc_happy_hours') {
    if( !preg_match('/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i',$request->post['time_from']) ) {
      throw new Exception($language->get('error_time_from'));
    }

    if ( !preg_match('/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i',$request->post['time_to']) ) {
      throw new Exception($language->get('error_time_to'));
    }
  }
}

// check product code
function validate_category_id($request, $language)
{
  global $db;
  $statement = $db->prepare("SELECT * FROM `categorys` WHERE `category_id` = ?");
  $statement->execute(array($request->post['category_id']));

  if ($statement->rowCount() <= 0) {
    throw new Exception($language->get('error_category_id_not_exist'));
  }
}

// create product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'CREATE')
{
  try {

    // check create permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'create_product')) {
      throw new Exception($language->get('error_create_permission'));
    }

    // validate post data
    validate_request_data($request, $language);

    $Hooks->do_action('Before_Create_Refund');
  
    // insert product into database    
    $data = $discbycategory_model->addPromotions($request->post);

    // get box info
    $result = $discbycategory_model->getPromotions($data['promotions_type'],$data['id']);

    $Hooks->do_action('After_Create_Refund', $result);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_success'), 'id' => $data['id'], 'result' => $result));
    exit();

  } 
  catch (Exception $e) {
    
    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// update product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'UPDATE')
{
  try {
    
    validate_request_data($request, $language);

    $promotions_id = $request->post['promotions_id'];

    if (DEMO && $promotions_id == 1) {
      throw new Exception($language->get('error_update_permission'));
    }

    $Hooks->do_action('Before_Update_Refund', $promotions_id);
    
    // edit product        
    $discbycategory_model->editPromotions($promotions_id, $request->post);

    $Hooks->do_action('After_Update_Refund', $promotions_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_update_success'), 'id' => $promotions_id));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// delete 
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'DELETE')
{
  try {
    // fetch brand by id
    $promotions_id   = $request->post['promotions_id'];
    $promotions_type = $request->post['promotions_type'];
    $data = $discbycategory_model->getPromotions($promotions_type, $promotions_id);
    // check product exist or not
    if (!isset($data['id'])) {
      throw new Exception($language->get('text_not_found'));
    }

    $Hooks->do_action('Before_Delete_Refund', $request);

    $action_type = $request->post['action_type'];
    
    $discbycategory_model->deletePromotions($promotions_type, $promotions_id);
    $message = $language->get('text_delete');

    $Hooks->do_action('After_Delete_Refund', $data);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $message, 'id' => $promotions_id, 'action_type' => $action_type));
    exit();

  } 
  catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
}

// product create form
if (isset($request->get['action_type']) && $request->get['action_type'] == 'CREATE') 
{
  validate_request_data($request, $language);
  $Hooks->do_action('Before_Showing_Promotions_Form');
  include 'template/promotions_create_form.php';
  $Hooks->do_action('After_Showing_Promotions_Form');

  exit();
}

// edit form
if (isset($request->get['id']) AND isset($request->get['promotions_type']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'EDIT') {

  // fetch product info
  $data = $discbycategory_model->getPromotions($request->get['promotions_type'],$request->get['id']);

  $Hooks->do_action('Before_Showing_Refund_Edit_Form', $data);
  include 'template/refund_edit_form.php';
  $Hooks->do_action('After_Showing_Refund_Edit_Form', $data);

  exit();
}

// product delete form
if (isset($request->get['id']) AND isset($request->get['promotions_type']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'DELETE') {
    
  // fetch product info
  $data = $discbycategory_model->getPromotions($request->get['promotions_type'],$request->get['id']);

  $Hooks->do_action('Before_Showing_Refund_Delete_Form', $data);
  include 'template/refund_delete_form.php';
  $Hooks->do_action('After_Showing_Refund_Delete_Form', $data);

  exit();
}

/**
 *===================
 * START DATATABLE
 *===================
 */

$Hooks->do_action('Before_Showing_Refund_List');

$where_query = '';

$from = from() .'01:00:00';
$to = to().'23:59:59';
if($from && $to){
  $where_query .= "return_product_info.return_date >= '$from' AND return_product_info.return_date <= '$to'";
}

$where_query .= " AND return_product_info.store_id = ".store_id();


// DB table to use

  $table = "(SELECT 
            return_product_info.return_id AS return_id,
            return_product_info.invoice_id AS invoice_id,
            return_product_info.product_id AS product_id,
            return_product_info.customer_id AS customer_id,
            return_product_info.user_id AS user_id,
            return_product_info.comments AS comments,
            return_product_info.return_date AS return_date,
            return_product_info.store_id AS store_id,

            products.p_id AS p_id,
            products.p_name AS p_name,

            customers.customer_id AS customerid,
            customers.customer_name AS customer_name,

            users.id AS id,
            users.username AS username

            FROM return_product_info as  return_product_info,
            products as products,
            customers as customers,
            users as users
            where 
            $where_query
            AND products.p_id=return_product_info.product_id AND customers.customer_id=return_product_info.customer_id AND
              users.id=return_product_info.user_id
              
          ) as return_product_info";




// Table's primary key
$primaryKey = 'return_id';
$columns = array(
  array(
      'db' => 'return_id',
      'dt' => 'DT_RowId',
      'formatter' => function( $d, $row ) {
          return 'row_'.$d;
      }
  ),
  array( 
      'db' => 'return_id',   
      'dt' => 'select' ,
      'formatter' => function($d, $row) {
          return '<input type="checkbox" name="selected[]" value="' . $row['return_id'] . '">';
      }
  ),
  array( 
    'db' => 'comments',   
    'dt' => 'comments',
    'formatter' => function($d, $row) use($language) {
       if($row['comments']){
          return 'Damaged';
       }
       return 'Returned to Inventory';
    } 
  ),
  array( 'db' => 'return_id',  'dt' => 'return_id' ),
  array( 'db' => 'return_date',  'dt' => 'return_date' ),
  //array( 'db' => 'quantity',  'dt' => 'quantity' ),
  array( 'db' => 'p_name',  'dt' => 'p_name' ),
  array( 'db' => 'invoice_id',  'dt' => 'invoice_id' ),

  // array(
  //   'db' => 'invoice_id',   
  //   'dt' => 'invoice_id' ,
  //   'formatter' => function($d, $row) use($language) {
  //      return 'invoice_id';
  //   } 
  // ),
  // array( 
  //   'db' => 'invoice_id', 
  //   'db' => 'invoice_id', 
  //   'dt' => 'view_btn' ,
  //   'formatter' => function($d, $row) use($language) {
  //     return 'invoice_id2';
  //   }
  // ),
  array( 
    'db' => 'customer_name',   
    'dt' => 'customer_name'
  ),
  array( 
    'db' => 'username',   
    'dt' => 'username',
  )
);
 

// output for datatable
echo json_encode(
  SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns, null, $where_query)
);

$Hooks->do_action('After_Showing_Discount_By_Coupons_List');

/**
 *===================
 * END DATATABLE
 *===================
 */