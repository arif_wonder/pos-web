<?php 
ob_start();
session_start();
include '../_init.php';

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('product');

$store_id = store_id();

// return product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['p_id']))
{
  try {

    // check product return permission
    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'product_return')) {
      throw new Exception($language->get('error_return_permission'));
    }

    $p_id = $request->post['p_id'];
    
    // validate quantity
    if(!validateInteger($request->post['quantity'])) {
      throw new Exception($language->get('error_quantity'));
    }

    // validate invoice id
    if(empty($request->post['invoice_id'])) {
      throw new Exception($language->get('error_invoice_id'));
    } 

    $quantity = $request->post['quantity'];
    $invoice_id = $request->post['invoice_id'];

    // check, if invoice exist or not
    $statement = $db->prepare("SELECT * FROM `buying_info` WHERE `invoice_id` = ?");
    $statement->execute(array($invoice_id));
    $invoice = $statement->fetch(PDO::FETCH_ASSOC);
    $paid_amount = 0;
    if (!$invoice) {
      throw new Exception($language->get('error_invoice_not_found'));
    }

    $buying_date_time = strtotime($invoice['buy_date'] . ' ' . $invoice['buy_time']);

    if (invoice_edit_lifespan() > $buying_date_time) {
      throw new Exception($language->get('error_edit_duration_expired'));
    }

    // check, if invoice item exist or not
    $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `invoice_id` = ? AND `item_id` = ? AND `status` IN ('stock', 'active')");
    $statement->execute(array($invoice_id, $p_id));
    $invoice_item = $statement->fetch(PDO::FETCH_ASSOC);
    if (!$invoice_item) {
      throw new Exception($language->get('error_invoice_item_not_found'));
    }

    $quantity_available = $invoice_item['item_quantity'] - $invoice_item['total_sell'];

    if ($quantity > $quantity_available) {
      throw new Exception($language->get('error_quantity'));
    }

    $statement = $db->prepare("SELECT * FROM `products`
      LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`)
      WHERE `p2s`.`store_id` = ? AND `p_id` = ?");
    $statement->execute(array($store_id, $p_id));
    $product = $statement->fetch(PDO::FETCH_ASSOC);

    $Hooks->do_action('Before_Product_Return', $p_id);

    // return product
    $item_quantity = $invoice_item['item_quantity'] - $quantity;
    $item_total = $invoice_item['item_buying_price'] * $item_quantity;

    $paid_amount = $invoice_item['item_total'] - ($quantity * $invoice_item['item_buying_price']);
    $statement = $db->prepare("UPDATE `buying_price` SET `paid_amount` = ? WHERE `store_id` = ? AND `invoice_id` = ?");
    $statement->execute(array($paid_amount, $store_id, $invoice_id));

    $statement = $db->prepare("UPDATE `buying_item` SET `item_quantity` = ?, `item_total` = ? WHERE `id` = ?");
    $statement->execute(array($item_quantity, $item_total, $invoice_item['id']));

    $statement = $db->prepare("UPDATE `product_to_store` SET `quantity_in_stock` = `quantity_in_stock` - $quantity WHERE `store_id` = ? AND `product_id` = ?");
    $statement->execute(array($store_id, $p_id));

    $Hooks->do_action('After_Product_Return', $p_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_return_success'), 'id' => $p_id));
    exit();
    
  } catch (Exception $e) {

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

$p_id = isset($request->get['p_id']) ? (int)$request->get['p_id'] : ''; 
include 'template/product_return.php';