<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_product')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('management');

// LOAD PRODUCT MODEL
$unites_model = $registry->get('loader')->model('unites');

// validate post data
function validate_request_data($request, $language) {
  if ( empty($request->post['title']) ) {
    throw new Exception($language->get('error_title'));
  }
}

// create product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'CREATE')
{
  try {
    // validate post data
    validate_request_data($request, $language);

    $Hooks->do_action('Before_Create_Tax_Rates');
  
    // insert product into database    
    $insertedId = $unites_model->addUnites($request->post);

    // get box info
    $result = $unites_model->getUnites($insertedId);

    $Hooks->do_action('After_Create_Tax_Rates', $result);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_success'), 'id' => $insertedId, 'result' => $result));
    exit();

  } 
  catch (Exception $e) {
    
    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// update product
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'UPDATE')
{
  try {
    
    validate_request_data($request, $language);

    $unit_id = $request->post['unit_id'];

    $Hooks->do_action('Before_Update_Tax_Rates', $unit_id);
    
    // edit product        
    $unites_model->editUnites($unit_id, $request->post);

    $Hooks->do_action('After_Update_Tax_Rates', $unit_id);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $language->get('text_update_success'), 'id' => $unit_id));
    exit();

  } catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
} 

// delete 
if ($request->server['REQUEST_METHOD'] == 'POST' && isset($request->post['action_type']) && $request->post['action_type'] == 'DELETE')
{
  try {
    // fetch brand by id
    $unit_id   = $request->post['unit_id'];
    $data = $unites_model->getUnites($unit_id);
    // check product exist or not
    if (!isset($data['unit_id'])) {
      throw new Exception($language->get('text_not_found'));
    }

    $Hooks->do_action('Before_Delete_Tax_Rates', $request);

    $action_type = $request->post['action_type'];
    
    $unites_model->deleteUnites($unit_id);
    $message = $language->get('text_delete');

    $Hooks->do_action('After_Delete_Tax_Rates', $data);

    header('Content-Type: application/json');
    echo json_encode(array('msg' => $message, 'id' => $unit_id, 'action_type' => $action_type));
    exit();

  } 
  catch (Exception $e) { 

    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $e->getMessage()));
    exit();
  }
}

// product create form
if (isset($request->get['action_type']) && $request->get['action_type'] == 'CREATE') 
{
  validate_request_data($request, $language);
  $Hooks->do_action('Before_Showing_Tax_Rates_Form');
  include 'template/unites_create_form.php';
  $Hooks->do_action('After_Showing_Tax_Rates_Form');

  exit();
}

// edit form
if (isset($request->get['id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'EDIT') {

  // fetch product info
  $data = $unites_model->getUnites($request->get['id']);

  $Hooks->do_action('Before_Showing_Tax_Rates_Edit_Form', $data);
  include 'template/unites_edit_form.php';
  $Hooks->do_action('After_Showing_Tax_Rates_Edit_Form', $data);

  exit();
}

// product delete form
if (isset($request->get['id']) AND isset($request->get['action_type']) && $request->get['action_type'] == 'DELETE') {
    
  // fetch product info
  $data = $unites_model->getUnites($request->get['id']);

  $Hooks->do_action('Before_Showing_Tax_Rates_Delete_Form', $data);
  include 'template/unites_delete_form.php';
  $Hooks->do_action('After_Showing_Tax_Rates_Delete_Form', $data);

  exit();
}

/**
 *===================
 * START DATATABLE
 *===================
 */

$Hooks->do_action('Before_Showing_Promotions_List');

$where_query = '';
 
// DB table to use
$table = "(SELECT * FROM unites ORDER BY title ASC ) as tax_rate";
 
// Table's primary key
$primaryKey = 'unit_id';
$columns = array(
  array(
      'db' => 'unit_id',
      'dt' => 'DT_RowId',
      'formatter' => function( $d, $row ) {
          return 'row_'.$d;
      }
  ),
  array( 
    'db' => 'unit_id',   
    'dt' => 'select' ,
    'formatter' => function($d, $row) {
        return '<input type="checkbox" name="selected[]" value="' . $row['unit_id'] . '">';
    }
  ),
  array( 'db' => 'unit_id',  'dt' => 'unit_id' ),
  array( 'db' => 'title',  'dt' => 'title' ),
  array( 'db' => 'description',  'dt' => 'description' ),
  array( 
    'db' => 'unit_id', 
    'dt' => 'view_btn' ,
    'formatter' => function($d, $row) use($language) {
      return '<a class="btn btn-sm btn-block btn-warning" title="'.$language->get('button_view').'" href="unites_details.php?unit_id='.$row['unit_id'].'"><i class="fa fa-eye"></i></a>';
    }
  ),
  array( 
    'db' => 'unit_id',   
    'dt' => 'edit_btn' ,
    'formatter' => function($d, $row) use($language) {
        return'<button class="btn btn-sm btn-block btn-primary edit-tax-rates" type="button" title="'.$language->get('button_edit').'"><i class="fa fa-pencil"></i></button>';
    }
  ),
  array( 
    'db' => 'unit_id',   
    'dt' => 'delete_btn' ,
    'formatter' => function($d, $row) use($language) {
      return'<button class="btn btn-sm btn-block btn-danger promotions-delete" type="button" title="'.$language->get('button_delete').'"><i class="fa fa-trash"></i></button>';
    }
  )
);
 

// output for datatable
echo json_encode(
  SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns, null, $where_query)
);

$Hooks->do_action('After_Showing_Discount_By_Coupons_List');

/**
 *===================
 * END DATATABLE
 *===================
 */