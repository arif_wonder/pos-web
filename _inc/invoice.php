<?php
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_invoice_list')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// LOAD LANGUAGE FILE
$language->load('invoice');

// LOAD INVOICE MODEL
$invoice_model = $registry->get('loader')->model('invoice');

// delete invoice
if($request->server['REQUEST_METHOD'] == 'POST' && $request->post['action_type'] == 'DELETE')
{
    try {

        // check permission
        if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'delete_invoice')) {
          throw new Exception($language->get('error_delete_permission'));
        }

        // validate invoice id
        if (empty($request->post['invoice_id'])) {
            throw new Exception($language->get('error_invoice_id'));
        }

        $store_id = store_id();
        $invoice_id = $request->post['invoice_id'];

        // check, if invoice exist or not
        $statement = $db->prepare("SELECT * FROM `selling_info` WHERE `store_id` = ? AND `invoice_id` = ?");
        $statement->execute(array($store_id, $invoice_id));
        $selling_info = $statement->fetch(PDO::FETCH_ASSOC);
        if (!$selling_info) {
            throw new Exception($language->get('error_invoice_id'));
        }

        // check invoice delete duration
        $selling_date_time = strtotime($selling_info['created_at']);
        if (invoice_delete_lifespan() > $selling_date_time) {
          throw new Exception($language->get('error_delete_duration_expired'));
        }

        $customer_id = $selling_info['customer_id'];
        $currency_code = $selling_info['currency_code'];

        // check, if this is last invoice of the customer  or not
        $statement = $db->prepare("SELECT * FROM `selling_info` WHERE `store_id` = ? AND `customer_id` = ? ORDER BY `info_id` DESC");
        $statement->execute(array($store_id, $customer_id));
        $invoice = $statement->fetch(PDO::FETCH_ASSOC);
        if ($invoice_id != $invoice['invoice_id']) {
            throw new Exception($language->get('error_edit_invoice'));
        }

        //fetch invoice price
        $statement = $db->prepare("SELECT * FROM `selling_price` WHERE `store_id` = ? AND `invoice_id` = ?");
        $statement->execute(array($store_id, $invoice_id));
        $price_info = $statement->fetch(PDO::FETCH_ASSOC);
        if (!$price_info) {
            throw new Exception($language->get('error_selling_price_info'));
        }

        // customer present due
        $present_due = $price_info['previous_due'];

        // fetch selling invoice item
        $statement = $db->prepare("SELECT * FROM `selling_item` WHERE `store_id` = ? AND `invoice_id` = ?");
        $statement->execute(array($store_id, $invoice_id));
        $selling_items = $statement->fetchAll(PDO::FETCH_ASSOC);

        // check, if invoice item exist or not
        if (!$statement->rowCount()) {
            throw new Exception($language->get('error_selling_item'));
        }

        foreach ($selling_items as $product) {

            $product_id = $product['item_id'];
            $item_quantity = $product['item_quantity'];
            $buying_invoice_id = $product['buying_invoice_id'];

            // fetch buying invoice item
            $statement = $db->prepare("SELECT * FROM `buying_item` WHERE `store_id` = ? AND `invoice_id` = ? AND `item_id` = ? AND `status` = ?");
            $statement->execute(array($store_id, $buying_invoice_id, $product_id, 'active'));
            $buying_item = $statement->fetch(PDO::FETCH_ASSOC);
            if (!$buying_item) {
              throw new Exception($language->get('error_invoice_closed'));
            }

            // check if the buying invocie is sold out or not if sold out then the item is unable to return
            if ($buying_item['status'] == 'sold' || $buying_item['status'] != 'active') {
              throw new Exception('The product item ' . $buying_item['item_name'] . ' can not be returned. Because the invoice is closed.');
            }

            if ($item_quantity > (int)$buying_item['total_sell']) {
              throw new Exception($item_quantity . ' ' . $buying_item['item_name'] . ' can not be returned.');
            }

            for ($i=0; $i < $item_quantity; $i++) {
              // increment item valude in buying item table
              $statement = $db->prepare("UPDATE `buying_item` SET `total_sell` = `total_sell` - 1 WHERE `store_id` = ? AND `item_id` = ? AND `status` = ?");
              $statement->execute(array($store_id, $product_id, 'active'));
            }

            // increment product quantity
            $statement = $db->prepare("UPDATE `product_to_store` SET `quantity_in_stock` = `quantity_in_stock` + $item_quantity WHERE `store_id` = ? AND `product_id` = ?");
            $statement->execute(array($store_id, $product_id));

            // delete selling item from invoice
            $statement = $db->prepare("DELETE FROM `selling_item` WHERE `store_id` = ? AND `item_id` = ? AND `invoice_id` = ?");
            $statement->execute(array($store_id, $product_id, $invoice_id));
        }

        // update customer balance
        $update_due = $db->prepare("UPDATE `customer_to_store` SET `due_amount` = ?, `currency_code` = ? WHERE `customer_id` = ? AND `store_id` = ?");
        $update_due->execute(array($present_due, $currency_code, $customer_id, $store_id));

        // delete invoice info
        $statement = $db->prepare("DELETE FROM  `selling_info` WHERE `store_id` = ? AND `invoice_id` = ? LIMIT 1");
        $statement->execute(array($store_id, $invoice_id));

        // delete invoice price info
        $statement = $db->prepare("DELETE FROM  `selling_price` WHERE `store_id` = ? AND `invoice_id` = ? LIMIT 1");
        $statement->execute(array($store_id, $invoice_id));

        header('Content-Type: application/json');
        echo json_encode(array('msg' => $language->get('text_delete_success')));
        exit();

    } catch(Exception $e) {

        header('HTTP/1.1 422 Unprocessable Entity');
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode(array('errorMsg' => $e->getMessage()));
        exit();
  }
}

// fetch invoice
if ($request->server['REQUEST_METHOD'] == 'GET' && isset($request->get['invoice_id']))
{
    try {

        // validate invoice id
        $invoice_id = $request->get['invoice_id'];
        $invoice = $invoice_model->getInvoiceInfo($invoice_id);
        if (!$invoice) {
            throw new Exception($language->get('error_invoice_id'));
        }

        // fetch invoice info
        $statement = $db->prepare("SELECT selling_info.*, selling_price.*, customers.customer_name FROM `selling_info`
            LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`)
            LEFT JOIN `customers` ON (`selling_info`.`customer_id` = `customers`.`customer_id`)
            WHERE `selling_info`.`invoice_id` = ?");
        $statement->execute(array($invoice_id));
        $invoice = $statement->fetch(PDO::FETCH_ASSOC);
        if (empty($invoice)) {
            throw new Exception($language->get('error_selling_not_found'));
        }

        if (isset($request->get['action_type']) && $request->get['action_type'] == 'EDIT') {
            $selling_date_time = strtotime($invoice['created_at']);
            if (invoice_edit_lifespan() > $selling_date_time) {
                throw new Exception($language->get('error_duration_expired'));
            }
        }

        // fetch invoice item
        $statement = $db->prepare("SELECT * FROM `selling_item` WHERE invoice_id = ?");
        $statement->execute(array($invoice_id));
        $selling_items = $statement->fetchAll(PDO::FETCH_ASSOC);
        if (empty($selling_items)) {
            throw new Exception($language->get('error_selling_item'));
        }

        $invoice['items'] = $selling_items;

        header('Content-Type: application/json');
        echo json_encode(array('msg' => $language->get('text_success'), 'invoice' => $invoice));
        exit();

    } catch(Exception $e) {

        header('HTTP/1.1 422 Unprocessable Entity');
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode(array('errorMsg' => $e->getMessage()));
        exit();
    }
}

// view invoice details
if (isset($request->get['action_type']) AND $request->get['action_type'] == 'INVOICEDETAILS') {

    try {

        $user_id = isset($request->get['user_id']) ? $request->get['user_id'] : null;
        $where_query = "`selling_info`.`inv_type` = 'sell' AND `created_by` = ? AND `is_visible` = ?";
        $from = from() ? from() : date('Y-m-d');
        $to = to() ? to() : date('Y-m-d');
        $where_query .= date_range_filter($from, $to);
        $statement = $db->prepare("SELECT * FROM `selling_info`
            LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`)
            WHERE $where_query");
        $statement->execute(array($user_id, 1));
        $invoices = $statement->fetchAll(PDO::FETCH_ASSOC);
        if (!$statement->rowCount() > 0) {
            throw new Exception($language->get('error_not_found'));
        }

        include('template/user_invoice_details.php');
        exit();

    } catch (Exception $e) {

        header('HTTP/1.1 422 Unprocessable Entity');
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode(array('errorMsg' => $e->getMessage()));
        exit();
    }
}

// view invoice due details
if (isset($request->get['action_type']) AND $request->get['action_type'] == 'INVOICEDUEDETAILS') {

    try {

        $user_id = isset($request->get['user_id']) ? $request->get['user_id'] : null;
        $where_query = "`selling_info`.`inv_type` = 'sell' AND `created_by` = ? AND `is_visible` = ? AND `selling_price`.`todays_due` > 0";
        $from = from() ? from() : date('Y-m-d');
        $to = to() ? to() : date('Y-m-d');
        $where_query .= date_range_filter($from, $to);

        $statement = $db->prepare("SELECT * FROM `selling_info`
            LEFT JOIN `selling_price` ON (`selling_info`.`invoice_id` = `selling_price`.`invoice_id`)
            WHERE $where_query");
        $statement->execute(array($user_id, 1));
        $invoices = $statement->fetchAll(PDO::FETCH_ASSOC);
        if (!$statement->rowCount() > 0) {
            throw new Exception($language->get('error_not_found'));
        }

        include('template/user_invoice_due_details.php');
        exit();

    } catch (Exception $e) {

        header('HTTP/1.1 422 Unprocessable Entity');
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode(array('errorMsg' => $e->getMessage()));
        exit();
    }
}

/**
 *===================
 * START DATATABLE
 *===================
 */

$Hooks->do_action('Before_Showing_Invoice_List');

$where_query = "selling_info.inv_type != 'due_paid' AND selling_info.is_visible = 1 AND selling_info.store_id = " . store_id();

$from = from();
$to = to();

// DB table to use
$table = "(SELECT
            selling_info.*,
            cust.customer_name AS cust_name,
            cust.customer_mobile AS customer_mobile,
            users.id,
            users.username,
            selling_price.payable_amount,
            selling_price.paid_by_credit,
            selling_price.previous_due,
            selling_price.paid_amount,
            selling_price.todays_due,
            selling_price.present_due AS present_due
            FROM selling_info 
            JOIN customers AS cust ON cust.customer_id = selling_info.customer_id
            LEFT JOIN `users` ON selling_info.created_by = users.id
            LEFT JOIN `selling_price` ON selling_info.invoice_id = selling_price.invoice_id
            WHERE $where_query 
        ) as selling_info";

// Table's primary key
$primaryKey = 'info_id';

$columns = array(
    array( 'db' => 'edit_counter', 'dt' => 'edit_counter' ),
    array( 
    'db' => 'invoice_id',   
    'dt' => 'select' ,
    'formatter' => function($d, $row) {
        return '<input type="checkbox" name="selected[]" value="' . $row['invoice_id'] . '">';
    }
  ),
    array( 'db' => 'paid_by_credit', 'dt' => 'paid_by_credit' ),
    array( 
        'db' => 'payment_method',
        'dt' => 'payment_method',
        'formatter' => function( $d, $row) {

            $payment_method = get_the_payment_method($row['payment_method'], 'name');
            return $payment_method;
        }
    ),    
    array( 'db' => 'invoice_id', 'dt' => 'id' ),
    array(
        'db' => 'invoice_id',
        'dt' => 'invoice_id',
        'formatter' => function( $d, $row) {
            $o = $row['invoice_id'];
            if ($row['edit_counter'] > 0) {
                $o .= ' <span class="fa fa-edit" title="'.$row['edit_counter'].' time(s) edited"></span>';
            }
            return $o;
        }
    ),
    array(
      'db' => 'inv_type',
      'dt' => 'inv_type' ,
      'formatter' => function($d, $row) {
        if ($row['inv_type'] == 'due_paid') {
          return '<label class="label label-success">' . str_replace('_', ' ', $row['inv_type']) . '</label>';
        }
        return '<label class="label label-info">' . $row['inv_type'] . '</label>';
      }
    ),
    array(
      'db' => 'created_at',
      'dt' => 'created_at' ,
      'formatter' => function($d, $row) {
        return $row['created_at'];
      }
    ),
    array(
      'db' => 'customer_mobile',
      'dt' => 'customer_mobile' ,
      'formatter' => function($d, $row) {
        return $row['customer_mobile'];
      }
    ),
    array(
        'db' => 'customer_id',
        'db' => 'cust_name',
        'dt' => 'customer_name',
        'formatter' => function( $d, $row) {
            // $customer = get_the_customer($row['customer_id']);
            return '<a href="customer_profile.php?customer_id=' . $row['customer_id'] . '">' .$row['cust_name']. '</a>';
        }
    ),
    array(
        'db' => 'username',
        'dt' => 'username',
    ),
    array(
        'db' => 'previous_due',
        'dt' => 'prev_due',
        'formatter' => function($d, $row) {
            if ($row['inv_type'] == 'due_paid') {
                return;
            }
            return currency_format($row['previous_due']);
        }
    ),
    array(
        'db' => 'payable_amount',
        'dt' => 'invoice_amount',
        'formatter' => function($d, $row) {
             if ($row['inv_type'] == 'due_paid') {
                return;
            }
            return currency_format($row['payable_amount']);
        }
    ),
    array(
        'db' => 'paid_amount',
        'dt' => 'paid_amount',
        'formatter' => function($d, $row) use($invoice_model) {
            // $paid_amount = (float)$row['paid_amount'] + (float)$invoice_model->getDuePaid($row['customer_id'], $row['invoice_id']);
            // return currency_format($paid_amount);
             return currency_format($row['paid_amount']);
        }
    ),
    array(
        'db' => 'present_due',
        'dt' => 'present_due2',
    ),
    array(
        'db' => 'present_due',
        'db' => 'customer_id',
        'db' => 'invoice_id',
        'dt' => 'due_amount',
        'formatter' => function($d, $row) use($invoice_model) {
            if ($row['inv_type'] == 'due_paid') {
                return;
            }
            if (empty($row['present_due'])) {
                $present_due = 0;
            }
            else {
                $present_due = $row['present_due'];
            }
            // $present_due - 
            $present_due = $invoice_model->getDuePaid($row['customer_id'], $row['invoice_id']);
            return ($present_due);
            // return currency_format($row['present_due']);
        }
    ),
    array(
        'db' => 'invoice_id',
        'db' => 'present_due',
        'db' => 'customer_id',
        'dt' => 'status',
        'formatter' => function($d, $row) use($language, $invoice_model)  {
            // if ($invoice_model->isLastInvoice($row['customer_id'], $row['invoice_id']) || $row['customer_id'] == 1) {

            //     $present_due =  $invoice_model->getDuePaid($row['customer_id'], $row['invoice_id']);
            //return $row['payable_amount'];
            if ($row['paid_amount'] == 0) {
                return '<span class="label label-danger">'.$language->get('text_unpaid').'</span>';
            } 
            else if ($row['payable_amount'] > $row['paid_amount']) {
                    return '<span class="label label-warning">'.$language->get('text_partial').'</span>';
            } 

                //$row['payable_amount']
                
                else {
                    return '<span class="label label-success">'.$language->get('text_paid').'</span>';
                }
            //}
            //return '-';
        }
    ),
    array(
        'db' => 'invoice_id',
        'dt' => 'btn_view',
        'formatter' => function($d, $row) use($language) {
            return '<a class="btn btn-sm btn-block btn-info" href="invoice-print.php?invoice_id='.$row['invoice_id'].'" title="'.$language->get('button_view_receipt').'"><i class="fa fa-eye"></i></a>';
        }
    ),
    array(
        'db' => 'invoice_id',
        'dt' => 'btn_edit',
        'formatter' => function($d, $row) use($db, $language) {

            $selling_date_time = strtotime($row['created_at']);
            $statement = $db->prepare("SELECT * FROM `selling_info` WHERE `customer_id` = ? ORDER BY `info_id` DESC LIMIT 1");
            $statement->execute(array($row['customer_id']));
            $invoice = $statement->fetch(PDO::FETCH_ASSOC);

            // if ($row['inv_type'] == 'due_paid') {
            //     return '<a class="btn btn-sm btn-block btn-default" href="#" disabled><span class="fa fa-pencil"></span></a>';
            // }

            // if (!has_invoice_edit_permission($row['created_at'], $row['customer_id'], $row['invoice_id']) || $row['invoice_id'] != $invoice['invoice_id']) {
            //     return '<a class="btn btn-sm btn-block btn-default" href="#" ><span class="fa fa-pencil"></span></a>';
            // }
            // pos.php
            // return'<button class="btn btn-sm btn-block btn-primary edit-refuncd-by-invoice" disabled type="button" title="'.$language->get('button_allready_return_product').'"><img src="../assets/wonderpillars/img/imgpsh_fullsize.png"></button>';
            return '<a class="btn btn-sm btn-block btn-warning edit-refuncd-by-invoice" href="refund-by-invoice.php?customer_id='.$row['customer_id'].'&invoice_id='.$row['invoice_id'].'" title="Return"><img src="../assets/wonderpillars/img/imgpsh_fullsize.png"></a>';
        }
    ),
    array(
        'db' => 'invoice_id',
        'dt' => 'btn_delete',
        'formatter' => function($d, $row) use($db, $language) {

            $selling_date_time = strtotime($row['created_at']);
            $statement = $db->prepare("SELECT * FROM `selling_info` WHERE `customer_id` = ? ORDER BY `info_id` DESC LIMIT 1");
            $statement->execute(array($row['customer_id']));
            $invoice = $statement->fetch(PDO::FETCH_ASSOC);

            if (invoice_delete_lifespan() > $selling_date_time || $row['invoice_id'] != $invoice['invoice_id']) {
                return '<a class="btn btn-sm btn-block btn-default" href="#" disabled><span class="fa fa-trash"></span></a>';
            }

            return '<button class="btn btn-sm btn-block btn-danger" id="delete-invoice" title="'.$language->get('button_delete').'"><i class="fa fa-trash"></i></button>';

        }
    )
);

// output for datatable
echo json_encode(
    SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns)
);

$Hooks->do_action('After_Showing_Invoice_List');

/**
 *===================
 * END DATATABLE
 *===================
 */