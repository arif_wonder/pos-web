<?php
/*
| -----------------------------------------------------
| PRODUCT NAME: 	SIMPLE POINT OF SELL
| -----------------------------------------------------
| AUTHOR:			wonderpillars.com
| -----------------------------------------------------
| EMAIL:			info@wonderpillars.com
| -----------------------------------------------------
| COPYRIGHT:		RESERVED BY wonderpillars.com
| -----------------------------------------------------
| WEBSITE:			http://wonderpillars.com
| -----------------------------------------------------
*/
abstract class Model 
{
	protected $registry;

	public function __construct($registry) 
	{
		$this->registry = $registry;
	}

	public function __get($key) 
	{
		return $this->registry->get($key);
	}

	public function __set($key, $value) 
	{
		$this->registry->set($key, $value);
	}
	public function __pr($data){
		echo "<pre>";
		print_r($data);
	}
	public function users_bank($user_id,$user_type,$data,$bank_id='') {
    	$query_str = $bank_id ? 'UPDATE' : 'INSERT';
    	$wh_bank_id = $bank_id ? 'WHERE bank_id = ?' : '';
    	$users_bank = $this->db->prepare("$query_str `users_bank` 
    							SET 
						    		bank_name = ?, 
						    		bank_acc_number = ?, 
						    		ifsc_code = ?, 
						    		user_id = ?, 
						    		user_type = ?
						    	$wh_bank_id
				    		");
    	$params = array(
			    		$data['bank_name']  ? : null, 
			    		$data['bank_acc_number']  ? : null, 
			    		$data['ifsc_code']  ? : null, 
			    		$user_id, 
			    		$user_type
			    	);
    	if ($bank_id) {
    		$params[] = $bank_id;
    	}
    	$users_bank->execute($params);
	} //// end of the bank 
	public function user_details($user_id,$user_type,$data,$user_details_id='') {
		$query_str = $user_details_id ? 'UPDATE' : 'INSERT';
    	$wh_user_details_id = $user_details_id ? 'WHERE user_details_id = ?' : '';
    	$user_details = $this->db->prepare("$query_str `user_details`
    								SET
										user_id = ? , 
										user_type = ? , 
										vehicle_number = ? ,
										vehicle_model = ? ,
										number_of_vehicles = ? ,
										busi_name = ? ,
										busi_address = ? ,
										busi_contact_number = ? ,
										busi_annual_turnover = ? ,
										busi_trans_to_office = ? ,
										ser_man_designation = ? ,
										ser_man_comp_name = ? , 
										ser_man_comp_address = ? ,
										ser_man_comp_cont_number = ? ,
										ser_man_trans_to_office = ? ,
										perma_address = ? ,
										perma_city = ? , 
										perma_state = ? ,
										perma_zip_code = ? ,
										cor_address = ? ,
										cor_city = ? ,
										cor_state = ? ,
										cor_zip_code  = ? 
										$wh_user_details_id
									");
    	$params = array(
						$user_id, 
						$user_type, 
						$data['vehicle_number'] ?: null,
						$data['vehicle_model'] ?: null,
						$data['number_of_vehicles'] ?: 0,
						$data['busi_name'] ?: null,
						$data['busi_address'] ?: null,
						$data['busi_contact_number'] ?: null,
						$data['busi_annual_turnover'] ?: null,
						$data['busi_trans_to_office'] ?: null,
						$data['ser_man_designation'] ?: null,
						$data['ser_man_comp_name'] ?: null,
						$data['ser_man_comp_address'] ?: null,
						$data['ser_man_comp_cont_number'] ?: null,
						$data['ser_man_trans_to_office'] ?: null,
						$data['perma_address'] ?: null,
						$data['perma_city'] ?: null,
						$data['perma_state'] ?: null,
						$data['perma_zip_code'] ?: null,
						$data['cor_address'] ?: null,
						$data['cor_city'] ?: null,
						$data['cor_state'] ?: null,
						$data['cor_zip_code'] ?: null
						
			    	);
    	if ($user_details_id) {
    		$params[] = $user_details_id;
    	}
    	$user_details->execute($params);
    }
}