<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if your logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_payment_report')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

$where_query = "selling_info.store_id = " . store_id();

$from = from();
$to = to();
if($from && $to){
  $where_query .= date_range_filter($from, $to);
}


// DB table to use
$table = "(SELECT selling_info.*, payments.name, SUM(selling_price.paid_amount) as total FROM selling_info 
        LEFT JOIN payments ON (selling_info.payment_method = payments.payment_id)
        LEFT JOIN selling_price ON (selling_info.invoice_id = selling_price.invoice_id)
        WHERE $where_query
        GROUP BY selling_info.payment_method
        ORDER BY total DESC) as payment";

// Table's primary key
$primaryKey = 'info_id';
$columns = array(
  array( 'db' => 'payment_method', 'dt' => 'payment_id' ),
  array( 'db' => 'name', 'dt' => 'payment_name' ),
  array( 
    'db' => 'info_id',   
    'dt' => 'select' ,
    'formatter' => function($d, $row) {
        return '<input type="checkbox" name="selected[]" value="' . $row['info_id'] . '">';
    }
  ),
  array( 
    'db' => 'name',   
    'dt' => 'payment_name' ,
    'formatter' => function($d, $row) {
      return '<a href="payment.php?payment_id='.$row['payment_method'].'&payment_name='.$row['name'].'">'.$row['name'].'</a>';
    }
  ),
  array( 
      'db' => 'total',  
      'dt' => 'paid_amount',
      'formatter' => function( $d, $row ) {
        $total = $row['total'];
        return currency_format($total);
      }
    ),
    array( 
      'db' => 'created_at',  
      'dt' => 'datetime',
      'formatter' => function( $d, $row ) {
        return format_only_date($row['created_at']);
      }
    ),
);
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

echo json_encode(
    SSP::complex($request->get, $sql_details, $table, $primaryKey, $columns)
);