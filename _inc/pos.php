<?php 
ob_start();
session_start();
include ("../_init.php");

// check, if user logged in or not
// if user is not logged in then return an alert message
if (!$user->isLogged()) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_login')));
  exit();
}

// check, if user has reading permission or not
// if user have not reading permission return an alert message
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'create_invoice')) {
  header('HTTP/1.1 422 Unprocessable Entity');
  header('Content-Type: application/json; charset=UTF-8');
  echo json_encode(array('errorMsg' => $language->get('error_read_permission')));
  exit();
}

// fetch customer by id
if ($request->server['REQUEST_METHOD'] == 'GET' && isset($request->get['action_type']) && $request->get['action_type'] == 'CUSTOMER') {
	try {

		// validte customer id
		if (!validateInteger($request->get['customer_id'])) {
			throw new Exception($language->get('error_customer_id'));
		}

		$id = $request->get['customer_id'];

		$statement = $db->prepare("SELECT * FROM `customers`  
			LEFT JOIN `customer_to_store` c2s ON (`customers`.`customer_id` = `c2s`.`customer_id`)
			WHERE `c2s`.`store_id` = ? AND `customers`.`customer_id` = ? AND `c2s`.`status` = ?");
		$statement->execute(array(store_id(), $id, 1));
		$the_customer = $statement->fetch(PDO::FETCH_ASSOC);
		$customer = $the_customer ? $the_customer : array();

	    header('Content-Type: application/json');
	    echo json_encode($customer); 
	    exit();

	} catch (Exception $e) {

	    header('HTTP/1.1 422 Unprocessable Entity');
	    header('Content-Type: application/json; charset=UTF-8');
	    echo json_encode(array('errorMsg' => $e->getMessage()));
	    exit();
	}
}

// fetch customer list
if ($request->server['REQUEST_METHOD'] == 'GET' && isset($request->get['action_type']) && $request->get['action_type'] == 'CUSTOMERLIST') {
	try {

		$limit = isset($request->get['limit']) ? (int)$request->get['limit'] : 20;
		$field = $request->get['field'];
		$query_string = $request->get['query_string'];
		$statement = $db->prepare("SELECT * FROM `customers` 
			LEFT JOIN `customer_to_store` c2s ON (`customers`.`customer_id` = `c2s`.`customer_id`)
			WHERE UPPER($field) LIKE '" . strtoupper($query_string) . "%' AND `c2s`.`store_id` = ? AND `c2s`.`status` = ? GROUP BY `customers`.`customer_id` ORDER BY `customers`.`customer_id` DESC LIMIT $limit");
		$statement->execute(array(store_id(), 1));
		$customers = $statement->fetchAll(PDO::FETCH_ASSOC);
		
		$customer_array = array();
		if ($statement->rowCount() > 0) {
		    $customer_array = $customers;
		}

	    header('Content-Type: application/json');
	    echo json_encode($customer_array); 
	    exit();

	} catch (Exception $e) {

	    header('HTTP/1.1 422 Unprocessable Entity');
	    header('Content-Type: application/json; charset=UTF-8');
	    echo json_encode(array('errorMsg' => $e->getMessage()));
	    exit();
	}
}

// fetch a product item
if ($request->server['REQUEST_METHOD'] == 'GET' && isset($request->get['action_type']) && $request->get['action_type'] == 'PRODUCTITEM')
{
	try {

		if (isset($request->get["is_edit_mode"]) && $request->get["is_edit_mode"])	 {
		    if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'add_item_to_invoice')) {
		      throw new Exception($language->get('error_item_add_permission'));
		    }
		}

		// validate product id
		if (!isset($request->get['p_id'])) {
			throw new Exception($language->get('error_product_id'));
		}

		$p_id = (int)$request->get['p_id'];

		$statement = $db->prepare("SELECT * FROM `products` LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`) WHERE `p2s`.`store_id` = ? AND (`p_id` = ? OR `p_code` = ?) AND `p2s`.`status` = ? AND `p2s`.`quantity_in_stock` > ? AND `p2s`.`e_date` > NOW()");
		$statement->execute(array(store_id(), $p_id, $p_id, 1, 0));
		$product = $statement->fetch(PDO::FETCH_ASSOC);
		if (!$product) {
			throw new Exception($language->get('error_product_not_found'));
		}
		$product = array_replace($product, array('p_name' => html_entity_decode($product['p_name'])));
		echo json_encode($product); 
		exit();

	} catch (Exception $e) { 

	    header('HTTP/1.1 422 Unprocessable Entity');
	    header('Content-Type: application/json; charset=UTF-8');
	    echo json_encode(array('errorMsg' => $e->getMessage()));
	    exit();
	}
}

// fetch product list
if ($request->server['REQUEST_METHOD'] == 'GET' && isset($request->get['action_type']) && $request->get['action_type'] == 'PRODUCTLIST')
{
	try {

		$products = array();

		$limit = isset($request->get['limit']) ? (int)$request->get['limit'] : 50;
		$field = $request->get['field'];
		$query_string = $request->get['query_string'];
		$category_id = $request->get['category_id'];

		if (!$query_string) {
			if ($category_id) {
				$statement = $db->prepare("SELECT * FROM `products` 
				LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`) 
				WHERE `p2s`.`store_id` = ? AND `p2s`.`quantity_in_stock` > ? AND `p2s`.`status` = ? AND `products`.`category_id` = ?
				GROUP BY `product_id` ORDER BY `product_id` DESC");
				$statement->execute(array(store_id(), 0, 1, $category_id));
			} else {
				$statement = $db->prepare("SELECT `products`.*, `selling_item`.`item_id`, SUM(`selling_item`.`item_total`) as `total` FROM `selling_item` 
				RIGHT JOIN `products` ON (`selling_item`.`item_id` = `products`.`p_id`) 
				RIGHT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`) 
				WHERE `p2s`.`store_id` = ? AND `p2s`.`quantity_in_stock` > ? AND `p2s`.`status` = ?
				GROUP BY `product_id` ORDER BY `total` DESC LIMIT $limit");
				$statement->execute(array(store_id(), 0, 1));
			}
	    	$products = $statement->fetchAll(PDO::FETCH_ASSOC);
		}

		if ($query_string || (!$query_string && empty($products))) {
			if ($category_id) {
				$statement = $db->prepare("SELECT * FROM `products` 
				LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`) 
				WHERE `p2s`.`store_id` = ? AND `p2s`.`quantity_in_stock` > ? AND UPPER($field) LIKE '%" . strtoupper($query_string) . "%' AND `p2s`.`status` = ? AND `products`.`category_id` = ? ORDER BY `product_id` DESC");
				$statement->execute(array(store_id(), 0, 1, $category_id));
			} else {
				$statement = $db->prepare("SELECT * FROM `products` 
				LEFT JOIN `product_to_store` p2s ON (`products`.`p_id` = `p2s`.`product_id`) 
				WHERE `p2s`.`store_id` = ? AND `p2s`.`quantity_in_stock` > ? AND UPPER($field) LIKE '%" . strtoupper($query_string) . "%' AND `p2s`.`status` = ? ORDER BY `product_id` DESC LIMIT $limit");
				$statement->execute(array(store_id(), 0, 1));
			}
			$products = $statement->fetchAll(PDO::FETCH_ASSOC);
		}

		function updateImageValue(&$image, $key) {
		  if($key == 'p_image') {
		    if (FILEMANAGERPATH && is_file(FILEMANAGERPATH.$image) && file_exists(FILEMANAGERPATH.$image))  {
		    	$image = FILEMANAGERURL.$image;
		    } elseif (is_file(DIR_STORAGE . 'products/' . $image) && file_exists(DIR_STORAGE . 'products/' . $image)) {
		    	$image = root_url().'/storage/products'.$image;
		    } else {
		    	$image = root_url().'/assets/wonderpillars/img/noproduct.png';
		    }
		  }
		}
		array_walk_recursive($products, 'updateImageValue');

		function updateNameValue(&$data, $key) {
		  if($key == 'p_name') {
		    $data = htmlspecialchars_decode($data);
		  }
		}
		array_walk_recursive($products, 'updateNameValue');

	    header('Content-Type: application/json');
	    echo json_encode(array_values($products)); 
	    exit();
		
	} catch (Exception $e) {

	    header('HTTP/1.1 422 Unprocessable Entity');
	    header('Content-Type: application/json; charset=UTF-8');
	    echo json_encode(array('errorMsg' => $e->getMessage()));
	    exit();
	}
}