window.$(window.document).ready(function ($) {
    "use strict";

    //Notification options
    window.toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-center",
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };

    var username, password;

    var POSAPI = "/api/public";
    console.log(window.location.host);
    //var POSAPI = "http://13.233.161.62/api/public";

    // Login button click event
    $("#login-btn").on("click", function (e) {

        e.preventDefault();
        if($('#capcha').val() == ''){
            window.toastr.warning('Please check i am not a robot', "Warning!");
            return false;
        }
        var $usernameInput = $("input[name=\"username\"]");
        var $passwordInput = $("input[name=\"password\"]");
        username = $usernameInput.val();
        password = $passwordInput.val();
        loginToPos(username,password);

        // // Remove alert box, if exist
        // $(window.document).find(".alert").remove();

        // var $tag = $(this);#login-btn
        // var $usernameInput = $("input[name=\"username\"]");
        // var $passwordInput = $("input[name=\"password\"]");

        // // Disable Button & Input fields
        // var $btn = $tag.button("loading");
        // $usernameInput.attr("disabled", "disabled");
        // $passwordInput.attr("disabled", "disabled");

        // if (!window.isDemo || (!username || password)) {
        //   username = $usernameInput.val();
        //   password = $passwordInput.val();
        // }

        // // Ajax request to verify access
        // $.ajax({
        //     url: "index.php?action_type=LOGIN",
        //     type: "POST",
        //     dataType: "json",
        //     data: {username: username, password: password},
        //     success: function (response) {

        //       // Ajax request to verify access
        //       if (response.count_user_store > 1) {

        //           $.ajax({
        //             url: "/pos/pos-api/public/api/login",
        //             type: "POST",
        //             dataType: "json",
        //             data: {username: username, password: password},
        //             success: function (response) {
        //               localStorage.setItem('token', response.data.token);
        //               localStorage.setItem('email', username);
        //               localStorage.setItem('password', password);
        //               window.location = 'store_select.php';
        //             },
        //             error: function (response) {   
                              
        //           }});
                
                
        //       } else {
        //         $.ajax({
        //           url: window.baseUrl + "/admin/dashboard.php?active_store_id=" + response.store_id,
        //           method: "GET",
        //           dataType: "json"
        //         }).
        //         then(function(response) {
        //           var alertMsg = response.msg;
        //           window.toastr.success(alertMsg, "Success!");
        //           $.ajax({
        //             url: "/pos/pos-api/public/api/login",
        //             type: "POST",
        //             dataType: "json",
        //             data: {username: username, password: password},
        //             success: function (response) {
        //               localStorage.setItem('token', response.data.token);
        //               localStorage.setItem('email', username);
        //               localStorage.setItem('password', password);
        //               window.location = window.baseUrl + "/admin/dashboard.php";
        //             },
        //             error: function (response) {   
                              
        //           }});
                  
        //         }, function(response) {
        //           var errorMsg = JSON.parse(response.responseText);
        //           var alertMsg = "<div>";
        //               alertMsg += "<p>" + errorMsg.errorMsg + ".</p>";
        //               alertMsg += "</div>";
        //           window.toastr.warning(alertMsg, "Warning!");
        //         });
        //       }
        //     },
        //     error: function (response) {

        //         // Enable Button & Input fields
        //         $btn.button("reset");
        //         $usernameInput.attr("disabled", false);
        //         $passwordInput.attr("disabled", false);

        //         // Show error message
        //         window.toastr.warning(JSON.parse(response.responseText).errorMsg, "Warning!");
        //     }
        // });
    });

    function saveSession(){
            var user = JSON.parse(localStorage.getItem('user_posapi'));
            var db_info = user.db_info;
            $.ajax({
            url: "index.php?action_type=savesession",
            type: "POST",
            // dataType: "json",
            data: {db_info: db_info},
            success: function (response) {
              loginWeb();
            },
            error: function (response) {
            }
        });
    }

    function loginToPos(username,password){
        // Ajax request to verify access
        var $tag = $('#login-btn');
        var $btn = $tag.button("loading");
        var $usernameInput = $("input[name=\"username\"]");
        var $passwordInput = $("input[name=\"password\"]");
        $usernameInput.attr("disabled", "disabled");
        $passwordInput.attr("disabled", "disabled");
        $.ajax({
            url: POSAPI+"/login?username="+username+"&password="+password,
            type: "POST",
            dataType: "json",
            data: {username: username, password: password},
            success: function (response) {
              
              localStorage.setItem('token', response.data.token);
              localStorage.setItem('email', response.data.email);
              localStorage.setItem('password', password);
              localStorage.setItem('user_posapi', JSON.stringify(response.data));
              //save session
              //saveSession();
              loginWeb();
              
            },
            error: function (response) {
              console.log('loginpos', response)
                //Enable Button & Input fields
                $btn.button("reset");
                $usernameInput.attr("disabled", false);
                $passwordInput.attr("disabled", false);

                // Show error message
                window.toastr.warning('Invalid Credentials', "Warning!");
            }
        });
    }


    function loginWeb(){

      var $tag = $('#login-btn');
      var $usernameInput = $("input[name=\"username\"]");
      var $passwordInput = $("input[name=\"password\"]");

      // Disable Button & Input fields
      var $btn = $tag.button("loading");
      $usernameInput.attr("disabled", "disabled");
      $passwordInput.attr("disabled", "disabled");

      if (!window.isDemo || (!username || password)) {
        username = $usernameInput.val();
        password = $passwordInput.val();
      }

      var user = JSON.parse(localStorage.getItem('user_posapi'));
      var change_password = user.change_pswd;
      var db_info = user.db_info;
      // Ajax request to verify access
        $.ajax({
            url: "index.php?action_type=LOGIN",
            type: "POST",
            dataType: "json",
            data: {username: username, password: password, db_info: db_info},
            success: function (response) {
            user.db_info = null;
            localStorage.setItem('user_posapi', JSON.stringify(user));
            if(user.change_pswd == 1){
                localStorage.setItem('change_password', true);
                window.location.href = window.baseUrl +'/admin/password.php';
                return false;
            }
            else{
                window.location.reload();
                return false;
            }
            
              //console.log('response login', response);
              // Ajax request to verify access
              if (response.count_user_store > 1) {
                window.location = 'store_select.php';
                
              } else {
                $.ajax({
                  url: window.baseUrl + "/admin/dashboard.php?active_store_id=" + response.store_id,
                  method: "GET",
                  dataType: "json"
                }).
                then(function(response) {
                  var alertMsg = response.msg;
                  window.toastr.success(alertMsg, "Success!");
                  
                }, function(response) {
                  var errorMsg = JSON.parse(response.responseText);
                  var alertMsg = "<div>";
                      alertMsg += "<p>" + errorMsg.errorMsg + ".</p>";
                      alertMsg += "</div>";
                  window.toastr.warning(alertMsg, "Warning!");
                });
              }
            },
            error: function (response) {
console.log('response.responseText', response.responseText);
                // Enable Button & Input fields
                $btn.button("reset");
                $usernameInput.attr("disabled", false);
                $passwordInput.attr("disabled", false);

                // Show error message
                window.toastr.warning(JSON.parse(response.responseText).errorMsg, "Warning!");
            }
        });
    }

    $("#credentials table tbody tr").on("click", function (e) {
      e.preventDefault();
      username = $(this).find(".username").data("username"); 
      password = $(this).find(".password").data("password");
      $("input[name=\"username\"]").val(username); 
      $("input[name=\"password\"]").val(password); 
      $("#login-btn").trigger("click");
    });

    $(document).delegate(".activate-store", "click", function(e) {
        e.preventDefault();

        var $tag = $(this);
        var actionUrl = $tag.attr("href");
        
        $.ajax({
            url: actionUrl,
            method: "GET",
            cache: false,
            processData: false,
            contentType: false,
            dataType: "json"
        }).
        then(function(response) {
            $(":input[type=\"button\"]").prop("disabled", false);
            var alertMsg = response.msg;
            window.toastr.success(alertMsg, "Success!");
            window.location = window.baseUrl + "/admin/dashboard.php";
        }, function(response) {

          var errorMsg = JSON.parse(response.responseText);
          $(":input[type=\"button\"]").prop("disabled", false);
          var alertMsg = "<div>";
              alertMsg += "<p>" + errorMsg.errorMsg + ".</p>";
              alertMsg += "</div>";
          window.toastr.warning(alertMsg, "Warning!");

        });
    });

    // Send Resent Button
    $("#reset-btn").on("click", function (e) {
        e.preventDefault();

        // Remove alert box, if exist
        $(document).find(".alert").remove();

        if($('#capcha_reset').val() == ''){
            window.toastr.warning('Please check i am not a robot', "Warning!");
            return false;
        }

        // Declare button and input fields
        var $tag = $(this);
        var $emailInput = $("input[name=\"email\"]");

        // Disable Button & Input fields
        var $btn = $tag.button("loading");
        $emailInput.attr("disabled", "disabled");
        $("body").addClass("overlay-loader");

        // Ajax request to verify access
        $.ajax({
            url: "index.php?action_type=SEND_PASSWORD_RESET_CODE",
            type: "POST",
            dataType: "json",
            data: {email: $emailInput.val(), capcha_reset: true},
            success: function (response) {
                $("body").removeClass("overlay-loader");
                $btn.button("reset");
                $("input[name=\"email\"]").attr("disabled", false);
                // show success message
                var successMsg = "<div class=\"alert alert-success\">";
                successMsg += "<p><i class=\"fa fa-check\"></i> " + response.msg + ".</p>";
                successMsg += "</div>";
                $(window.document).find(".modal-body").before(successMsg);
            },
            error: function (response) {
                $("body").removeClass("overlay-loader");
                // enable Button & Input fields
                $btn.button("reset");
                $("input[name=\"email\"]").attr("disabled", false);
                // show error message
                var alertMsg = "<div class=\"alert alert-danger\">";
                alertMsg += "<p><i class=\"fa fa-warning\"></i> " + JSON.parse(response.responseText).errorMsg + ".</p>";
                alertMsg += "</div>";
                $(window.document).find(".modal-body").before(alertMsg);
            }
        });
    });

    // Reset Confirm Button
    $("#reset-confirm-btn").on("click", function (e) {
        e.preventDefault();

        // Remove alert box, if exist
        $(document).find(".alert").remove();

        // Declare button and input fields
        var $tag = $(this);

        var $resetCodeInput = $("input[name=\"fp_code\"]");
        var $passwordInput = $("input[name=\"password\"]");
        var $passwordConfirmInput = $("input[name=\"password_confirm\"]");

        // Disable Button & Input fields
        var $btn = $tag.button("loading");
        $passwordInput.attr("disabled", "disabled");
        $passwordConfirmInput.attr("disabled", "disabled");
        $("body").addClass("overlay-loader");

        // Ajax request to verify access
        $.ajax({
            url: "password_reset.php?action_type=RESET",
            type: "POST",
            dataType: "json",
            data: {fp_code: $resetCodeInput.val(),password: $passwordInput.val(),password_confirm: $passwordConfirmInput.val()},
            success: function (response) {

                $("body").removeClass("overlay-loader");

                $btn.button("reset");
                $passwordInput.attr("disabled", false);
                $passwordConfirmInput.attr("disabled", false);

                window.toastr.success(response.msg, "Success!");

                window.location.href = 'index.php';
            },
            error: function (response) {

                $("body").removeClass("overlay-loader");

                // Enable Button & Input fields
                $btn.button("reset");
                $passwordInput.attr("disabled", false);
                $passwordConfirmInput.attr("disabled", false);

                window.toastr.warning(JSON.parse(response.responseText).errorMsg, "Warning!");
            }
        });
    });

    

    // Centering Loginbox horizontally
    var BoxCentering = function () {
        var $loginBox = $(".login-box");
        var $windowHeight = $(window).height();
        var $loginBoxHeight = $loginBox.innerHeight();
        var $marginTop = ($windowHeight / 2) - ($loginBoxHeight / 2);
        $loginBox.css("marginTop", $marginTop + "px");
    };

    // Login box keeps at center always
    BoxCentering();

    // Centering Login box during scroll
    $(window).on("resize", function () {
        BoxCentering();
    });



});