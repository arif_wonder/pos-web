window.angularApp.controller("DashboardController", [
    "$scope",
    "API_URL",
    "window",
    "jQuery",
    "$http",
    "ProductCreateModal",
    "BoxCreateModal",
    "SupplierCreateModal",
    "CustomerCreateModal",
    "UserCreateModal",
    "UserGroupCreateModal",
    "BuyingProductModal",
    "EmailModal",
function ($scope,
    API_URL,
    window,
    $,
    $http,
    ProductCreateModal,
    BoxCreateModal,
    SupplierCreateModal,
    CustomerCreateModal,
    UserCreateModal,
    UserGroupCreateModal,
    BuyingProductModal,
    EmailModal
) {
    "use strict";

    // create new product
    $scope.createNewProduct = function () {
        $scope.hideCategoryAddBtn = true;
        $scope.hideSupAddBtn = true;
        $scope.hideBoxAddBtn = true;
        ProductCreateModal($scope);
    };

    // create new box
    $scope.createNewBox = function () {
        BoxCreateModal($scope);
    };

    // create new supplier
    $scope.createNewSupplier = function () {
        SupplierCreateModal($scope);
    };

    // create new customer
    $scope.createNewCustomer = function () {
        CustomerCreateModal($scope);
    };

    // create new user
    $scope.createNewUser = function () {
        $scope.hideGroupAddBtn = true;
        UserCreateModal($scope);
    };

    // create new usergroup
    $scope.createNewUsergroup = function () {
        UserGroupCreateModal($scope);
    };

    if (!window.totalProduct && window.user.group_name == 'admin') {
        $scope.BuyingProductModalCallback = function ($scopeData) {
            // window.location = baseUrl+'/admin/dashboard.php';
        };

        // $scope.ProductCreateModalCallback = function ($scopeData) {
        //     $scope.product = $scopeData.product;
        //     BuyingProductModal($scope);
        // };

        // $scope.createNewProduct = function () {
        //     $scope.hideCategoryAddBtn = true;
        //     $scope.hideSupAddBtn = true;
        //     $scope.hideBoxAddBtn = true; 
        //     ProductCreateModal($scope);
        // };
        // $scope.createNewProduct();
    }

    
     // $("#change_password").submit(function(e){
     //    e.preventDefault();
     //    var $tag = $(this);
     //    var form = $($tag.data("form"));

     //    $.ajax({
     //        url: window.baseUrl + "/_inc/ajax.php?action_type=changepassword",
     //        method: "POST",
     //        data: form.serialize(),
     //        success: function (response) {
     //          console.log(response);
     //        },
     //        error: function (response) {   
                      
     //    }});
     // });

}]);