window.angularApp.controller("StoreActionController", [
    "$scope",
    "API_URL",
    "POS_ADMIN_API_URL",
    "window",
    "jQuery",
    "$compile",
    "$uibModal",
    "$http",
    "$sce",
function (
    $scope,
    API_URL,
    POS_ADMIN_API_URL,
    window,
    $,
    $compile,
    $uibModal,
    $http,
    $sce
) {
    "use strict";

    $('#token').val(localStorage.getItem('admin_token'));
    // update store
    $("#update-store-btn").on("click", function(e) {
        e.preventDefault();

        var $tag = $(this);
        var $btn = $tag.button("loading");
        var form = $($tag.data('form'));
        form.find(".alert").remove();
        var actionUrl = form.attr("action");
        
        $http({
            url: window.baseUrl + "/_inc/" + actionUrl,
            method: "POST",
            data: form.serialize(),
            cache: false,
            processData: false,
            contentType: false,
            dataType: "json"
        }).
        then(function(response) {

            $btn.button("reset");
            $(":input[type=\"button\"]").prop("disabled", false);
            var alertMsg = response.data.msg;
            window.location.href = 'store.php';
            //window.toastr.success(alertMsg, "Success!");

        }, function(response) {

            $btn.button("reset");
            $(":input[type=\"button\"]").prop("disabled", false);
            var alertMsg = "<div>";
            window.angular.forEach(response.data, function(value) {
                alertMsg += "<p>" + value + ".</p>";
            });
            alertMsg += "</div>";
            window.toastr.warning(alertMsg, "Warning!");
        });
    });

    $(document).ready(function(){
        
        
        if(!localStorage.getItem('admin_token') || localStorage.getItem('admin_token') == 'null'){
            
            

                var username = localStorage.getItem('email');
                var password = localStorage.getItem('password');

                $.ajax({
                    url: POS_ADMIN_API_URL+"login",
                    type: "POST",
                    dataType: "json",
                    data: {username: username, password: password},
                    success: function (response) {
                      localStorage.setItem('admin_token', response.data.token);
                       localStorage.setItem('admin_token', response.data.token);
                       $('#token').val(localStorage.getItem('admin_token'));
                    },
                    error: function (response) {   
                              
                }});
            
        }
        
    })

    // create store
    $("#create-store-btn").on("click", function(e) {


        e.preventDefault();

        var $tag = $(this);
        var $btn = $tag.button("loading");
        var form = $($tag.data('form'));
        form.find(".alert").remove();
        var actionUrl = form.attr("action");
         console.log(actionUrl);
        $http({
            url: window.baseUrl + "/_inc/" + actionUrl,
            method: "POST",
            data: form.serialize(),
            cache: false,
            processData: false,
            contentType: false,
            dataType: "json"
        }).
        then(function(response) {

            $btn.button("reset");
            $(":input[type=\"button\"]").prop("disabled", false);
            var alertMsg = response.data.msg;
            window.toastr.success(alertMsg, "Success!");
            window.location = 'store_single.php?store_id=' + response.data.id + '&box_state=open';

        }, function(response) {

            $btn.button("reset");
            $(":input[type=\"button\"]").prop("disabled", false);
            var alertMsg = "<div>";
            window.angular.forEach(response.data, function(value) {
                alertMsg += "<p>" + value + ".</p>";
            });
            alertMsg += "</div>";
            window.toastr.warning(alertMsg, "Warning!");
        });
    });

    $scope.isMailFunction = false;
    $scope.isSEndMail = false;
    $scope.isSMTP = false;
    $scope.triggerMailServer = function(value) {
        if (value == "mail_function") {
            $scope.isMailFunction = true;
            $scope.isSendMail = false;
            $scope.isSMTP = false;
        }
        if (value == "send_mail") {
            $scope.isMailFunction = false;
            $scope.isSendMail = true;
            $scope.isSMTP = false;
        }
        if (value == "smtp_server") {
            $scope.isMailFunction = false;
            $scope.isSendMail = false;
            $scope.isSMTP = true;
        }
    };
    $scope.triggerMailServer(window.settings.email_driver);

    $('#email_driver').on('select2:select', function (e) {
        var data = e.params.data;
        var isMailFunction = false;
        var isSendMail = false;
        var isSMTP = false;

        if (data.element.value == "mail_function") {
            isMailFunction = true;
            isSendMail = false;
            isSMTP = false;
        }
        if (data.element.value == "send_mail") {
            isMailFunction = false;
            isSendMail = true;
            isSMTP = false;
        }
        if (data.element.value == "smtp_server") {
            isMailFunction = false;
            isSendMail = false;
            isSMTP = true;
        }

        $scope.$apply(function () {
            $scope.isMailFunction = isMailFunction;
            $scope.isSendMail = isSendMail;
            $scope.isSMTP = isSMTP;
        });
        
    });

    $('#create-store-btn-next').on("click", function(e) {
        $('#active_payment_section').trigger('click');
        $('#image-section').hide();
        $('#first_step_button').hide();
    });

    $('#back-btn-previous').on("click", function(e) {
        e.preventDefault();
        $('#image-section').show();
        $('#first_step_button').show();
        $('#active_general_section').trigger('click');

    });

    $('.active_payment_section').on('click', function(){
        $('#image-section').hide();
        $('#first_step_button').hide();
    });

     $('.active_general_section').on("click", function(e) {
        console.log('working');
        e.preventDefault();
        $('#image-section').show();
        $('#first_step_button').show();
    });

     $('#active_general_section').on("click", function(e) {
        console.log('working');
        e.preventDefault();
        $('#image-section').show();
        $('#first_step_button').show();
    });
}]);