window.angularApp.controller("ReportPaymentController", [
    "$scope",
    "API_URL",
    "window",
    "jQuery",
    "$compile",
    "$uibModal",
    "$http",
    "$sce",
    "EmailModal",
function (
    $scope,
    API_URL,
    window,
    $,
    $compile,
    $uibModal,
    $http,
    $sce,
    EmailModal
) {
    "use strict";

    var dt = $("#report-report-list");
    
    var $from = window.getParameterByName("from");
    var $to = window.getParameterByName("to");

    //================
    // start datatable
    //================

    dt.dataTable({
        "oLanguage": {sProcessing: "<img src='../assets/wonderpillars/img/loading2.gif'>"},
        "processing": true,
        "dom": 'lfBrtip',
        "serverSide": true,
        "ajax": API_URL + "/_inc/report_payment.php?from="+$from+"&to="+$to,
        "order": [[ 1, "desc"]],
        "fixedHeader": true,
        "columnDefs": [
            {"targets": [0], "orderable": false},
          {"className": "text-right", "targets": [3]},
          {"className": "text-center", "targets": [0,1]},
          { 
                "targets": [0],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#report-report-list thead tr th:eq(0)").html());
                }
            },
            { 
                "targets": [1],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#report-report-list thead tr th:eq(1)").html());
                }
            },
            { 
                "targets": [2],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#report-report-list thead tr th:eq(2)").html());
                }
            },
            { 
                "targets": [3],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#report-report-list thead tr th:eq(3)").html());
                }
            },
        ],
        "aoColumns": [
            {data : 'select'},          
            {data : 'payment_id'},          
            {data : 'payment_name'},          
            {data : 'paid_amount'}
            // {data : 'datetime'}
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var pageTotal;
            var api = this.api();
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            // Total over all pages at column 2
            pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            // Update footer
            $( api.column( 2 ).footer() ).html(
                window.formatDecimal(pageTotal, 2)
            );
        },
        "pageLength": window.settings.datatable_item_limit,
        "buttons": [
            {
                extend:    'print',
                footer: true,
                text:      '<i class="fa fa-print"></i>',
                titleAttr: 'Print',
                title: "Payment Report",
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .append(
                            '<div><b><i>Powered by: wonderpillars.com</i></b></div>'
                        )
                        .prepend(
                            '<div class="dt-print-heading"><img class="logo" src="'+window.logo+'"/><h2 class="title">'+window.store.name+'</h2><p>Printed on: '+window.formatDate(new Date())+'</p></div>'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend:    'copyHtml5',
                footer: true,
                text:      '<i class="fa fa-files-o"></i>',
                titleAttr: 'Copy',
                title: window.store.name + " > Payment Report",
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend:    'excelHtml5',
                footer: true,
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                title: window.store.name + " > Payment Report",
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend:    'csvHtml5',
                footer:     true,
                text:      '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV',
                title: window.store.name + " > Payment Report",
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                }
            },
            {
                extend:    'pdfHtml5',
                footer: true,
                download: 'open',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                title: window.store.name + " > Payment Report",
                exportOptions: {
                    columns: [ 0, 1, 2, 3 ]
                },
                customize: function (doc) {
                    doc.content[1].table.widths =  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    doc.pageMargins = [10,10,10,10];
                    doc.defaultStyle.fontSize = 8;
                    doc.styles.tableHeader.fontSize = 8;
                    doc.styles.title.fontSize = 10;
                    // Remove spaces around page title
                    doc.content[0].text = doc.content[0].text.trim();
                    // Header
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'center',
                        fontSize: 8,
                        text: 'Printed on: '+window.formatDate(new Date()),
                    });
                    // Create a footer
                    doc['footer']=(function(page, pages) {
                        return {
                            columns: [
                                'Powered by wonderpillars.com',
                                {
                                    // This is the right column
                                    alignment: 'right',
                                    text: ['page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
                                }
                            ],
                            margin: [10, 0]
                        };
                    });
                    // Styling the table: create style object
                    var objLayout = {};
                    // Horizontal line thickness
                    objLayout['hLineWidth'] = function(i) { return 0.5; };
                    // Vertikal line thickness
                    objLayout['vLineWidth'] = function(i) { return 0.5; };
                    // Horizontal line color
                    objLayout['hLineColor'] = function(i) { return '#aaa'; };
                    // Vertical line color
                    objLayout['vLineColor'] = function(i) { return '#aaa'; };
                    // Left padding of the cell
                    objLayout['paddingLeft'] = function(i) { return 4; };
                    // Right padding of the cell
                    objLayout['paddingRight'] = function(i) { return 4; };
                    // Inject the object in the document
                    doc.content[1].layout = objLayout;
                }
            }
        ],
    });

    var $inc = 0;
    var t = setInterval(function() {
        if ($("#report-report-list tbody tr").length) {
            $("#report-report-list tbody tr").each(function(i){
                if (!$(this).find("td:first-child").hasClass("dataTables_empty")) {
                    // $(this).find("td:first-child").text((i + 1));
                }
            });
            $inc++;
        }
        if ($inc) {
            clearInterval(t);
        }
    }, 100);

    //================
    // end datatable
    //================

     /*********** SELECT AND DESELECT ROW(S) ***********/
    $(document).on( 'change', 'input[name*=\'select\']', function () {
        var checked = $(this).prop('checked');
        if (checked) {
            dt.DataTable().row($(this).closest('tr')).select();
        } else {
            dt.DataTable().row($(this).closest('tr')).deselect();
        }
    } );
    $(document).on( 'change', 'input.check-all', function () {
        var checked = $(this).prop('checked');
        if (checked) {
            dt.DataTable().rows().select();
            $('table.dataTable input[type="checkbox"]').each(function(){
                $(this).prop('checked', true);
            });
        } else {
            dt.DataTable().rows().deselect();
            $('table.dataTable input[type="checkbox"]').each(function(){
                $(this).prop('checked', false);
            });
        }
    } );
    /*********** SELECT AND DESELECT ROW(S) END ***********/

    // append email button into datatable buttons
    // $(".dt-buttons").append("<button id=\"email-btn\" class=\"btn btn-default buttons-email\" tabindex=\"0\" aria-controls=\"invoice-invoice-list\" type=\"button\" title=\"Email\"><span><i class=\"fa fa-envelope\"></i></span></button>");
    
    // send list through email
    $("#email-btn").on( "click", function (e) {
        e.stopPropagation();
        e.preventDefault();
        var thehtml = dt.html();
        EmailModal({template: "report", subject: "Payment Report", title:"Payment Report", html: thehtml});
    });

}]);