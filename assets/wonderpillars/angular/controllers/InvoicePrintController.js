window.angularApp.controller("InvoicePrintController", [
    "$scope",
    "API_URL",
    "POS_API_URL",
    "TOKEN",
    "window",
    "jQuery",
    "$http",
function (
    $scope,
    API_URL,
    POS_API_URL,
    TOKEN,
    window,
    $,
    $http,
) {
    "use strict";

       
    $http.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('token');
    $scope.invoice_info = false;
    $scope.invoice_items = false;
    $scope.invoice_id = false;
    $scope.isLoading = true;

   

    $scope.discountAmount = function(data){
        var dis = 0;
        if(data.item_discount){
            dis =  data.item_discount;
        }
        return (parseInt(data.item_total)-parseInt(dis));
    }

    $scope.getSubtotal = function(){

        var sub_total = 0;
        window.angular.forEach($scope.invoice_items, function(item, key) {
            sub_total += (parseInt(item.product.buying_item.item_selling_price)- parseInt(item.product.p_discount));
        });
        return sub_total;
    }

    $scope.getPromotionalDiscount = function(){

        var sub_total = 0;
        window.angular.forEach($scope.invoice_items, function(item, key) {
            sub_total += (parseInt(item.product.buying_item.item_selling_price)- parseInt(item.product.p_discount));
        });
        return $scope.invoice_info.discount_amount;
    }

    $scope.getAllProductAmount = function(){
        var product_discount = 0;
        window.angular.forEach($scope.invoice_items, function(item, key) {
            if(item.product.p_discount){
                 product_discount += (parseInt(item.product.p_discount));
            }
        });
        return product_discount;
    }

    $scope.calculateTax = function(data){ 
        return data.item_tax;
    }

    $scope.calculateTaxPerItem = function(data){
        var tax = data.item_tax
        var taxPerItem = data.item_total- eval(tax+data.item_discount);
        return taxPerItem;
    }

    $scope.calculatePayableAmount = function(data){
        var gross_totalPlusprevious_due = parseFloat(data.gross_total)+parseFloat(data.previous_due);
        var discountplus_paid_by_credit = parseFloat(data.discount_amount)+parseFloat(data.paid_by_credit);
        
        return eval(gross_totalPlusprevious_due-discountplus_paid_by_credit);
    }

    $scope.paid_amount = function(data){
        return parseFloat(data.paid_amount)-parseFloat(data.paid_by_credit);
    }

    $(document).ready(function(){

        var username = localStorage.getItem('email');
        var password = localStorage.getItem('password');

        
        
        $http({
            url: POS_API_URL+"/login?username="+username+'&password='+password,
            method: "POST",
            cache: false,
            processData: false
        }).
        then(function(response) {
            localStorage.setItem('token', response.data.data.token);
            var url = window.location.href.split('=');
            var url2 = url[1].split('&');
            $http({
            url: POS_API_URL + "/invoice/get/"+url2[0],
            method: "GET",
            cache: false,
            processData: false
            }).
            then(function(response) {
                $scope.invoice_info = response.data.data.invoice_info;
                $scope.invoice_items = response.data.data.invoice_items;
                $scope.invoice_id = response.data.data.invoice_id;
                $scope.isLoading = false;
                if(url2[1] !== undefined){
                     setTimeout(function(){ 
                         window.print();
                     }, 1000);
                }

            }, function(response) {
                $scope.isLoading = false;
                if(response.status === 401 && response.statusText == 'Unauthorized'){
                    window.location.href = 'logout.php'
                }
               
            });

        }, function(response) {

           
        });


        
    })
    
    
}]);