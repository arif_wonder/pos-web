window.angularApp.controller("BulkSmsController", [
    "$scope",
    "API_URL",
    "POS_API_URL",
    "TOKEN",
    "window",
    "jQuery",
    "$http",
    "DiscountByCouponsEditModal",
    "DiscountByCouponsDeleteModal",
    "EmailModal",
function (
    $scope,
    API_URL,
    POS_API_URL,
    TOKEN,
    window,
    $,
    $http,
    DiscountByCouponsEditModal,
    DiscountByCouponsDeleteModal,
    EmailModal
) {
    "use strict";

        $scope.customerArray = [];
        $http.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('token');
        $scope.showCustomerList = function () {
        $http({
            url: POS_API_URL + "/customer",
            method: "GET",
            cache: false,
            processData: false,
            contentType: false,
            dataType: "json"
        }).
        then(function(response) {
            $scope.hideCustomerDropdown = false;
            $scope.customerArray = response.data.data;
           

        }, function(response) {
            if(response.status === 401 && response.statusText == 'Unauthorized'){
                var username =  localStorage.getItem('email');
                var password =  localStorage.getItem('password');
                $scope.relogin(username, password, 'showcustomer');
            }
            else{
                 window.toastr.warning(response.data.errorMsg, "Warning!");
            }
            if (window.store.sound_effect == 1) {
                window.storeApp.playSound("error.mp3");
            }
           
        });
    };

    //click checkbox
    $(document).on('click', '#select_all', function(){
        if($(this).is(":checked")){
            $('.select_option').prop('selected', 'selected');
        }
        else{
             $('.select_option').prop('selected', false);
        }
    });

    //select option
    $(document).on('click', '.select_option', function(){
        $('#select_all').prop('checked',false)
    });

    // create brand
    $(document).delegate("#form-submit", "click", function(e) {
       
        e.preventDefault();
        var message = $('#description').val();
        var customer_phones = [];
        var i = 0;
        $("input:checkbox.selected_customer:checked").each(function(index,customer){
            if(i != 0){
                customer_phones = customer_phones+','+customer.value;
            }
            else{
                customer_phones = customer.value;
            }
            i++;
        });
        

        if(!customer_phones.length){
            window.toastr.warning('Please select customer', "Warning!");
            return false;
        }
        if(message == ''){
            window.toastr.warning('Please write message', "Warning!");
            return false;
        }

        var $tag = $(this);
        var $btn = $tag.button("loading");
        $http({
            url: POS_API_URL + "/message/bulk/send?customer_phones="+customer_phones+"&message="+message,
            method: "POST",
            cache: false,
            processData: false
        }).
        then(function(response) {
            $btn.button("reset");
            $(":input[type=\"button\"]").prop("disabled", false);
            $('#description').val('');
            $('#select_all').prop('checked', false);
            $('.selectoption').val(null).trigger('change');
            window.toastr.success('Message send successfully', "Success!");

        }, function(response) {

            $btn.button("reset");
            $(":input[type=\"button\"]").prop("disabled", false);
            var alertMsg = "<div>";
            window.angular.forEach(response.data, function(value) {
                alertMsg += "<p>" + value + ".</p>";
            });
            alertMsg += "</div>";
            window.toastr.warning(alertMsg, "Warning!");
        });
    });

    $scope.relogin = function (username, password, $url = false) {
        $.ajax({
            url: POS_API_URL+"/login",
            type: "POST",
            dataType: "json",
            data: {username: username, password: password},
            success: function (response) {
              localStorage.setItem('token', response.data.token);
              localStorage.setItem('email', username);
              localStorage.setItem('password', password);
              $http.defaults.headers.common['Authorization'] = 'Bearer '+response.data.token;
              if($url == 'showcustomer'){
                $scope.showCustomerList();
              }
            },
            error: function (response) {   
                      
        }});
    };


    $(document).ready(function(e) {
        $scope.showCustomerList();
        setTimeout(function(){ 
            $('.selectoption').removeClass("select2-hidden-accessible");
            $('.select2').remove();
        }, 200);
    });

    
    

    // append email button into datatable buttons
    // $(".dt-buttons").append("<button id=\"email-btn\" class=\"btn btn-default buttons-email\" tabindex=\"0\" aria-controls=\"invoice-invoice-list\" type=\"button\" title=\"Email\"><span><i class=\"fa fa-envelope\"></i></span></button>");
    
    
}]);