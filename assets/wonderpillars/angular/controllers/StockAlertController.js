window.angularApp.controller("StockAlertController", [
    "$scope",
    "API_URL",
    "window",
    "jQuery",
    "$compile",
    "$uibModal",
    "$http",
    "$sce",
    "BuyingProductModal",
    "EmailModal",
function (
    $scope,
    API_URL,
    window,
    $,
    $compile,
    $uibModal,
    $http,
    $sce,
    BuyingProductModal,
    EmailModal
) {
    "use strict";

    var dt = $("#product-product-list");
    var i;

    var hideColums = dt.data("hide-colums").split(",");
    var hideColumsArray = [];
    if (hideColums.length) {
        for (i = 0; i < hideColums.length; i+=1) {     
           hideColumsArray.push(parseInt(hideColums[i]));
        }
    }
    
    //================
    // start datatable
    //================

    dt.dataTable({
        "oLanguage": {sProcessing: "<img src='../assets/wonderpillars/img/loading2.gif'>"},
        "processing": true,
        "dom": "lfBrtip",
        "serverSide": true,
        "ajax": API_URL + "/_inc/product.php?stock_query=1&productwise=1",
        "fixedHeader": true,
        "order": [[ 0, "asc"]],
        "columnDefs": [
            {"targets": 6, "orderable": false},
            {"className": "text-right", "targets": [4]},
            {"className": "text-center", "targets": [0, 5]},
            { "visible": false, "targets": hideColumsArray},
            { 
                "targets": [0],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#product-product-list thead tr th:eq(0)").html());
                }
            },
            { 
                "targets": [1],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#product-product-list thead tr th:eq(1)").html());
                }
            },
            { 
                "targets": [2],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#product-product-list thead tr th:eq(2)").html());
                }
            },
            { 
                "targets": [3],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#product-product-list thead tr th:eq(3)").html());
                }
            },
            { 
                "targets": [4],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#product-product-list thead tr th:eq(4)").html());
                }
            },
            { 
                "targets": [5],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#product-product-list thead tr th:eq(5)").html());
                }
            },
            { 
                "targets": [6],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#product-product-list thead tr th:eq(6)").html());
                }
            },
        ],
        "aoColumns": [
            {data : "p_id"},
            {
                "mData": "p_name",
                "orderable": false,
                "mRender": function (data, type, row) {
                    return "<a href=\"product.php?p_id=" + row['p_id'] + "&p_name=" + row['p_name'] + "\">" + row['p_name'] + "</a";
                }
            },
            {data : "supplier"},
            {data : "supplier_mobile"},
            {data : "buy_price"},
            {data : "danger_stock"},
            {data : "buy_btn"}
        ],
        "pageLength": window.settings.datatable_item_limit,
        "buttons": [
            {
                extend:    "print",
                footer: true,
                text:"<i class=\"fa fa-print\"></i>",
                titleAttr: "Print",
                title: "Stock Alert",
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .append(
                            '<div><b><i>Powered by: wonderpillars.com</i></b></div>'
                        )
                        .prepend(
                            '<div class="dt-print-heading"><img class="logo" src="'+window.logo+'"/><h2 class="title">'+window.store.name+'</h2><p>Printed on: '+window.formatDate(new Date())+'</p></div>'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            // {
            //     extend:    "copyHtml5",
            //     footer: true,
            //     text:      "<i class=\"fa fa-files-o\"></i>",
            //     titleAttr: "Copy",
            //     title: window.store.name + " > Stock Alert",
            //     exportOptions: {
            //         columns: [ 0, 1, 2, 3, 4, 5 ]
            //     }
            // },
            {
                extend:    "excelHtml5",
                footer: true,
                text:      "<i class=\"fa fa-file-excel-o\"></i>",
                titleAttr: "Excel",
                title: window.store.name + " > Stock Alert",
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend:    "csvHtml5",
                footer:     true,
                text:      "<i class=\"fa fa-file-text-o\"></i>",
                titleAttr: "CSV",
                title: window.store.name + " > Stock Alert",
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend:    "pdfHtml5",
                footer: true,
                download: "open",
                text:      "<i class=\"fa fa-file-pdf-o\"></i>",
                titleAttr: "PDF",
                title: window.store.name + " > Stock Alert",
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                },
                customize: function (doc) {
                    doc.content[1].table.widths =  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    doc.pageMargins = [10,10,10,10];
                    doc.defaultStyle.fontSize = 8;
                    doc.styles.tableHeader.fontSize = 8;
                    doc.styles.title.fontSize = 10;
                    // Remove spaces around page title
                    doc.content[0].text = doc.content[0].text.trim();
                    // Header
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'center',
                        fontSize: 8,
                        text: 'Printed on: '+window.formatDate(new Date()),
                    });
                    // Create a footer
                    doc['footer']=(function(page, pages) {
                        return {
                            columns: [
                                'Powered by wonderpillars.com',
                                {
                                    // This is the right column
                                    alignment: 'right',
                                    text: ['page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
                                }
                            ],
                            margin: [10, 0]
                        };
                    });
                    // Styling the table: create style object
                    var objLayout = {};
                    // Horizontal line thickness
                    objLayout['hLineWidth'] = function(i) { return 0.5; };
                    // Vertikal line thickness
                    objLayout['vLineWidth'] = function(i) { return 0.5; };
                    // Horizontal line color
                    objLayout['hLineColor'] = function(i) { return '#aaa'; };
                    // Vertical line color
                    objLayout['vLineColor'] = function(i) { return '#aaa'; };
                    // Left padding of the cell
                    objLayout['paddingLeft'] = function(i) { return 4; };
                    // Right padding of the cell
                    objLayout['paddingRight'] = function(i) { return 4; };
                    // Inject the object in the document
                    doc.content[1].layout = objLayout;
                }
            }
        ],
    });

    //================
    // end datatable
    //================

     /*********** SELECT AND DESELECT ROW(S) ***********/
    $(document).on( 'change', 'input[name*=\'select\']', function () {
        var checked = $(this).prop('checked');
        if (checked) {
            dt.DataTable().row($(this).closest('tr')).select();
        } else {
            dt.DataTable().row($(this).closest('tr')).deselect();
        }
    } );
    $(document).on( 'change', 'input.check-all', function () {
        var checked = $(this).prop('checked');
        if (checked) {
            dt.DataTable().rows().select();
            $('table.dataTable input[type="checkbox"]').each(function(){
                $(this).prop('checked', true);
            });
        } else {
            dt.DataTable().rows().deselect();
            $('table.dataTable input[type="checkbox"]').each(function(){
                $(this).prop('checked', false);
            });
        }
    } );
    /*********** SELECT AND DESELECT ROW(S) END ***********/

    // buy product
    $(document).delegate(".buy-product", "click", function (e) {
        e.stopPropagation();
        e.preventDefault();

        var product = dt.DataTable().row($(this).closest("tr")).data();
       
        BuyingProductModal(product);
    });

    // append email button into datatable buttons
    // $(".dt-buttons").append("<button id=\"email-btn\" class=\"btn btn-default buttons-email\" tabindex=\"0\" aria-controls=\"invoice-invoice-list\" type=\"button\" title=\"Email\"><span><i class=\"fa fa-envelope\"></i></span></button>");
    
    // send product list through email
    $("#email-btn").on( "click", function (e) {
        e.stopPropagation();
        e.preventDefault();
        dt.find("thead th:nth-child(7), tbody td:nth-child(7), tfoot th:nth-child(7)").addClass("hide-in-mail");
        var thehtml = dt.html();
        EmailModal({template: "default", subject: "Product List (Stock Alert)", title:"Product (Stock Alert)", html: thehtml});
    });
}]);