window.angularApp.controller("PaymentController", [
    "$scope",
    "API_URL",
    "window",
    "jQuery",
    "$compile",
    "$uibModal",
    "$http",
    "$sce",
    "PaymentEditModal",
    "PaymentDeleteModal",
function (
    $scope,
    API_URL,
    window,
    $,
    $compile,
    $uibModal,
    $http,
    $sce,
    PaymentEditModal,
    PaymentDeleteModal
) {
    "use strict";

    var dt = $("#payment-payment-list");
    var paymentId;
    var i;
    
    var hideColums = dt.data("hide-colums").split(",");
    var hideColumsArray = [];
    if (hideColums.length) {
        for (i = 0; i < hideColums.length; i+=1) {     
           hideColumsArray.push(parseInt(hideColums[i]));
        }
    }

    //================
    // start datatable
    //================

    dt.dataTable({
        "oLanguage": {sProcessing: "<img src='../assets/wonderpillars/img/loading2.gif'>"},
        "processing": true,
        "dom": "lfBrtip",
        "serverSide": true,
        "ajax": API_URL + "/_inc/payment.php",
        "fixedHeader": true,
        "order": [[ 1, "asc"]],
        "columnDefs": [
            {"targets": [0,5, 6], "orderable": false},
            {"className": "text-center", "targets": [0,1, 2, 4, 5, 6]},
            {"visible": false,  "targets": hideColumsArray},
            { 
                "targets": [0],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#payment-payment-list thead tr th:eq(0)").html());
                }
            },
            { 
                "targets": [1],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#payment-payment-list thead tr th:eq(0)").html());
                }
            },
            { 
                "targets": [2],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#payment-payment-list thead tr th:eq(1)").html());
                }
            },
            { 
                "targets": [3],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#payment-payment-list thead tr th:eq(2)").html());
                }
            },
            { 
                "targets": [4],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#payment-payment-list thead tr th:eq(3)").html());
                }
            },
            { 
                "targets": [5],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#payment-payment-list thead tr th:eq(4)").html());
                }
            },
            { 
                "targets": [6],
                'createdCell':  function (td, cellData, rowData, row, col) {
                   $(td).attr('data-title', $("#payment-payment-list thead tr th:eq(5)").html());
                }
            },
            
        ],
        "aoColumns": [
            {data : "select"},
            {data : "payment_id"},
            {data : "name"},
            {data : "details"},
            {data : "status"},
            {data : "btn_edit"},
            {data : "btn_delete"}
        ],
        "pageLength": window.settings.datatable_item_limit,
        "buttons": [
            {
                extend:    "print",
                text:      "<i class=\"fa fa-print\"></i>",
                titleAttr: "Print",
                title: "Payment Method List",
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .append(
                            '<div><b><i>Powered by: wonderpillars.com</i></b></div>'
                        )
                        .prepend(
                            '<div class="dt-print-heading"><img class="logo" src="'+window.logo+'"/><h2 class="title">'+window.store.name+'</h2><p>Printed on: '+window.formatDate(new Date())+'</p></div>'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                },
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                }
            },
            // {
            //     extend:    "copyHtml5",
            //     text:      "<i class=\"fa fa-files-o\"></i>",
            //     titleAttr: "Copy",
            //     title: window.store.name + " > Payment Method List",
            //     exportOptions: {
            //         columns: [ 0, 1, 2, 3, 4 ]
            //     }
            // },
            {
                extend:    "excelHtml5",
                text:      "<i class=\"fa fa-file-excel-o\"></i>",
                titleAttr: "Excel",
                title: window.store.name + " > Payment Method List",
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                }
            },
            {
                extend:    "csvHtml5",
                text:      "<i class=\"fa fa-file-text-o\"></i>",
                titleAttr: "CSV",
                title: window.store.name + " > Payment Method List",
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                }
            },
            {
                extend:    "pdfHtml5",
                text:      "<i class=\"fa fa-file-pdf-o\"></i>",
                titleAttr: "PDF",
                download: "open",
                title: window.store.name + " > Payment Method List",
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                },
                customize: function (doc) {
                    doc.content[1].table.widths =  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    doc.pageMargins = [10,10,10,10];
                    doc.defaultStyle.fontSize = 8;
                    doc.styles.tableHeader.fontSize = 8;
                    doc.styles.title.fontSize = 10;
                    // Remove spaces around page title
                    doc.content[0].text = doc.content[0].text.trim();
                    // Header
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'center',
                        fontSize: 8,
                        text: 'Printed on: '+window.formatDate(new Date()),
                    });
                    // Create a footer
                    doc['footer']=(function(page, pages) {
                        return {
                            columns: [
                                'Powered by wonderpillars.com',
                                {
                                    // This is the right column
                                    alignment: 'right',
                                    text: ['page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
                                }
                            ],
                            margin: [10, 0]
                        };
                    });
                    // Styling the table: create style object
                    var objLayout = {};
                    // Horizontal line thickness
                    objLayout['hLineWidth'] = function(i) { return 0.5; };
                    // Vertikal line thickness
                    objLayout['vLineWidth'] = function(i) { return 0.5; };
                    // Horizontal line color
                    objLayout['hLineColor'] = function(i) { return '#aaa'; };
                    // Vertical line color
                    objLayout['vLineColor'] = function(i) { return '#aaa'; };
                    // Left padding of the cell
                    objLayout['paddingLeft'] = function(i) { return 4; };
                    // Right padding of the cell
                    objLayout['paddingRight'] = function(i) { return 4; };
                    // Inject the object in the document
                    doc.content[1].layout = objLayout;
                }
            }
        ],
    });

    //================
    // end datatable
    //================

     /*********** SELECT AND DESELECT ROW(S) ***********/
    $(document).on( 'change', 'input[name*=\'select\']', function () {
        var checked = $(this).prop('checked');
        if (checked) {
            dt.DataTable().row($(this).closest('tr')).select();
        } else {
            dt.DataTable().row($(this).closest('tr')).deselect();
        }
    } );
    $(document).on( 'change', 'input.check-all', function () {
        var checked = $(this).prop('checked');
        if (checked) {
            dt.DataTable().rows().select();
            $('table.dataTable input[type="checkbox"]').each(function(){
                $(this).prop('checked', true);
            });
        } else {
            dt.DataTable().rows().deselect();
            $('table.dataTable input[type="checkbox"]').each(function(){
                $(this).prop('checked', false);
            });
        }
    } );
    /*********** SELECT AND DESELECT ROW(S) END ***********/

    // create payment method
    $(document).delegate("#create-payment-submit", "click", function(e) {
        e.preventDefault();
        var $tag = $(this);
        var $btn = $tag.button("loading");
        var form = $($tag.data("form"));
        form.find(".alert").remove();
        var actionUrl = form.attr("action");
        
        $http({
            url: window.baseUrl + "/_inc/" + actionUrl,
            method: "POST",
            data: form.serialize(),
            cache: false,
            processData: false,
            contentType: false,
            dataType: "json"
        }).
        then(function(response) {

            $btn.button("reset");
            $(":input[type=\"button\"]").prop("disabled", false);
            var alertMsg = response.data.msg;
            window.toastr.success(alertMsg, "Success!");
            $("#reset").trigger("click");
            $(".select2").val('1').trigger("change");

            paymentId = response.data.id;
            dt.DataTable().ajax.reload(function(json) {
                if ($("#row_"+paymentId).length) {
                    $("#row_"+paymentId).flash("yellow", 5000);
                }
            }, false);

        }, function(response) {

            $btn.button("reset");
            $(":input[type=\"button\"]").prop("disabled", false);
            var alertMsg = "<div>";
            window.angular.forEach(response.data, function(value) {
                alertMsg += "<p>" + value + ".</p>";
            });
            alertMsg += "</div>";
            window.toastr.warning(alertMsg, "Warning!");
        });
    });

    // edit payment method
    $(document).delegate("#edit-payment", "click", function(e) {
        e.stopPropagation();
        e.preventDefault();
        var d = dt.DataTable().row( $(this).closest("tr") ).data();
        PaymentEditModal(d);
    });

    // delete payment method
    $(document).delegate("#delete-payment", "click", function(e) {
        e.stopPropagation();
        e.preventDefault();
        var datatable = dt;
        var d = datatable.DataTable().row( $(this).closest("tr") ).data();
        PaymentDeleteModal(d);
    });

    $("#delete-all").on("click", function(e) {
        e.preventDefault();

        var $tag = $(this);
        var form = $($tag.data("form"));
        var actionUrl = form.attr("action");

        // Sweet Alert
        window.swal({
          title: "Are You Sure?",
          text: "Delete All Selected Payment Method!",
          icon: "warning",
          buttons: true,
          showCancelButton: false,
        })
        .then(function (willDelete) {
            if (willDelete) {
                var $btn = $tag.button("loading");
                $http({
                    url: window.baseUrl + "/_inc/" + actionUrl + "?action=delete",
                    method: "POST",
                    data: form.serialize(),
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: "json"
                }).
                then(function(response) {
                    $btn.button("reset");
                    dt.DataTable().ajax.reload( null, false );
                    // alert box
                    window.swal(response.data.msg)
                    .then(function() {
                        window.location = window.location;
                    });
                }, function(response) {
                    $btn.button("reset");
                    $(":input[type=\"button\"]").prop("disabled", false);
                    var alertMsg = "<div>";
                    window.angular.forEach(response.data, function(value) {
                        alertMsg += "<p>" + value + ".</p>";
                    });
                    alertMsg += "</div>";
                    window.toastr.warning(alertMsg, "Warning!");
                });
            }
        });
    });

    // open edit modal dialog box by query string
    if (window.getParameterByName("payment_id") && window.getParameterByName("payment_name")) {
        paymentId = window.getParameterByName("payment_id");
        var customerName = window.getParameterByName("payment_name");
        dt.DataTable().search(customerName).draw();
        dt.DataTable().ajax.reload(function(json) {
            $.each(json.data, function(index, obj) {
                if (obj.DT_RowId === "row_" + paymentId) {
                    PaymentEditModal({payment_id: paymentId, payment_name: obj.payment_name});
                    return false;
                }
            });
        }, false);
    }

}]);