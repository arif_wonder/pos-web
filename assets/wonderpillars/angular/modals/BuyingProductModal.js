window.angularApp.factory("BuyingProductModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function ($parentData) { 
        var invoice = $parentData;
        if ($parentData.product) {
            invoice = $parentData.product
        }
        var id;
        var sup_id = invoice.sup_id;
        var sup_name = invoice.sup_name;
        var subTotal;
        var quantity;
        var unitPrice;
        var filenameDisplayer;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeBuyProductModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-money\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/buying.php?sup_id=" + sup_id + "&invoice_id=" + invoice.invoice_id + '&action_type=EDIT',
                  method: "GET"
                })
                .then(function (response, status, headers, config) {

                    $scope.modal_title = "Invoice for " + sup_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.timePicker();
                        filenameDisplayer = $(document).find('.bootstrap-filestyle input');
                        if (invoice.invoice_url) {
                            filenameDisplayer.val($(invoice.invoice_url).attr('href'));
                        }
                    }, 100);

                }, function (response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeBuyProductModal();
                    });
                });

                $scope.total = "0";
                $scope.searchBoxText = "";
                var total = "0";

                //autocomplete script
                $(document).on("focus", ".autocomplete-product", function (e) {
                    e.stopPropagation();
                    e.preventDefault();

                    var $this = $(this);

                    $this.attr('autocomplete', 'off');

                    var type = $this.data("type");

                    var autoTypeNo; 

                    if(type =="p_id" ) autoTypeNo = 0;
                    if(type =="p_name" ) autoTypeNo = 1;

                    $this.autocomplete({
                        source: function (request, response) {
                            return $http({
                                url: window.baseUrl + "/_inc/ajax.php",
                                dataType: "json",
                                method: "post",
                                data: $.param({
                                   sup_id: sup_id,
                                   name_starts_with: request.term,
                                   type: type
                                }),
                            })
                            .then(function (data) {
                                return response( $.map( data.data, function (item) {

                                    var code = item.split("|");

                                    return {
                                        label: code[autoTypeNo],
                                        value: code[autoTypeNo],
                                        data : item
                                    };

                                }));
                            }, function (data) {
                               window.swal("Oops!", response.data.errorMsg, "error");
                            });

                        },
                        focusOpen: true,
                        autoFocus: true,
                        minLength: 0,
                        select: function ( event, ui ) {

                            var names = ui.item.data.split("|");

                            // add product
                            var data = {
                                itemId: names[0],
                                itemName: names[1],
                                categoryId: names[2],
                                itemQuantity: names[3],
                                itemPrice: names[4],
                                itemSellPrice: names[5],
                                supplier: sup_name,
                                tttt: 'yyyyyyyyyyyy'
                            };

                            $scope.addProduct(data);

                        }, 
                        open: function () {

                            $(".ui-autocomplete").perfectScrollbar();

                        }, 
                        close: function () {
                            $(document).find(".autocomplete-product").blur();
                            $(document).find(".autocomplete-product").val("");

                        },
                    }).bind("focus", function() { 

                        $(this).autocomplete("search"); 

                    });

                });

                $(document).on("blur",".rsell",function(){
                    id = $(this).data("id");
                    var inpVal = $(this).val().toString();
                    var p_discount_type = $("#p_discount_type_"+id).val();
                    var p_discount = $("#p_discount_"+id).val().toString();
                    if (p_discount_type==='fixed') {

                        if ( parseFloat(inpVal) < parseFloat(p_discount) ) {
                            $(this).val('');
                            window.swal("Please enter minimum "+p_discount, "error");
                        }
                    }
                });

                $(document).on("blur",".defected_quantity",function(){
                    id = $(this).data("id");
                    validateQuantity(id);
                });

                $(document).on("blur",".rquantity, .rcost",function (){
                    id = $(this).data("id");
                    validateQuantity(id);
                    $scope.calculate(id);
                });

                function validateQuantity(id) {
                    var quantity = parseInt( $("#quantity_"+id).val() );
                    var defected_quantity = parseInt( $("#defected_quantity_"+id).val() );
                    console.log(quantity+defected_quantity);
                    if (defected_quantity >= quantity) {
                        window.swal("Oops!", "Defected quantity should be lesser than Quantity", "error");
                        $("#defected_quantity_"+id).val('');
                    }
                };

                $(document).delegate(".spodel", "click", function () {
                    id = $(this).data("id");
                    $("#"+id).remove();
                    $scope.calculate(id);
                });

                $scope.calculate = function (id) {
                    var total = 0;
                    var quantity = $(document).find("#quantity_"+id);
                    var unitPrice = $(document).find("#cost_"+id);
                    subTotal = $(document).find("#subTotal_"+id);
                    subTotal.text(parseInt(quantity.val())*parseFloat(unitPrice.val()));

                    $(document).find(".ssubTotal").each(function (i, obj) {
                        total = total + parseFloat($(this).text());
                    });

                    $scope.$apply(function () {
                        $scope.total = total;
                    });
                };

                // add product
                $scope.addProduct = function(data) {
                    var html = "<tr id=\""+data.itemId+"\" class=\""+data.itemId+"\" data-item-id=\""+data.itemId+"\">";

                    html += "<td class=\"text-center\" data-title=\"Supplier\">";
                    html += "<span class=\"supplier\">"+data.supplier+"</span>";
                    html += "</td>";

                    html += "<td style=\"min-width:100px;\" data-title=\"Product Name\">";
                    html += "<input type=\"hidden\" id=\"p_discount_"+data.itemId+"\" value=\""+data.p_discount+"\">";
                    html += "<input type=\"hidden\" id=\"p_discount_type_"+data.itemId+"\" value=\""+data.p_discount_type+"\">";
                    
                    html += "<input name=\"product["+data.itemId+"][id]\" type=\"hidden\" class=\"rid\" value=\""+data.itemId+"\">";
                    html += "<input name=\"product["+data.itemId+"][name]\" type=\"hidden\" class=\"rname\" value=\""+data.itemName+"\">";
                    html += "<input name=\"product["+data.itemId+"][category_id]\" type=\"hidden\" class=\"categoryid\" value=\""+data.categoryId+"\">";
                    html += "<span class=\"sname\" id=\"name_"+data.itemId+"\">"+data.itemName+"</span>";
                    html += "</td>";
                    html += "<td class=\"text-center\" data-title=\"Available\">";
                    html += "<span class=\"savailable\" id=\"available_"+data.itemId+"\">"+data.itemQuantity+"</span>";
                    html += "</td>";

                    html += "<td style=\"padding:2px;\" data-title=\"Defect Items\">";
                    html += "<input class=\"form-control number input-sm kb-pad text-center defected_quantity\" name=\"product["+data.itemId+"][defected_quantity]\" type=\"text\" value=\"0\" data-id=\""+data.itemId+"\" id=\"defected_quantity_"+data.itemId+"\" onclick=\"this.select();\">";
                    html += "</td>";

                    html += "<td style=\"padding:2px;\" data-title=\"Product Name\">";
                    html += "<input class=\"form-control number input-sm kb-pad text-center rquantity\" name=\"product["+data.itemId+"][quantity]\" type=\"text\" value=\"1\" data-id=\""+data.itemId+"\" id=\"quantity_"+data.itemId+"\" onclick=\"this.select();\" onKeyUp=\"if(this.value<0){this.value=0;}\">";
                    html += "</td>";


                    html += "<td style=\"padding:2px; min-width:80px;\" data-title=\"Buy Price\">";
                    html += "<input class=\"form-control input-sm kb-pad text-center rcost\" name=\"product["+data.itemId+"][cost]\" type=\"text\" value=\""+window.getNumber(data.itemPrice)+"\" data-id=\""+data.itemId+"\" data-item=\""+data.itemId+"\" id=\"cost_"+data.itemId+"\" onclick=\"this.select();\">";
                    html += "</td>";
                    html += "<td style=\"padding:2px; min-width:80px;\" data-title=\"Sell Price\">";
                    html += "<input class=\"form-control input-sm kb-pad text-center rsell\" name=\"product["+data.itemId+"][sell]\" type=\"text\" value=\""+window.getNumber(data.itemSellPrice)+"\" data-id=\""+data.itemId+"\" data-item=\""+data.itemId+"\" id=\"sell_"+data.itemId+"\" onclick=\"this.select();\">";
                    html += "</td>";
                    html += "<td class=\"text-right\" data-title=\"Total\">";
                    html += "<span class=\"text-right ssubTotal\" id=\"subTotal_"+data.itemId+"\">"+window.getNumber(data.itemPrice)+"</span>";
                    html += "</td>";
                    html += "<td class=\"text-center\">";
                    html += "<i class=\"fa fa-trash-o tip pointer spodel\" data-id=\""+data.itemId+"\" title=\"Remove\"></i>";
                    html += "</td>";
                    html += "</tr>";

                    total = parseFloat($scope.total) + parseFloat(window.getNumber(data.itemPrice));
                    var quantity, unitPrice;
                    // update existing if find
                    if ($("#"+data.itemId).length) {
                        quantity = $(document).find("#quantity_"+data.itemId);

                        unitPrice = $(document).find("#cost_"+data.itemId);
                        subTotal = $(document).find("#subTotal_"+data.itemId);
                        quantity.val(parseInt(quantity.val()) + 1);
                        subTotal.text(parseFloat(subTotal.text()) + parseFloat(data.itemPrice));
                    } else {
                        $(document).find("#poTable tbody").append(html);
                    }

                    $scope.$apply(function () {
                        $scope.total = total;
                    });
                };

                // add product to invoice when call from product
                if (invoice.p_id) {
                    if (timer) {
                        window.clearInterval(timer);
                    }
                    var timer = window.setInterval(function(){

                        if ($(document).find("#poTable").length) {                         
                            // add product
                            var data = {
                                itemId: invoice.p_id,
                                categoryId: invoice.category_id,
                                itemName: invoice.p_name,
                                itemQuantity: invoice.quantity_in_stock,
                                itemPrice: invoice.buy_price,
                                itemSellPrice: invoice.sell_price,
                                supplier: invoice.sup_name,
                                p_discount: invoice.p_discount,
                                p_discount_type: invoice.p_discount_type,
                            };
                            
                            $scope.addProduct(data);

                            window.clearInterval(timer);
                        }

                    }, 100);
                }

                // Buying Confirm
                $(document).delegate("#buying-confirm-btn", "click", function (e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: new FormData(form[0]),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json",
                        headers: {
                            'Content-Type': undefined
                        }
                    }).
                    then(function (response) {
                        var invoiceId = response.data.id;
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + "</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                $scope.closeBuyProductModal();
                                $(document).find(".close").trigger("click");

                                // Callback
                                if ($parentData.BuyingProductModalCallback) {
                                    $parentData.BuyingProductModalCallback($scope);
                                }

                                if ($(datatable).length) {
                                    $(datatable).DataTable().ajax.reload(function (json) {
                                        if ($("#row_"+invoiceId).length) {
                                            $("#row_"+invoiceId).flash("yellow", 5000);
                                        }
                                    }, false);
                                }
                                    
                            } else {
                                if ($(datatable)) {
                                    $(datatable).DataTable().ajax.reload(null, false);
                                }
                            }
                        });


                    }, function (response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function (value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });


                $(document).delegate("#attachment", 'change', function() {
                    var file = this.files[0];
                    var imagefile = file.type;
                    var match= ["image/jpeg","image/png","image/jpg","image/png","image/gif"];
                    if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {
                        alert("Invalid file type");
                        return false;
                    } else {
                        // var filenameDisplayer = $(document).find('.bootstrap-filestyle input');
                        // filenameDisplayer.val(this.files[0].name);
                        $scope.$apply(function () {
                           
                            // $scope.attachmentFile = file.name;
                            filenameDisplayer.val(file.name);
                        });
                    }
                });
                
                $scope.closeBuyProductModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "lg",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);