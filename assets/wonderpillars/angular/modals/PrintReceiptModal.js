window.angularApp.factory("PrintReceiptModal", ["API_URL", "window", "jQuery", "$http", "$rootScope", function (API_URL, window, $, $http, $scope) {
    return function($scope) {

        var receipt_data = {};
        receipt_data.store_name = window.store.name + "\n";

        receipt_data.header = "";
        receipt_data.header += window.store.name + "\n";
        receipt_data.header += window.store.address + "\n";
        receipt_data.header += window.store.mobile + "\n";
        receipt_data.info += "\n";

        receipt_data.info = "";
        receipt_data.info += "Datetime:" + $scope.invoiceInfo.created_at + "\n";
        receipt_data.info += "Invoice ID:" + $scope.invoiceInfo.invoice_id + "\n";
        receipt_data.info += "Created By:" + $scope.invoiceInfo.created_by + "\n";
        receipt_data.info += "\n";
        receipt_data.info += "Customer:" + $scope.invoiceInfo.customer_name + "\n";
        receipt_data.info += "Customer Contact:" + $scope.invoiceInfo.customer_contact + "\n";
        receipt_data.info += "\n";

        receipt_data.items = "";
        window.angular.forEach($scope.invoiceItems, function($row, key) {
            receipt_data.items += "#" + key + " " + $row.item_name + "\n";
            receipt_data.items += $row.item_quantity + " x " + $row.item_price + "   " + $row.item_quantity * $row.item_price + "\n";
        });

        receipt_data.totals = "";
        receipt_data.totals += "Subtotal: " + $scope.invoiceInfo.payable_amount + "\n";
        receipt_data.totals += "Discount:" + $scope.invoiceInfo.discount_amount + "\n";
        receipt_data.totals += "Tax:" + $scope.invoiceInfo.tax_amount + "\n";
        receipt_data.totals += "Grand Total:" + $scope.invoiceInfo.payable_amount + "\n";
        receipt_data.totals += "Paid Amount:" + $scope.invoiceInfo.paid_amount + "\n";
        receipt_data.totals += "Due Amount:" + $scope.invoiceInfo.todays_due + "\n";

        receipt_data.footer = "";
        if ($scope.invoiceInfo.invoice_note) {
            receipt_data.footer += $scope.invoiceInfo.invoice_note + "\n\n";
        }  

        var socket_data = {
            'printer': window.printer,
            'logo': '',
            'text': receipt_data,
            'cash_drawer': '',
        };
        $.get(window.baseUrl + '/_inc/print.php', {data: JSON.stringify(socket_data)});    
    };
}]);