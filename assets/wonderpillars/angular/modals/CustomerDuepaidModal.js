window.angularApp.factory("CustomerDuePaidModal", ["API_URL","POS_API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL,POS_API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(customer) {

        $http.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('token');
        var invoiceID;
        $scope.confirm_button = false;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeCustomerDepositModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-plus\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/duepaid.php?customer_id=" + customer.id + "&action_type=PAID_FORM",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Due Pay for " + customer.name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeCustomerDepositModal();
                    });
                });


                $scope.sendNotification = function(){
                    var amount = $('#paid_amount').val();
                    console.log(amount);
                    var validate = $scope.validateAmount(amount);

                    // if(!validate){
                    //     window.swal("Oops!", 'Please enter valid amount', "error");
                    //     return false;
                    // }

                    if(!amount){
                        window.swal("Oops!", 'Please enter valid amount', "error");
                        return false;
                    }

                    if(parseInt($('#total_due_amount').val()) < parseInt(amount)){
                        window.swal("Oops!", "You can't paid greater than due amount", "error");
                        return false;
                    }
                    $('#paid_button').attr('disabled', true);
                    //var payment_method = $('#payment_method').val();
                    var payment_method = $('#payment_method')[0].selectedOptions[0].id;
                    var user_posapi = JSON.parse(localStorage.getItem('user_posapi'));
                    
                    payment_method = payment_method.trim();
                   
                    $http({
                        url: POS_API_URL + "/message/p2p/send?customer_id="+user_posapi.user_id+"&amount="+amount+"&payment_mode="+payment_method,
                        method: "POST",
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(zepResponse) {
                        $scope.confirm_button = true;
                        $('#paid_amount').attr('readonly', true);
                        $('#payment_method').attr('readonly', true);
                        $('#payment_token').val(zepResponse.data.data.payment_id);
                        
                    }, function(response) {
                        if(response.status === 401 && response.statusText == 'Unauthorized'){
                            var username =  localStorage.getItem('email');
                            var password =  localStorage.getItem('password');
                            $scope.relogin(username, password, 'product');
                        }
                        else{
                            window.swal("Oops!", "Notification not sent,Please try again", "error");
                        }
                        $('#paid_button').attr('disabled', false);
                        
                    }); 
                };


                //relogin
                $scope.relogin = function (username, password, $product = false) {
                    $.ajax({
                        url: POS_API_URL+"/login",
                        type: "POST",
                        dataType: "json",
                        data: {username: username, password: password},
                        success: function (response) {
                          localStorage.setItem('token', response.data.token);
                          localStorage.setItem('email', username);
                          localStorage.setItem('password', password);
                          $scope.sendNotification();
                        },
                        error: function (response) {   
                                  
                    }});
                };

                $scope.validateAmount = function (inputtxt) 
                { 
                    var decimal=  /^[-+]?[0-9]+$/; 
                    if(inputtxt.match(decimal)) 
                    { 
                        return true;
                    }
                    else
                    { 
                        return false;
                    }
                } 

                // Confirm duepaid
                $(document).delegate("#duepaid-confirm-btn", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeCustomerDepositModal();
                            $(document).find(".close").click();
                            customer.dueAmount = response.data.customer.due_amount;  
                            $("#customer-due-amount").text(customer.dueAmount);
                            
                            invoiceID = response.data.id;
                            $(datatable).DataTable().ajax.reload(function(json) {
                                if ($("#row_"+invoiceID).length) {
                                    $("#row_"+invoiceID).flash("yellow", 5000);
                                }
                            }, false);                          
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                $scope.closeCustomerDepositModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);