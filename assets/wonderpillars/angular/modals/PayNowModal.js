window.angularApp.factory("PayNowModal", [ "API_URL", "POS_API_URL","window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", "PrintReceiptModal", "EmailModal", 
function ( API_URL,POS_API_URL, window, $, $http, $uibModal, $sce, $scope, PrintReceiptModal, EmailModal
) {
    "use strict";
    return function($scope) {
        if (window.store.sound_effect == 1) {
            window.storeApp.playSound("modal_opened.mp3");
        }
        $scope.done = false;
        $scope.paidAmountByCustomer = 0;
        $scope.notificationSent = false;
        $scope.paymentNameLater = false;
        $scope.paymentidLater = false;
        $scope.reference_id = false;
        $scope.paidAmount = window.formatDecimal($scope.totalPayable, 2);
        $scope.totalPayable =  parseFloat($scope.totalPayable)-parseFloat($scope.coupan_code_discount);
        $scope.productDiscount = 0;
        window.angular.forEach($scope.itemArray, function(item, key) {
            $scope.productDiscount = $scope.productDiscount+item.discount;
        });
        
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-show=\"!done\" ng-click=\"backToPos();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                            "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-user\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/template/pos_payment_form.php",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = $scope.customerName;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        $(document).find("#payable-amount").focus().select();
                    }, 100);

                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closePayNowModal();
                    });
                });



                // pay button action
                $scope.pay = function(paymentId, paymentName, confirm_payment = false, paymentmethod_id=false) {

                    if(!confirm_payment){
                        paymentName = paymentName.toUpperCase();
                    }
                    
                    if(paymentName != 'CASH'){
                       paymentName = 'CARD'; 
                    }

                $scope.paymentNameLater = paymentName;
                $scope.paymentidLater = paymentId;

                    $(document).find(".modal").addClass("overlay-loader");
                    var pay_by_credit = 'false';
                    if($("#pay_by_credit").is(":checked")){
                        pay_by_credit = 'true';
                    }

                    
                    var total_payable_amount = $('#payable-amount').val();
console.log(total_payable_amount);
console.log($scope.dueAmount+
                        ($scope.totalPrice));
                    var regex  = /^\d+(\.\d{1,2})?$/;
                    var numStr = total_payable_amount;
                    console.log(regex.test(numStr));
                    if (!regex.test(numStr)){
                        window.swal("Oops!", 'Please enter a valid amount to paid', "error");
                        $(document).find(".modal").removeClass("overlay-loader");
                        return false;
                    }

                    if(pay_by_credit == 'true'){
                        console.log('true true', $('#payable-amount_hidden').val());
                        console.log('credit amount', $scope.creditAmount);
                        if($('#payable-amount_hidden').val() > $scope.creditAmount){

                            var remainAmount = $('#payable-amount_hidden').val()-$scope.creditAmount;
                            console.log('remainAmount', remainAmount);
                             console.log('total_payable_amount', total_payable_amount);
                            if(total_payable_amount > remainAmount){
                                window.swal("Oops!", 'Please enter a valid amount to paid', "error");
                                $(document).find(".modal").removeClass("overlay-loader");
                                return false;
                            }
                        }
                    }

                    //$scope.totalPrice = parseFloat($scope.totalPrice)- parseFloat($scope.coupan_code_discount);
                    if(total_payable_amount <1 && $scope.creditAmount<total_payable_amount){
                        window.swal("Oops!", 'Please enter a valid amount to paid', "error");
                        $(document).find(".modal").removeClass("overlay-loader");
                        return false;
                    }
                    if(total_payable_amount > $scope.dueAmount+
                        ($scope.totalPrice)){
                        window.swal("Oops!", "You can't not pay more than your payable amount", "error");
                        $(document).find(".modal").removeClass("overlay-loader");
                        return false;
                    }

                    if(confirm_payment){
                        $scope.madePayment(paymentId, total_payable_amount,pay_by_credit,confirm_payment, paymentmethod_id);
                    }
                    else{
                       $http({
                        url: POS_API_URL + "/message/p2p/send?customer_id="+$scope.customerId+"&amount="+total_payable_amount+"&payment_mode="+paymentName,
                        method: "POST",
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(zepResponse) {
                        
                        $scope.notificationSent = true;
                        $scope.reference_id = zepResponse.data.data.payment_id;
                        console.log('first', zepResponse);
                        console.log('first', zepResponse.data.data.payment_id);
                        console.log('second', $scope.reference_id);
                        $(document).find(".modal").removeClass("overlay-loader");
                        //$scope.paymentSuccess(response, total_payable_amount, payment_mode);
                        //$scope.madePayment(paymentId, total_payable_amount,pay_by_credit,zepResponse.data.ref_no);
                    }, function(zepResponse) {
                       // $scope.reference_id = 'dsaddsa';
                        //console.log($scope.reference_id);
                        $scope.notificationSent = true;
                        //$scope.madePayment(paymentId, total_payable_amount,pay_by_credit,'fdsfdffsdf');
                        //$scope.paymentSuccess(response, total_payable_amount, payment_mode);      
                        window.swal("Oops!", 'Notification not sent', "error");
                        $(document).find(".modal").removeClass("overlay-loader");
                    }); 
                    }

                    


                    //  $http({
                    //     url: POS_API_URL + "/payments/order?payment_method_id="+paymentId+"&customer_id="+$scope.customerId+"&invoice_note="+$scope.invoiceNote+"&previous_due="+ $scope.dueAmount+"&paid_amount="+total_payable_amount+"&coupon_code="+$scope.coupan_Code+"&use_credit="+pay_by_credit,
                    //     method: "POST",
                    //     cache: false,
                    //     processData: false,
                    //     contentType: false,
                    //     dataType: "json"
                    // }).
                    // then(function(response) {
                    //    $scope.payByZep(response, total_payable_amount, paymentName);
                    // }, function(response) {
                    //     if (window.store.sound_effect == 1) {
                    //         window.storeApp.playSound("error.mp3");
                    //     }
                                  
                    //     window.swal("Oops!", response.data.errors.paid_amount[0], "error");
                    //     $(document).find(".modal").removeClass("overlay-loader");
                    // });
                };

                $scope.payByEnterKey = function($event) {
                    if(($event.keyCode || $event.which) == 13){
                        $scope.pay(1, 'Cash on Delivery');
                    }
                };


                $scope.closePayNowModal = function () {
                    if (!window.getParameterByName("customer_id") || !window.getParameterByName("invoice_id")) {
                        $scope.resetPos();
                    }
                    $uibModalInstance.dismiss("cancel");
                    if (window.settings.after_sell_page == "invoice" || (window.getParameterByName("customer_id") && window.getParameterByName("invoice_id"))) {
                        window.location = window.baseUrl + "/admin/view_invoice.php?invoice_id=" + $scope.invoiceId;
                    }
                };

                // back to pos button
                $scope.backToPos = function() {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "lg",
            backdrop  : "static",
            keyboard: true, // ESC key close enable/disable
            resolve: {
                userForm: function () {
                    // return customerForm;
                }
            }
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
            if (window.store.sound_effect == 1) {
                window.storeApp.playSound("modal_closed.mp3");
            }
        });



        $scope.madePayment = function(paymentId, total_payable_amount,pay_by_credit, ref_id, madePayment_id){

            //var total_payable_amount = $('#payable-amount_hidden').val();
            $http({
                url: POS_API_URL + "/payments/order?payment_method_id="+madePayment_id+"&customer_id="+$scope.customerId+"&invoice_note="+$scope.invoiceNote+"&previous_due="+ $scope.dueAmount+"&paid_amount="+total_payable_amount+"&coupon_code="+$scope.coupan_Code+"&use_credit="+pay_by_credit+"&ref_no="+ref_id,
                method: "POST",
                cache: false,
                processData: false,
                contentType: false,
                dataType: "json"
            }).
            then(function(response) {
               //$scope.payByZep(response, total_payable_amount, paymentName);
                $scope.paymentSuccess(response, total_payable_amount, false);
            }, function(response) {
                if (window.store.sound_effect == 1) {
                    window.storeApp.playSound("error.mp3");
                }
                          
                //window.swal("Oops!", response.data.errors.paid_amount[0], "error");
                $(document).find(".modal").removeClass("overlay-loader");
            });
        }

        $scope.payByZep = function(response, total_payable_amount, payment_mode){
            
                     $http({
                        url: POS_API_URL + "/message/p2p/send?invoice_id="+response.data.data.invoice_id+"&amount="+total_payable_amount+"&payment_mode="+payment_mode,
                        method: "POST",
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(zepResponse) {

                        $scope.paymentSuccess(response, total_payable_amount, payment_mode);
                    }, function(zepResponse) {
                        $scope.paymentSuccess(response, total_payable_amount, payment_mode);      
                        window.swal("Oops!", 'Notification not sent', "error");
                        $(document).find(".modal").removeClass("overlay-loader");
                    });
        }

        $scope.sendInvoiceViaSms = function(invoiceId){
            $(document).find(".modal").addClass("overlay-loader");
         $http({
            url: POS_API_URL + "/message/invoice/send?invoice_id="+invoiceId,
            method: "POST",
            cache: false,
            processData: false,
            contentType: false,
            dataType: "json"
        }).
        then(function(response) {
            $(document).find(".modal").removeClass("overlay-loader");
            window.swal('Invoice sent successfully.', "");
        }, function(response) {
            window.swal("Oops!", 'Sms not sent', "error");
            $(document).find(".modal").removeClass("overlay-loader");
        });
        }

        $scope.whatsapp = function(invoiceId){
            
            $(document).find(".modal").addClass("overlay-loader");
             $http({
                url: POS_API_URL + "/message/invoice/send/wa?invoice_id="+invoiceId,
                method: "POST",
                cache: false,
                processData: false,
                contentType: false,
                dataType: "json"
            }).
            then(function(response) {
                $(document).find(".modal").removeClass("overlay-loader");
                window.swal('WhatsApp sent successfully.', "");
            }, function(response) {
                window.swal("Oops!", 'WhatsApp not sent', "error");
                $(document).find(".modal").removeClass("overlay-loader");
            });
        }

        $scope.paymentSuccess = function(response, total_payable_amount, payment_mode){
            $(document).find(".modal").removeClass("overlay-loader");
            $scope.invoiceId = response.data.data.invoice_id;
            $scope.invoiceInfo = response.data.data.invoice_info;
            $scope.invoiceItems = response.data.data.invoice_items;
            $scope.done = true;
            $scope.notificationSent = false;
            if (window.store.sound_effect == 1) {
                window.storeApp.playSound("modify.mp3");
            }
            if (window.store.auto_print == 1 && window.store.remote_printing == 1) {
                PrintReceiptModal($scope);
            } else if (window.store.auto_print == 1) {
                window.open(window.baseUrl + "/admin/view_invoice.php?invoice_id=" + $scope.invoiceInfo.invoice_id);
            }
            $scope.showProductList();
        }

        $scope.totalDiscount = function(productDiscount, coupanDiscount){
            return parseFloat(productDiscount)+parseFloat(coupanDiscount);
        }

        //subtotal without tax
        $scope.subTotalWithoutTax = function(total, tax){
            return parseFloat(total)-parseFloat(tax);
        }

        $scope.checkBoxChange = function(){
            
            if($('#pay_by_credit').is(":checked")){
               // $scope.totalPrice = parseFloat($scope.totalPrice)- parseFloat($scope.coupan_code_discount);
                if($scope.creditAmount >= (parseFloat($scope.totalPrice)- parseFloat($scope.coupan_code_discount))){
                    $('#payable-amount').prop('disabled', true);
                    $('#payable-amount').val('0.00');
                }
                else{
                    $('#payable-amount').prop('disabled', false);
                    var remainingAmount = (parseFloat($scope.totalPrice)+parseFloat($scope.dueAmount))-parseFloat($scope.creditAmount);
                    $('#payable-amount').val(remainingAmount.toFixed(2));
                }
            }
            if(!$('#pay_by_credit').is(":checked")){
                $('#payable-amount').prop('disabled', false);
                $('#payable-amount').val(
                    (parseFloat($scope.totalPrice)+parseFloat($scope.dueAmount)).toFixed(2));
            }
        }

        // sending invoice via email
        $scope.sendInvoiceViaEmail = function (invoiceId) {

            if (!invoiceId) {
                if (window.store.sound_effect == 1) {
                    window.storeApp.playSound("error.mp3");
                }
                window.swal("Oops!", "Invalid invoice id", "error");
                return false;
            }

            // fetch invocie and send thought the provided email
            $http({
                url: API_URL + "/admin/view_invoice.php?invoice_id=" + invoiceId,
                method: "GET",
                cache: false,
                processData: false,
                dataType: "html"
            }).
            then(function(response) {
                var invoiceContent = $(response.data).find("#invoice").html();
                if (!invoiceContent || invoiceContent == "undefined") {
                    if (window.store.sound_effect == 1) {
                        window.storeApp.playSound("error.mp3");
                    }
                    window.swal("Oops!", "Invalid invoice id", "error");
                    return false;
                }
                var recipientName = $scope.customerName;
                var invoice = {
                    template: "invoice", 
                    subject: "Invoice", 
                    title: "Invoice", 
                    recipientName: recipientName, 
                    senderName: window.store.name,
                    html: invoiceContent
                };
                EmailModal(invoice);
            }, function(response) {
                if (window.store.sound_effect == 1) {
                    window.storeApp.playSound("error.mp3");
                }
                window.swal("Oops!", "an Unknown error occured!", "error");
            });
        };
    };
}]);