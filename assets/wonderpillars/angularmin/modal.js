window.angularApp.factory("AddInvoiceNoteModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function($scope) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeAddNoteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<textarea id=\"note\" class=\"form-control\" rows=\"3\" placeholder=\"Type invoice note here...\">{{ invoiceNote }}</textarea>" +
                        "</div>" +
                        "<div class=\"modal-footer\">" +
                            "<button ng-click=\"clear();\" class=\"btn btn-warning pull-left\" type=\"button\">Cancel</button>" +
                            "<button ng-click=\"closeAddNoteModal();\" id=\"reset-btn\" name=\"reset-btn\" class=\"btn btn-success\">Ok</button>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {

                $scope.modal_title = "Add Invoice Note";            
                 var invoiceNote;

                $(document).on("change keyup blur", "#note", function () {

                    var $this = $(this);
                    invoiceNote = $this.val();

                    $("#invoice-note").data("note", invoiceNote);
                    $scope.invoiceNote = invoiceNote;
                });

                $scope.closeAddNoteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };

                $scope.clear = function () {
                    $("#invoice-note").data("note", "");
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("BarcodePrintModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(product) {

        var symbology = 'code_39';
        var limit = 1;

        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closePrintBarcodeModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-barcode\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/barcode.php?code=" + product.p_id + "&symbology=" + symbology + "&limit=" + limit, // upc_a, code_39,code_93,  code_128, ean_2 
                  method: "GET",
                  dataType: "HTML",
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = product.p_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        $(document).find(".modal-lg").css("width", "90%");
                        window.storeApp.select2();
                    }, 100);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Load specified number of barcode
                $(document.body).delegate('.generate-barcode', 'change', function() {

                    $scope.rawHtml = 'Loading...';

                    limit = parseInt($("#barcode-limit").val());
                    symbology = $("#barcode-symbology").val();

                    $http({
                      url: window.baseUrl + "/_inc/barcode.php?code=" + product.p_id + "&symbology=" + symbology + "&limit=" + limit,
                      method: "GET",
                      dataType: "HTML",
                    })
                    .then(function(response, status, headers, config) {
                        $scope.rawHtml = $sce.trustAsHtml(response.data);

                        setTimeout(function() {
                            $(document).find(".modal-lg").css("width", "90%");
                            window.storeApp.select2();
                        }, 100);

                    }, function(data) {

                       window.swal("Oops!", "an error occured!", "error");
                    });
                });

                $scope.closePrintBarcodeModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "lg",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("BoxCreateModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function($scope) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeBoxCreateModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-plus\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/box.php?action_type=CREATE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Create New Box";
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 500);
                    
                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                // Submit Box Form
                $(document).delegate("#create-box-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {

                            // close modalwindow
                            $scope.closeBoxCreateModal();
                            $(document).find(".close").trigger("click");

                            // insert box into select2
                            var select = $(document).find("#box_id");
                            if (select.length) {
                            
                                var option = $("<option></option>").
                                     attr("selected", true).
                                     text(response.data.box.box_name).
                                     val(response.data.id);
                                option.appendTo(select);
                                select.trigger("change");
                            }

                            // increase store count
                            var boxCount = $(document).find("#box-count h3");
                            if (boxCount) {
                                boxCount.text(parseInt(boxCount.text()) + 1);
                            }

                            // Callback
                            if ($scope.BoxCreateModalCallback) {
                                $scope.BoxCreateModalCallback($scope);
                            }

                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeBoxCreateModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("BoxDeleteModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function(API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(box) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                           "<button ng-click=\"closeBoxDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-trash\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/box.php?box_id=" + box.box_id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = box.box_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);
                    
                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeBoxDeleteModal();
                    });
                });

                $(document).delegate("#box-delete", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(datatable).DataTable().ajax.reload(null, false);

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {

                            $scope.closeBoxDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                        // Callback
                        if ($scope.BoxDeleteModalCallback) {
                            $scope.BoxDeleteModalCallback($scope);
                        }

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeBoxDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });
        
        uibModalInstance.result.catch(function () { 
                uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("BoxEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(box) {
        var boxId;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeBoxEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile='rawHtml'>Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: API_URL + "/_inc/box.php?box_id=" + box.box_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                $scope.modal_title = box.box_name;
                $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                $(document).delegate("#box-update", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: API_URL + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {

                                $scope.closeBoxEditModal();
                                $(document).find(".close").trigger("click");
                                boxId = response.data.id;
                                
                                $(datatable).DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+boxId).length) {
                                        $("#row_"+boxId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {
                                $(datatable).DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeBoxEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("BrandDeleteModal", [
    "API_URL", 
    "window", 
    "jQuery", 
    "$http", 
    "$uibModal", 
    "$sce", 
    "$rootScope", 
    function (
        API_URL, 
        window, 
        $, 
        $http, 
        $uibModal, 
        $sce, 
        $scope
    ) {
    return function(product,trindex) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeBrandDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-delete\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/brand.php?brand_id=" + product.brand_id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = product.p_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product delete form
                $(document).delegate("#product-delete-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + "</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $("#product-brad-list").DataTable().ajax.reload( null, false);
                       

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")

                        .then(function(value) {
                            
                            $scope.closeBrandDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeBrandDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("BrandEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(product) {
        var productId;
        var uibModalInstance = $uibModal.open({
            // animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeProductEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/brand.php?brand_id=" + product.brand_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = product.p_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.select2();

                        // Generate random number
                        $(".random_num").click(function(){
                            $(this).parent(".input-group").children("input").val(window.storeApp.generateCardNo(8));
                        });
                    }, 500);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product update form
                $(document).delegate("#product-brand-update-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function (response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {

                                // close modalwindow
                                $scope.closeProductEditModal();
                                $(document).find(".close").trigger("click");
                                $("body").removeClass("modal-open");

                                productId = response.data.id;
                                
                                $("#product-brad-list").DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+productId).length) {
                                        $("#row_"+productId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {

                                $("#product-brad-list").DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                
                // Close modal
                $scope.closeProductEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
            setTimeout(function() {
                $("body").removeClass("modal-open");
            }, 500);
        });

    };
}]);
window.angularApp.factory("BuyingInvoiceViewModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function (invoice) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeBuyInvoiceViewModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-eye\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/buying.php?invoice_id=" + invoice.invoice_id + '&action_type=VIEW',
                  method: "GET"
                })
                .then(function (response, status, headers, config) {
                    $scope.modal_title = "View Invoice";
                    $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function (response) {
                   window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeBuyInvoiceViewModal();
                    });
                });

                $scope.closeBuyInvoiceViewModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("BuyingProductModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function ($parentData) { 
        var invoice = $parentData;
        if ($parentData.product) {
            invoice = $parentData.product
        }
        var id;
        var sup_id = invoice.sup_id;
        var sup_name = invoice.sup_name;
        var subTotal;
        var quantity;
        var unitPrice;
        var filenameDisplayer;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeBuyProductModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-money\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/buying.php?sup_id=" + sup_id + "&invoice_id=" + invoice.invoice_id + '&action_type=EDIT',
                  method: "GET"
                })
                .then(function (response, status, headers, config) {

                    $scope.modal_title = "Invoice for " + sup_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.timePicker();
                        filenameDisplayer = $(document).find('.bootstrap-filestyle input');
                        if (invoice.invoice_url) {
                            filenameDisplayer.val($(invoice.invoice_url).attr('href'));
                        }
                    }, 100);

                }, function (response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeBuyProductModal();
                    });
                });

                $scope.total = "0";
                $scope.searchBoxText = "";
                var total = "0";

                //autocomplete script
                $(document).on("focus", ".autocomplete-product", function (e) {
                    e.stopPropagation();
                    e.preventDefault();

                    var $this = $(this);

                    $this.attr('autocomplete', 'off');

                    var type = $this.data("type");

                    var autoTypeNo; 

                    if(type =="p_id" ) autoTypeNo = 0;
                    if(type =="p_name" ) autoTypeNo = 1;

                    $this.autocomplete({
                        source: function (request, response) {
                            return $http({
                                url: window.baseUrl + "/_inc/ajax.php",
                                dataType: "json",
                                method: "post",
                                data: $.param({
                                   sup_id: sup_id,
                                   name_starts_with: request.term,
                                   type: type
                                }),
                            })
                            .then(function (data) {
                                return response( $.map( data.data, function (item) {

                                    var code = item.split("|");

                                    return {
                                        label: code[autoTypeNo],
                                        value: code[autoTypeNo],
                                        data : item
                                    };

                                }));
                            }, function (data) {
                               window.swal("Oops!", response.data.errorMsg, "error");
                            });

                        },
                        focusOpen: true,
                        autoFocus: true,
                        minLength: 0,
                        select: function ( event, ui ) {

                            var names = ui.item.data.split("|");

                            // add product
                            var data = {
                                itemId: names[0],
                                itemName: names[1],
                                categoryId: names[2],
                                itemQuantity: names[3],
                                itemPrice: names[4],
                                itemSellPrice: names[5],
                                supplier: sup_name,
                                tttt: 'yyyyyyyyyyyy'
                            };

                            $scope.addProduct(data);

                        }, 
                        open: function () {

                            $(".ui-autocomplete").perfectScrollbar();

                        }, 
                        close: function () {
                            $(document).find(".autocomplete-product").blur();
                            $(document).find(".autocomplete-product").val("");

                        },
                    }).bind("focus", function() { 

                        $(this).autocomplete("search"); 

                    });

                });

                $(document).on("blur",".rsell",function(){
                    id = $(this).data("id");
                    var inpVal = $(this).val().toString();
                    var p_discount_type = $("#p_discount_type_"+id).val();
                    var p_discount = $("#p_discount_"+id).val().toString();
                    if (p_discount_type==='fixed') {

                        if ( parseFloat(inpVal) < parseFloat(p_discount) ) {
                            $(this).val('');
                            window.swal("Please enter minimum "+p_discount, "error");
                        }
                    }
                });

                $(document).on("change keyup blur",".defected_quantity",function(){
                    id = $(this).data("id");
                    validateQuantity(id);
                });

                $(document).on("change keyup blur",".rquantity, .rcost",function (){
                    id = $(this).data("id");
                    validateQuantity(id);
                    $scope.calculate(id);
                });

                function validateQuantity(id) {
                    var quantity = $("#quantity_"+id).val();
                    var defected_quantity = $("#defected_quantity_"+id).val();
                    if (defected_quantity>quantity) {
                        $("#defected_quantity_"+id).val('');
                    }
                };

                $(document).delegate(".spodel", "click", function () {
                    id = $(this).data("id");
                    $("#"+id).remove();
                    $scope.calculate(id);
                });

                $scope.calculate = function (id) {
                    var total = 0;
                    var quantity = $(document).find("#quantity_"+id);
                    var unitPrice = $(document).find("#cost_"+id);
                    subTotal = $(document).find("#subTotal_"+id);
                    subTotal.text(parseInt(quantity.val())*parseFloat(unitPrice.val()));

                    $(document).find(".ssubTotal").each(function (i, obj) {
                        total = total + parseFloat($(this).text());
                    });

                    $scope.$apply(function () {
                        $scope.total = total;
                    });
                };

                // add product
                $scope.addProduct = function(data) {
                    var html = "<tr id=\""+data.itemId+"\" class=\""+data.itemId+"\" data-item-id=\""+data.itemId+"\">";

                    html += "<td class=\"text-center\" data-title=\"Supplier\">";
                    html += "<span class=\"supplier\">"+data.supplier+"</span>";
                    html += "</td>";

                    html += "<td style=\"min-width:100px;\" data-title=\"Product Name\">";
                    html += "<input type=\"hidden\" id=\"p_discount_"+data.itemId+"\" value=\""+data.p_discount+"\">";
                    html += "<input type=\"hidden\" id=\"p_discount_type_"+data.itemId+"\" value=\""+data.p_discount_type+"\">";
                    
                    html += "<input name=\"product["+data.itemId+"][id]\" type=\"hidden\" class=\"rid\" value=\""+data.itemId+"\">";
                    html += "<input name=\"product["+data.itemId+"][name]\" type=\"hidden\" class=\"rname\" value=\""+data.itemName+"\">";
                    html += "<input name=\"product["+data.itemId+"][category_id]\" type=\"hidden\" class=\"categoryid\" value=\""+data.categoryId+"\">";
                    html += "<span class=\"sname\" id=\"name_"+data.itemId+"\">"+data.itemName+"</span>";
                    html += "</td>";
                    html += "<td class=\"text-center\" data-title=\"Available\">";
                    html += "<span class=\"savailable\" id=\"available_"+data.itemId+"\">"+data.itemQuantity+"</span>";
                    html += "</td>";

                    html += "<td style=\"padding:2px;\" data-title=\"Defect Items\">";
                    html += "<input class=\"form-control number input-sm kb-pad text-center defected_quantity\" name=\"product["+data.itemId+"][defected_quantity]\" type=\"text\" value=\"0\" data-id=\""+data.itemId+"\" id=\"defected_quantity_"+data.itemId+"\" onclick=\"this.select();\">";
                    html += "</td>";

                    html += "<td style=\"padding:2px;\" data-title=\"Product Name\">";
                    html += "<input class=\"form-control number input-sm kb-pad text-center rquantity\" name=\"product["+data.itemId+"][quantity]\" type=\"text\" value=\"1\" data-id=\""+data.itemId+"\" id=\"quantity_"+data.itemId+"\" onclick=\"this.select();\" onKeyUp=\"if(this.value<0){this.value=0;}\">";
                    html += "</td>";


                    html += "<td style=\"padding:2px; min-width:80px;\" data-title=\"Buy Price\">";
                    html += "<input class=\"form-control input-sm kb-pad text-center rcost\" name=\"product["+data.itemId+"][cost]\" type=\"text\" value=\""+window.getNumber(data.itemPrice)+"\" data-id=\""+data.itemId+"\" data-item=\""+data.itemId+"\" id=\"cost_"+data.itemId+"\" onclick=\"this.select();\">";
                    html += "</td>";
                    html += "<td style=\"padding:2px; min-width:80px;\" data-title=\"Sell Price\">";
                    html += "<input class=\"form-control input-sm kb-pad text-center rsell\" name=\"product["+data.itemId+"][sell]\" type=\"text\" value=\""+window.getNumber(data.itemSellPrice)+"\" data-id=\""+data.itemId+"\" data-item=\""+data.itemId+"\" id=\"sell_"+data.itemId+"\" onclick=\"this.select();\">";
                    html += "</td>";
                    html += "<td class=\"text-right\" data-title=\"Total\">";
                    html += "<span class=\"text-right ssubTotal\" id=\"subTotal_"+data.itemId+"\">"+window.getNumber(data.itemPrice)+"</span>";
                    html += "</td>";
                    html += "<td class=\"text-center\">";
                    html += "<i class=\"fa fa-trash-o tip pointer spodel\" data-id=\""+data.itemId+"\" title=\"Remove\"></i>";
                    html += "</td>";
                    html += "</tr>";

                    total = parseFloat($scope.total) + parseFloat(window.getNumber(data.itemPrice));
                    var quantity, unitPrice;
                    // update existing if find
                    if ($("#"+data.itemId).length) {
                        quantity = $(document).find("#quantity_"+data.itemId);

                        unitPrice = $(document).find("#cost_"+data.itemId);
                        subTotal = $(document).find("#subTotal_"+data.itemId);
                        quantity.val(parseInt(quantity.val()) + 1);
                        subTotal.text(parseFloat(subTotal.text()) + parseFloat(data.itemPrice));
                    } else {
                        $(document).find("#poTable tbody").append(html);
                    }

                    $scope.$apply(function () {
                        $scope.total = total;
                    });
                };

                // add product to invoice when call from product
                if (invoice.p_id) {
                    if (timer) {
                        window.clearInterval(timer);
                    }
                    var timer = window.setInterval(function(){

                        if ($(document).find("#poTable").length) {                         
                            // add product
                            var data = {
                                itemId: invoice.p_id,
                                categoryId: invoice.category_id,
                                itemName: invoice.p_name,
                                itemQuantity: invoice.quantity_in_stock,
                                itemPrice: invoice.buy_price,
                                itemSellPrice: invoice.sell_price,
                                supplier: invoice.sup_name,
                                p_discount: invoice.p_discount,
                                p_discount_type: invoice.p_discount_type,
                            };
                            
                            $scope.addProduct(data);

                            window.clearInterval(timer);
                        }

                    }, 100);
                }

                // Buying Confirm
                $(document).delegate("#buying-confirm-btn", "click", function (e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: new FormData(form[0]),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json",
                        headers: {
                            'Content-Type': undefined
                        }
                    }).
                    then(function (response) {
                        var invoiceId = response.data.id;
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + "</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                $scope.closeBuyProductModal();
                                $(document).find(".close").trigger("click");

                                // Callback
                                if ($parentData.BuyingProductModalCallback) {
                                    $parentData.BuyingProductModalCallback($scope);
                                }

                                if ($(datatable).length) {
                                    $(datatable).DataTable().ajax.reload(function (json) {
                                        if ($("#row_"+invoiceId).length) {
                                            $("#row_"+invoiceId).flash("yellow", 5000);
                                        }
                                    }, false);
                                }
                                    
                            } else {
                                if ($(datatable)) {
                                    $(datatable).DataTable().ajax.reload(null, false);
                                }
                            }
                        });


                    }, function (response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function (value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });


                $(document).delegate("#attachment", 'change', function() {
                    var file = this.files[0];
                    var imagefile = file.type;
                    var match= ["image/jpeg","image/png","image/jpg","image/png","image/gif"];
                    if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {
                        alert("Invalid file type");
                        return false;
                    } else {
                        // var filenameDisplayer = $(document).find('.bootstrap-filestyle input');
                        // filenameDisplayer.val(this.files[0].name);
                        $scope.$apply(function () {
                           
                            // $scope.attachmentFile = file.name;
                            filenameDisplayer.val(file.name);
                        });
                    }
                });
                
                $scope.closeBuyProductModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "lg",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("CategoryCreateModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function($scope) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeCategoryCreateModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-plus\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/category.php?action_type=CREATE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Create New Category";
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 500);

                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                // Submit Box Form
                $(document).delegate("#create-category-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {

                            // close modalwindow
                            $scope.closeCategoryCreateModal();
                            $(document).find(".close").trigger("click");

                            // insert category into select2
                            var select = $(document).find('#category_id');
                            if (select.length) {
                                var option = $('<option></option>').
                                     attr('selected', true).
                                     text(response.data.category.category_name).
                                     val(response.data.id);
                                option.appendTo(select);
                                select.trigger('change');
                            }

                            // increase category count
                            var categoryCount = $(document).find("#category-count h3");
                            if (categoryCount) {
                                categoryCount.text(parseInt(categoryCount.text()) + 1);
                            }

                            // Callback
                            if ($scope.CategoryCreateModalCallback) {
                                $scope.CategoryCreateModalCallback($scope);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeCategoryCreateModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("CategoryDeleteModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(category) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeCategoryDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-trash\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/category.php?category_id=" + category.category_id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response) {
                    $scope.modal_title = category.category_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);

                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeCategoryDeleteModal();
                    });
                });

                $(document).delegate("#category-delete", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(datatable).DataTable().ajax.reload( null, false );

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeCategoryDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeCategoryDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("CategoryEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(category) {
        var categoryId;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeCategoryEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/category.php?category_id=" + category.category_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = category.category_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);

                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                $(document).delegate("#category-update", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                $scope.closeCategoryEditModal();
                                $(document).find(".close").trigger("click");
                                categoryId = response.data.id;
                                
                                $(datatable).DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+categoryId).length) {
                                        $("#row_"+categoryId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {
                                $(datatable).DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                $scope.closeCategoryEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
    window.angularApp.factory("CurrencyEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
        return function(currency) {
            var currencyId;
            var uibModalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: "modal-title",
                ariaDescribedBy: "modal-body",
                template: "<div class=\"modal-header\">" +
                                "<button ng-click=\"closeCurrencyEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                               "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                            "</div>" +
                            "<div class=\"modal-body\" id=\"modal-body\">" +
                                "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                            "</div>",
                controller: function ($scope, $uibModalInstance) {
                    $http({
                      url: API_URL + "/_inc/currency.php?currency_id=" + currency.currency_id + "&action_type=EDIT",
                      method: "GET"
                    })
                    .then(function(response, status, headers, config) {
                        $scope.modal_title = currency.title;
                        $scope.rawHtml = $sce.trustAsHtml(response.data);

                        setTimeout(function() {
                            window.storeApp.select2();
                        }, 100);

                    }, function(response) {
                        window.swal("Oops!", response.data.errorMsg, "error")
                        .then(function() {
                            $scope.closeCurrencyEditModal();
                        });
                    });

                    $(document).delegate("#currency-update", "click", function(e) {
                        
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        e.preventDefault();

                        var $tag = $(this);
                        var $btn = $tag.button("loading");
                        var form = $($tag.data("form"));
                        var datatable = $tag.data("datatable");
                        form.find(".alert").remove();
                        var actionUrl = form.attr("action");
                        $http({
                            url: API_URL + "/_inc/" + actionUrl,
                            method: "POST",
                            data: form.serialize(),
                            cache: false,
                            processData: false,
                            contentType: false,
                            dataType: "json"
                        }).
                        then(function(response) {

                            $btn.button("reset");
                            var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                            form.find(".box-body").before(alertMsg);

                            // Sweet Alert
                            window.swal({
                              title: "Success!",
                              text: response.data.msg,
                              icon: "success",
                              buttons: true,
                              dangerMode: false,
                            })
                            .then(function (willDelete) {
                                if (willDelete) {
                                    $scope.closeCurrencyEditModal();
                                    $(document).find(".close").trigger("click");
                                    currencyId = response.data.id;
                                    
                                    $(datatable).DataTable().ajax.reload(function(json) {
                                        if ($("#row_"+currencyId).length) {
                                            $("#row_"+currencyId).flash("yellow", 5000);
                                        }
                                    }, false);

                                } else {
                                    $(datatable).DataTable().ajax.reload(null, false);
                                }
                            });

                        }, function(response) {

                            $btn.button("reset");

                            var alertMsg = "<div class=\"alert alert-danger\">";
                            window.angular.forEach(response.data, function(value, key) {
                                alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                            });
                            alertMsg += "</div>";
                            form.find(".box-body").before(alertMsg);

                            $(":input[type=\"button\"]").prop("disabled", false);

                            window.swal("Oops!", response.data.errorMsg, "error");
                        });

                    });

                    $scope.closeCurrencyEditModal = function () {
                        $uibModalInstance.dismiss("cancel");
                    };
                },
                scope: $scope,
                size: "md",
                backdrop  : "static",
                keyboard: true,
            });

            uibModalInstance.result.catch(function () { 
                uibModalInstance.close(); 
            });
        };
    }]);
window.angularApp.factory("CustomerCreateModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function($scope) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeCustomerCreateModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-plus\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/customer.php?action_type=CREATE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Create New Customer";
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 500);
                    
                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                // Submit Customer Form
                $(document).delegate("#create-customer-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {

                            $scope.customerId = response.data.id;
                            $scope.dueAmount = response.data.due_amount;

                            $(document).find("input[name=\"customer-name\"]").val(response.data.customer_name + ' (' + response.data.customer_contact + ')');
                            $(document).find("input[name=\"customer-id\"]").val(response.data.id);

                            // increase customer count
                            var customerCount = $(document).find("#customer-count h3");
                            if (customerCount) {
                                customerCount.text(parseInt(customerCount.text()) + 1);
                            }

                            // close modalwindow
                            $scope.closeCustomerCreateModal();
                            $(document).find(".close").trigger("click");

                            // Callback
                            if ($scope.CustomerCreateModalCallback) {
                                $scope.CustomerCreateModalCallback($scope);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeCustomerCreateModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop: "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("CustomerDeleteModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(customer) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeCustomerDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-trash\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/customer.php?customer_id=" + customer.customer_id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response) {
                    $scope.modal_title = customer.customer_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);

                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeCustomerDeleteModal();
                    });
                });

                $(document).delegate("#customer-delete", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(datatable).DataTable().ajax.reload( null, false );

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeCustomerDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeCustomerDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("CustomerDuePaidModal", ["API_URL","POS_API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL,POS_API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(customer) {

        $http.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('token');
        var invoiceID;
        $scope.confirm_button = false;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeCustomerDepositModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-plus\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/duepaid.php?customer_id=" + customer.id + "&action_type=PAID_FORM",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Due Pay for " + customer.name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeCustomerDepositModal();
                    });
                });


                $scope.sendNotification = function(){
                    var amount = $('#paid_amount').val();
                    console.log(amount);
                    var validate = $scope.validateAmount(amount);

                    // if(!validate){
                    //     window.swal("Oops!", 'Please enter valid amount', "error");
                    //     return false;
                    // }

                    if(!amount){
                        window.swal("Oops!", 'Please enter valid amount', "error");
                        return false;
                    }

                    if(parseInt($('#total_due_amount').val()) < parseInt(amount)){
                        window.swal("Oops!", "You can't paid greater than due amount", "error");
                        return false;
                    }
                    $('#paid_button').attr('disabled', true);
                    //var payment_method = $('#payment_method').val();
                    var payment_method = $('#payment_method')[0].selectedOptions[0].id;
                    var user_posapi = JSON.parse(localStorage.getItem('user_posapi'));
                    
                    payment_method = payment_method.trim();
                   
                    $http({
                        url: POS_API_URL + "/message/p2p/send?customer_id="+user_posapi.user_id+"&amount="+amount+"&payment_mode="+payment_method,
                        method: "POST",
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(zepResponse) {
                        $scope.confirm_button = true;
                        $('#paid_amount').attr('readonly', true);
                        $('#payment_method').attr('readonly', true);
                        $('#payment_token').val(zepResponse.data.data.payment_id);
                        
                    }, function(response) {
                        if(response.status === 401 && response.statusText == 'Unauthorized'){
                            var username =  localStorage.getItem('email');
                            var password =  localStorage.getItem('password');
                            $scope.relogin(username, password, 'product');
                        }
                        else{
                            window.swal("Oops!", "Notification not sent,Please try again", "error");
                        }
                        $('#paid_button').attr('disabled', false);
                        
                    }); 
                };


                //relogin
                $scope.relogin = function (username, password, $product = false) {
                    $.ajax({
                        url: POS_API_URL+"/login",
                        type: "POST",
                        dataType: "json",
                        data: {username: username, password: password},
                        success: function (response) {
                          localStorage.setItem('token', response.data.token);
                          localStorage.setItem('email', username);
                          localStorage.setItem('password', password);
                          $scope.sendNotification();
                        },
                        error: function (response) {   
                                  
                    }});
                };

                $scope.validateAmount = function (inputtxt) 
                { 
                    var decimal=  /^[-+]?[0-9]+$/; 
                    if(inputtxt.match(decimal)) 
                    { 
                        return true;
                    }
                    else
                    { 
                        return false;
                    }
                } 

                // Confirm duepaid
                $(document).delegate("#duepaid-confirm-btn", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeCustomerDepositModal();
                            $(document).find(".close").click();
                            customer.dueAmount = response.data.customer.due_amount;  
                            $("#customer-due-amount").text(customer.dueAmount);
                            
                            invoiceID = response.data.id;
                            $(datatable).DataTable().ajax.reload(function(json) {
                                if ($("#row_"+invoiceID).length) {
                                    $("#row_"+invoiceID).flash("yellow", 5000);
                                }
                            }, false);                          
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                $scope.closeCustomerDepositModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("CustomerEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(customer) {
        var customerId;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeCustomerEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/customer.php?customer_id=" + customer.customer_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = customer.customer_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);

                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                $(document).delegate("#customer-update", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                $scope.closeCustomerEditModal();
                                $(document).find(".close").trigger("click");
                                customerId = response.data.id;
                                
                                $(datatable).DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+customerId).length) {
                                        $("#row_"+customerId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {
                                $(datatable).DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                $scope.closeCustomerEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("SupportDeskModal", ["API_URL", "$http", "$uibModal", "$sce", "$rootScope", function(API_URL, $http, $uibModal, $sce, $scope) {
    return function() {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"cancel();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-headphones\"></span> Support Desk</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: API_URL + "/_inc/template/partials/supportdesk_modal.php",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function(data) {
                   window.swal("Oops!", "an unknown error occured!", "error");
                });
                $scope.cancel = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("DiscountByCategoryDeleteModal", [
    "API_URL", 
    "window", 
    "jQuery", 
    "$http", 
    "$uibModal", 
    "$sce", 
    "$rootScope", 
    function (
        API_URL, 
        window, 
        $, 
        $http, 
        $uibModal, 
        $sce, 
        $scope
    ) {
    return function(disc_by_cat) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeBrandDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-delete\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/discount_by_category.php?id=" + disc_by_cat.id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = disc_by_cat.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product delete form
                $(document).delegate("#disc-bycat-delete-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + "</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $("#discount-by-category-list").DataTable().ajax.reload( null, false);
                        if (response.data.action_type == "soft_delete") {
                            $("#total-trash").text(parseInt($("#total-trash").text())+1);
                        }

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeBrandDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeBrandDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("DiscountByCategoryEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(disc_by_cat) {
        var productId;
        var uibModalInstance = $uibModal.open({
            // animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeProductEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/discount_by_category.php?id=" + disc_by_cat.id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = disc_by_cat.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.select2();

                        // Generate random number
                        $(".random_num").click(function(){
                            $(this).parent(".input-group").children("input").val(window.storeApp.generateCardNo(8));
                        });
                    }, 500);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product update form
                $(document).delegate("#discbycategory-update-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function (response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {

                                // close modalwindow
                                $scope.closeProductEditModal();
                                $(document).find(".close").trigger("click");
                                $("body").removeClass("modal-open");

                                productId = response.data.id;
                                
                                $("#discount-by-category-list").DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+productId).length) {
                                        $("#row_"+productId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {

                                $("#discount-by-category-list").DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                
                // Close modal
                $scope.closeProductEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
            setTimeout(function() {
                $("body").removeClass("modal-open");
            }, 500);
        });

    };
}]);
window.angularApp.factory("DiscountByCouponsDeleteModal", [
    "API_URL", 
    "window", 
    "jQuery", 
    "$http", 
    "$uibModal", 
    "$sce", 
    "$rootScope", 
    function (
        API_URL, 
        window, 
        $, 
        $http, 
        $uibModal, 
        $sce, 
        $scope
    ) {
    return function(disc_by_cat) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeDiscountByCouponsDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-delete\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/discount_by_coupons.php?id=" + disc_by_cat.id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = disc_by_cat.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product delete form
                $(document).delegate("#disc-bycoupons-delete-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + "</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $("#discount-by-coupons-list").DataTable().ajax.reload( null, false);
                        if (response.data.action_type == "soft_delete") {
                            $("#total-trash").text(parseInt($("#total-trash").text())+1);
                        }

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeDiscountByCouponsDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeDiscountByCouponsDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("DiscountByCouponsEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(disc_by_cat) {
        var productId;
        var uibModalInstance = $uibModal.open({
            // animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeProductEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/discount_by_coupons.php?id=" + disc_by_cat.id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = disc_by_cat.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.select2();

                        // Generate random number
                        $(".random_num").click(function(){
                            $(this).parent(".input-group").children("input").val(window.storeApp.generateCardNo(8));
                        });
                    }, 500);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product update form
                $(document).delegate("#discbycoupons-update-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function (response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {

                                // close modalwindow
                                $scope.closeProductEditModal();
                                $(document).find(".close").trigger("click");
                                $("body").removeClass("modal-open");

                                productId = response.data.id;
                                
                                $("#discount-by-coupons-list").DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+productId).length) {
                                        $("#row_"+productId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {

                                $("#discount-by-coupons-list").DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                
                // Close modal
                $scope.closeProductEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
            setTimeout(function() {
                $("body").removeClass("modal-open");
            }, 500);
        });

    };
}]);
window.angularApp.factory("DueCollectionDetailsModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    var $from = window.getParameterByName("from");
    var $to = window.getParameterByName("to");
    return function($scope) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeDueCollectionModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-list\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/duepaid.php?action_type=DUEPAIDDETAILS&created_by=" + $scope.createdBy + "&from=" + $from + "&to=" + $to,
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Due Collection Details";
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 500);
                    
                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                $scope.closeDueCollectionModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "lg",
            backdrop  : "dynamic", //static
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("EmailModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(content) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"cancel();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-envelope\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>" +
                        "<div class=\"modal-footer\">" +
                            "<a id=\"sendEmailBtn\" class=\"btn-success btn btn-sm\" data-loading-text=\"Sending...\"><span class=\"fa fa-fw fa-send-o\"></span> SEND</a>" +
                            "<a ng-click=\"cancel();\" class=\"btn-danger btn btn-sm\" data-dismiss=\"modal\" aria-label=\"Close\"><span class=\"fa fa-fw- fa-close\"></span> CLOSE</a>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $scope.modal_title = content.title;
                var form = "<form method=\"post\" action=\"#\" id=\"email-form\">";
                form += "<input type=\"hidden\" name=\"template\" value=\""+content.template+"\">";
                form += "<input type=\"hidden\" name=\"subject\" value=\""+content.subject+"\">";
                form += "<input type=\"hidden\" name=\"title\" value=\""+content.title.trim()+"\">";
                form += "<input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"Please, type a valid email address\" required>";
                form += "<textarea style=\"display:none;\" name=\"emailbody\">"+content.html.trim()+"</textarea>";
                form += "</form>";
                $scope.rawHtml = $sce.trustAsHtml(form);
                $scope.content = content;

                // Send Email
                $(document).delegate("#sendEmailBtn", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();
 
                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    $("body").addClass("overlay-loader");
                    
                    $http({
                        url: window.baseUrl + "/_inc/send_email.php",
                        method: "POST",
                        data: $("#email-form").serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $("body").removeClass("overlay-loader");
                        $btn.button("reset");

                        // Success alert
                        window.swal("Success", response.data.msg, "success").then(function() {
                            $scope.cancel();
                        });

                    }, function(response) {
                        $("body").removeClass("overlay-loader");
                        $btn.button("reset");
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.cancel = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("ExpenseEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(invoice) {
        var expenseId;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeExpenseEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\">{{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile='rawHtml'>Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: API_URL + "/_inc/expense.php?invoice_id=" + invoice.invoice_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = 'Edit Expense Details';
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.timePicker();
                    }, 100);

                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                $(document).delegate("#edit-invoice-update", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: API_URL + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                $scope.closeExpenseEditModal();
                                $(document).find(".close").trigger("click");
                                expenseId = response.data.id;
                                
                                $(datatable).DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+expenseId).length) {
                                        $("#row_"+expenseId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {
                                $(datatable).DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeExpenseEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("ExpenseViewModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function (invoice) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeExpenceViewModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/expense.php?invoice_id=" + invoice.invoice_id + '&action_type=VIEW',
                  method: "GET"
                })
                .then(function (response, status, headers, config) {
                    $scope.modal_title = "View Invoice";
                    $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function (response) {
                   window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeExpenceViewModal();
                    });
                });

                $scope.closeExpenceViewModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("keyboardShortcutModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function() {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"cancel();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-keyboard-o\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: API_URL + "/_inc/template/partials/keyboard_shortcut.php",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Keyboard Shortcut";
                    $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function(data) {
                   window.swal("Oops!", "an unknown error occured!", "error");
                });
                $scope.cancel = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });
        
        uibModalInstance.result.then(function (selectedItem) {
            // ...
        }, function () {
            $("#keyboard-shortcut").removeClass("open");
        });
    };
}]);
window.angularApp.factory("PaymentDeleteModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(payment) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closePaymentDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-trash\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/payment.php?payment_id=" + payment.payment_id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = payment.name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);
                    
                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closePaymentDeleteModal();
                    });
                });

                $(document).delegate("#payment-delete", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(datatable).DataTable().ajax.reload( null, false );

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closePaymentDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closePaymentDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("PaymentEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(payment) {
        var paymentId;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closePaymentEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: API_URL + "/_inc/payment.php?payment_id=" + payment.payment_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = payment.name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);

                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                $(document).delegate("#payment-update", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: API_URL + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                $scope.closePaymentEditModal();
                                $(document).find(".close").trigger("click");
                                paymentId = response.data.id;
                                
                                $(datatable).DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+paymentId).length) {
                                        $("#row_"+paymentId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {
                                $(datatable).DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {

                        $btn.button("reset");

                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        $(":input[type=\"button\"]").prop("disabled", false);

                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closePaymentEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("PayNowModal", [ "API_URL", "POS_API_URL","window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", "PrintReceiptModal", "EmailModal", 
function ( API_URL,POS_API_URL, window, $, $http, $uibModal, $sce, $scope, PrintReceiptModal, EmailModal
) {
    "use strict";
    return function($scope) {
        if (window.store.sound_effect == 1) {
            window.storeApp.playSound("modal_opened.mp3");
        }
        $scope.done = false;
        $scope.paidAmountByCustomer = 0;
        $scope.notificationSent = false;
        $scope.paymentNameLater = false;
        $scope.paymentidLater = false;
        $scope.reference_id = false;
        $scope.paidAmount = window.formatDecimal($scope.totalPayable, 2);
        $scope.productDiscount = 0;
        window.angular.forEach($scope.itemArray, function(item, key) {
            $scope.productDiscount = $scope.productDiscount+item.discount;
        });
        
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-show=\"!done\" ng-click=\"backToPos();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                            "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-user\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/template/pos_payment_form.php",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = $scope.customerName;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        $(document).find("#payable-amount").focus().select();
                    }, 100);

                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closePayNowModal();
                    });
                });



                // pay button action
                $scope.pay = function(paymentId, paymentName, confirm_payment = false, paymentmethod_id=false) {

                    if(!confirm_payment){
                        paymentName = paymentName.toUpperCase();
                    }
                    
                    if(paymentName != 'CASH'){
                       paymentName = 'CARD'; 
                    }

                $scope.paymentNameLater = paymentName;
                $scope.paymentidLater = paymentId;

                    $(document).find(".modal").addClass("overlay-loader");
                    var pay_by_credit = 'false';
                    if($("#pay_by_credit").is(":checked")){
                        pay_by_credit = 'true';
                    }

                    
                    var total_payable_amount = $('#payable-amount').val();

                    var regex  = /^\d+(\.\d{1,2})?$/;
                    var numStr = total_payable_amount;
                    console.log(regex.test(numStr));
                    if (!regex.test(numStr)){
                        window.swal("Oops!", 'Please enter a valid amount to paid', "error");
                        $(document).find(".modal").removeClass("overlay-loader");
                        return false;
                    }

                    if(pay_by_credit == 'true'){
                        console.log('true true', $('#payable-amount_hidden').val());
                        console.log('credit amount', $scope.creditAmount);
                        if($('#payable-amount_hidden').val() > $scope.creditAmount){

                            var remainAmount = $('#payable-amount_hidden').val()-$scope.creditAmount;
                            console.log('remainAmount', remainAmount);
                             console.log('total_payable_amount', total_payable_amount);
                            if(total_payable_amount > remainAmount){
                                window.swal("Oops!", 'Please enter a valid amount to paid', "error");
                                $(document).find(".modal").removeClass("overlay-loader");
                                return false;
                            }
                        }
                    }

                    if(total_payable_amount <1 && $scope.creditAmount<total_payable_amount){
                        window.swal("Oops!", 'Please enter a valid amount to paid', "error");
                        $(document).find(".modal").removeClass("overlay-loader");
                        return false;
                    }
                    if(total_payable_amount > $scope.dueAmount+
                        ($scope.totalPrice-$scope.coupan_code_discount)){
                        window.swal("Oops!", "You can't not pay more than your payable amount", "error");
                        $(document).find(".modal").removeClass("overlay-loader");
                        return false;
                    }

                    if(confirm_payment){
                        $scope.madePayment(paymentId, total_payable_amount,pay_by_credit,confirm_payment, paymentmethod_id);
                    }
                    else{
                       $http({
                        url: POS_API_URL + "/message/p2p/send?customer_id="+$scope.customerId+"&amount="+total_payable_amount+"&payment_mode="+paymentName,
                        method: "POST",
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(zepResponse) {
                        
                        $scope.notificationSent = true;
                        $scope.reference_id = zepResponse.data.data.payment_id;
                        console.log('first', zepResponse);
                        console.log('first', zepResponse.data.data.payment_id);
                        console.log('second', $scope.reference_id);
                        $(document).find(".modal").removeClass("overlay-loader");
                        //$scope.paymentSuccess(response, total_payable_amount, payment_mode);
                        //$scope.madePayment(paymentId, total_payable_amount,pay_by_credit,zepResponse.data.ref_no);
                    }, function(zepResponse) {
                       // $scope.reference_id = 'dsaddsa';
                        //console.log($scope.reference_id);
                        $scope.notificationSent = true;
                        //$scope.madePayment(paymentId, total_payable_amount,pay_by_credit,'fdsfdffsdf');
                        //$scope.paymentSuccess(response, total_payable_amount, payment_mode);      
                        window.swal("Oops!", 'Notification not sent', "error");
                        $(document).find(".modal").removeClass("overlay-loader");
                    }); 
                    }

                    


                    //  $http({
                    //     url: POS_API_URL + "/payments/order?payment_method_id="+paymentId+"&customer_id="+$scope.customerId+"&invoice_note="+$scope.invoiceNote+"&previous_due="+ $scope.dueAmount+"&paid_amount="+total_payable_amount+"&coupon_code="+$scope.coupan_Code+"&use_credit="+pay_by_credit,
                    //     method: "POST",
                    //     cache: false,
                    //     processData: false,
                    //     contentType: false,
                    //     dataType: "json"
                    // }).
                    // then(function(response) {
                    //    $scope.payByZep(response, total_payable_amount, paymentName);
                    // }, function(response) {
                    //     if (window.store.sound_effect == 1) {
                    //         window.storeApp.playSound("error.mp3");
                    //     }
                                  
                    //     window.swal("Oops!", response.data.errors.paid_amount[0], "error");
                    //     $(document).find(".modal").removeClass("overlay-loader");
                    // });
                };

                $scope.payByEnterKey = function($event) {
                    if(($event.keyCode || $event.which) == 13){
                        $scope.pay(1, 'Cash on Delivery');
                    }
                };


                $scope.closePayNowModal = function () {
                    if (!window.getParameterByName("customer_id") || !window.getParameterByName("invoice_id")) {
                        $scope.resetPos();
                    }
                    $uibModalInstance.dismiss("cancel");
                    if (window.settings.after_sell_page == "invoice" || (window.getParameterByName("customer_id") && window.getParameterByName("invoice_id"))) {
                        window.location = window.baseUrl + "/admin/view_invoice.php?invoice_id=" + $scope.invoiceId;
                    }
                };

                // back to pos button
                $scope.backToPos = function() {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "lg",
            backdrop  : "static",
            keyboard: true, // ESC key close enable/disable
            resolve: {
                userForm: function () {
                    // return customerForm;
                }
            }
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
            if (window.store.sound_effect == 1) {
                window.storeApp.playSound("modal_closed.mp3");
            }
        });



        $scope.madePayment = function(paymentId, total_payable_amount,pay_by_credit, ref_id, madePayment_id){

            //var total_payable_amount = $('#payable-amount_hidden').val();
            $http({
                url: POS_API_URL + "/payments/order?payment_method_id="+madePayment_id+"&customer_id="+$scope.customerId+"&invoice_note="+$scope.invoiceNote+"&previous_due="+ $scope.dueAmount+"&paid_amount="+total_payable_amount+"&coupon_code="+$scope.coupan_Code+"&use_credit="+pay_by_credit+"&ref_no="+ref_id,
                method: "POST",
                cache: false,
                processData: false,
                contentType: false,
                dataType: "json"
            }).
            then(function(response) {
               //$scope.payByZep(response, total_payable_amount, paymentName);
                $scope.paymentSuccess(response, total_payable_amount, false);
            }, function(response) {
                if (window.store.sound_effect == 1) {
                    window.storeApp.playSound("error.mp3");
                }
                          
                //window.swal("Oops!", response.data.errors.paid_amount[0], "error");
                $(document).find(".modal").removeClass("overlay-loader");
            });
        }

        $scope.payByZep = function(response, total_payable_amount, payment_mode){
            
                     $http({
                        url: POS_API_URL + "/message/p2p/send?invoice_id="+response.data.data.invoice_id+"&amount="+total_payable_amount+"&payment_mode="+payment_mode,
                        method: "POST",
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(zepResponse) {

                        $scope.paymentSuccess(response, total_payable_amount, payment_mode);
                    }, function(zepResponse) {
                        $scope.paymentSuccess(response, total_payable_amount, payment_mode);      
                        window.swal("Oops!", 'Notification not sent', "error");
                        $(document).find(".modal").removeClass("overlay-loader");
                    });
        }

        $scope.sendInvoiceViaSms = function(invoiceId){
         $http({
            url: POS_API_URL + "/message/invoice/send?invoice_id="+invoiceId,
            method: "POST",
            cache: false,
            processData: false,
            contentType: false,
            dataType: "json"
        }).
        then(function(response) {

            window.swal('Invoice sent successfully.', "");
        }, function(response) {
            window.swal("Oops!", 'Sms not sent', "error");
            $(document).find(".modal").removeClass("overlay-loader");
        });
        }

        $scope.whatsapp = function(invoiceId){
            
            $(document).find(".modal").addClass("overlay-loader");
             $http({
                url: POS_API_URL + "/message/invoice/send/wa?invoice_id="+invoiceId,
                method: "POST",
                cache: false,
                processData: false,
                contentType: false,
                dataType: "json"
            }).
            then(function(response) {
                $(document).find(".modal").removeClass("overlay-loader");
                window.swal('WhatsApp sent successfully.', "");
            }, function(response) {
                window.swal("Oops!", 'WhatsApp not sent', "error");
                $(document).find(".modal").removeClass("overlay-loader");
            });
        }

        $scope.paymentSuccess = function(response, total_payable_amount, payment_mode){
            $(document).find(".modal").removeClass("overlay-loader");
            $scope.invoiceId = response.data.data.invoice_id;
            $scope.invoiceInfo = response.data.data.invoice_info;
            $scope.invoiceItems = response.data.data.invoice_items;
            $scope.done = true;
            $scope.notificationSent = false;
            if (window.store.sound_effect == 1) {
                window.storeApp.playSound("modify.mp3");
            }
            if (window.store.auto_print == 1 && window.store.remote_printing == 1) {
                PrintReceiptModal($scope);
            } else if (window.store.auto_print == 1) {
                window.open(window.baseUrl + "/admin/view_invoice.php?invoice_id=" + $scope.invoiceInfo.invoice_id);
            }
            $scope.showProductList();
        }

        $scope.totalDiscount = function(productDiscount, coupanDiscount){
            return parseFloat(productDiscount)+parseFloat(coupanDiscount);
        }

        //subtotal without tax
        $scope.subTotalWithoutTax = function(total, tax){
            return parseFloat(total)-parseFloat(tax);
        }

        $scope.checkBoxChange = function(){
            
            if($('#pay_by_credit').is(":checked")){
                
                if($scope.creditAmount >= $scope.totalPrice){
                    $('#payable-amount').prop('disabled', true);
                    $('#payable-amount').val('0.00');
                }
                else{
                    $('#payable-amount').prop('disabled', false);
                    var remainingAmount = (parseFloat($scope.totalPrice)+parseFloat($scope.dueAmount))-parseFloat($scope.creditAmount);
                    $('#payable-amount').val(remainingAmount.toFixed(2));
                }
            }
            if(!$('#pay_by_credit').is(":checked")){
                $('#payable-amount').prop('disabled', false);
                $('#payable-amount').val(
                    (parseFloat($scope.totalPrice)+parseFloat($scope.dueAmount)).toFixed(2));
            }
        }

        // sending invoice via email
        $scope.sendInvoiceViaEmail = function (invoiceId) {

            if (!invoiceId) {
                if (window.store.sound_effect == 1) {
                    window.storeApp.playSound("error.mp3");
                }
                window.swal("Oops!", "Invalid invoice id", "error");
                return false;
            }

            // fetch invocie and send thought the provided email
            $http({
                url: API_URL + "/admin/view_invoice.php?invoice_id=" + invoiceId,
                method: "GET",
                cache: false,
                processData: false,
                dataType: "html"
            }).
            then(function(response) {
                var invoiceContent = $(response.data).find("#invoice").html();
                if (!invoiceContent || invoiceContent == "undefined") {
                    if (window.store.sound_effect == 1) {
                        window.storeApp.playSound("error.mp3");
                    }
                    window.swal("Oops!", "Invalid invoice id", "error");
                    return false;
                }
                var recipientName = $scope.customerName;
                var invoice = {
                    template: "invoice", 
                    subject: "Invoice", 
                    title: "Invoice", 
                    recipientName: recipientName, 
                    senderName: window.store.name,
                    html: invoiceContent
                };
                EmailModal(invoice);
            }, function(response) {
                if (window.store.sound_effect == 1) {
                    window.storeApp.playSound("error.mp3");
                }
                window.swal("Oops!", "an Unknown error occured!", "error");
            });
        };
    };
}]);
window.angularApp.factory("POSFilemanagerModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function (data) {
        var uibModalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeFileManager()\" type=\"button\" id=\"close-filemanger\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\">{{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-bodyx\" id=\"filemanager\">" +
                            "<div bind-html-compile=\"rawHtml\"><div style=\"padding:10px;\">Loading...</div></div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/admin/filemanager.php?ajax=1&target=" + data.target + "&thumb=" + data.thumb,
                  dataType: "html",
                  method: "GET"
                })
                .then(function (response, status, headers, config) {
                    $("body").addClass("filemanager-open");
                    $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function (response) {
                    window.swal("Oops!", response.data.errorMsg, "error").then(function() {
                        $scope.closeFileManager();
                    });
                });

                $scope.closeFileManager = function () {
                    $uibModalInstance.dismiss("cancel");
                    setTimeout(function() {
                        
                        if ($(document).find('.modal').length) {
                            $("body").addClass("modal-open");
                        }
                    }, 200);
                };
            },
            scope: $scope,
            size: "lg",
            backdrop  : "static",
            keyboard: false,
        });
        
        uibModalInstance.result.catch(function () { 
            setTimeout(function() {
                $("body").removeClass("modal-open");
                $("body").removeClass("filemanager-open");
            }, 500);
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("PrinterDeleteModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(printer) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeprinterDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-trash\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/printer.php?printer_id=" + printer.printer_id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response) {
                    $scope.modal_title = printer.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);

                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeprinterDeleteModal();
                    });
                });

                $(document).delegate("#printer-delete", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(datatable).DataTable().ajax.reload( null, false );

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeprinterDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeprinterDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("PrinterEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function($rscope) {
        var printerId;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeprinterEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/printer.php?printer_id=" + $rscope.printer.printer_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = $rscope.printer.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);
                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);
                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                $(document).delegate("#printer-update", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                $scope.closeprinterEditModal();
                                $(document).find(".close").trigger("click");
                                printerId = response.data.id;
                                
                                $(datatable).DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+printerId).length) {
                                        $("#row_"+printerId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {
                                $(datatable).DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeprinterEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("PrintReceiptModal", ["API_URL", "window", "jQuery", "$http", "$rootScope", function (API_URL, window, $, $http, $scope) {
    return function($scope) {

        var receipt_data = {};
        receipt_data.store_name = window.store.name + "\n";

        receipt_data.header = "";
        receipt_data.header += window.store.name + "\n";
        receipt_data.header += window.store.address + "\n";
        receipt_data.header += window.store.mobile + "\n";
        receipt_data.info += "\n";

        receipt_data.info = "";
        receipt_data.info += "Datetime:" + $scope.invoiceInfo.created_at + "\n";
        receipt_data.info += "Invoice ID:" + $scope.invoiceInfo.invoice_id + "\n";
        receipt_data.info += "Created By:" + $scope.invoiceInfo.created_by + "\n";
        receipt_data.info += "\n";
        receipt_data.info += "Customer:" + $scope.invoiceInfo.customer_name + "\n";
        receipt_data.info += "Customer Contact:" + $scope.invoiceInfo.customer_contact + "\n";
        receipt_data.info += "\n";

        receipt_data.items = "";
        window.angular.forEach($scope.invoiceItems, function($row, key) {
            receipt_data.items += "#" + key + " " + $row.item_name + "\n";
            receipt_data.items += $row.item_quantity + " x " + $row.item_price + "   " + $row.item_quantity * $row.item_price + "\n";
        });

        receipt_data.totals = "";
        receipt_data.totals += "Subtotal: " + $scope.invoiceInfo.payable_amount + "\n";
        receipt_data.totals += "Discount:" + $scope.invoiceInfo.discount_amount + "\n";
        receipt_data.totals += "Tax:" + $scope.invoiceInfo.tax_amount + "\n";
        receipt_data.totals += "Grand Total:" + $scope.invoiceInfo.payable_amount + "\n";
        receipt_data.totals += "Paid Amount:" + $scope.invoiceInfo.paid_amount + "\n";
        receipt_data.totals += "Due Amount:" + $scope.invoiceInfo.todays_due + "\n";

        receipt_data.footer = "";
        if ($scope.invoiceInfo.invoice_note) {
            receipt_data.footer += $scope.invoiceInfo.invoice_note + "\n\n";
        }  

        var socket_data = {
            'printer': window.printer,
            'logo': '',
            'text': receipt_data,
            'cash_drawer': '',
        };
        $.get(window.baseUrl + '/_inc/print.php', {data: JSON.stringify(socket_data)});    
    };
}]);
window.angularApp.factory("ProductCreateModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function($scope) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeProductCreateModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-plus\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/product.php?action_type=CREATE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Create New Product";
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.select2();

                        // Generate random number
                        $(".random_num").click(function(){
                            $(this).parent(".input-group").children("input").val(window.storeApp.generateCardNo(8));
                        });
                        
                    }, 500);

                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                // Submit Product Form
                $(document).delegate("#create-product-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {

                            $scope.product = response.data.product;

                            // close modalwindow
                            $scope.closeProductCreateModal();
                            $(document).find(".close").trigger("click");

                            // insert product into select2
                            var select = $(document).find('#product_id');
                            if (select.length) {
                                var option = $('<option></option>').
                                     attr('selected', true).
                                     text(response.data.product.p_name).
                                     val(response.data.id);
                                option.appendTo(select);
                                select.trigger('change');
                            }

                            // increase product count
                            var productCount = $(document).find("#product-count h3");
                            if (productCount) {
                                productCount.text(parseInt(productCount.text()) + 1);
                            }

                            // Callback
                            if ($scope.ProductCreateModalCallback) {
                                $scope.ProductCreateModalCallback($scope);
                            }

                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeProductCreateModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("ProductDeleteModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(product) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeProductDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-delete\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/product.php?p_id=" + product.p_id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = product.p_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product delete form
                $(document).delegate("#product-delete-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + "</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $("#product-product-list").DataTable().ajax.reload( null, false);
                        if (response.data.action_type == "soft_delete") {
                            $("#total-trash").text(parseInt($("#total-trash").text())+1);
                        }

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeProductDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeProductDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("ProductEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(product) {
        var productId;
        var uibModalInstance = $uibModal.open({
            // animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeProductEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/product.php?p_id=" + product.p_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = product.p_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.select2();

                        // Generate random number
                        $(".random_num").click(function(){
                            $(this).parent(".input-group").children("input").val(window.storeApp.generateCardNo(8));
                        });
                    }, 500);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product update form
                $(document).delegate("#product-update-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function (response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {

                                // close modalwindow
                                $scope.closeProductEditModal();
                                $(document).find(".close").trigger("click");
                                $("body").removeClass("modal-open");

                                productId = response.data.id;
                                
                                $("#product-product-list").DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+productId).length) {
                                        $("#row_"+productId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {

                                $("#product-product-list").DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                
                // Close modal
                $scope.closeProductEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
            setTimeout(function() {
                $("body").removeClass("modal-open");
            }, 500);
        });

    };
}]);
window.angularApp.factory("ProductReturnModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(product) {
        var productId;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeProductDecrementModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-refresh\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/product_return.php?p_id=" + product.p_id,
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                $scope.modal_title = product.p_name;
                $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                 // Submit product return form
                $(document).delegate("#save-product-return", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    //var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i>" + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                $scope.closeProductDecrementModal();
                                $(document).find(".close").trigger("click");
                                productId = response.data.id;
                                
                                $("#product-product-list").DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+productId).length) {
                                        $("#row_"+productId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {
                                $("#product-product-list").DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });
                
                // Close modal
                $scope.closeProductDecrementModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("ProductViewModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(product) {
       var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeProductViewModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-eye\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/product.php?p_id=" + product.p_id + "&action_type=VIEW",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = product.p_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeProductViewModal();
                    });
                });

                // Close modal
                $scope.closeProductViewModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });

    };
}]);
window.angularApp.factory("PromotionsDeleteModal", [
    "API_URL", 
    "window", 
    "jQuery", 
    "$http", 
    "$uibModal", 
    "$sce", 
    "$rootScope", 
    function (
        API_URL, 
        window, 
        $, 
        $http, 
        $uibModal, 
        $sce, 
        $scope
    ) {
    return function(result) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closePromotionsDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-delete\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/promotions.php?id=" + result.id + "&promotions_type=" + result.edit_promotions_type + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = result.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product delete form
                $(document).delegate("#promotions-delete-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();
                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + "</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $("#promotions-list").DataTable().ajax.reload( null, false);
                        if (response.data.action_type == "soft_delete") {
                            $("#total-trash").text(parseInt($("#total-trash").text())+1);
                        }

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closePromotionsDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closePromotionsDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("PromotionsEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(result) {
        var productId;
        var uibModalInstance = $uibModal.open({
            // animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closePromotionsEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/promotions.php?id=" + result.id + "&promotions_type=" + result.edit_promotions_type + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = result.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.select2();

                        // Generate random number
                        $(".random_num").click(function(){
                            $(this).parent(".input-group").children("input").val(window.storeApp.generateCardNo(8));
                        });
                    }, 500);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product update form
                $(document).delegate("#promotions-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function (response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {

                                // close modalwindow
                                $scope.closePromotionsEditModal();
                                $(document).find(".close").trigger("click");
                                $("body").removeClass("modal-open");

                                productId = response.data.id;
                                
                                $("#promotions-list").DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+productId).length) {
                                        $("#row_"+productId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {

                                $("#promotions-list").DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                
                // Close modal
                $scope.closePromotionsEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
            setTimeout(function() {
                $("body").removeClass("modal-open");
            }, 500);
        });

    };
}]);
window.angularApp.factory("RefundByInvoiceEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(result) {
        
        var productId;
        var uibModalInstance = $uibModal.open({
            // animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeRefundByInvoiceEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/refund-by-invoice.php?sell_item_id=" + result.sell_item_id + "&customer_id=" + result.customer_id + "&invoice_id=" + result.invoice_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = result.item_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.select2();

                        // Generate random number
                        $(".random_num").click(function(){
                            $(this).parent(".input-group").children("input").val(window.storeApp.generateCardNo(8));
                        });
                    }, 500);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product update form
                $(document).delegate("#refund_by_invoice_submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function (response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {

                                // close modalwindow
                                $scope.closeRefundByInvoiceEditModal();
                                $(document).find(".close").trigger("click");
                                $("body").removeClass("modal-open");

                                productId = response.data.id;
                                
                                $("#refund-by-invoice-list").DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+productId).length) {
                                        $("#row_"+productId).flash("yellow", 5000);
                                    }
                                }, false);

                            } 
                            else {
                                $("#refund-by-invoice-list").DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                
                // Close modal
                $scope.closeRefundByInvoiceEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
            setTimeout(function() {
                $("body").removeClass("modal-open");
            }, 500);
        });

    };
}]);
window.angularApp.factory("RefundDeleteModal", [
    "API_URL", 
    "window", 
    "jQuery", 
    "$http", 
    "$uibModal", 
    "$sce", 
    "$rootScope", 
    function (
        API_URL, 
        window, 
        $, 
        $http, 
        $uibModal, 
        $sce, 
        $scope
    ) {
    return function(result) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeRefundDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-delete\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/refund.php?id=" + result.id + "&promotions_type=" + result.edit_promotions_type + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = result.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product delete form
                $(document).delegate("#promotions-delete-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();
                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + "</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $("#promotions-list").DataTable().ajax.reload( null, false);
                        if (response.data.action_type == "soft_delete") {
                            $("#total-trash").text(parseInt($("#total-trash").text())+1);
                        }

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeRefundDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeRefundDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("RefundEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(result) {
        var productId;
        var uibModalInstance = $uibModal.open({
            // animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeRefundEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/refund.php?id=" + result.id + "&promotions_type=" + result.edit_promotions_type + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = result.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.select2();

                        // Generate random number
                        $(".random_num").click(function(){
                            $(this).parent(".input-group").children("input").val(window.storeApp.generateCardNo(8));
                        });
                    }, 500);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product update form
                $(document).delegate("#promotions-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function (response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {

                                // close modalwindow
                                $scope.closeRefundEditModal();
                                $(document).find(".close").trigger("click");
                                $("body").removeClass("modal-open");

                                productId = response.data.id;
                                
                                $("#promotions-list").DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+productId).length) {
                                        $("#row_"+productId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {

                                $("#promotions-list").DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                
                // Close modal
                $scope.closePromotionsEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
            setTimeout(function() {
                $("body").removeClass("modal-open");
            }, 500);
        });

    };
}]);
window.angularApp.factory("StoreDeleteModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(store) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeStoreDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-trash\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/store.php?store_id=" + store.store_id + "&token="+localStorage.getItem('admin_token')+"&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = store.name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);
                    
                }, function(data) {
                    window.swal("Oops!", window.response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeStoreDeleteModal();
                    });
                });

                $(document).delegate("#store-delete", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(datatable).DataTable().ajax.reload( null, false );

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeStoreDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeStoreDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("SupplierCreateModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function($scope) {
        var uibSupplierModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeSupplierCreateModal();\" type=\"button\" class=\"close supplier-create-modal\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-plus\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/supplier.php?action_type=CREATE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Create New Supplier";
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 500);

                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                // Submit Box Form
                $(document).delegate("#create-supplier-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {

                            // close modalwindow
                            $scope.closeSupplierCreateModal();
                            $(document).find(".close").trigger("click");

                            // insert supplier into select2
                            var select = $(document).find('#sup_id');
                            if (select.length) {
                                var option = $('<option></option>').
                                     attr('selected', true).
                                     text(response.data.supplier.sup_name).
                                     val(response.data.id);
                                option.appendTo(select);
                                select.trigger('change');
                            }

                            // increase supplier count
                            var supplierCount = $(document).find("#supplier-count h3");
                            if (supplierCount) {
                                supplierCount.text(parseInt(supplierCount.text()) + 1);
                            }

                            // Callback
                            if ($scope.SupplierCreateModalCallback) {
                                $scope.SupplierCreateModalCallback($scope);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeSupplierCreateModal = function () {
                    uibSupplierModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibSupplierModalInstance.result.catch(function () { 
            uibSupplierModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("SupplierDeleteModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(supplier) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeSupplierDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-trash\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/supplier.php?sup_id=" + supplier.sup_id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = supplier.sup_name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);
                
                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeSupplierDeleteModal();
                    });
                });

                $(document).delegate("#supplier-delete", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(datatable).DataTable().ajax.reload( null, false );

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeSupplierDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeSupplierDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("SupplierEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(supplier) {
        var supId;
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeSupplierEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/supplier.php?sup_id=" + supplier.sup_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                $scope.modal_title = supplier.sup_name;
                $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                $(document).delegate("#supplier-update", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(datatable).DataTable().ajax.reload( null, false );

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                $scope.closeSupplierEditModal();
                                $(document).find(".close").trigger("click");
                                supId = response.data.id;
                                
                                $(datatable).DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+supId).length) {
                                        $("#row_"+supId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {
                                $(datatable).DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeSupplierEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("TaxRatesDeleteModal", [
    "API_URL", 
    "window", 
    "jQuery", 
    "$http", 
    "$uibModal", 
    "$sce", 
    "$rootScope", 
    function (
        API_URL, 
        window, 
        $, 
        $http, 
        $uibModal, 
        $sce, 
        $scope
    ) {
    return function(result) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeTaxRatesDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-delete\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/tax_rates.php?id=" + result.tax_rate_id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = result.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product delete form
                $(document).delegate("#tax-rates-delete-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();
                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + "</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $("#tax-rates-list").DataTable().ajax.reload( null, false);
                        if (response.data.action_type == "soft_delete") {
                            $("#total-trash").text(parseInt($("#total-trash").text())+1);
                        }

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeTaxRatesDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeTaxRatesDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("TaxRatesEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(result) {
        var productId;
        var uibModalInstance = $uibModal.open({
            // animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeTaxRatesEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/tax_rates.php?id=" + result.tax_rate_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = result.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.select2();

                        // Generate random number
                        $(".random_num").click(function(){
                            $(this).parent(".input-group").children("input").val(window.storeApp.generateCardNo(8));
                        });
                    }, 500);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product update form
                $(document).delegate("#tax-rates-update-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function (response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                // close modalwindow
                                $scope.closeTaxRatesEditModal();
                                $(document).find(".close").trigger("click");
                                $("body").removeClass("modal-open");

                                productId = response.data.id;
                                
                                $("#tax-rates-list").DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+productId).length) {
                                        $("#row_"+productId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {

                                $("#tax-rates-list").DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                
                // Close modal
                $scope.closeTaxRatesEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
            setTimeout(function() {
                $("body").removeClass("modal-open");
            }, 500);
        });

    };
}]);
window.angularApp.factory("UnitesEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(result) {
        var productId;
        var uibModalInstance = $uibModal.open({
            // animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeUnitesEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/unites.php?id=" + result.unit_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = result.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.datePicker();
                        window.storeApp.select2();

                        // Generate random number
                        $(".random_num").click(function(){
                            $(this).parent(".input-group").children("input").val(window.storeApp.generateCardNo(8));
                        });
                    }, 500);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product update form
                $(document).delegate("#unites-update-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function (response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                            alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                            alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                // close modalwindow
                                $scope.closeUnitesEditModal();
                                $(document).find(".close").trigger("click");
                                $("body").removeClass("modal-open");

                                productId = response.data.id;
                                
                                $("#unites-list").DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+productId).length) {
                                        $("#row_"+productId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {

                                $("#unites-list").DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });
                
                // Close modal
                $scope.closeUnitesEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
            setTimeout(function() {
                $("body").removeClass("modal-open");
            }, 500);
        });

    };
}]);
window.angularApp.factory("UnitesDeleteModal", [
    "API_URL", 
    "window", 
    "jQuery", 
    "$http", 
    "$uibModal", 
    "$sce", 
    "$rootScope", 
    function (
        API_URL, 
        window, 
        $, 
        $http, 
        $uibModal, 
        $sce, 
        $scope
    ) {
    return function(result) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeUnitesDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-delete\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/unites.php?id=" + result.unit_id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = result.title;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                }, function(data) {
                   window.swal("Oops!", "an error occured!", "error");
                });

                // Submit product delete form
                $(document).delegate("#unites-delete-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();
                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + "</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $("#unites-list").DataTable().ajax.reload( null, false);
                        if (response.data.action_type == "soft_delete") {
                            $("#total-trash").text(parseInt($("#total-trash").text())+1);
                        }

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeUnitesDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeUnitesDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("UserCreateModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function($scope) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeUserCreateModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-plus\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/user.php?action_type=CREATE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Create New User";
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 500);
                    
                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                // Submit Box Form
                $(document).delegate("#create-user-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {

                            // close modalwindow
                            $scope.closeUserCreateModal();
                            $(document).find(".close").trigger("click");

                            // insert user into select2
                            var select = $(document).find('#user_id');
                            if (select.length) {
                                var option = $('<option></option>').
                                     attr('selected', true).
                                     text(response.data.user.username).
                                     val(response.data.id);
                                option.appendTo(select);
                                select.trigger('change');
                            }

                            // increase user count
                            var userCount = $(document).find("#user-count h3");
                            if (userCount) {
                                userCount.text(parseInt(userCount.text()) + 1);
                            }

                            // Callback
                            if ($scope.UserCreateModalCallback) {
                                $scope.UserCreateModalCallback($scope);
                            }

                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeUserCreateModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("UserDeleteModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(user) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeUserDeleteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-trash\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/user.php?id=" + user.id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = user.username;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);
                
                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeUserDeleteModal();
                    });
                });

                $(document).delegate("#user-delete", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(datatable).DataTable().ajax.reload( null, false );

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeUserDeleteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeUserDeleteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("UserEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(user) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeUserEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/user.php?id=" + user.id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = user.username;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);

                }, function(data) {
                   window.swal("Oops!", window.response.data.errorMsg, "error");
                });

                $scope.adminUrl = 'http://13.233.161.62/admin/public/api';

                $(document).delegate("#user-update", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        //UPDATE TO ADMIN
                        $scope.editUserToAdmin(form.serialize());
                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                $scope.closeUserEditModal();
                                $(document).find(".close").trigger("click");
                                window.userId = response.data.id;
                                
                                $(datatable).DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+window.userId).length) {
                                        $("#row_"+window.userId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {
                                $(datatable).DataTable().ajax.reload(null, false);


                            }
                        });

                    }, function(response) {

                        $btn.button("reset");

                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        $(":input[type=\"button\"]").prop("disabled", false);

                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeUserEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };


                $scope.editUserToAdmin = function(){
                    var user_id = $('#id').val();
                    var name = $('#username').val();
                    var email = $('#email').val();
                    var mobile = $('#mobile').val();
                    
                    //console.log('editUserToAdmin');
                    $http.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('admin_token');
                    $http({
                        url: $scope.adminUrl+"/edit-user?name="+name+"&email="+email+"&mobile="+mobile+"&user_id="+user_id,
                        method: "POST",
                        dataType: "json",
                        success: function (apiresponse) {
                          console.log(apiresponse);
                          
                        },
                        error: function (response) {   
                                
                    }});
                }


            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("UserGroupCreateModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function($scope) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeUserGroupCreateModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-plus\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/user_group.php?action_type=CREATE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Create New UserGroup";
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 500);
                    
                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                // Submit Box Form
                $(document).delegate("#create-usergroup-submit", "click", function(e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {

                            // close modalwindow
                            $scope.closeUserGroupCreateModal();
                            $(document).find(".close").trigger("click");

                            // insert usergroup into select2
                            var select = $(document).find('#group_id');
                            if (select.length) {
                                var option = $('<option></option>').
                                     attr('selected', true).
                                     text(response.data.usergroup.name).
                                     val(response.data.id);
                                option.appendTo(select);
                                select.trigger('change');
                            }

                            // increase usergroup count
                            var usergroupCount = $(document).find("#usergroup-count h3");
                            if (usergroupCount) {
                                usergroupCount.text(parseInt(usergroupCount.text()) + 1);
                            }

                            // Callback
                            if ($scope.UserGroupCreateModalCallback) {
                                $scope.UserGroupCreateModalCallback($scope);
                            }

                        });

                    }, function(response) {
                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeUserGroupCreateModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("UserGroupDeleteModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(usergroup) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                        "<button ng-click=\"closeUsergroupDelteModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                       "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-trash\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                        "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                    "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/user_group.php?group_id=" + usergroup.group_id + "&action_type=DELETE",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = usergroup.name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 100);
                    
                }, function(response) {
                    window.swal("Oops!", response.data.errorMsg, "error")
                    .then(function() {
                        $scope.closeUsergroupDelteModal();
                    });
                });

                $(document).delegate("#user-group-delete", "click", function(e) {

                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(datatable).DataTable().ajax.reload( null, false );

                        // Sweet Alert
                        window.swal("Success", response.data.msg, "success")
                        .then(function(value) {
                            $scope.closeUsergroupDelteModal();
                            $(document).find(".close").trigger("click");
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });

                });

                $scope.closeUsergroupDelteModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
             uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("UserGroupEditModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function(API_URL, window, $, $http, $uibModal, $sce, $scope) {
    return function(usergroup) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeUsergroupEditModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-pencil\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/user_group.php?group_id=" + usergroup.group_id + "&action_type=EDIT",
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = usergroup.name;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);
                }, function(data) {
                   window.swal("Oops!", window.response.data.errorMsg, "error");
                });

                $(document).delegate(".user-group-update", "click", function(e) {
                    
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();

                    var $tag = $(this);
                    var $btn = $tag.button("loading");
                    var form = $($tag.data("form"));
                    var datatable = $tag.data("datatable");
                    form.find(".alert").remove();
                    var actionUrl = form.attr("action");
                    $http({
                        url: window.baseUrl + "/_inc/" + actionUrl,
                        method: "POST",
                        data: form.serialize(),
                        cache: false,
                        processData: false,
                        contentType: false,
                        dataType: "json"
                    }).
                    then(function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-success\">";
                        alertMsg += "<p><i class=\"fa fa-check\"></i> " + response.data.msg + ".</p>";
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);

                        // Sweet Alert
                        window.swal({
                          title: "Success!",
                          text: response.data.msg,
                          icon: "success",
                          buttons: true,
                          dangerMode: false,
                        })
                        .then(function (willDelete) {
                            if (willDelete) {
                                $scope.closeUsergroupEditModal();
                                $(document).find(".close").trigger("click");
                                window.groupId = response.data.id;
                                
                                $(datatable).DataTable().ajax.reload(function(json) {
                                    if ($("#row_"+window.groupId).length) {
                                        $("#row_"+ window.groupId).flash("yellow", 5000);
                                    }
                                }, false);

                            } else {
                                $(datatable).DataTable().ajax.reload(null, false);
                            }
                        });

                    }, function(response) {

                        $btn.button("reset");
                        var alertMsg = "<div class=\"alert alert-danger\">";
                        window.angular.forEach(response.data, function(value, key) {
                            alertMsg += "<p><i class=\"fa fa-warning\"></i> " + value + ".</p>";
                        });
                        alertMsg += "</div>";
                        form.find(".box-body").before(alertMsg);
                        $(":input[type=\"button\"]").prop("disabled", false);
                        window.swal("Oops!", response.data.errorMsg, "error");
                    });
                });

                $scope.closeUsergroupEditModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "lg",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);
window.angularApp.factory("UserInvoiceDetailsModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    var $from = window.getParameterByName("from");
    var $to = window.getParameterByName("to");
    return function($scope) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeDueCollectionModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\"><span class=\"fa fa-fw fa-list\"></span> {{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/invoice.php?action_type=INVOICEDETAILS&user_id=" + $scope.userID + "&from=" + $from + "&to=" + $to,
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Invoice List of " + $scope.username;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 500);
                    
                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                $scope.closeDueCollectionModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "lg",
            backdrop  : "static",
            keyboard: true, // ESC key close enable/disable
            resolve: {
                userForm: function () {
                    // return usergroupForm;
                }
            }
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);

window.angularApp.factory("UserInvoiceDueDetailsModal", ["API_URL", "window", "jQuery", "$http", "$uibModal", "$sce", "$rootScope", function (API_URL, window, $, $http, $uibModal, $sce, $scope) {
    var $from = window.getParameterByName("from");
    var $to = window.getParameterByName("to");
    return function($scope) {
        var uibModalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            template: "<div class=\"modal-header\">" +
                            "<button ng-click=\"closeDueCollectionModal();\" type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
                           "<h3 class=\"modal-title\" id=\"modal-title\">{{ modal_title }}</h3>" +
                        "</div>" +
                        "<div class=\"modal-body\" id=\"modal-body\">" +
                            "<div bind-html-compile=\"rawHtml\">Loading...</div>" +
                        "</div>",
            controller: function ($scope, $uibModalInstance) {
                $http({
                  url: window.baseUrl + "/_inc/invoice.php?action_type=INVOICEDUEDETAILS&user_id=" + $scope.userID + "&from=" + $from + "&to=" + $to,
                  method: "GET"
                })
                .then(function(response, status, headers, config) {
                    $scope.modal_title = "Invoice List of " + $scope.username;
                    $scope.rawHtml = $sce.trustAsHtml(response.data);

                    setTimeout(function() {
                        window.storeApp.select2();
                    }, 500);
                    
                }, function(response) {
                   window.swal("Oops!", response.data.errorMsg, "error");
                });

                $scope.closeDueCollectionModal = function () {
                    $uibModalInstance.dismiss("cancel");
                };
            },
            scope: $scope,
            size: "md",
            backdrop  : "static",
            keyboard: true,
        });

        uibModalInstance.result.catch(function () { 
            uibModalInstance.close(); 
        });
    };
}]);