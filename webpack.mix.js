const mix = require('laravel-mix');

// Main CSS
mix.styles([

    // Plugins
    'assets/custome/custome-style.css',
    'assets/bootstrap/css/bootstrap.css',
    'assets/jquery-ui/jquery-ui.min.css',
    'assets/font-awesome/css/font-awesome.css',
    'assets/morris/morris.css',
    'assets/select2/select2.min.css',
    'assets/datepicker/datepicker3.css',
    'assets/timepicker/bootstrap-timepicker.css',
    'assets/datetimepicker/bootstrap-datetimepicker.min.css',
    'assets/datetimepicker/bootstrap-datetimepicker.css',
    'assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
    'assets/perfectScroll/css/perfect-scrollbar.css',
    'assets/toastr/toastr.min.css',

    // Filemanager
    'assets/wonderpillars/css/filemanager/dialogs.css',
    'assets/wonderpillars/css/filemanager/main.css',

    // Theme
    'assets/wonderpillars/css/theme.css',
    'assets/wonderpillars/css/skins/skin-black.css',
    'assets/wonderpillars/css/skins/skin-blue.css',
    'assets/wonderpillars/css/skins/skin-green.css',
    'assets/wonderpillars/css/skins/skin-red.css',
    'assets/wonderpillars/css/skins/skin-yellow.css',

    // DataTable
    'assets/DataTables/datatables.min.css',

    // Main CSS
    'assets/wonderpillars/css/main.css',

    // Responsive CSS
    'assets/wonderpillars/css/responsive.css',
    'assets/wonderpillars/css/pos/custom.css',

    // Print CSS
    'assets/wonderpillars/css/print.css',

],'assets/wonderpillars/cssmin/main.css');



// POS CSS
mix.styles([

    'assets/bootstrap/css/bootstrap.css',
    'assets/font-awesome/css/font-awesome.css',
    'assets/datepicker/datepicker3.css',
    'assets/timepicker/bootstrap-timepicker.min.css',
    'assets/perfectScroll/css/perfect-scrollbar.css',
    'assets/select2/select2.min.css',
    'assets/toastr/toastr.min.css',
    'assets/contextMenu/dist/jquery.contextMenu.min.css',

    // Filemanager
    'assets/wonderpillars/css/filemanager/dialogs.css',
    'assets/wonderpillars/css/filemanager/main.css',

    // Theme
    'assets/wonderpillars/css/theme.css',
    'assets/wonderpillars/css/skins/skin-black.css',
    'assets/wonderpillars/css/skins/skin-blue.css',
    'assets/wonderpillars/css/skins/skin-green.css',
    'assets/wonderpillars/css/skins/skin-red.css',
    'assets/wonderpillars/css/skins/skin-yellow.css',
    'assets/wonderpillars/css/main.css',

    // Main
    'assets/wonderpillars/css/pos/skeleton.css',
    'assets/wonderpillars/css/pos/pos.css',
    'assets/wonderpillars/css/pos/custom.css',
    'assets/wonderpillars/css/pos/responsive.css',

],'assets/wonderpillars/cssmin/pos.css');



// // LOGIN CSS
// mix.styles([

//     'assets/bootstrap/css/bootstrap.css',
//     'assets/toastr/toastr.min.css',
//     'assets/wonderpillars/css/theme.css',
//     'assets/wonderpillars/css/login.css',

// ],'assets/wonderpillars/cssmin/login.css');



// Angular JS
mix.scripts([

    'assets/wonderpillars/angular/lib/angular/angular.min.js',
    'assets/wonderpillars/angular/lib/angular/angular-sanitize.js',
    'assets/wonderpillars/angular/lib/angular/angular-bind-html-compile.min.js',
    'assets/wonderpillars/angular/lib/angular/ui-bootstrap-tpls-2.5.0.min.js',
    'assets/wonderpillars/angular/lib/angular/angular-route.min.js',
    'assets/wonderpillars/angular/lib/angular-translate/dist/angular-translate.min.js',
    'assets/wonderpillars/angular/lib/ng-file-upload/dist/ng-file-upload.min.js',
    'assets/wonderpillars/angular/angularApp.js',
    
],'assets/wonderpillars/angularmin/angular.js');

// Angular Filemanager JS
mix.scripts([

    'assets/wonderpillars/angular/filemanager/js/directives/directives.js',
    'assets/wonderpillars/angular/filemanager/js/filters/filters.js',
    'assets/wonderpillars/angular/filemanager/js/providers/config.js',
    'assets/wonderpillars/angular/filemanager/js/entities/chmod.js',
    'assets/wonderpillars/angular/filemanager/js/entities/item.js',
    'assets/wonderpillars/angular/filemanager/js/services/apihandler.js',
    'assets/wonderpillars/angular/filemanager/js/services/apimiddleware.js',
    'assets/wonderpillars/angular/filemanager/js/services/filenavigator.js',
    'assets/wonderpillars/angular/filemanager/js/providers/translations.js',
    'assets/wonderpillars/angular/filemanager/js/controllers/main.js',
    'assets/wonderpillars/angular/filemanager/js/controllers/selector-controller.js',

],'assets/wonderpillars/angularmin/filemanager.js');



// Angular Modal JS
mix.scripts([

    'assets/wonderpillars/angular/modals/AddInvoiceNoteModal.js',
    'assets/wonderpillars/angular/modals/BarcodePrintModal.js',
    'assets/wonderpillars/angular/modals/BoxCreateModal.js',
    'assets/wonderpillars/angular/modals/BoxDeleteModal.js',
    'assets/wonderpillars/angular/modals/BoxEditModal.js',
    'assets/wonderpillars/angular/modals/BrandDeleteModal.js',
    'assets/wonderpillars/angular/modals/BrandEditModal.js',
    'assets/wonderpillars/angular/modals/BuyingInvoiceViewModal.js',
    'assets/wonderpillars/angular/modals/BuyingProductModal.js',
    'assets/wonderpillars/angular/modals/CategoryCreateModal.js',
    'assets/wonderpillars/angular/modals/CategoryDeleteModal.js',
    'assets/wonderpillars/angular/modals/CategoryEditModal.js',
    'assets/wonderpillars/angular/modals/CurrencyEditModal.js',
    'assets/wonderpillars/angular/modals/CustomerCreateModal.js',
    'assets/wonderpillars/angular/modals/CustomerDeleteModal.js',
    'assets/wonderpillars/angular/modals/CustomerDuepaidModal.js',
    'assets/wonderpillars/angular/modals/CustomerEditModal.js',
    'assets/wonderpillars/angular/modals/SupportDeskModal.js',
    'assets/wonderpillars/angular/modals/DiscountByCategoryDeleteModal.js',
    'assets/wonderpillars/angular/modals/DiscountByCategoryEditModal.js',
    'assets/wonderpillars/angular/modals/DiscountByCouponsDeleteModal.js',
    'assets/wonderpillars/angular/modals/DiscountByCouponsEditModal.js',
    'assets/wonderpillars/angular/modals/DueCollectionDetailsModal.js',
    'assets/wonderpillars/angular/modals/EmailModal.js',
    'assets/wonderpillars/angular/modals/ExpenseEditModal.js',
    'assets/wonderpillars/angular/modals/ExpenseViewModal.js',
    'assets/wonderpillars/angular/modals/KeyboardShortcutModal.js',
    'assets/wonderpillars/angular/modals/PaymentDeleteModal.js',
    'assets/wonderpillars/angular/modals/PaymentEditModal.js',
    'assets/wonderpillars/angular/modals/PayNowModal.js',
    'assets/wonderpillars/angular/modals/POSFilemanagerModal.js',
    'assets/wonderpillars/angular/modals/PrinterDeleteModal.js',
    'assets/wonderpillars/angular/modals/PrinterEditModal.js',
    'assets/wonderpillars/angular/modals/PrintReceiptModal.js',
    'assets/wonderpillars/angular/modals/ProductCreateModal.js',
    'assets/wonderpillars/angular/modals/ProductDeleteModal.js',
    'assets/wonderpillars/angular/modals/ProductEditModal.js',
    'assets/wonderpillars/angular/modals/ProductReturnModal.js',
    'assets/wonderpillars/angular/modals/ProductViewModal.js',
    'assets/wonderpillars/angular/modals/PromotionsDeleteModal.js',
    'assets/wonderpillars/angular/modals/PromotionsEditModal.js',
    'assets/wonderpillars/angular/modals/RefundByInvoiceEditModal.js',
    'assets/wonderpillars/angular/modals/RefundDeleteModal.js',
    'assets/wonderpillars/angular/modals/RefundEditModal.js',
    'assets/wonderpillars/angular/modals/StoreDeleteModal.js',
    'assets/wonderpillars/angular/modals/SupplierCreateModal.js',
    'assets/wonderpillars/angular/modals/SupplierDeleteModal.js',
    'assets/wonderpillars/angular/modals/SupplierEditModal.js',
    'assets/wonderpillars/angular/modals/TaxRatesDeleteModal.js',
    'assets/wonderpillars/angular/modals/TaxRatesEditModal.js',
    'assets/wonderpillars/angular/modals/UnitesEditModal.js',
    'assets/wonderpillars/angular/modals/UnitesDeleteModal.js',
    'assets/wonderpillars/angular/modals/UserCreateModal.js',
    'assets/wonderpillars/angular/modals/UserDeleteModal.js',
    'assets/wonderpillars/angular/modals/UserEditModal.js',
    'assets/wonderpillars/angular/modals/UserGroupCreateModal.js',
    'assets/wonderpillars/angular/modals/UserGroupDeleteModal.js',
    'assets/wonderpillars/angular/modals/UserGroupEditModal.js',
    'assets/wonderpillars/angular/modals/UserInvoiceDetailsModal.js',

],'assets/wonderpillars/angularmin/modal.js');



// Main JS
mix.scripts([

    'assets/jquery/jquery.min.js',
    'assets/jquery-ui/jquery-ui.min.js',
    'assets/bootstrap/js/bootstrap.min.js',
    'assets/chartjs/Chart.min.js',
    'assets/sparkline/jquery.sparkline.min.js',
    'assets/datepicker/bootstrap-datepicker.js',
    'assets/datepicker/bootstrap-datepicker.js',
    'assets/moment/moment.min.js',
    'assets/datetimepicker/bootstrap-datetimepicker.js',
    'assets/datetimepicker/bootstrap-datetimepicker.min.js',
    'assets/timepicker/bootstrap-timepicker.min.js',
    'assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
    'assets/select2/select2.min.js',
    'assets/perfectScroll/js/perfect-scrollbar.jquery.min.js',
    'assets/sweetalert/sweetalert.min.js',
    'assets/toastr/toastr.min.js',
    'assets/accounting/accounting.min.js',
    'assets/wonderpillars/js/ie.js',
    'assets/wonderpillars/js/theme.js',
    'assets/wonderpillars/js/common.js',
    'assets/wonderpillars/js/main.js',
    'assets/DataTables/datatables.min.js',
    'assets/wonderpillars/angularmin/angular.js',
    'assets/wonderpillars/angularmin/modal.js',
    'assets/wonderpillars/angularmin/filemanager.js',

],'assets/wonderpillars/jsmin/main.js');



// POS JS
mix.scripts([

    'assets/jquery/jquery.min.js',
    'assets/jquery-ui/jquery-ui.min.js',
    'assets/bootstrap/js/bootstrap.min.js',
    'assets/wonderpillars/angularmin/angular.js',
    'assets/wonderpillars/angular/angularApp.js',
    'assets/wonderpillars/angularmin/filemanager.js',
    'assets/wonderpillars/angularmin/modal.js',

    'assets/datepicker/bootstrap-datepicker.js',
    'assets/timepicker/bootstrap-timepicker.min.js',
    'assets/select2/select2.min.js',
    'assets/perfectScroll/js/perfect-scrollbar.jquery.min.js',
    'assets/sweetalert/sweetalert.min.js',
    'assets/toastr/toastr.min.js',
    'assets/accounting/accounting.min.js',
    'assets/underscore/underscore.min.js',
    'assets/contextMenu/dist/jquery.contextMenu.min.js',
    'assets/wonderpillars/js/ie.js',

    'assets/wonderpillars/js/common.js',
    'assets/wonderpillars/js/main.js',
    'assets/wonderpillars/js/pos/pos.js',

],'assets/wonderpillars/jsmin/pos.js');


// LOGIN JS
mix.scripts([

    'assets/jquery/jquery.min.js',
    'assets/bootstrap/js/bootstrap.min.js',
    'assets/toastr/toastr.min.js',
    'assets/wonderpillars/js/forgot-password.js',
    'assets/wonderpillars/js/login.js',

],'assets/wonderpillars/jsmin/login.js');



// How to build assets
// npm run dev
// npm run production