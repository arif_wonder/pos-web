<?php
//label
$_['label_promotion_type']  = [
								'disc_for_festival'   =>'Discount for festival',
								'disc_happy_hours'    =>'Discount for happy hours',
								'disc_for_anniversary'=>'Discount for anniversary'
							];
$_['discount_type']  = [
						'fixed'     =>'Fixed',
						'precentage'=>'Precentage'
					];
$_['label_title'] 		    = 'Title';
$_['label_description'] 	= 'Description';
$_['label_category'] 		= 'Category';
$_['label_from_date'] 		    = 'From Date';
$_['label_to_date'] 		    = 'To Date';
$_['label_discount'] 		    = 'Discount';
$_['label_discount_type'] 		= 'Discount Type';
$_['label_min_purchase_amount']	= 'Min Purchase Amount';

$_['text_delete_title']	= 'Delete discount by category';
$_['text_delete_coupons_title']	= 'Delete discount by coupons';
$_['text_not_found']	= 'Record not found';
$_['text_delete']	    = 'Record has been deleted successfully';

$_['title_dis_by_category'] 		= 'Discount by category';
$_['text_dis_by_category'] 		= 'Discount by category';
$_['title_dis_by_coupons'] 		= 'Discount by coupons';
$_['text_dis_by_coupons'] 		= 'Discount by coupons';
// error
$_['error_category_id'] 		= 'Please select category';
$_['error_category_id_not_exist']='Selected category id not exist in the database';

$_['error_time_from']  = 'Time from is not valid formate';
$_['error_time_to']  = 'Time to is not valid formate';
$_['text_taxrates_delete_title']	= 'Delete discount by category';
$_['text_unites_delete_title']	= 'Delete units';

