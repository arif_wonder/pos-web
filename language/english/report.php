<?php

//text
$_['text_itemwise']						= 'Item wise';

//button
$_['button_itemwise']					= 'Item wise';
$_['button_categorywise']				= 'Category wise';
$_['button_supplierwise']				= 'Supplier wise';

//label
$_['label_category_name']				= 'Category Name';
$_['label_sup_name']					= 'Supplier Name';