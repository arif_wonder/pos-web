<?php

	//text
	$_['text_keyboard_shortcut']			= 'Keyboard shortcut';
	$_['text_add_note']						= 'Add note';
	$_['text_summary']						= 'Summary';
	$_['text_search_product']				= 'Search product by name / Barcode scanner';
	$_['text_back_to_pos']					= 'Back to POS';
	$_['text_settings']						= 'Settings';

	//label
	$_['label_pay_amount']					= 'Paid amount';
	$_['label_payment_received']			= 'Payment received ';
	$_['label_print_receipt']				= 'Print';
	$_['label_email_receipt']				= 'Email';
	$_['label_total_items']					= 'Total items';
	$_['label_discount']					= 'Discount';
	$_['label_total_payable']				= 'Payable amount ';
	$_['label_qunatity']					= 'Qty.';
	$_['label_due']							= 'Due';
	$_['label_add_to_cart']					= 'Add To Cart';

	//button
	$_['button_pay']						= 'Pay now';
	$_['button_done']						= 'Done';

	//error
	$_['error_invoice_paid_amount']			= 'Paid amount is not valid';
	$_['error_invoice_exceed_paid_amount']	= 'Paid amount can\'t be greater than payable amount';
	$_['error_invoice_below_paid_amount']	= 'Paid amount can\'t be Negative value';
	$_['error_invoice_previous_due']		= 'Error previous due';
	$_['error_create_permission']      		= 'You have no permission to create Invoice!';
	$_['error_invoice_payable_amount']      = 'Payable amount is not valid';
	$_['error_item_not_found']     			= 'Item is not found';
	$_['error_quantity_exceed']     		= 'Product quantity is exceed';
	$_['error_invoice_previous_due']		= 'Error previous due';
	$_['error_create_permission']      		= 'You have no permission to create Invoice!';
	$_['error_create_due_permission']      	= 'You have no permission to create due!';
	$_['error_due_paid_permission']      	= 'You have no permission to due paid';