<?php

	//text
	$_['text_payment']					= 'Payment';
	$_['text_payments']					= 'Payments';
	$_['text_update_title']				= 'Update payment method';
	$_['text_delete_title']				= 'Delete payment method';
	$_['text_success']					= 'Payment method successfully added';
	$_['text_update_success']			= 'Payment method details successfully updated';
	$_['text_delete_success']			= 'Payment method successfully deleted ';
	$_['text_delete_instruction']		= 'What should be done with the content belongs to this payment method?';
	
	//label
	$_['label_insert_to_invoice']		= 'Insert all invoice to';
	$_['label_invoice_to']				= 'Move data to';
	$_['label_payment_name']			= 'Payment Method Name';

	//error
	$_['error_payment_method_exist']	= 'Payment method already exist';
	$_['error_payment_name']			= 'Please select a payment method';
	$_['error_payment_method_name']		= 'Payment method name is Invalid';
	$_['error_sort_order']				= 'Position is invalid';
	$_['error_delete_action']			= 'Please select a payment method';
	$_['error_payment_title_exist']		= 'Payment method title already exist';
	$_['error_payment_code_exist']		= 'Payment method code already exist';