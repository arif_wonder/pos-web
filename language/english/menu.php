<?php

// menu Text
$_['menu_home']								= 'HOME';
$_['menu_sell']          					= 'POS';
$_['menu_store']          					= 'STORE';
$_['menu_invoice']          				= 'INVOICE';
$_['menu_dashboard']          				= 'DASHBOARD';
$_['menu_shop']          					= 'MY SHOP';
$_['menu_pos']          					= 'POINT OF SALE';
$_['menu_create_expense']       			= 'ADD EXPENSE';
$_['menu_expense_list']         			= 'EXPENSE LIST';
$_['menu_expenditure']						= 'EXPENDITURE';
$_['menu_product']          				= 'PRODUCT';
$_['menu_management']          				= 'MANAGEMENT';
$_['menu_disbycategory']          		    = 'DISCOUNT BY CATEGORY';
$_['menu_disbycoupons']          		    = 'COUPONS';
$_['menu_category']          				= 'CATEGORY';
$_['menu_filemanager']          			= 'FILEMANAGER';
$_['menu_product_import']       			= 'PRODUCT IMPORT';
$_['menu_stock_alert']         			 	= 'STOCK ALERT';
$_['menu_expired']          				= 'EXPIRED';
$_['menu_supplier']          				= 'SUPPLIER';
$_['menu_box']          					= 'BOX';
$_['menu_customer']          				= 'CUSTOMER';
$_['menu_reports']          				= 'REPORTS';
$_['menu_report']          					= 'REPORT';
$_['menu_report_daily']         			= 'DAILY REPORT';
$_['menu_report_overview']         			= 'OVERVIEW REPORT';
$_['menu_sell_report']         			 	= 'SALES REPORT';
$_['menu_buy_report']          				= 'PURCHASE REPORT';
$_['menu_due_coll_report']					= 'OUTSTANDING PAYMENT REPORT';
$_['menu_advance_report']       			= 'ADVANCE PAID REPORT';
$_['menu_payment_report']       			= 'PAYMENT REPORT';
$_['menu_sell_purchase_report'] 			= 'BUYING REPORT';
$_['menu_one_page_report']     				= 'ONE PAGE REPORT';
$_['menu_smart_report']         			= 'SMART REPORT';
$_['menu_monthly_report']       			= 'MONTHLY REPORT';
$_['menu_report_stock']         			= 'STOCK REPORT';
$_['menu_analytics']          				= 'ANALYTICS';
$_['menu_currency']          				= 'CURRENCY';
$_['menu_user']          					= 'USER';
$_['menu_usergroup']          				= 'USERGROUP';
$_['menu_password']          				= 'PASSWORD';
$_['menu_about']          					= 'ABOUT US';
$_['menu_user_preference']      			= 'USER PREFERENCE';
$_['menu_system_preference']    			= 'SYSTEM SETTING';
$_['menu_system']							= 'SETTING';
$_['menu_payment_method']					= 'PAYMENT METHOD';
$_['menu_localization']						= 'LOCALIZATION';
$_['menu_backup_restore']					= 'BACKUP/RESTORE';
$_['menu_create_store']						= 'ADD STORE';
$_['menu_store_list']						= 'STORE LIST';
$_['menu_report_collection']				= 'COLLECTION REPORT';
$_['menu_due_collection']					= 'DUE COLLECTION';
$_['menu_printer']							= 'PRINTER';
$_['menu_store_change']						= 'STORE CHANGE';
$_['menu_refund_process_management']		= 'REFUNDS';
