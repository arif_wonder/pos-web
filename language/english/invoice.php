<?php

//text
$_['text_delete_success']								= 'Invoice successfully deleted';
$_['text_from']											= 'From';
$_['text_to']											= 'To';

//label
$_['label_due']											= 'Due';
$_['label_vat_reg_no']									= 'VAT Registration Number';

//button
$_['button_back_to_pos']								= 'Back to POS';

//error
$_['error_edit_invoice']								= 'Error Invoice edit';
$_['error_invoice_closed']								= 'This invoice is closed';