<?php

//text
$_['text_success']						= 'Invoice Successfully Created.';
$_['text_update_title']					= 'Invoice Details';
$_['text_update_success']				= 'Invoice successfully updated';

//label
$_['label_force_upload']				= 'Force upload';

//error
$_['error_invoice_id']					= 'Invoice ID is not valid';
$_['error_product_item']				= 'Product can not be empty';
$_['error_image_exist']					= 'Invoice document already exist';
$_['error_date']						= 'Date is not valid';
$_['error_time']						= 'Time is not valid';
$_['error_quantity']					= 'Product quantity is not valid';
$_['error_buying_price']				= 'Buy price is not valid';
$_['error_selling_price']				= 'Sale price is not valid';
$_['error_low_selling_price']			= 'Sale price must be greater than buy amount';
$_['error_invoice_exist']				= 'Invoice ID already exist';
$_['error_product_sold_out']			= 'Sold out';

//placeholder
$_['placeholder_search_product']		= 'Search Product Here...';

//hint
$_['hint_attachment']					= 'Invoice scan copy that you will get from supplier';
$_['hint_search_product']				= 'Search the product that you want to buy from this supplier';
$_['hint_invoice_id']					= 'Give the invoive number that you were recieve from suppiler';