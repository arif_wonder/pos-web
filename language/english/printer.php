<?php

$_['title_printer']							= 'Printer';
$_['text_printer_title']					= 'Printer';
$_['text_new_printer_title']				= 'Add New Printer';
$_['label_type']							= 'Type';
$_['label_char_per_line']					= 'Charracter Per Line';
$_['label_ip_address']						= 'IP Address';
$_['label_port']							= 'Port';
$_['text_printer_list_title']				= 'View All Printer';
$_['label_path']							= 'Path';
$_['error_title']							= 'Title is not valid';
$_['error_char_per_line']					= 'Charracter in per line is not valid';
$_['error_path_ip_or_port']					= 'Path or IP is not valid';
$_['text_success']							= 'Printer successfully added';
$_['text_update_success']					= 'Printer settings successfully updated';
$_['text_update_title']						= 'Update Printer Settings';
$_['text_delete_success']					= 'Printer successfully deleted';