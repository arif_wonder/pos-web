<?php

//text
$_['text_expense']						= 'Expense';
$_['text_expense_title']				= 'Expenditure';
$_['text_new_expense_title']			= 'Add New Expense';
$_['text_expense_list_title']			= 'View All Expense';
$_['text_update_title']					= 'Update';
$_['text_delete_title']					= 'Delete';
$_['text_success']						= 'Expense successfully added';
$_['text_update_success']				= 'Expense details successfully updated';
$_['text_delete_success']				= 'Expense successfully deleted';

//label
$_['label_price']						= 'Expense amount';
$_['label_details']						= 'Expense details';
$_['label_cost']						= 'Amount';

//error
$_['error_title']						= 'Invalid title!';
$_['error_price']						= 'Invalid amount!';
$_['error_invoice_exist']				= 'Invoice ID already exist';
$_['error_time']						= 'Time is not valid';

//hint
$_['hint_invoice_id']					= 'This is the voucher number'; 