<?php

//text
$_['text_delete_success']								= 'تم حذف الفاتورة بنجاح';
$_['text_from']											= 'من عند';
$_['text_to']											= 'إلى';

//label
$_['label_due']											= 'بسبب';
$_['label_vat_reg_no']									= 'رقم التسجيل الضريبي';

//button
$_['button_back_to_pos']								= 'العودة إلى POS';

//error
$_['error_edit_invoice']								= 'خطأ تحرير الفاتورة';
$_['error_invoice_closed']								= 'هذه الفاتورة مغلقة';