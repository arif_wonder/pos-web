<?php

//title
$_['title_category']						= 'الفئة';

//text
$_['text_category_list_title']				= 'قائمة الفئات';
$_['text_update_title']						= 'تحديث التفاصيل';
$_['text_category_title']					= 'الفئة';
$_['text_new_category_title']				= 'إضافة فئة جديدة';
$_['text_success']							= 'فئة تم إنشاؤها بنجاح';
$_['text_update_success']					= 'تم تحديث الفئة بنجاح';
$_['text_delete_success']					= 'تم حذف الفئة بنجاح';
$_['text_delete_title']						= 'حذف الفئة';
$_['text_delete_instruction']				= 'ما الذي يجب القيام به مع المحتوى ينتمي إلى هذه الفئة؟';

//label
$_['label_category_name']					= 'اسم التصنيف';
$_['label_category_slug']					= 'فئة سبيكة';
$_['label_parent']							= 'الأبوين';
$_['label_category_details']				= 'تفاصيل الفئة';
$_['label_specialist']						= 'متخصص';
$_['label_technologist']					= 'التكنوليجي';
$_['label_total_item']						= 'البند الكلي';
$_['label_consultant']						= 'طبيب طبيب';
$_['label_tested_by']						= 'مفحوصة من قبل';
$_['label_insert_invoice_to']				= 'الانتقال إلى';

//error
$_['error_category_name']					= 'اسم الفئة غير صالح';
$_['error_new_category_name']				= 'الرجاء تحديد الفئة';