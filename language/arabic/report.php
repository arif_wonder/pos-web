<?php

//text
$_['text_itemwise']						= 'البند الحكيم';

//button
$_['button_itemwise']					= 'البند الحكيم';
$_['button_categorywise']				= 'فئة الحكمة';
$_['button_supplierwise']				= 'المورد الحكيم';

//label
$_['label_category_name']				= 'اسم التصنيف';
$_['label_sup_name']					= 'اسم المورد';