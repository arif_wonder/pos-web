<?php

//text
$_['text_expense']						= 'مصروف';
$_['text_expense_title']				= 'المصروفات';
$_['text_new_expense_title']			= 'إضافة حساب جديد';
$_['text_expense_list_title']			= 'عرض جميع النفقات';
$_['text_update_title']					= 'تحديث';
$_['text_delete_title']					= 'حذف';
$_['text_success']						= 'تمت إضافة النفقات بنجاح';
$_['text_update_success']				= 'تم تحديث تفاصيل النفقات بنجاح';
$_['text_delete_success']				= 'تم حذف النفقات بنجاح';

//label
$_['label_price']						= 'مبلغ المصروفات';
$_['label_details']						= 'تفاصيل النفقات';
$_['label_cost']						= 'كمية';

//error
$_['error_title']						= 'عنوان حساب غير صالح!';
$_['error_price']						= 'مبلغ حساب غير صالح!';
$_['error_invoice_exist']				= 'معرف الفاتورة موجود بالفعل';
$_['error_time']						= 'الوقت غير صالح';

//hint
$_['hint_invoice_id']					= 'هذا هو رقم القسيمة';