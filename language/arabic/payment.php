<?php 

	//label
	$_['text_payment']					= 'دفع';
	$_['text_payments']					= 'المدفوعات';
	$_['text_update_title']				= 'تحديث طريقة الدفع';
	$_['text_delete_title']				= 'حذف طريقة الدفع';
	$_['text_success']					= 'تمت إضافة طريقة الدفع بنجاح';
	$_['text_update_success']			= 'تم تحديث تفاصيل طريقة الدفع بنجاح';
	$_['text_delete_instruction']		= 'ما الذي يجب فعله مع المحتوى ينتمي إلى طريقة الدفع هذه؟';
	$_['text_delete_success']			= 'تم حذف طريقة الدفع بنجاح';

	//label	
	$_['label_insert_to_invoice']		= 'أدخل كل الفاتورة إلى';
	$_['label_invoice_to']				= 'نقل البيانات إلى';
	$_['label_payment_name']			= 'اسم طريقة الدفع';

	//error
	$_['error_payment_method_exist']	= 'طريقة الدفع موجودة بالفعل';
	$_['error_payment_name']			= 'الرجاء اختيار طريقة الدفع';
	$_['error_delete_action']			= 'الرجاء اختيار طريقة الدفع';
	$_['error_payment_method_name']		= 'اسم طريقة الدفع غير صالح';
	$_['error_sort_order']				= 'الموضع غير صالح';
	$_['error_payment_title_exist']		= 'عنوان طريقة الدفع موجود بالفعل';
	$_['error_payment_code_exist']		= 'رمز طريقة الدفع موجود بالفعل';