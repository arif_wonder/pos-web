<?php

//label
$_['text_success']						= 'تم إنشاء الفاتورة بنجاح.';
$_['text_update_title']					= 'تفاصيل الفاتورة';
$_['text_update_success']				= 'تم تحديث الفاتورة بنجاح';

//label
$_['label_force_upload']				= 'قوة الرفع';

//error
$_['error_invoice_id']					= 'معرف الفاتورة غير صالح';
$_['error_product_item']				= 'لا يمكن أن يكون المنتج فارغا';
$_['error_image_exist']					= 'مستند الفاتورة موجود بالفعل';
$_['error_date']						= 'التاريخ غير صالح';
$_['error_time']						= 'الوقت غير صالح';
$_['error_quantity']					= 'كمية المنتج غير صالحة';
$_['error_buying_price']				= 'شراء السعر غير صالح';
$_['error_selling_price']				= 'سعر البيع غير صالح';
$_['error_low_selling_price']			= 'يجب أن يكون سعر البيع أكبر من مبلغ الشراء';
$_['error_invoice_exist']				= 'معرف الفاتورة موجود بالفعل';
$_['error_product_sold_out']			= 'بيعت كلها';

//placeholder
$_['placeholder_search_product']		= '...البحث عن منتج هنا';

//hint
$_['hint_attachment']					= 'الفاتورة مسح نسخة التي سوف تحصل من المورد';
$_['hint_search_product']				= 'البحث عن المنتج الذي تريد شراء من هذا المورد';
$_['hint_invoice_id']					= 'إعطاء رقم الفاتورة التي تم استلامها من المورد';