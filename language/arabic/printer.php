<?php

$_['title_printer']							= 'طابعة';
$_['text_printer_title']					= 'طابعة';
$_['text_new_printer_title']				= 'أضف طابعة جديدة';
$_['label_type']							= 'اكتب';
$_['label_char_per_line']					= 'حرف لكل خط';
$_['label_ip_address']						= 'عنوان IP';
$_['label_port']							= 'ميناء';
$_['text_printer_list_title']				= 'عرض جميع الطابعات';
$_['label_path']							= 'مسار';
$_['error_title']							= 'العنوان غير صالح';
$_['error_char_per_line']					= 'الحرف في كل سطر غير صالح';
$_['error_path_ip_or_port']					= 'المسار أو IP غير صالح';
$_['text_success']							= 'تمت إضافة الطابعة بنجاح';
$_['text_update_success']					= 'تم تحديث إعدادات الطابعة بنجاح';
$_['text_update_title']						= 'تحديث إعدادات الطابعة';
$_['text_delete_success']					= 'تم حذف الطابعة بنجاح';