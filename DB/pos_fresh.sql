CREATE DATABASE  IF NOT EXISTS `pos` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pos`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: pos
-- ------------------------------------------------------
-- Server version	5.6.38-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `box_to_store`
--

DROP TABLE IF EXISTS `box_to_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `box_to_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `box_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `box_to_store`
--

LOCK TABLES `box_to_store` WRITE;
/*!40000 ALTER TABLE `box_to_store` DISABLE KEYS */;
INSERT INTO `box_to_store` VALUES (1,1,1,1,0);
/*!40000 ALTER TABLE `box_to_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boxes`
--

DROP TABLE IF EXISTS `boxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boxes` (
  `box_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `box_name` varchar(100) NOT NULL,
  `box_details` text,
  PRIMARY KEY (`box_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boxes`
--

LOCK TABLES `boxes` WRITE;
/*!40000 ALTER TABLE `boxes` DISABLE KEYS */;
INSERT INTO `boxes` VALUES (1,'Default Box','Default Box details here...');
/*!40000 ALTER TABLE `boxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buying_info`
--

DROP TABLE IF EXISTS `buying_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buying_info` (
  `info_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `inv_type` enum('buy','expense','others','') NOT NULL DEFAULT 'buy',
  `store_id` int(10) unsigned NOT NULL DEFAULT '1',
  `total_item` int(10) unsigned NOT NULL DEFAULT '0',
  `status` enum('stock','using','','') NOT NULL DEFAULT 'stock',
  `total_sell` int(10) unsigned NOT NULL DEFAULT '0',
  `buy_date` date NOT NULL,
  `buy_time` time DEFAULT NULL,
  `sup_id` int(10) unsigned NOT NULL DEFAULT '0',
  `creator` int(10) unsigned NOT NULL DEFAULT '0',
  `invoice_note` text,
  `invoice_url` varchar(255) DEFAULT NULL,
  `is_visible` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buying_info`
--

LOCK TABLES `buying_info` WRITE;
/*!40000 ALTER TABLE `buying_info` DISABLE KEYS */;
INSERT INTO `buying_info` VALUES (1,'Supplier_Invoce_1','buy',1,1,'stock',0,'2018-12-10','12:43:00',1,1,'',NULL,1,'2018-12-10 07:13:00',NULL);
/*!40000 ALTER TABLE `buying_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buying_item`
--

DROP TABLE IF EXISTS `buying_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buying_item` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `store_id` int(10) unsigned NOT NULL DEFAULT '1',
  `item_id` int(10) unsigned NOT NULL,
  `category_id` int(10) NOT NULL DEFAULT '0',
  `item_name` varchar(100) NOT NULL,
  `item_buying_price` float NOT NULL DEFAULT '0',
  `item_selling_price` float NOT NULL DEFAULT '0',
  `item_quantity` int(10) unsigned NOT NULL,
  `total_sell` int(10) unsigned NOT NULL DEFAULT '0',
  `status` enum('stock','active','sold','') NOT NULL DEFAULT 'stock',
  `item_total` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buying_item`
--

LOCK TABLES `buying_item` WRITE;
/*!40000 ALTER TABLE `buying_item` DISABLE KEYS */;
INSERT INTO `buying_item` VALUES (1,'Supplier_Invoce_1',1,1,1,'Prod_1',100,110,1,0,'active',100);
/*!40000 ALTER TABLE `buying_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buying_price`
--

DROP TABLE IF EXISTS `buying_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buying_price` (
  `price_id` int(10) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `store_id` int(10) unsigned NOT NULL DEFAULT '1',
  `paid_amount` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`price_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buying_price`
--

LOCK TABLES `buying_price` WRITE;
/*!40000 ALTER TABLE `buying_price` DISABLE KEYS */;
INSERT INTO `buying_price` VALUES (1,'Supplier_Invoce_1',1,100);
/*!40000 ALTER TABLE `buying_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_to_store`
--

DROP TABLE IF EXISTS `category_to_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_to_store` (
  `c2s_id` int(10) NOT NULL AUTO_INCREMENT,
  `ccategory_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c2s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_to_store`
--

LOCK TABLES `category_to_store` WRITE;
/*!40000 ALTER TABLE `category_to_store` DISABLE KEYS */;
INSERT INTO `category_to_store` VALUES (1,1,1,1,0);
/*!40000 ALTER TABLE `category_to_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorys`
--

DROP TABLE IF EXISTS `categorys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorys` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(60) NOT NULL,
  `category_slug` varchar(60) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `category_details` longtext,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorys`
--

LOCK TABLES `categorys` WRITE;
/*!40000 ALTER TABLE `categorys` DISABLE KEYS */;
INSERT INTO `categorys` VALUES (1,'Default Category','default_category',0,'','2018-08-17 05:28:16','2018-10-04 14:13:46');
/*!40000 ALTER TABLE `categorys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` VALUES (1,'Pound Sterling','GBP','£','','2',0.61250001,'2018-09-19 14:40:00'),(2,'US Dollar','USD','$','','2',1.00000000,'2018-09-19 14:40:00'),(3,'Euro','EUR','','€','2',0.78460002,'2018-09-19 14:40:00'),(4,'Hong Kong Dollar','HKD','HK$','','2',7.82223988,'2018-09-19 12:00:00'),(5,'Indian Rupee','INR','₹','','2',64.40000153,'2018-09-19 12:00:00'),(6,'Russian Ruble','RUB','₽','','2',56.40359879,'2018-09-19 12:00:00'),(7,'Chinese Yuan Renminbi','CNY','¥','','2',6.34509993,'2018-09-19 12:00:00'),(8,'Australian Dollar','AUD','$','','2',1.26543999,'2018-09-19 12:00:00');
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency_to_store`
--

DROP TABLE IF EXISTS `currency_to_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency_to_store` (
  `ca2s_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ca2s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency_to_store`
--

LOCK TABLES `currency_to_store` WRITE;
/*!40000 ALTER TABLE `currency_to_store` DISABLE KEYS */;
INSERT INTO `currency_to_store` VALUES (1,1,1,1,0),(2,2,1,1,0),(3,3,1,1,0),(4,4,1,1,0),(5,5,1,1,0),(6,6,1,1,0),(7,7,1,1,0),(8,8,1,1,0);
/*!40000 ALTER TABLE `currency_to_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_to_store`
--

DROP TABLE IF EXISTS `customer_to_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_to_store` (
  `c2s_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `currency_code` varchar(100) NOT NULL DEFAULT 'USD',
  `due_amount` float NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`c2s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_to_store`
--

LOCK TABLES `customer_to_store` WRITE;
/*!40000 ALTER TABLE `customer_to_store` DISABLE KEYS */;
INSERT INTO `customer_to_store` VALUES (1,1,1,'USD',0,1,0);
/*!40000 ALTER TABLE `customer_to_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) NOT NULL,
  `customer_email` varchar(100) DEFAULT NULL,
  `customer_mobile` varchar(14) DEFAULT NULL,
  `customer_address` text,
  `customer_sex` tinyint(1) NOT NULL DEFAULT '1',
  `customer_age` int(10) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Walking Customer','walker@itsolution24.com','017000000000','Earth',1,20,'2018-03-24 14:18:37');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_to_store`
--

DROP TABLE IF EXISTS `payment_to_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_to_store` (
  `p2s_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`p2s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_to_store`
--

LOCK TABLES `payment_to_store` WRITE;
/*!40000 ALTER TABLE `payment_to_store` DISABLE KEYS */;
INSERT INTO `payment_to_store` VALUES (1,1,1,1,0);
/*!40000 ALTER TABLE `payment_to_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `details` text,
  `created_at` date DEFAULT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,'Cash on Delivery','Payment method details goes here...','2018-03-24');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `printer_to_store`
--

DROP TABLE IF EXISTS `printer_to_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `printer_to_store` (
  `p2s_id` int(10) NOT NULL AUTO_INCREMENT,
  `pprinter_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `path` varchar(255) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `port` varchar(10) DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`p2s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `printer_to_store`
--

LOCK TABLES `printer_to_store` WRITE;
/*!40000 ALTER TABLE `printer_to_store` DISABLE KEYS */;
INSERT INTO `printer_to_store` VALUES (1,1,1,'','192.234.43.3','9100',1,0);
/*!40000 ALTER TABLE `printer_to_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `printers`
--

DROP TABLE IF EXISTS `printers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `printers` (
  `printer_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(55) NOT NULL,
  `type` varchar(25) NOT NULL,
  `profile` varchar(25) NOT NULL,
  `char_per_line` tinyint(3) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`printer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `printers`
--

LOCK TABLES `printers` WRITE;
/*!40000 ALTER TABLE `printers` DISABLE KEYS */;
INSERT INTO `printers` VALUES (1,'Common Printer','network','',200,'2018-09-27 13:52:04');
/*!40000 ALTER TABLE `printers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_to_store`
--

DROP TABLE IF EXISTS `product_to_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_to_store` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `buy_price` float NOT NULL DEFAULT '0',
  `sell_price` float NOT NULL DEFAULT '0',
  `quantity_in_stock` int(10) unsigned NOT NULL DEFAULT '0',
  `sup_id` int(10) unsigned NOT NULL,
  `box_id` int(10) unsigned NOT NULL,
  `e_date` date NOT NULL,
  `p_date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_to_store`
--

LOCK TABLES `product_to_store` WRITE;
/*!40000 ALTER TABLE `product_to_store` DISABLE KEYS */;
INSERT INTO `product_to_store` VALUES (1,1,1,100,110,1,1,1,'2020-03-31','2018-12-10',1,1);
/*!40000 ALTER TABLE `product_to_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `p_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_code` varchar(50) NOT NULL,
  `p_name` varchar(100) NOT NULL,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `p_image` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`p_id`),
  UNIQUE KEY `p_code` (`p_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'94490933','Prod_1',1,'','Test');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selling_info`
--

DROP TABLE IF EXISTS `selling_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selling_info` (
  `info_id` int(10) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(100) NOT NULL,
  `edit_counter` int(10) unsigned NOT NULL DEFAULT '0',
  `inv_type` enum('sell','due_paid','') NOT NULL DEFAULT 'sell',
  `store_id` int(10) unsigned NOT NULL DEFAULT '1',
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `currency_code` varchar(3) NOT NULL,
  `payment_method` int(10) unsigned NOT NULL,
  `ref_invoice_id` varchar(100) DEFAULT NULL,
  `ref_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `invoice_note` text,
  `return_note` text,
  `due_paid_note` text,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `is_due` tinyint(1) NOT NULL DEFAULT '0',
  `is_visible` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `return_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selling_info`
--

LOCK TABLES `selling_info` WRITE;
/*!40000 ALTER TABLE `selling_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `selling_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selling_item`
--

DROP TABLE IF EXISTS `selling_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selling_item` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(100) NOT NULL,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sup_id` int(10) NOT NULL DEFAULT '0',
  `store_id` int(10) unsigned NOT NULL DEFAULT '1',
  `item_id` int(10) unsigned NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `item_price` float NOT NULL DEFAULT '0',
  `item_discount` float NOT NULL DEFAULT '0',
  `item_tax` float NOT NULL DEFAULT '0',
  `item_quantity` int(10) unsigned NOT NULL,
  `total_buying_price` float NOT NULL DEFAULT '0',
  `item_total` float NOT NULL DEFAULT '0',
  `buying_invoice_id` varchar(100) DEFAULT NULL,
  `print_counter` int(10) unsigned NOT NULL DEFAULT '0',
  `print_counter_time` timestamp NULL DEFAULT NULL,
  `printed_by` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selling_item`
--

LOCK TABLES `selling_item` WRITE;
/*!40000 ALTER TABLE `selling_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `selling_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selling_price`
--

DROP TABLE IF EXISTS `selling_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selling_price` (
  `price_id` int(10) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(100) NOT NULL,
  `store_id` int(10) unsigned NOT NULL DEFAULT '1',
  `subtotal` float DEFAULT '0',
  `discount_type` enum('plain','percentage','','') NOT NULL DEFAULT 'plain',
  `discount_amount` float DEFAULT '0',
  `tax_amount` float NOT NULL DEFAULT '0',
  `previous_due` float NOT NULL DEFAULT '0',
  `payable_amount` float DEFAULT '0',
  `paid_amount` float NOT NULL DEFAULT '0',
  `todays_due` float NOT NULL DEFAULT '0',
  `present_due` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`price_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selling_price`
--

LOCK TABLES `selling_price` WRITE;
/*!40000 ALTER TABLE `selling_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `selling_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `version` varchar(10) NOT NULL,
  `is_update_available` tinyint(1) NOT NULL DEFAULT '0',
  `update_version` varchar(100) DEFAULT NULL,
  `update_link` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'1.1',0,NULL,NULL,'2018-09-14 18:00:00','2018-09-14 14:20:58');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores` (
  `store_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `mobile` varchar(14) NOT NULL,
  `country` varchar(50) NOT NULL,
  `zip_code` varchar(50) NOT NULL,
  `currency` varchar(100) NOT NULL DEFAULT 'USD',
  `vat_reg_no` varchar(250) DEFAULT NULL,
  `cashier_id` int(10) unsigned NOT NULL,
  `address` longtext NOT NULL,
  `receipt_printer` varchar(100) DEFAULT NULL,
  `cash_drawer_codes` varchar(100) DEFAULT NULL,
  `char_per_line` tinyint(4) NOT NULL DEFAULT '42',
  `remote_printing` tinyint(1) NOT NULL DEFAULT '1',
  `printer` int(11) DEFAULT NULL,
  `order_printers` varchar(100) DEFAULT NULL,
  `auto_print` tinyint(1) NOT NULL DEFAULT '0',
  `local_printers` tinyint(1) DEFAULT NULL,
  `logo` text,
  `favicon` varchar(250) DEFAULT NULL,
  `preference` longtext,
  `sound_effect` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '0',
  `feedback_status` varchar(100) NOT NULL DEFAULT 'ready',
  `feedback_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores`
--

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` VALUES (1,'Mohan Co-operative-1','9123456789','US','1200','USD','1200',2,'Mobiocean','','x1C',42,0,2,'[\"1\",\"2\"]',1,1,'1_logo.png','1_favicon.png','a:18:{s:8:\"timezone\";s:12:\"Asia/Kolkata\";s:21:\"invoice_edit_lifespan\";i:1440;s:26:\"invoice_edit_lifespan_unit\";s:6:\"minute\";s:23:\"invoice_delete_lifespan\";i:1440;s:28:\"invoice_delete_lifespan_unit\";s:6:\"minute\";s:3:\"tax\";i:0;s:20:\"stock_alert_quantity\";i:10;s:20:\"datatable_item_limit\";i:25;s:15:\"after_sell_page\";s:3:\"pos\";s:19:\"invoice_footer_text\";s:26:\"Thank you for choosing us!\";s:10:\"email_from\";s:20:\"Mohan Co-operative-1\";s:13:\"email_address\";s:2:\"US\";s:12:\"email_driver\";s:11:\"smtp_server\";s:9:\"smtp_host\";s:15:\"smtp.google.com\";s:13:\"smtp_username\";s:0:\"\";s:13:\"smtp_password\";s:0:\"\";s:9:\"smtp_port\";i:465;s:7:\"ssl_tls\";s:3:\"ssl\";}',1,0,'ready','2018-09-29 20:25:01',1,'2018-09-24 18:00:00');
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_to_store`
--

DROP TABLE IF EXISTS `supplier_to_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_to_store` (
  `s2s_id` int(10) NOT NULL AUTO_INCREMENT,
  `sup_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `balance` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`s2s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_to_store`
--

LOCK TABLES `supplier_to_store` WRITE;
/*!40000 ALTER TABLE `supplier_to_store` DISABLE KEYS */;
INSERT INTO `supplier_to_store` VALUES (1,1,1,1,0,0);
/*!40000 ALTER TABLE `supplier_to_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `sup_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sup_name` varchar(100) NOT NULL,
  `sup_mobile` varchar(14) DEFAULT NULL,
  `sup_email` varchar(100) DEFAULT NULL,
  `sup_address` text,
  `sup_details` longtext,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`sup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'Default Supplier','01722334455','info@supplier.com','USA','','2018-10-06 09:39:05');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `slug` varchar(100) CHARACTER SET utf16 NOT NULL,
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `permission` text NOT NULL,
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `slug` (`slug`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES (1,'Admin','admin',1,1,'a:1:{s:6:\"access\";a:88:{s:16:\"read_sell_report\";s:4:\"true\";s:20:\"read_overview_report\";s:4:\"true\";s:22:\"read_collection_report\";s:4:\"true\";s:27:\"read_full_collection_report\";s:4:\"true\";s:26:\"read_due_collection_report\";s:4:\"true\";s:13:\"read_analysis\";s:4:\"true\";s:21:\"send_report_via_email\";s:4:\"true\";s:15:\"read_buy_report\";s:4:\"true\";s:19:\"read_payment_report\";s:4:\"true\";s:17:\"read_stock_report\";s:4:\"true\";s:17:\"send_report_email\";s:4:\"true\";s:14:\"create_invoice\";s:4:\"true\";s:17:\"read_invoice_list\";s:4:\"true\";s:12:\"view_invoice\";s:4:\"true\";s:14:\"update_invoice\";s:4:\"true\";s:19:\"add_item_to_invoice\";s:4:\"true\";s:24:\"remove_item_from_invoice\";s:4:\"true\";s:14:\"delete_invoice\";s:4:\"true\";s:13:\"email_invoice\";s:4:\"true\";s:10:\"create_due\";s:4:\"true\";s:14:\"due_collection\";s:4:\"true\";s:12:\"read_product\";s:4:\"true\";s:14:\"create_product\";s:4:\"true\";s:14:\"update_product\";s:4:\"true\";s:14:\"delete_product\";s:4:\"true\";s:14:\"import_product\";s:4:\"true\";s:19:\"product_bulk_action\";s:4:\"true\";s:18:\"delete_all_product\";s:4:\"true\";s:13:\"read_category\";s:4:\"true\";s:15:\"create_category\";s:4:\"true\";s:15:\"update_category\";s:4:\"true\";s:15:\"delete_category\";s:4:\"true\";s:16:\"read_stock_alert\";s:4:\"true\";s:20:\"read_expired_product\";s:4:\"true\";s:13:\"print_barcode\";s:4:\"true\";s:19:\"restore_all_product\";s:4:\"true\";s:13:\"read_supplier\";s:4:\"true\";s:15:\"create_supplier\";s:4:\"true\";s:15:\"update_supplier\";s:4:\"true\";s:15:\"delete_supplier\";s:4:\"true\";s:21:\"read_supplier_profile\";s:4:\"true\";s:8:\"read_box\";s:4:\"true\";s:10:\"create_box\";s:4:\"true\";s:10:\"update_box\";s:4:\"true\";s:10:\"delete_box\";s:4:\"true\";s:21:\"create_buying_invoice\";s:4:\"true\";s:21:\"update_buying_invoice\";s:4:\"true\";s:21:\"delete_buying_invoice\";s:4:\"true\";s:14:\"product_return\";s:4:\"true\";s:12:\"read_expense\";s:4:\"true\";s:14:\"create_expense\";s:4:\"true\";s:14:\"update_expense\";s:4:\"true\";s:14:\"delete_expense\";s:4:\"true\";s:13:\"read_customer\";s:4:\"true\";s:21:\"read_customer_profile\";s:4:\"true\";s:15:\"create_customer\";s:4:\"true\";s:15:\"update_customer\";s:4:\"true\";s:15:\"delete_customer\";s:4:\"true\";s:9:\"read_user\";s:4:\"true\";s:11:\"create_user\";s:4:\"true\";s:11:\"update_user\";s:4:\"true\";s:11:\"delete_user\";s:4:\"true\";s:15:\"change_password\";s:4:\"true\";s:14:\"read_usergroup\";s:4:\"true\";s:16:\"create_usergroup\";s:4:\"true\";s:16:\"update_usergroup\";s:4:\"true\";s:16:\"delete_usergroup\";s:4:\"true\";s:13:\"read_currency\";s:4:\"true\";s:15:\"create_currency\";s:4:\"true\";s:15:\"update_currency\";s:4:\"true\";s:15:\"change_currency\";s:4:\"true\";s:15:\"delete_currency\";s:4:\"true\";s:16:\"read_filemanager\";s:4:\"true\";s:19:\"read_payment_method\";s:4:\"true\";s:21:\"create_payment_method\";s:4:\"true\";s:21:\"update_payment_method\";s:4:\"true\";s:21:\"delete_payment_method\";s:4:\"true\";s:10:\"read_store\";s:4:\"true\";s:12:\"create_store\";s:4:\"true\";s:12:\"update_store\";s:4:\"true\";s:12:\"delete_store\";s:4:\"true\";s:14:\"activate_store\";s:4:\"true\";s:14:\"upload_favicon\";s:4:\"true\";s:11:\"upload_logo\";s:4:\"true\";s:20:\"read_user_preference\";s:4:\"true\";s:22:\"update_user_preference\";s:4:\"true\";s:9:\"filtering\";s:4:\"true\";s:22:\"read_keyboard_shortcut\";s:4:\"true\";}}'),(2,'Cashier','cashier',2,1,'a:1:{s:6:\"access\";a:46:{s:16:\"read_sell_report\";s:4:\"true\";s:22:\"read_collection_report\";s:4:\"true\";s:27:\"read_full_collection_report\";s:4:\"true\";s:26:\"read_due_collection_report\";s:4:\"true\";s:13:\"read_analysis\";s:4:\"true\";s:15:\"read_buy_report\";s:4:\"true\";s:19:\"read_payment_report\";s:4:\"true\";s:17:\"read_stock_report\";s:4:\"true\";s:17:\"send_report_email\";s:4:\"true\";s:14:\"create_invoice\";s:4:\"true\";s:17:\"read_invoice_list\";s:4:\"true\";s:12:\"view_invoice\";s:4:\"true\";s:14:\"update_invoice\";s:4:\"true\";s:19:\"add_item_to_invoice\";s:4:\"true\";s:24:\"remove_item_from_invoice\";s:4:\"true\";s:14:\"delete_invoice\";s:4:\"true\";s:13:\"email_invoice\";s:4:\"true\";s:12:\"read_product\";s:4:\"true\";s:13:\"read_supplier\";s:4:\"true\";s:15:\"create_supplier\";s:4:\"true\";s:15:\"update_supplier\";s:4:\"true\";s:15:\"delete_supplier\";s:4:\"true\";s:8:\"read_box\";s:4:\"true\";s:10:\"create_box\";s:4:\"true\";s:10:\"update_box\";s:4:\"true\";s:10:\"delete_box\";s:4:\"true\";s:21:\"create_buying_invoice\";s:4:\"true\";s:12:\"read_expense\";s:4:\"true\";s:14:\"create_expense\";s:4:\"true\";s:13:\"read_customer\";s:4:\"true\";s:21:\"read_customer_profile\";s:4:\"true\";s:15:\"create_customer\";s:4:\"true\";s:9:\"read_user\";s:4:\"true\";s:14:\"read_usergroup\";s:4:\"true\";s:16:\"create_usergroup\";s:4:\"true\";s:13:\"read_currency\";s:4:\"true\";s:15:\"create_currency\";s:4:\"true\";s:15:\"update_currency\";s:4:\"true\";s:16:\"read_filemanager\";s:4:\"true\";s:19:\"read_payment_method\";s:4:\"true\";s:21:\"create_payment_method\";s:4:\"true\";s:10:\"read_store\";s:4:\"true\";s:20:\"read_user_preference\";s:4:\"true\";s:22:\"update_user_preference\";s:4:\"true\";s:9:\"filtering\";s:4:\"true\";s:22:\"read_keyboard_shortcut\";s:4:\"true\";}}'),(3,'Salesman','salesman',3,1,'a:1:{s:6:\"access\";a:17:{s:14:\"create_invoice\";s:4:\"true\";s:17:\"read_invoice_list\";s:4:\"true\";s:12:\"view_invoice\";s:4:\"true\";s:19:\"add_item_to_invoice\";s:4:\"true\";s:24:\"remove_item_from_invoice\";s:4:\"true\";s:14:\"delete_invoice\";s:4:\"true\";s:13:\"email_invoice\";s:4:\"true\";s:10:\"create_due\";s:4:\"true\";s:14:\"due_collection\";s:4:\"true\";s:12:\"read_product\";s:4:\"true\";s:13:\"read_supplier\";s:4:\"true\";s:8:\"read_box\";s:4:\"true\";s:19:\"read_payment_method\";s:4:\"true\";s:20:\"read_user_preference\";s:4:\"true\";s:22:\"update_user_preference\";s:4:\"true\";s:9:\"filtering\";s:4:\"true\";s:22:\"read_keyboard_shortcut\";s:4:\"true\";}}');
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_to_store`
--

DROP TABLE IF EXISTS `user_to_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_to_store` (
  `u2s_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`u2s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_to_store`
--

LOCK TABLES `user_to_store` WRITE;
/*!40000 ALTER TABLE `user_to_store` DISABLE KEYS */;
INSERT INTO `user_to_store` VALUES (1,1,1,1,0),(2,2,1,1,0),(3,3,1,1,0);
/*!40000 ALTER TABLE `user_to_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `raw_password` varchar(100) DEFAULT NULL,
  `pass_reset_code` varchar(32) DEFAULT NULL,
  `reset_code_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(40) NOT NULL,
  `preference` longtext NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Your Name','vivek.bansal@wonderpillars.in','9123456789','21232f297a57a5a743894a0e4a801fc3','admin','','0000-00-00 00:00:00','::1','a:4:{s:8:\"language\";s:7:\"english\";s:10:\"base_color\";s:5:\"green\";s:14:\"pos_side_panel\";s:5:\"right\";s:11:\"pos_pattern\";s:13:\"brickwall.jpg\";}','2018-12-10 12:42:27','2018-12-10 12:42:27'),(2,2,'Cashier Name','cashier@wonderpillars.in','01711111111','21232f297a57a5a743894a0e4a801fc3','admin','','0000-00-00 00:00:00','::1','a:4:{s:8:\"language\";s:7:\"english\";s:10:\"base_color\";s:5:\"green\";s:14:\"pos_side_panel\";s:5:\"right\";s:11:\"pos_pattern\";s:31:\"abstract-attractive-backdro.jpg\";}','2018-12-10 12:42:27','2018-12-10 12:42:27'),(3,3,'Salesman','salesman@wonderpillars.in','01722222222','21232f297a57a5a743894a0e4a801fc3','admin','','0000-00-00 00:00:00','::1','a:4:{s:8:\"language\";s:7:\"english\";s:10:\"base_color\";s:5:\"green\";s:14:\"pos_side_panel\";s:5:\"right\";s:11:\"pos_pattern\";s:27:\"blank-close-up-crumpled.jpg\";}','2018-12-10 12:42:27','2018-12-10 12:42:27');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'pos'
--

--
-- Dumping routines for database 'pos'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-10 12:44:49
