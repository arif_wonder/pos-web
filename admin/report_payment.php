<?php 
ob_start();
session_start();
include ("../_init.php");

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_payment_report')) {
  redirect(root_url() . '/admin/dashboard.php');
}

// LOAD LANGUAGE FILE
$language->load('payment');

// SET DOCUMENT TITLE
$document->setTitle($language->get('title_payment_report'));

// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/ReportPaymentController.js');

// ADD BODY CLASS
$document->setBodyClass('sidebar-collapse');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php") ;
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper" ng-controller="ReportPaymentController">

  <!-- Content Header Start -->
  <section class="content-header">
    <?php include ("../_inc/template/partials/apply_filter.php"); ?>
    <h1>
      <?php echo $language->get('text_payment_report_title'); ?>
      <small>
        <?php echo store('name'); ?>   
      </small>
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="dashboard.php">
          <i class="fa fa-dashboard"></i> 
          <?php echo $language->get('text_dashboard'); ?>
        </a>
      </li>
      <li class="active">
        <?php echo $language->get('text_payment_report_title'); ?>
      </li>
    </ol>
  </section>
  <!-- Content Header End -->

  <!-- Content Start Start -->
  <section class="content">

   
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-success">
          <div class="box-header">
            <h3 class="box-title">
              <?php echo $language->get('text_payment_report_sub_title'); ?>
            </h3>
              <!--filter form start -->
              <?php include('date_filter_form.php'); ?>
              <!--filter form end --> 
          </div>
          <div class="box-body">
            <div class="table-responsive">  
              <table id="report-report-list" class="table table-bordered table-striped table-hover">
                <thead>
                  <tr class="bg-gray">
                    <th class="w-5 product-head text-center">
                      <input type="checkbox" class="check-all" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
                    </th>
                    <th class="w-10">
                      <?php echo $language->get('label_serial_no'); ?>
                    </th>
                    <th class="w-50">
                      <?php echo $language->get('label_payment_name'); ?>
                    </th>
                    <th class="w-20">
                      <?php echo $language->get('label_amount'); ?>
                    </th>
                   <!--  <th class="w-20">
                      <?php //echo $language->get('label_date'); ?>
                    </th> -->
                  </tr>
                </thead>
                <tfoot>
                  <tr class="bg-gray">
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Content End -->

</div>
<!-- Content Wrapper End -->

<?php include ("footer.php"); ?>