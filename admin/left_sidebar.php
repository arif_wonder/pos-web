<aside class="main-sidebar">
  <section class="sidebar">

    <!--  Sidebar User Panel Start-->
    <div class="user-panel">
      <div class="pull-left image">
        <svg class="svg-icon"><use href="#icon-avatar"></svg>
      </div>
      <div class="pull-left info">
        <p class="username" title="<?php echo $user->getUserName(); ?>">
          <?php echo ucfirst(limit_char($user->getUserName(), 15)); ?>
        </p>
        <a href="" onClick="return false;">
          <i class="fa fa-circle user-status-dot"></i> 
          <?php echo limit_char($user->getRole(), 14); ?> 
        </a>
      </div>
    </div>  
    <!-- Sidebar User Panel End -->

    <!-- Sidebar Menu Start -->
    <ul class="sidebar-menu">
      <li class="<?php echo current_nav() == 'admin' || current_nav() == 'dashboard' ? ' active' : null; ?>">
        <a href="dashboard.php">
          <i class="fa fa-tachometer" aria-hidden="true"><use href="#icon-dashboard"></i>
          <!-- <svg class="svg-icon"><use href="#icon-dashboard"></svg> -->
          <span>
            <?php echo $language->get('menu_dashboard'); ?>
          </span>
        </a>
      </li>

      <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'create_invoice')) : ?>
        <!-- <li class="<?php //echo current_nav() == 'pos' ? 'active' : null; ?>">
          <a href="pos.php">
            <svg class="svg-icon"><use href="#icon-create-invoice"></svg>
            <span>
              <?php //echo $language->get('menu_pos'); ?>
            </span>
          </a>
        </li> -->
      <?php endif; ?>

      <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_invoice_list')) : ?>
        <li class="<?php echo current_nav() == 'invoice' ? ' active' : null; ?>">
          <a href="invoice.php" title="<?php echo $language->get('text_invoice'); ?>">
            <i class="fa fa-hdd-o" aria-hidden="true"><use href="#icon-invoice-list"></i>
            <!-- <svg class="svg-icon"><use href="#icon-invoice-list"></svg> -->
            <span>
              <?php echo $language->get('menu_invoice'); ?>
            </span>
          </a>
        </li>
      <?php endif; ?>

      <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'due_collection')) : ?>
        <li class="<?php echo current_nav() == 'due_collection' ? 'active' : null; ?>">
          <a href="due_collection.php">
           <i class="fa fa-pencil"><use href="#icon-money"></i>
           
            <span>
              <?php echo $language->get('menu_due_collection'); ?>
            </span>
          </a>
        </li>
      <?php endif; ?>

        <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_customer')) : ?>
        <li class="<?php echo current_nav() == 'customer' || current_nav() == 'customer_profile' ? 'active' : null; ?>">
          <a href="customer.php">
            <i class="fa fa-users"><use href="#icon-group"></i>
            <!-- <svg class="svg-icon"><use href="#icon-group"></svg> -->
            <span>
              <?php echo $language->get('menu_customer'); ?>
            </span>
          </a>
        </li>
      <?php endif; ?>

            <!-- =========================== management =========================== -->
      <?php if ($user->getGroupId() == 1 
                || $user->hasPermission('access', 'read_category_by_discount')
                || $user->hasPermission('access', 'read_coupons')
                || $user->hasPermission('access', 'read_promotions')
                || $user->hasPermission('access', 'send_bulk_sms')
                || $user->hasPermission('access', 'manage_whatsapp_link')
              ) : ?>
        <li class="treeview<?php echo current_nav() == 'discount_by_category'
        || current_nav() == 'discount_by_coupons'
        || current_nav() == 'promotions'
        || current_nav() == 'bulk-sms'
         ? ' active' : null; ?>">
          <a href="#">
            <i class="fa fa-star-o"><use href="#icon-star"></i>
            <!-- <svg class="svg-icon"><use href="#icon-star"></svg> -->
            <span>
              <?php echo 'Marketing'; ?>
            </span> 
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
             <?php if ($user->getGroupId() == 1 
             || $user->hasPermission('access', 'read_category_by_discount')) : ?>

              <li class="<?php echo current_nav() == 'discount_by_category' ? ' active' : null; ?>">
                <a href="discount_by_category.php">
                  <svg class="svg-icon"><use href="#icon-star"></svg>
                  <?php echo $language->get('menu_disbycategory'); ?>
                </a>
              </li>
            <?php endif; ?>
            <?php if ($user->getGroupId() == 1 
             || $user->hasPermission('access', 'read_coupons')) : ?>

              <li class="<?php echo current_nav() == 'discount_by_coupons' ? ' active' : null; ?>">
                <a href="discount_by_coupons.php">
                  <svg class="svg-icon"><use href="#icon-star"></svg>
                  <?php echo $language->get('menu_disbycoupons'); ?>
                </a>
              </li>
             <?php endif; ?>
            <?php if ($user->getGroupId() == 1 
             || $user->hasPermission('access', 'read_promotions')) : ?>
                <!-- <li class="<?php// echo current_nav() == 'promotions' ? ' active' : null; ?>">
                  <a href="promotions.php">
                    <svg class="svg-icon"><use href="#icon-star"></svg>
                    <?php //echo 'PROMOTIONS';
                      //$language->get('menu_promotion'); 
                    ?>
                  </a>
                </li> -->
            <?php endif; ?>
            <?php if ($user->getGroupId() == 1 
             || $user->hasPermission('access', 'send_bulk_sms')) : ?>
                <li class="<?php echo current_nav() == 'bulk-sms' ? ' active' : null; ?>">
                  <a href="bulk-sms.php">
                    <svg class="svg-icon"><use href="#icon-star"></svg>
                    <?php echo 'Bulk Sms';
                     
                    ?>
                  </a>
                </li>
            <?php endif; ?>
            
<!-- href="http://ecampaignr.com/stg/ecampaignr/" -->
            <?php if ($user->getGroupId() == 1 
             || $user->hasPermission('access', 'manage_whatsapp_link')) : ?>
                  <li class="<?php echo current_nav() == 'management' ? ' active' : null; ?>">
                    <a id="ecampaignr" data-toggle="modal" data-target="#ecampaignrModal" target="_blank" >
                      <svg class="svg-icon"><use href="#icon-star"></svg>
                      <?php echo 'Whatsapp Management';
                        //$language->get('menu_promotion'); 
                      ?>
                    </a>
                  </li>
            <?php endif; ?>
            
          </ul>
        </li>
      <?php endif; ?>

         <?php if ($user->getGroupId() == 1 
                || $user->hasPermission('access', 'read_overview_report')
                || $user->hasPermission('access', 'read_sell_report')
                || $user->hasPermission('access', 'read_buy_report')
                || $user->hasPermission('access', 'read_due_collection_report')
                || $user->hasPermission('access', 'read_payment_report')
                || $user->hasPermission('access', 'read_stock_report')
                || $user->hasPermission('access', 'read_refund_report')
                || $user->hasPermission('access', 'read_damage_report')
                || $user->hasPermission('access', 'read_defect_report')
              ) : ?>

            <li class="treeview<?php echo current_nav() == 'report_overview' || current_nav() == 'report_collection' || current_nav() == 'report_sell_itemwise' || current_nav() == 'report_sell_categorywise' || current_nav() == 'report_sell_supplierwise' || current_nav() == 'report_buy_itemwise' || current_nav() == 'report_buy_categorywise' || current_nav() == 'report_buy_supplierwise' || 
              current_nav() == 'refund' ||
              current_nav() == 'damage' ||
              current_nav() == 'report_defect' ||
              current_nav() == 'report_due_collection' || 
              current_nav() == 'report_payment' || 
              current_nav() == 'report_stock'  ? ' active' : null; ?>">
        
         <?php if ($user->getGroupId() == 1 
                || $user->hasPermission('access', 'read_overview_report')
                || $user->hasPermission('access', 'read_sell_report')
                || $user->hasPermission('access', 'read_buy_report')
                || $user->hasPermission('access', 'read_due_collection_report')
                || $user->hasPermission('access', 'read_payment_report')
                || $user->hasPermission('access', 'read_stock_report')
                || $user->hasPermission('access', 'read_refund_report')
                || $user->hasPermission('access', 'read_damage_report')
                || $user->hasPermission('access', 'read_defect_report')
              ) : ?>
            <a href="report_overview.php">
              <i class="fa fa-file-text-o" aria-hidden="true"><use href="#icon-report"></i>
              <!-- <svg class="svg-icon"><use href="#icon-report"></svg> -->
              <span><?php echo $language->get('menu_reports'); ?></span>
               <i class="fa fa-angle-left pull-right"></i>
            </a>
        <?php endif; ?>

        <ul class="treeview-menu">
          
          <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_overview_report')) : ?>
            <li class="<?php echo current_nav() == 'report_overview' ? ' active' : null; ?>">
              <a href="report_overview.php">
                <svg class="svg-icon"><use href="#icon-eye"></svg>
                <?php echo $language->get('menu_report_overview'); ?>
              </a>
            </li>
          <?php endif; ?>

          <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_collection_report')) : ?>
           <!--  <li class="<?php echo current_nav() == 'report_collection' ? ' active' : null; ?>">
              <a href="report_collection.php">
                <svg class="svg-icon"><use href="#icon-money"></svg>
                <?php echo $language->get('menu_report_collection'); ?>
              </a>
            </li> -->
          <?php endif; ?>

          <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_sell_report')) : ?>
            <li class="<?php echo current_nav() == 'report_sell_itemwise' || current_nav() == 'report_sell_categorywise' || current_nav() == 'report_sell_supplierwise' ? ' active' : null; ?>">
              <a href="report_sell_itemwise.php"> 
                <svg class="svg-icon"><use href="#icon-report"></svg>
                <?php echo $language->get('menu_sell_report'); ?>
              </a>
            </li>
          <?php endif; ?>

          <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_buy_report')) : ?>
            <li class="<?php echo current_nav() == 'report_buy_itemwise' || current_nav() == 'report_buy_categorywise' || current_nav() == 'report_buy_supplierwise' ? 'active' : null; ?>">
              <a href="report_buy_itemwise.php">
                <svg class="svg-icon"><use href="#icon-money"></svg>
                <span>
                  <?php echo $language->get('menu_buy_report'); ?>
                </span>
              </a>
            </li>
          <?php endif; ?>

          <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_due_collection_report')) : ?>
            <li class="<?php echo current_nav() == 'report_due_collection' ? 'active' : null; ?>">
              <a href="report_due_collection.php">
                <svg class="svg-icon"><use href="#icon-collection"></svg>
                <span>
                  <?php echo $language->get('menu_due_coll_report'); ?>
                </span>
              </a>
            </li>
          <?php endif; ?>



          <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_payment_report')) : ?>
            <li class="<?php echo current_nav() == 'report_payment' ? 'active' : null; ?>">
              <a href="report_payment.php">
                <svg class="svg-icon"><use href="#icon-money"></svg>
                <span>
                  <?php echo $language->get('menu_payment_report'); ?>
                </span>
              </a>
            </li>
          <?php endif; ?>

          <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_stock_report')) : ?>
            <li class="<?php echo current_nav() == 'report_stock' ? 'active' : null; ?>">
              <a href="report_stock.php">
                <svg class="svg-icon"><use href="#icon-list"></svg>
                <span>
                  <?php echo $language->get('menu_report_stock'); ?>
                </span>
              </a>
            </li>
            <?php endif; ?>
            <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_refund_report')) : ?>
              <li class="<?php echo current_nav() == 'refund' ? ' active' : null; ?>">
                <a href="refund.php">
                  <svg class="svg-icon"><use href="#icon-star"></svg>
                  <?php echo 'Refund Report'; ?>
                </a>
              </li>
             <?php endif; ?>
             <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_damage_report')) : ?>
                <li class="<?php echo current_nav() == 'damage' ? ' active' : null; ?>">
                  <a href="damage.php?filter=damage">
                    <svg class="svg-icon"><use href="#icon-star"></svg>
                    <?php echo 'Damage Report'; ?>
                  </a>
                </li>
             <?php endif; ?>
            <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_defect_report')) : ?>
              <li class="<?php echo current_nav() == 'report_defect' ? ' active' : null; ?>">
                <a href="report_defect.php">
                  <svg class="svg-icon"><use href="#icon-star"></svg>
                  <?php echo 'Defect Report'; ?>
                </a>
              </li>
            <?php endif; ?>

        </ul>
      </li>
       <?php endif; ?>

        <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_analytics')) : ?>
        <li class="<?php echo current_nav() == 'analytics' ? 'active' : null; ?>">
          <a href="analytics.php">
            <i class="fa fa-line-chart" aria-hidden="true"><use href="#icon-analytics"></i>
            <!-- <svg class="svg-icon"><use href="#icon-analytics"></svg> -->
            <span>
              <?php echo $language->get('menu_analytics'); ?>
            </span>
          </a>
        </li>
      <?php endif; ?>

      <!-- ================================================ -->
      <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_supplier')) : ?>
        <li class="<?php echo current_nav() == 'supplier' || current_nav() == 'supplier_profile' ? 'active' : null; ?>">
          <a href="supplier.php">
            <i class="fa fa-archive" aria-hidden="true"><use href="#icon-supplier"></i>
<!--             <svg class="svg-icon"><use href="#icon-supplier"></svg> -->
            <span>
              <?php echo $language->get('menu_supplier'); ?>
            </span>
          </a>
        </li>
      <?php endif; ?>



      <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_expense')) : ?>
        <li class="<?php echo current_nav() == 'expense' ? ' active' : null; ?>">
          <a href="expense.php">
            <i class="fa fa-book" aria-hidden="true"><use href="#icon-expense"></i>
            <!-- <svg class="svg-icon"><use href="#icon-expense"></svg> -->
            <span>
              <?php echo 'Expenses'; ?>
            </span>
          </a>

        </li>
      <?php endif; ?>

        <?php if ($user->getGroupId() == 1 
                || $user->hasPermission('access', 'read_brand')
                || $user->hasPermission('access', 'read_category')
                || $user->hasPermission('access', 'read_product')
                || $user->hasPermission('access', 'import_product')
                || $user->hasPermission('access', 'read_tax')
                || $user->hasPermission('access', 'read_unit')

                || $user->hasPermission('access', 'read_store')
                || $user->hasPermission('access', 'read_user')
                || $user->hasPermission('access', 'create_store')
                || $user->hasPermission('access', 'read_usergroup')
                || $user->hasPermission('access', 'change_password')

              ) : ?>
     
        <li class="treeview<?php echo current_nav() == 'product' || current_nav() == 'product_details' || current_nav() == 'category' || current_nav() == 'upload_product_image' ||current_nav() == 'import_product' || current_nav() == 'stock_alert' || current_nav() == 'expired' ? ' active' : null; ?>">
          <a href="#">
            <i class="fa fa-star-o"><use href="#icon-star"></i>
            <!-- <svg class="svg-icon"><use href="#icon-star"></svg> -->
            <span>
              <?php echo 'System Management'; ?>
            </span> 
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <?php if ($user->getGroupId() == 1 
                || $user->hasPermission('access', 'read_brand')
                || $user->hasPermission('access', 'read_category')
                || $user->hasPermission('access', 'read_product')
                || $user->hasPermission('access', 'import_product')
                || $user->hasPermission('access', 'read_tax')
                || $user->hasPermission('access', 'read_unit')
              ) : ?>
          <ul class="treeview-menu">
            
              <li class="treeview<?php echo current_nav() == 'product' || current_nav() == 'product_details' || current_nav() == 'category' || current_nav() == 'upload_product_image' || current_nav() == 'import_product' || current_nav() == 'stock_alert' || current_nav() == 'expired' ? ' active' : null; ?>">
                <a href="#">
                  <svg class="svg-icon"><use href="#icon-star"></svg>
                  <span>
                    <?php echo 'inventory Management'; ?>
                  </span> 
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
          <ul class="treeview-menu">
             <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_brand')): ?>

              <li class="<?php echo current_nav() == 'brand' ? ' active' : null; ?>">
                <a href="brand.php">
                  <svg class="svg-icon"><use href="#icon-star"></svg>
                  <?php echo 'Brand Management';
                    //$language->get('menu_brand'); 
                  ?>
                </a>
              </li>
               <?php endif; ?>
               <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_category')): ?>
              <li class="<?php echo current_nav() == 'category' ? ' active' : null; ?>">
                <a href="category.php">
                  <svg class="svg-icon"><use href="#icon-category"></svg>
                   <?php echo 'Category Management'; ?>
                </a>
              </li>
            <?php endif; ?>
            <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_product')) : ?>
              <li class="<?php echo current_nav() == 'product' || current_nav() == 'product_details' || current_nav() == 'category' || current_nav() == 'import_product' || current_nav() == 'stock_alert' || current_nav() == 'expired' ? ' active' : null; ?>">
                <a href="product.php">
                  <svg class="svg-icon"><use href="#icon-star"></svg>
                  <span>
                    <?php echo 'Product Management'; ?>
                  </span> 
                 <!--  <i class="fa fa-angle-left pull-right"></i> -->
                </a>
              </li>
            <?php endif; ?>
            

             <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'import_product')): ?>
              <li class="<?php echo current_nav() == 'import_product' ? ' active' : null; ?>">
                <a href="import_product.php">
                  <svg class="svg-icon"><use href="#icon-import"></svg>
                  <?php echo 'Import'; ?>
                </a>
              </li>
            <?php endif; ?>

            <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'import_product')): ?>
              <li class="<?php echo current_nav() == 'upload_product_image' ? ' active' : null; ?>">
                <a href="upload_product_image.php">
                  <svg class="svg-icon"><use href="#icon-import"></svg>
                  <?php echo 'Import Product Image'; ?>
                </a>
              </li>
            <?php endif; ?>

            <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_tax')) : ?>
        <li class="<?php echo current_nav() == 'tax_rates' ? 'active' : null; ?>">
          <a href="tax_rates.php">
            <svg class="svg-icon"><use href="#icon-supplier"></svg>
            <span>
              <?php echo 'TAX RATES'; //$language->get('menu_tax_rates'); ?>
            </span>
          </a>
        </li>
      <?php endif; ?>

       <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_unit')) : ?>
        <li class="<?php echo current_nav() == 'unites' ? 'active' : null; ?>">
          <a href="unites.php">
            <svg class="svg-icon"><use href="#icon-supplier"></svg>
            <span>
              <?php echo 'UNITS'; //$language->get('menu_unites'); ?>
            </span>
          </a>
        </li>
      <?php endif; ?>
            
          </ul>
        </li>
            
          </ul>
      <?php endif; ?>

        <?php if ($user->getGroupId() == 1 
                || $user->hasPermission('access', 'read_store')
                || $user->hasPermission('access', 'read_user')
                || $user->hasPermission('access', 'create_store')
                || $user->hasPermission('access', 'read_usergroup')
                || $user->hasPermission('access', 'change_password')

              ) : ?>
          <ul class="treeview-menu">
            
              <li class="treeview<?php echo current_nav() == 'store_create' || current_nav() == 'store' || current_nav() == 'user' || current_nav() == 'user_group' || current_nav() == 'password' || current_nav() == 'expired' ? ' active' : null; ?>">
                  <a href="#">
                    <svg class="svg-icon"><use href="#icon-star"></svg>
                    <span>
                      <?php echo 'User Management'; ?>
                    </span> 
                    <i class="fa fa-angle-left pull-right"></i>
                  </a>
          
          <ul class="treeview-menu">
            <?php if ($user->getGroupId() == 1 
                || $user->hasPermission('access', 'read_store')
                || $user->hasPermission('access', 'create_store')

              ) : ?>
              <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_store')) : ?>
              <li class="treeview<?php echo current_nav() == 'store' || current_nav() == 'store_single' || current_nav() == 'store_create' ? ' active' : null; ?>">
                <a href="#">
                  <svg class="svg-icon"><use href="#icon-list"></svg>
                  <span>
                    <?php echo $language->get('menu_store'); ?>
                  </span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                      <ul class="treeview-menu">
                        <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'create_store')): ?>
                          <li class="<?php echo current_nav() == 'store_create' ? ' active' : null; ?>">
                            <a href="store_create.php">
                              <svg class="svg-icon"><use href="#icon-plus"></svg>
                              <?php echo $language->get('menu_create_store'); ?>
                            </a>
                          </li>
                        <?php endif; ?>
                        <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_store')): ?>
                          <li class="<?php echo current_nav() == 'store' ? ' active' : null; ?>">
                            <a href="store.php">
                              <i class="fa fa-fw fa-square"></i>
                               <?php echo $language->get('menu_store_list'); ?>
                            </a>
                          </li>
                        <?php endif; ?>
                      </ul>
              </li>
               <?php endif; ?>
            <?php endif; ?>

                        <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_user')) : ?>
                        <li class="<?php echo current_nav() == 'user' ? 'active' : null; ?>">
                          <a href="user.php">
                            <svg class="svg-icon"><use href="#icon-user"></svg>
                            <span>
                              <?php echo $language->get('menu_user'); ?>
                            </span>
                          </a>
                        </li>
                      <?php endif; ?>
                      <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_usergroup')) : ?>
                        <li class="<?php echo current_nav() == 'user_group' ? 'active' : null; ?>">
                          <a href="user_group.php">
                            <svg class="svg-icon"><use href="#icon-group"></svg>
                            <span>
                              <?php echo $language->get('menu_usergroup'); ?>
                            </span>
                          </a>
                        </li>
                      <?php endif; ?>
                      <?php if (($user->getGroupId() == 1 || $user->hasPermission('access', 'change_password'))) : ?>
                        <li class="<?php echo current_nav() == 'password' ? 'active' : null; ?>">
                          <a href="password.php">
                            <svg class="svg-icon"><use href="#icon-password"></svg>
                            <span>
                              <?php echo $language->get('menu_password'); ?>
                            </span>
                          </a>
                        </li>
                      <?php endif; ?>
             
            
              </ul>
            </li>

                    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_payment_method')) : ?>
                      <li class="<?php echo current_nav() == 'payment' ? 'active' : null; ?>">
                        <a href="payment.php">
                          <svg class="svg-icon"><use href="#icon-money"></svg>
                          <span>
                            <?php echo $language->get('menu_payment_method'); ?>
                          </span>
                        </a>
                      </li>
                    <?php endif; ?>

                      <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_box')) : ?>
                        <li class="<?php echo current_nav() == 'box' ? 'active' : null; ?>">
                          <a href="box.php">
                            <svg class="svg-icon"><use href="#icon-box"></svg>
                            <span>
                              <?php echo $language->get('menu_box'); ?>
                            </span>
                          </a>
                        </li>
                      <?php endif; ?>

                        <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_printer')) : ?>
                          <!-- <li class="<?php echo current_nav() == 'printer' ? 'active' : null; ?>">
                            <a href="printer.php">
                              <svg class="svg-icon"><use href="#icon-printer"></svg>
                              <span>
                                <?php echo $language->get('menu_printer'); ?>
                              </span>
                            </a>
                          </li> -->
                        <?php endif; ?>
            
                      </ul>
                <?php endif; ?>
                    </li>
                    <?php endif; ?>
                     <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'activate_store')) : ?>
                    <li class="<?php echo current_nav() == 'store_select' ? 'active' : null; ?>">
                      <a href="../store_select.php">
                        <i class="fa fa-archive" aria-hidden="true"><use href="#icon-list"></i>
                        <!-- <svg class="svg-icon"><use href="#icon-list"></svg> -->
                        <span>
                          <?php echo $language->get('menu_store_change'); ?>
                        </span>
                      </a>
                    </li>
                  <?php endif; ?>
      
      <li id="sidebar-bottom"></li>
    </ul>
    <!-- Sidebar Menu End -->
  </section>
</aside>
<!-- Main Sidebar End-->



  <div class="modal fade" id="ecampaignrModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content mod-hed-btn-detai">
          <div class="modal-header">
            <button type="button" id="clickokbutton" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          
          </div>
          <div class="modal-body">   
            You will have to go to quick start and click add account. Input your mobile number and click Get QR code. Then scan the phone and click on Add account at the bottom of the page.
          </div>
          <div id="closepopup" class="form-group mod-btn-de">
                    <a target="_blank"  href="" >OK</a>
                </div>
        </div>

      </div>
    </div>

<script type="text/javascript">
  $(document).ready(function(){
      $('#closepopup').click(function(){
        //http://ecampaignr.com/stg/ecampaignr/
        $('#clickokbutton').click();
        window.open('http://ecampaignr.com/stg/ecampaignr/', '_blank');
      });
  })
  function goForAction(){
    console.log('clicking');
  }
</script>
    <style type="text/css">
      .mod-hed-btn-detai {
    /* width: 100%; */
    /* float: left; */
    text-align: center;
}

.mod-hed-btn-detai .modal-body {
    margin: 20px;
    font-size: 17px;
    text-align: center;
}

.mod-btn-cen {
    margin: 10px;
    text-align: center;
    border: 1px solid;
    background: #002d58;
    padding: 15px 30px;
    color: #fff;
}

.mod-btn-de {
        /* display: inline-block; */
    /* margin: 0 auto; */
    /* text-align: center; */
    background: #002d58;
    display: inline-block;
    margin: 0 auto;
    float: none;
    padding: 10px 40px;
    margin: 10px;
}
}

.mod-btn-cen a {
    color: #fff;
    font-size: 17px;
}

.mod-btn-de a {
    color: #fff;
}
    </style>