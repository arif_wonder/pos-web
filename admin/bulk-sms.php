<?php 
ob_start();
session_start();
include '../_init.php';
// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}
// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_product')) {
	redirect(root_url() . '/admin/dashboard.php');
}
// LOAD LANGUAGE FILE
$language->load('management');
// SET DOCUMENT TITLE
$document->setTitle('Bulk Sms');
// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/BulkSmsController.js');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php"); 
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper" ng-controller="BulkSmsController">
  	<!-- Content Header Start -->
	<section class="content-header">
		<h1>
			<?php echo 'Bulk Sms'; ?>
			<small>
				<?php echo store('name'); ?>	
			</small>
		</h1>
		<ol class="breadcrumb">
			<li>
				<a href="dashboard.php">
					<i class="fa fa-dashboard"></i> 
					<?php echo $language->get('text_dashboard'); ?>
				</a>
			</li>
			<li class="active">
				<?php echo 'Bulk Sms'; ?>	
			</li>
		</ol>
	</section>
  	<!-- Content Header End -->
	<!-- Content Start -->
	<section class="content">
		
	    
    	<?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'create_product')) : ?>
	    <div class="box box-info<?php echo create_box_state(); ?>">
	        

	        <?php if (isset($error_message)): ?>
	        	<div class="alert alert-danger">
					<p>
						<span class="fa fa-warning"></span> 
						<?php echo $error_message; ?>
					</p>
	        	</div>
	        <?php elseif (isset($success_message)): ?>
	          <div class="alert alert-success">
				<p>
					<span class="fa fa-check"></span> 
					<?php echo $success_message; ?>
				</p>
	          </div>
	        <?php endif; ?>

	        <!-- Include Product Form -->
	        <?php include('../_inc/template/send_bulk_sms_form.php'); ?>

	    </div>
	    <?php endif; ?>

	   

	</section>
  	<!-- Content end -->

</div>
<!--  Content Wrapper End -->

<?php include ("footer.php"); ?>