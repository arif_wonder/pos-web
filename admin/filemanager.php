<?php 
ob_start();
session_start();
include ("../_init.php");

// LOAD LANGUAGE FILE
$language->load('filemanager');

// FILEMANAGER MODAL WINDOW FOR AJAX CALLING
if(isset($request->get['ajax'])) 
{

  // check, if user logged in or not
  // if user is not logged in then return error
  if (!$user->isLogged()) {
    header('HTTP/1.1 422 Unprocessable Entity');
    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(array('errorMsg' => $language->get('error_login')));
    exit();
  }

  // check, if user has reading permission or not
  // if user have not reading permission return error
  // y

	include('../_inc/template/partials/filemanager_ajax.php');
	exit();
}

if (DEMO) {
  redirect(root_url() . '/admin/dashboard.php');
}

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}  

// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_filemanager')) {
  redirect(root_url() . '/admin/dashboard.php');
}

// SET DOCUMENT TITLE
$document->setTitle($language->get('title_filemanager'));

// ADD BODY CLASS
$document->setBodyClass('sidebar-collapse');

// INCLUDE HEADER AND FOOTER
include ("header.php");
include ("left_sidebar.php");
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper">

  <!-- Content Start -->
  <section class="content">

   
    
  	<div class="filemanger-width">
  		<?php
        include('../_inc/template/partials/filemanager.php');
      ?>
  	</div>
  </section>
  <!-- Content End -->
</div>
<!-- Content Wrapper End -->
    
<?php include ("footer.php"); ?>