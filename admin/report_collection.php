<?php
ob_start();
session_start();
include ("../_init.php");

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_collection_report')) {
  redirect(root_url() . '/admin/dashboard.php');
}

// LOAD LANGUAGE FILE
$language->load('report_collection');

// SET DOCUMENT TITLE
$document->setTitle($language->get('title_collection_report'));
$document->setBodyClass('sidebar-collapse');

// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/ReportCollectionController.js');
$document->addScript('../assets/wonderpillars/angular/modals/UserInvoiceDetailsModal.js');
$document->addScript('../assets/wonderpillars/angular/modals/DueCollectionDetailsModal.js');

// ADD BODY CLASS
$document->setBodyClass('sidebar-collapse');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php");
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper">

  <!--  Content Header Start -->
  <section class="content-header">
    <?php include ("../_inc/template/partials/apply_filter.php"); ?>
    <h1>
      <?php echo $language->get('text_collection_report_title'); ?>
      <small>
        <?php echo store('name'); ?>
      </small>
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="dashboard.php">
          <i class="fa fa-dashboard"></i> 
          <?php echo $language->get('text_dashboard'); ?>
        </a>
      </li>
      <li class="active">
        <?php echo $language->get('text_collection_report_title'); ?>
      </li>
    </ol>
  </section>
  <!-- Content Header End -->

  <!-- Content Start -->
  <section class="content">

   
    
    <div class="row">
      <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_collection_report')) : ?>
          <!-- Collection Report Start -->
          <div id="collection-report" class="col-md-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">
                  <?php echo $language->get('text_collection_report'); ?>
                </h3>
              </div>
              <!--filter form start -->
              <?php 
                //include('date_filter_form.php'); 
              ?>
               <!--filter form end -->    

              <div class="dashboard-widget box-body">
                <?php include('../_inc/template/partials/report_collection.php'); ?>
              </div>
            </div>
          </div>
          <!--Collection Report End -->
          <?php endif; ?>

      </div>
  </section>
  <!-- Content End -->

</div>
<!-- Content Wrapper End -->

<?php include ("footer.php"); ?>