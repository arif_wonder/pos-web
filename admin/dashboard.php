<?php 
ob_start();
session_start();
include '../_init.php';
include '../_inc/helper/countries.php';

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// LOAD LANGUAGE FILEs
$language->load('dashboard');
$language->load('menu');

// SET DOCUMENT TITLE
$document->setTitle($language->get('title_dashboard'));

// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/DashboardController.js');
$document->addScript('../assets/wonderpillars/angular/controllers/ReportCollectionController.js');

// ADD BODY CLASS
$document->setBodyClass('dashboard'); 

// INCLUDE HEADER AND FOOTER
include ("header.php");
include ("left_sidebar.php");
?>
<!-- Content Wrapper Start -->
<div class="content-wrapper" ng-controller="DashboardController">

    <!-- Content Header Start -->
  <section class="content-header">
    <?php include ("../_inc/template/partials/apply_filter.php"); ?>
    <h1>
      <?php echo $language->get('text_dashboard'); ?>
      <small>
        <?php echo store('name'); ?>
      </small>
    </h1>
  </section>
  <!-- ContentH eader End -->

  <!-- Content Start -->
  <section class="content">
    <?php
      if(isset($_SESSION) && isset($_SESSION['success_message_password_changed']) && $_SESSION['success_message_password_changed'] !=''){?>
    <div class="alert alert-success success-massage" >
        <p class="success-massage">
          <span class="fa fa-check id="success-massage"></span>
            password changed successfully.
        </p>
    </div>
    <?php 
      unset($_SESSION['success_message_password_changed']);
    } ?>
    <!-- Small Boxes Start -->
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <div id="invoice-count" class="small-box bg-green">
          <div class="inner">
            <h3>
              <?php echo total_invoice(from(), to()); ?>
            </h3>
            <p>
              <?php echo $language->get('text_total_invoice'); ?>
              <?php if (!from()) : ?>
                <?php echo '(Today)';?>
              <?php endif; ?>
            </p>
          </div>
          <div class="icon">
            <i class="fa fa-pencil"></i>
          </div>
          <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_customer')) : ?>
            <a href="invoice.php" class="small-box-footer">
              <?php echo $language->get('text_details'); ?> 
              <i class="fa fa-arrow-circle-right"></i>
            </a>
          <?php else:?>
            <a href="#" class="small-box-footer">
              &nbsp;
            </a>
          <?php endif;?>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div id="customer-count" class="small-box bg-red">
          <div class="inner">
            <h3>
              <?php echo total_customer(from(), to()); ?>
            </h3>
            <p>
              <?php echo $language->get('text_total_customer'); ?>
            </p>
          </div>
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
          <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_customer')) : ?>
            <a href="customer.php" class="small-box-footer">
              <?php echo $language->get('text_details'); ?> 
              <i class="fa fa-arrow-circle-right"></i>
            </a>
          <?php else:?>
            <a href="#" class="small-box-footer">
              &nbsp;
            </a>
          <?php endif;?>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div id="supplier-count" class="small-box bg-purple">
          <div class="inner">
            <h3>
              <?php echo total_supplier(); ?>
            </h3>
            <p>
              <?php echo $language->get('text_total_supplier'); ?>
            </p>
          </div>
          <div class="icon">
            <i class="fa fa-fw fa-shopping-cart"></i>
          </div>
          <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_supplier')) : ?>
            <a href="supplier.php" class="small-box-footer">
              <?php echo $language->get('text_details'); ?> 
              <i class="fa fa-arrow-circle-right"></i>
            </a>
          <?php else:?>
            <a href="#" class="small-box-footer">
              &nbsp;
            </a>
          <?php endif;?>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div id="product-count" class="small-box bg-yellow">
          <div class="inner">
            <h3>
              <?php echo total_product(); ?>
            </h3>
            <p>
              <?php echo $language->get('text_total_product'); ?>
            </p>
          </div>
          <div class="icon">
            <i class="fa fa-star"></i>
          </div>
          <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_product')) : ?>
            <a href="product.php" class="small-box-footer">
              <?php echo $language->get('text_details'); ?> 
              <i class="fa fa-arrow-circle-right"></i>
            </a>
          <?php else:?>
            <a href="#" class="small-box-footer">
              &nbsp;
            </a>
          <?php endif;?>
        </div>
      </div>
    </div>
    <!--Small Box End -->

    <?php include '../_inc/template/partials/action_buttons.php'; ?><hr>

    <!-- Collection Report Start -->
    <div class="row collection-report-container">
      <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_collection_report')) : ?>
          
          <!-- Collection Report Start -->
          <div id="collection-report" class="col-md-12">
            <div class="box box-info mb-0">
              <div class="box-header with-border">
                <h3 class="box-title">
                  <?php echo $language->get('text_collection_report'); ?>
                </h3>
              </div>
              <div class="dashboard-widget box-body">
                <?php include('../_inc/template/partials/report_collection.php'); ?>
              </div>
            </div>
          </div>

        <?php endif; ?>  
    </div>
    <!-- Collection Report End -->

    <hr>

    <div class="row">

      <div class="col-md-6">
        <div class="box box-info tour-item" id="profit-calculation">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php echo $language->get('text_report_title'); ?> 
              <small>of</small> 
              <?php echo date("F j, Y", strtotime(date('Y-m-d'))); ?>
            </h3>
          </div>
          <div class="box-body">
            <?php include '../_inc/template/partials/profit_calc.php'; ?>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="box box-info tour-item" id="recent_customer">
          <div class="box-header with-border">
            <h3 class="box-title">
              <?php echo $language->get('text_recent_customer_box_title'); ?>
            </h3>
          </div>
          <div class="box-body w-recent-customer">
            <?php if (count(recent_customers(1)) > 0) : ?>
              <ul class="unstyled list-group recent-cusomers">
                <?php foreach (recent_customers(5) as $customer) :
                  if ($customer['customer_mobile'] && !empty($customer['customer_mobile'])) {
                    $customer_contact = '('.$customer['customer_mobile'].')';
                  } else if ($customer['customer_email'] && !empty($customer['customer_email'])) {
                    $customer_contact = '('.$customer['customer_email'].')';
                  } ?>
                  <li class="list-group-item bg-blue">
                    <a style="color:#fff;" href="customer_profile.php?customer_id=<?php echo $customer['customer_id'];?>">
                      <i class="fa fa-fw fa-user"></i>
                      <?php echo limit_char($customer['customer_name'], 30); ?> 
                      <?php echo $customer_contact; ?>
                    </a>
                  </li>
                <?php endforeach; ?>
              </ul>
            <?php else: ?>
              <p class="not-found">
                <?php echo sprintf($language->get('text_not_found'), $language->get('text_customer')); ?>
              </p>
            <?php endif; ?>
          </div>
          <div class="box-footer text-center">
            <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_customer')) : ?>
              <a href="customer.php">
                <?php echo $language->get('text_details'); ?> 
                <span class="fa fa-arrow-circle-right"></span>
              </a>
            <?php else:?>
                &nbsp;
            <?php endif;?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 tour-item" id="buy_sell_comparison">
        <?php include '../_inc/template/partials/sell_analytics.php'; ?>
      </div>
    </div>
  </section>
  <!-- Content End -->
</div>


<!-- change password -->
<!-- <div class="row">
    <div id="passChangeModalOpenAuto" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" data-toggle="modal" data-target="#passChangeModalOpen">
      
    </div>

    <div class="modal fade" id="passChangeModalOpen" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> -->
            <!-- <h4 class="modal-title">
              <span class="fa fa-fw fa-lock"></span> 
              <?php echo $language->get('text_password_title'); ?>
            </h4>
          </div>
          <div class="modal-body">   
            <form id="change_password" class="form-horizontal" action="" method="post" enctype="multipart/formdata">
              <input type="hidden" name="action_type" value="changepassword">
              <div class="box-body">
                
                <div class="form-group">
                  <label for="new1" class="col-sm-4 control-label">
                    <?php echo $language->get('label_password_new'); ?>
                  </label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="new1" name="new1" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="new2" class="col-sm-4 control-label">
                    <?php echo $language->get('label_password_confirm'); ?>
                  </label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="new2" name="new2" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">&nbsp;</label>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-block btn-info pull-right" name="form_change_password">
                      <span class="fa fa-fw fa-pencil"></span> 
                      <?php echo $language->get('button_update'); ?>
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
 --> 


<!-- Content Wrapper End -->
<script type="text/javascript">
  // $(document).ready(function(){
  //   var regex  = /^\d+(\.\d{1,2})?$/;
  //   var numStr = "12.00";
  //   if (regex.test(numStr)){
      
  //   }
        
  // })
</script>
    
<?php include ("footer.php"); ?>