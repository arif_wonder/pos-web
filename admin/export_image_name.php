<?php
session_start();
if(isset($_SESSION['import_pro_image_name'])){

	$filename = "image_name.csv";
	$fp = fopen('php://output', 'w');

	$columns = ['name'];
	foreach ($columns as $column) {
		$header[] = $column;
	}	

	header('Content-type: application/csv');
	header('Content-Disposition: attachment; filename='.$filename);
	fputcsv($fp, $header);

	$arrayfilenames = $_SESSION['import_pro_image_name'];
	
	foreach($arrayfilenames as $key => $arrayfilename) {
		fputcsv($fp, [$arrayfilename]);
	}
	unset($_SESSION['import_pro_image_name']);
	exit;
}
?>