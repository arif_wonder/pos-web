<?php 
ob_start();
session_start();
include ("../_init.php");
// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}
// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'import_user')) {
	redirect(root_url() . '/admin/dashboard.php');
}
// LOAD LANGUAGE FILE
$language->load('import');
$language->load('user');
$message = $language->get('text_product_import_alert');
// SET DOCUMENT TITLE
$document->setTitle($language->get('title_import_user'));
// INCLUDE EXEL READER
require('../_inc/vendor/spreadsheet-reader/php-excel-reader/excel_reader2.php');
require('../_inc/vendor/spreadsheet-reader/SpreadsheetReader.php');
// INCLUDE HEADER AND FOOTER
include("header.php");
include ("left_sidebar.php");
$user_model = $registry->get('loader')->model('user');

if (isset($request->post['submit'])) {
	$allowed =  array('xlsx','xlsm' ,'xlsb','xltx','xltm' ,'xls','xlt','xls' ,'xla','xlw','xlr','ods','ots');
	$ext = pathinfo($_FILES['filename']['name'], PATHINFO_EXTENSION);
	
	if (in_array($ext,$allowed)) {

		$dir = '../storage/';
		if(!is_dir($dir)) {
            mkdir($dir, 0777, true);            //create directory with specified path
            chmod($dir, 0777);
        }

		$target_file= $dir.basename($_FILES['filename']['name']);
		move_uploaded_file($_FILES['filename']['tmp_name'], $target_file);

		try {

			$Reader = new SpreadsheetReader($target_file);
			$Sheets = $Reader->Sheets();

			$insert_status = array();
			$update_status = array();
			$total_item_no = 0;

			foreach ($Sheets as $Index => $Name) {
				$Reader->ChangeSheet($Index);
				$skipData = array();
				$err         = '';
				$err_message = '';

				foreach ($Reader as $column) {
					if (strtolower(trim($column[0])) == 'username' || !$column[0] ) continue;

						$pro_data['username'] = $column[0];
						$pro_data['email']    = $column[1];
						$pro_data['mobile']   = $column[2];
						$pro_data['password'] = $column[3];
						$pro_data['group_id'] = $column[4];
						$pro_data['user_store'] = $column[5];
						$pro_data['status']   = $column[6];
						$pro_data['device']   = $column[7];
						$pro_data['device_id']= $column[8];

						$pro_data['token']    = $_POST['token'];
						$err = validate_request_data($pro_data);
						if ($err=='') {
							$user_id = $user_model->addUser($pro_data);
							$total_item_no++;
						}//// end of the if conditions
				} //// end of the foreach ($Reader as $column) loop
			} //// end of the foreach ($Sheets as $Index => $Name) loop

			$success = 0;
			$error = 0;
			$message = '';
			$message .= '<h4>Total Item: ' . $total_item_no . '</h4>';
			$message .= '<p><strong>Insert Status</strong></p>';
			$message .= '<ul>';
			$message .= '<li>Total Inserted: ' . $total_item_no . '</li>';
			$message .= '<li>Error in: ' . $err_message . '</li>';
			$message .= '</ul>';

			unlink($target_file);
		}
	    catch(Exception $e) {
	    	unlink($target_file);
			$error_message = $e->getMessage();
		}
	}
	//header('Content-Type: application/json');
	//echo json_encode(array('msg' => 'Product imported successfully'));
	// $request->post = [];
	// if ($err=='') {
	// 	$_SESSION['success_message_import'] = 'User imported successfully';
	// 	$_SESSION['total_inserted'] = $total_item_no;
	// 	$success_message = 'User imported successfully';
	// 	header("Location: import_user.php");
	// 	exit;
	// }
}
// ==================
// validate post data
function validate_request_data($data, $language) 
{
  // validate username
  if (!validateString($data['username'])) {
    throw new Exception($language->get('error_user_name'));
  }

  // validate customer email & mobile
  if (!validateEmail($data['email']) && empty($data['mobile'])) {
    throw new Exception($language->get('error_user_email_or_mobile'));
  }

  // validate customer mobile
    if (!empty($data['mobile']) && strlen($data['mobile']) <10){
      throw new Exception($language->get('You must enter minimum ten number in Mobile field'));
    }

    // validate customer mobile
    if (!empty($data['mobile']) && strlen($data['mobile']) > 10){
      throw new Exception($language->get("You can't enter greater than ten number in Mobile field"));
    }

  // validate user group id
  if(!validateInteger($data['group_id'])) {
    throw new Exception($language->get('error_user_group'));
  } 

  if (!isset($data['user_store']) || empty($data['user_store'])) {
    throw new Exception($language->get('error_store'));
  }

  // validate status
  if (!is_numeric($data['status'])) {
    throw new Exception($language->get('error_status'));
  }

  // validate sort order
  if (!is_numeric($data['sort_order'])) {
    throw new Exception($language->get('error_sort_order'));
  }

  // validate device id
  if (empty($data['device_id'])) {
    throw new Exception('Please enter device id');
  }

  // validate device 
  if (empty($data['device'])) {
    throw new Exception('Please select device');
  }
}
?>
<!-- Content Wrapper Start -->
<div class="content-wrapper">
	<section class="content-header">
		<h1>
		  <?php echo sprintf($language->get('text_import_title'), $language->get('text_user')); ?>
			<small>
			  	<?php echo store('name'); ?>
			</small>
		</h1>
		<ol class="breadcrumb">
			<li>
			  	<a href="dashboard.php">
			  		<i class="fa fa-dashboard"></i>
			  		<?php echo $language->get('text_dashboard'); ?>
			  	</a>
			</li>
			<li class="active">
			  	<?php echo sprintf($language->get('text_import_title'), $language->get('text_user')); ?>
			</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-no-border">
					<div class="alert alert-info">
						<span class="fa fa-fw fa-info-circle"></span> <?php echo $message ; ?>
					</div>
					<?php if (isset($error_message)): ?>
					<div class="alert alert-danger">
					    <p>
					    	<span class="fa fa-warning"></span> 
					    	<?php echo $error_message ; ?>
					    </p>
					</div>
					<?php elseif (isset($success_message)): ?>
					<div class="alert alert-success">
					    <p>
					    	<span class="fa fa-check"></span> 
					    	<?php echo $success_message ; ?>
					    </p>
					</div>
					<?php endif; ?>

					<?php
						if(isset($_SESSION) && isset($_SESSION['success_message_import']) && $_SESSION['success_message_import'] !=''){?>
							<div class="alert alert-success">
					    <p>
					    	<span class="fa fa-check"></span> 
					    	<?php echo $_SESSION['success_message_import'] ;
					    		unset($_SESSION['success_message_import']);
					    	 ?>
					    </p>
					</div>
					
						<?php }
					?>

					<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
						<div class="box-body">

							<div class="form-group">
								<div class="col-sm-3">&nbsp;</div>
								<div class="col-sm-9">
							    	<?php echo $language->get('text_download'); ?>
								    <a href="../storage/samplefiles/user.xls" id="download_demo">
								    	<span class="fa fa-fw fa-download"></span> 
								    	<?php echo $language->get('button_download'); ?>
								    </a>
							 	</div>
							</div>

						  	<div class="form-group">
						    	<label for="filename" class="col-sm-3 control-label">
						    		<?php echo $language->get('text_select_xls_file'); ?>
						    	</label>
						        <div class="col-sm-5">	   
									<input type="hidden" id="sort_order" value="0" name="sort_order" required>
									<input type="hidden" id="admin_token"  name="token">         
									<input type="file" class="form-control" name="filename" id="filename" accept=".xls" required>
						        </div>
						 	</div>
						    <div class="form-group">
						        <div class="col-sm-5 col-sm-offset-3">
							        <button type="submit" class="btn btn-success" name="submit">
							        	<span class="fa fa-fw fa-upload"></span> 
							          	<?php echo $language->get('button_import'); ?>
							        </button>
						        </div>
						    </div>
						</div>
				  	</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    var login_data = {
          username: localStorage.getItem('email'),
          password: localStorage.getItem('password'),
      }
    var admin_url = '<?php echo ADMIN_API ?>';

    $.ajax({
      url: admin_url+"/login",
      method: "POST",
      data: login_data,
      dataType: "json",
      success: function (response) {
        $('#admin_token').val(response.data.token);
        localStorage.setItem('token', response.data.token);
      },
      error: function (response) {
      }
    });
  });
</script>
<?php include ("footer.php"); ?>