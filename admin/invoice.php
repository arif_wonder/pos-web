<?php
ob_start();
session_start();
include ("../_init.php");

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_invoice_list')) {
  redirect(root_url() . '/admin/dashboard.php');
}

// LOAD LANGUAGE FILE
$language->load('invoice');

// SET DOCUMENT TITLE
$document->setTitle($language->get('title_invoice'));

// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/InvoiceController.js');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php");
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper" ng-controller="InvoiceController">

	<!-- Content Header Start -->
	<section class="content-header">
		<?php include ("../_inc/template/partials/apply_filter.php"); ?>
		<h1>
		    <?php echo $language->get('text_invoice_title'); ?>
		    <small>
		    	<?php echo store('name'); ?>
		    </small>
		</h1>
	  	<ol class="breadcrumb">
		    <li>
		    	<a href="dashboard.php">
		    		<i class="fa fa-dashboard"></i> 
		    		<?php echo $language->get('text_dashboard'); ?>
		    	</a>
		    </li>
		    <li class="active">
		    	<?php echo $language->get('text_invoice_title'); ?>
		    </li>
	 	 </ol>
	</section>
	<!-- Content Header End -->

	<!-- Content Start -->
	<section class="content">

		
	    
		<div class="row">
		    <div class="col-xs-12">
		      	<div class="box box-info">
		      		<div class="box-header">
				        <h3 class="box-title">
				        	<?php 
				        		echo $language->get('text_invoice_sub_title');
				        		//echo $user->hasPermission('access', 'view_invoice');
				        	?>
				        </h3>
				     </div>
			      	<div class='box-body'>  
						<div class="table-responsive inv-tab-list"> 
						<?php
				            $hide_colums = "";
				            if ($user->getGroupId() != 1) {
				              if (! $user->hasPermission('access', 'view_invoice')) {
				                $hide_colums .= "13,";
				                
				              }
				              // if (! $user->hasPermission('access', 'update_invoice')) {
				              //   $hide_colums .= "10,";
				              // }
				              // if (! $user->hasPermission('access', 'delete_invoice')) {
				              //   $hide_colums .= "11,";
				              // }
				            }
				          ?>  

				          <!-- Invoice List Start -->
						  <table id="invoice-invoice-list"  class="table table-bordered table-striped table-hover" data-hide-colums="<?php echo $hide_colums; ?>">
						    <thead>
						      	<tr class="bg-gray">
						      		<th class="w-5 product-head">
				                      <input type="checkbox" class="check-all" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
				                    </th>
							        <th class="w-15">
							        	<?php echo $language->get('label_invoice_id'); ?>
							        </th>
							        <th class="w-20">
							        	<?php echo $language->get('label_datetime'); ?>
							        </th>
							        <th class="w-15">
							        	<?php echo $language->get('label_customer_name'); ?>
							        </th>
							        <th class="w-10">
							        	<?php echo $language->get('label_customer_mobile'); ?>
							        </th>
							        <th class="w-10">
							        	<?php echo 'Sales person'; ?>
							        </th>
							        <th class="w-5">
							        	<?php echo $language->get('label_invoice_amount'); ?> 
							        </th>
							        <th class="w-5">
							        	<?php echo $language->get('label_previous_due'); ?> 
							        </th>
							        <th class="w-5">
							        	<?php echo $language->get('label_invoice_paid'); ?>
							        </th>
							        <th class="w-5">
							        	<?php echo $language->get('label_invoice_due'); ?>
							        </th>
							        <th class="w-5">
							        	<?php echo 'Paid By Credit'; ?>
							        </th>
							        <th class="w-5">
							        	<?php echo 'Payment Mode'; ?>
							        </th>
							        <th class="w-5">
							        	<?php echo $language->get('label_status'); ?>
							        </th>
							        <th class="w-5">
							        	<?php echo $language->get('label_view'); ?>
							        </th>
							        <th class="w-5">
							        	<?php echo 'Return'; ?>
							        </th>
							        
						      	</tr>
						    </thead>


						  </table>
						  <!-- Invoice List End -->
						</div>  
			  		</div>
		      	</div>
		    </div>
	    </div>
	</section>
	<!-- Content End -->
</div>
<!-- Content Wrapper End -->

<?php include ("footer.php"); ?>
