<?php 
ob_start();
session_start();
include ("../_init.php");

// LOAD LANGUAGE FILE
$language->load('invoice');

// VALIDATE INVOICE ID
if (!isset($request->get['invoice_id'])) { 
  redirect('/pos');
}

// INVOICE MODEL
$invoice_model = $registry->get('loader')->model('invoice');
$invoice_id = $request->get['invoice_id'];
$invoice_info = $invoice_model->getInvoiceInfo($invoice_id);

if (!$invoice_info) {
  redirect('/pos');
}
//echo $store->get('gst_type');exit;
// FETCH INVOICE INFO
$invoice_info     = $invoice_model->getInvoiceInfo($invoice_id);
$cart_data = json_decode($invoice_info['cart_data']);
$tax_detail = [];
//print_r($invoice_info);exit;
//print_r($cart_data->products);exit;
$inv_type         = $invoice_info['inv_type'];
$created_at       = format_date($invoice_info['created_at']);
$customer_id      = $invoice_info['customer_id'];
$customer_name    = $invoice_info['customer_name'];
$customer_contact = $invoice_info['customer_mobile'] 
                      ? $invoice_info['customer_mobile'] 
                      : $invoice_info['customer_email'];
$currency_code    = $invoice_info['currency_code'];
$payment_method   = get_the_payment_method($invoice_info['payment_method'], 'name');
$invoice_note     = $invoice_info['invoice_note'];

$product_doscount = 0;

// FETCH INVOICE ITEMS
$invoice_items = $invoice_model->getInvoiceItems($invoice_id);

// FETCH INVOICE PRICE
$selling_price = $invoice_model->getSellingPrice($invoice_id);

// SET DOCUMENT TITLE
$document->setTitle($language->get('text_invoice') . ' - ' . $invoice_id);
 
// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/InvoiceViewController.js');  

// ADD BODY CLASS
$document->setBodyClass('sidebar-collapse');


?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!--[if !mso]><!-->

    <!--<![endif]-->
    <title>MOBIOCEAN </title>
    <!--[if gte mso 9]>
  <style type="text/css" media="all">
    sup { font-size: 100% !important; }
  </style>
  <![endif]-->

    <style type="text/css" media="screen">
        /* Linked Styles */
        
        body {
            padding: 0 !important;
            margin: 0 !important;
            display: block !important;
            min-width: 100% !important;
            width: 100% !important;
            -webkit-text-size-adjust: none;
            font-family: arial;
        }
        
        a {
            color: #00349d;
            font-weight: bold;
            text-decoration: none
        }
        
        p {
            padding: 0 !important;
            margin: 0 !important;
            line-height: 22px;
            font-size: 13px;
        }
        
        img {
            -ms-interpolation-mode: bicubic;
            /* Allow smoother rendering of resized image in Internet Explorer */
        }
        
        .mcnPreviewText {
            display: none !important;
        }
        
        h4,
        h5,
        h3,
        h6 {
            padding: 0px;
            margin: 0px;
        }
        
        .qtd1 td {
            font-size: 12px;
            text-align: center;
        }
    </style>
</head>

<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; font-family:arial; width:100% !important; background:#fff; -webkit-text-size-adjust:none;">

    <table width="320px" border="0" cellspacing="0" cellpadding="0" style="margin:auto; margin-top:0px;">

        <tr>
            <td align="center" colspan="2">
               
                <?php if ($store->get('logo')): ?>
                  <img src="<?php echo root_url(); ?>/assets/wonderpillars/img/logo-favicons/<?php echo $store->get('logo'); ?>" style="width:150px;">
                <?php else: ?>
                  <img src="<?php echo root_url(); ?>/assets/wonderpillars/img/logo-favicons/nologo.jpeg" style="width:150px;">
                <?php endif; ?>
            </td>
        </tr>

        <tr>
            <td align="center" colspan="2">
                <br>
                <h4 style="margin-bottom:6px; padding:0px; margin:0px;"><?php echo store('name'); ?></h4>
                <h6 style="margin-bottom:6px; padding:0px; margin:0px;"><?php echo store('address'); ?></h6>
                <h6 style="margin-bottom:6px; padding:0px; margin:0px;">Contact No: <?php echo store('mobile'); ?> </h6>
                <h6 style="margin-bottom:6px; padding:0px; margin:0px;">Invoice ID: <?php echo $invoice_id; ?> </h6>
                <h6 style="margin-bottom:6px; padding:0px; margin:0px;">GST Number: <?php echo store('vat_reg_no'); ?> </h6>
            </td>
        </tr>

        <tr>
            <td align="center" colspan="2">
                <h5 style="margin-top:2px;">Date: <?php echo $created_at; ?></h5></td>
        </tr>

        <tr>
            <td style="text-align:left;">

                <h6>
                Cashier Name - <?php echo $invoice_info['username'] ; ?> 
                <!-- <br> -->
                <!-- Order ID - 3452718 -->
              </h6>
            </td>

            <td style="text-align:right;">

                <h6>
                Customer Name - <?php echo $customer_name; ?>
                <br>
                <!-- Mobile - <?php echo $customer_contact ; ?> -->
              </h6>

            </td>
        </tr>

    </table>

    <table width="320px" border="0" cellspacing="3" cellpadding="3" style="margin:auto; margin-top:10px; font-size:12px;">

        <tr style="border-bottom:dashed 1px #eee; border-top:dashed 1px #eee;">
            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;">S. No.</th>
            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;">
                Qty.
                <br> Unit
            </th>
            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;">
                MRP(Rs)
                <?php
                    if($store->get('gst_type') == 'SGSTCGST'){
                        echo '<br> CGST%';
                    }
                ?>
                
            </th>
            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;">
                Disc.(Rs)
                <?php
                    if($store->get('gst_type') == 'SGSTCGST'){
                            echo '<br> SGST%';
                    }
                    else{
                        echo '<br> IGST%';
                    }
                ?>
                <br> 
            </th >
            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c; text-align:right;">
                Amount(Rs)
                <br> (Incl. GST)
            </th>
        </tr>
        <?php
            foreach($cart_data->products as $key => $product){

                $data = [
                    'taxrate' => $product->p_taxrate,
                ];
               
                    array_push($tax_detail, $product->p_taxrate);
                    //print_r($tax_detail);exit;
                ?>
                <tr>
                    <td colspan="5">

                        <p style="text-align:left; font-weight:600;">
                            <?php echo $product->product->p_name;?>
                        </p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p style="text-align:center; font-weight:600;">
                           <?php echo $key+1; ?> 
                        </p>
                    </td>
                    <td>
                        <p style="text-align:center; font-weight:600;"> 
                            <?php echo $product->quantity;?>
                            <br> <?php echo $product->product->p_unit;?>
                        </p>
                    </td>
                    <td>
                        <p style="text-align:center; font-weight:600;"> 
                            <?php echo number_format($product->product->buying_item->item_selling_price,2);

                            if($store->get('gst_type') == 'SGSTCGST')
                                {?>
                                    <br>
                                    <?php echo number_format(($product->product->p_taxrate/2),2); ?>
                            <?php }?>
                          </p>  
                    </td>
                    <td>
                        <p style="text-align:center; font-weight:600;"> 
                            <?php 
                                if($product->discount_detail && $product->discount_detail->flat){
                                    $product_doscount = $product_doscount+$product->discount_detail->flat->total_discount;
                                    echo number_format($product->discount_detail->flat->total_discount,2);
                                }
                                // else if($product->discount_detail && $product->discount_detail->precentage){
                                //     echo $product->discount_detail->precentage->total_discount;
                                // }
                                else{
                                    echo '0.00';
                                }
                            
                            if($store->get('gst_type') == 'SGSTCGST'){?>
                                <br>  
                                <?php echo number_format(($product->product->p_taxrate/2),2); ?>
                            <?php }
                            else{?>
                                <br>  
                                 <?php echo number_format($product->product->p_taxrate,2); ?>
                            <?php }?>
                            
                        </p>
                    </td>
                    <td>
                        <p style="text-align:right; font-weight:600;"> 
                            <?php
                            $dis = 0;
                            if($product->discount_detail && $product->discount_detail->flat){
                                    $dis =  $product->discount_detail->flat->total_discount;
                                }
                                // else if($product->discount_detail && $product->discount_detail->category){
                                //     $dis = $product->discount_detail->category->total_discount;
                                // }
                                
                            ?>
                            <?php echo number_format(($product->total_price-$dis),2);?>
                                
                        </p>
                    </td>

                </tr>
            <?php }?>
           
        

        

    </table>

    <table width="320px" border="0" cellspacing="2" cellpadding="2" style="margin:auto; margin-top:10px; border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c; text-align:right; font-weight:600; font-size:12px;">
        <tbody>
            <tr>
                <td>
                    Sub Total
                </td>
                <td>
                   <?php echo number_format(($invoice_info['subtotal']-$product_doscount),2); ?>
                </td>
            </tr>
            <tr>
                <td>
                    Promotional Discount
                </td>
                <td>
                    <?php echo number_format(($invoice_info['discount_amount']-$product_doscount),2); ?>
                </td>
            </tr>

            <tr>
                <td>
                    Previous Due
                </td>
                <td>
                    <?php 
                    if($invoice_info['previous_due'] !='0'){

                        echo number_format($invoice_info['previous_due'],2); 
                    }
                    else{
                        echo '0.00';
                    }
                    ?>
                </td>
            </tr>

            <tr >
                <td style=" border-top:dashed 1px #3c3c3c;">
                    Payable Amount
                </td>
                <td style=" border-top:dashed 1px #3c3c3c;">
                    <?php 
                        $subtotal = $invoice_info['subtotal']-$product_doscount;
                        $all_discount = $invoice_info['discount_amount'];
                     
                    echo number_format((($invoice_info['subtotal']+$invoice_info['previous_due'])-$all_discount),2); ?>
                </td>
            </tr>

            <tr >
                <td style="border-bottom:dashed 1px #3c3c3c; ">
                    Paid (CASH)
                </td>

                <td style="border-bottom:dashed 1px #3c3c3c; ">
                    <?php echo number_format($invoice_info['paid_amount'],2); ?>
                </td>
            </tr>
            <tr>
                <td>
                    Due
                </td>
                <td>
                    <?php echo number_format($invoice_info['present_due'],2);?> </td>
            </tr>

        </tbody>
    </table>
    <table width="320px" border="0" cellspacing="3" cellpadding="2" style="margin:auto; margin-bottom:6px; margin-top:10px; border-bottom:dashed 1px #3c3c3c;  text-align:right; font-weight:600; font-size:12px;">
        <tbody>

            <tr>
                <td colspan="3">
                    <h5 style="font-size:16px; text-align:center; margin: 10px;">GST Details</h5></td>
            </tr>

            <tr>

                <tr>

                    <td>
                        <p style="text-align:center; font-weight:600;"> GST%
                        </p>
                    </td>
                    <td>
                        <p style="text-align:center; font-weight:600;"> Taxable </p>
                    </td>
                    <td>
                        <p style="text-align:center; font-weight:600;"> Total TAX </p>
                    </td>

                </tr>
                <?php 
                    foreach($cart_data->products as $key => $product){?>
                        <tr>
                            <td>
                                <p style="text-align:center; font-weight:600;"> 
                                    <?php echo number_format($product->product->p_taxrate,2); ?>
                                </p>
                            </td>
                            <td>
                            <p style="text-align:center; font-weight:600;"> 
                                <?php

                                   $tax = $product->product->buying_item->item_selling_price*100/(100+$product->product->p_taxrate);
                                   echo number_format($tax*$product->quantity,2);
                                ?>
                                <!-- //$product->product->buying_item->item_selling_price- -->
                                <?php 
                               // echo $product->product->buying_item->item_selling_price-$product->tax; 
                                ?> 
                            </p>
                            </td>
                            <td>
                                <p style="text-align:center; font-weight:600;">
                                 <?php 
                                 $taxPerItem = $product->product->buying_item->item_selling_price-$tax;
                                 echo number_format($taxPerItem*$product->quantity, 2); ?> 
                            </p>
                            </td>

                        </tr>
                    <?php }?>
            </tr>

            <tr>
                <td colspan="3" style="text-align:center; padding-bottom:2px; padding: 10px; border-top:dashed 1px #3c3c3c; ">
                  
                    <p style="font-size:18px;">
                                    
                              <?php
                              
                               
                                  if(unserialize($store->get('preference'))['invoice_footer_text']){
                                    echo unserialize($store->get('preference'))['invoice_footer_text'];
                                  }
                                  else{
                                    echo 'Thank you for choosing us!';
                                  }
                              ?>                     
                               </p> 
                </td>
            </tr>

        </tbody>
    </table>
  <br>
  <div align="center"><img src="../footer_logo.png" style="width:150px;"></div>
  <br>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var print = window.location.href.split('&');
            if(print[1] !== undefined && print[1] == 'print=true'){
                window.print();
            }
        })
    </script>