<?php 
ob_start();
session_start();
include '../_init.php';
// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}
// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_unit')) {
	redirect(root_url() . '/admin/dashboard.php');
}
// LOAD LANGUAGE FILE
$language->load('management');
// SET DOCUMENT TITLE
$document->setTitle($language->get('title_unites'));
// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/UnitesController.js');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php"); 
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper" ng-controller="UnitesController">
  	<!-- Content Header Start -->
	<section class="content-header">
		<h1>
			<?php echo 'Units'
				//$language->get('text_unites'); 
			?>
			<small>
				<?php echo store('name'); ?>	
			</small>
		</h1>
		<ol class="breadcrumb">
			<li>
				<a href="dashboard.php">
					<i class="fa fa-dashboard"></i> 
					<?php echo $language->get('text_dashboard'); ?>
				</a>
			</li>
			<li class="active">
				<?php echo 'Units';
					//$language->get('text_unites'); 
				?>	
			</li>
		</ol>
	</section>
  	<!-- Content Header End -->
	<!-- Content Start -->
	<section class="content">
		
	    
    	<?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'create_unit')) : ?>
	    <div class="box box-info<?php echo create_box_state(); ?>">
	        <div class="box-header with-border">
				<h3 class="box-title">
					<span class="fa fa-fw fa-plus"></span> 
					<?php 
						echo 'Add New Unit';
						//sprintf($language->get('text_add_new'), 
						//$language->get('text_unites')); 
					?>
				</h3>
				<button  type="button" class="btn btn-box-tool add-new-btn" data-widget="collapse" data-collapse="true">
					<i class="fa <?php echo !create_box_state() ? 'fa-minus' : 'fa-plus'; ?>"></i>
				</button>
	        </div>

	        <?php if (isset($error_message)): ?>
	        	<div class="alert alert-danger">
					<p>
						<span class="fa fa-warning"></span> 
						<?php echo $error_message; ?>
					</p>
	        	</div>
	        <?php elseif (isset($success_message)): ?>
	          <div class="alert alert-success">
				<p>
					<span class="fa fa-check"></span> 
					<?php echo $success_message; ?>
				</p>
	          </div>
	        <?php endif; ?>

	        <!-- Include Product Form -->
	        <?php include('../_inc/template/unites_create_form.php'); ?>

	    </div>
	    <?php endif; ?>

	    <div class="row">
		    <form method="post" action="unites_bulk_action.php" enctype="multipart/form-data" id="unites-list-form">
			    <div class="col-xs-12">
			        <div class="box box-success">
				        <div class="box-header">
				            <h3 class="box-title">
				            	<?php 
				            		echo 'View All Units';
				            		//sprintf($language->get('text_view_all'), $language->get('text_tax_rates')); 
				            		?>	
				            </h3>
				            <!--Box Tools End-->
				            <div class="box-tools pull-right">
				              <!-- Filter Product Supplier Wise -->
				               <?php //include('../_inc/template/partials/product_filter.php'); ?>
				              <!-- Trash Box -->
				                <!-- <div class="btn-group">
				                  <a type="button" class="btn btn-danger" href="product.php?location=trash">
				                      <span class="fa fa-trash"></span> 
				                      <?php //echo $language->get('button_trash'); ?> 
				                      <i class="badge badge-warning" id="total-trash">
				                        <?php //echo total_trash_product(); ?>
				                      </i>
				                  </a>
				                </div> -->
				                <!-- Bulk Action -->
				                <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'product_bulk_action')) : ?>            
				              <!--   <div class="btn-group">
				                  <button type="button" class="btn btn-danger">
				                      <?php echo $language->get('button_bulk'); ?>
				                  </button>
				                  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
				                      <span class="caret"></span>
				                      <span class="sr-only">Toggle Dropdown</span>
				                  </button>
				                  <ul class="dropdown-menu" role="menu">
				                      <li>
				                        <a id="delete-all" href="#" data-form="#unites-list-form" data-loading-text="Deleting...">
				                          <?php echo $language->get('button_delete_all'); ?>
				                        </a>
				                      </li>
				                      <?php 
				                      if(isset($request->get['location']) && $request->get['location'] == 'trash') : ?>
				                      <li>
				                        <a id="restore-all" href="#" data-form="#unites-list-form" data-datatable="product-brad-list" data-loading-text="Restoring...">
				                            <?php echo $language->get('button_restore_all'); ?>
				                        </a>
				                      </li>
				                      <?php endif; ?>
				                   </ul>
				                </div> -->
				              <?php endif; ?>
				            </div>
				        </div>
						<div class="box-body">
							<div class="table-responsive">
								<?php
									$hide_colums = "";
									if ($user->getGroupId() != 1) {
										if (! $user->hasPermission('access', 'delete_unit')) {
											$hide_colums .= "0,";
										}
										if (! $user->hasPermission('access', 'read_unit')) {
											$hide_colums .= "4,";
										}
										if (! $user->hasPermission('access', 'update_unit')) {
											$hide_colums .= "5,";
										}
										if (! $user->hasPermission('access', 'delete_unit')) {
											$hide_colums .= "6,";
										}
									}
								?>  
								<table id="unites-list" class="table table-bordered table-striped table-hover" data-hide-colums="<?php echo $hide_colums; ?>">
								    <thead>
								        <tr class="bg-gray">
								        	<th class="w-5 product-head text-center">
						                      <input type="checkbox" class="check-all" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
						                    </th>
								            <th class="w-5">
								            	Id
								            </th>
								            <th class="w-15">
								            	<?php echo $language->get('label_title'); ?>
								            </th>
								            <!-- <th class="w-50">
								            	<?php echo $language->get('label_description'); ?>
								            </th> -->
								            <th class="w-5">
								            	<?php echo $language->get('label_view'); ?>
								            </th>
								            <th class="w-5">
								            	<?php echo $language->get('label_edit'); ?>
								            </th>
								            <th class="w-5">
								            	<?php echo $language->get('label_delete'); ?>
								            </th>
								        </tr>
								    </thead>
								    <tfoot>
										<tr class="bg-gray">
											<th class="w-5 product-head text-center">
						                      <input type="checkbox" class="check-all" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
						                    </th>
								            <th class="w-5">
								            	Id
								            </th>
									        <th class="w-15">
									        	<?php echo $language->get('label_title'); ?>
									        </th>
									       <!--  <th class="w-50">
									        	<?php echo $language->get('label_description'); ?>
									        </th> -->
									        <th class="w-5">
									        	<?php echo $language->get('label_view'); ?>
									        </th>
									        <th class="w-5">
									        	<?php echo $language->get('label_edit'); ?>
									        </th>
									        <th class="w-5">
									        	<?php echo $language->get('label_delete'); ?>
									        </th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
			        </div>
			    </div>
			</form>
	    </div>

	</section>
  	<!-- Content end -->

</div>
<!--  Content Wrapper End -->

<?php include ("footer.php"); ?>