<?php 
ob_start();
session_start();
include ("../_init.php");

if (isset($request->get['active_store_id']))
{
  redirect(root_url() . '/admin/store.php');
}

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_store')) {
  redirect(root_url() . '/admin/dashboard.php');
}

// LOAD LANGUAGE FILE
$language->load('store');

// SET DOCUMENT TITLE
$document->setTitle($language->get('title_store'));

// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/StoreController.js');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php") ;
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper">

  <!-- Content Header Start -->
  <section class="content-header" ng-controller="StoreController">
    <h1>
      <?php echo $language->get('text_store_title'); ?>
      <small>
        <?php echo store('name'); ?>
      </small>
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="dashboard.php">
          <i class="fa fa-dashboard"></i> 
          <?php echo $language->get('text_dashboard'); ?>
        </a>
      </li>
      <li class="active">
        <?php echo $language->get('title_store'); ?>
      </li>
    </ol>
  </section>
  <!-- Content Header End -->

  <!-- Content Start -->
  <section class="content">

    
    
    <div class="row">
      <form action="store_bulk_action.php" method="post" enctype="multipart/form-data" id="store-list-form">
      <div class="col-xs-12">
        <div class="box box-success">
          <div class="box-header">
             <?php
                if(isset($_SESSION) && isset($_SESSION['success_store_create']) && $_SESSION['success_store_create'] !=''){?>
              <div class="alert alert-success success-massage" >
                  <p class="success-massage">
                    <span class="fa fa-check id="success-massage"></span>
                      <?php echo $_SESSION['success_store_create']; ?>
                  </p>
              </div>
              <?php 
                unset($_SESSION['success_store_update']);
              } ?>
               <?php
                if(isset($_SESSION) && isset($_SESSION['success_store_update']) && $_SESSION['success_store_update'] !=''){?>
              <div class="alert alert-success success-massage" >
                  <p class="success-massage">
                    <span class="fa fa-check id="success-massage"></span>
                      <?php echo $_SESSION['success_store_update']; ?>
                  </p>
              </div>
              <?php 
                unset($_SESSION['success_store_update']);
              } ?>
            <h3 class="box-title">
              <?php echo $language->get('text_store_list_title'); ?>
            </h3>
            <!--Box Tools End-->
            <div class="box-tools pull-right">

              


              <!-- Filter Product Supplier Wise -->
               <?php //include('../_inc/template/partials/product_filter.php'); ?>
              <!-- Trash Box -->
                <!-- <div class="btn-group">
                  <a type="button" class="btn btn-danger" href="product.php?location=trash">
                      <span class="fa fa-trash"></span> 
                      <?php //echo $language->get('button_trash'); ?> 
                      <i class="badge badge-warning" id="total-trash">
                        <?php //echo total_trash_product(); ?>
                      </i>
                  </a>
                </div> -->
                <!-- Bulk Action -->
                <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'product_bulk_action')) : ?>            
               <!--  <div class="btn-group">
                  <button type="button" class="btn btn-danger">
                      <?php echo $language->get('button_bulk'); ?>
                  </button>
                  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                      <li>
                        <a id="delete-all" href="#" data-form="#store-list-form" data-loading-text="Deleting...">
                          <?php echo $language->get('button_delete_all'); ?>
                        </a>
                      </li>
                      <?php 
                      if(isset($request->get['location']) && $request->get['location'] == 'trash') : ?>
                      <li>
                        <a id="restore-all" href="#" data-form="#store-list-form" data-datatable="store-store-list" data-loading-text="Restoring...">
                            <?php echo $language->get('button_restore_all'); ?>
                        </a>
                      </li>
                      <?php endif; ?>
                   </ul>
                </div> -->
              <?php endif; ?>
            </div>
          </div>
          <div class="box-body">
            <div class="table-responsive">  
              <?php
                  $hide_colums = "";
                  if ($user->getGroupId() != 1) {
                    if (! $user->hasPermission('access', 'update_store')) {
                      $hide_colums .= "7,";
                    }
                    if (! $user->hasPermission('access', 'delete_store')) {
                      $hide_colums .= "8,";
                    }
                    if (! $user->hasPermission('access', 'activate_store')) {
                      $hide_colums .= "9,";
                    }
                  }
                ?> 
              <table id="store-store-list" class="table table-bordered table-striped table-hover" data-hide-colums="<?php echo $hide_colums; ?>">
                <thead>
                  <tr class="bg-gray">
                    <th class="w-5 product-head">
                      <input type="checkbox" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
                    </th>
                    <th>
                      <?php echo sprintf($language->get('label_serial_no'), null); ?>
                    </th>
                    <th>
                      <?php echo sprintf($language->get('label_name'), null); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_country'); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_address'); ?>
                    </th>
                    <th>
                      <?php echo 'Contact no'; ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_created_at'); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_status'); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_edit'); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_delete'); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_action'); ?>
                    </th>
                  </tr>
                </thead>

                <tfoot>
                  <tr class="bg-gray">
                    <th class="w-5 product-head">
                      <input type="checkbox" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
                    </th>
                    <th>
                      <?php echo sprintf($language->get('label_id'), null); ?>
                    </th>
                    <th>
                      <?php echo sprintf($language->get('label_name'), null); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_country'); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_address'); ?>
                     </th>
                    <th>
                      <?php echo 'Contact no'; ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_created_at'); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_status'); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_edit'); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_delete'); ?>
                    </th>
                    <th>
                      <?php echo $language->get('label_action'); ?>
                    </th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Content End -->

</div>
<!-- Content Wrapper End -->

<?php include ("footer.php"); ?>