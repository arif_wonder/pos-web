<?php
ob_start();
session_start();
include ("../_init.php");

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'due_collection')) {
  redirect(root_url() . '/admin/dashboard.php');
}

// LOAD LANGUAGE FILE
$language->load('due');

// SET DOCUMENT TITLE
$document->setTitle($language->get('title_due_collection'));

// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/modals/CustomerDuepaidModal.js');
$document->addScript('../assets/wonderpillars/angular/controllers/DueCollectionController.js');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php");
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper" ng-controller="DueCollectionController">

	<!-- Content Header Start -->
	<section class="content-header">
		<?php include ("../_inc/template/partials/apply_filter.php"); ?>
		<h1>
			<?php echo $language->get('text_due_coll_report_title'); ?>
			<small>
				<?php echo store('name'); ?>
			</small>
		</h1>
		<ol class="breadcrumb">
			<li>
				<a href="dashboard.php">
					<i class="fa fa-dashboard"></i> 
					<?php echo $language->get('text_dashboard'); ?>
				</a>
			</li>
			<li class="active">
				<?php echo $language->get('text_due_coll_report_title'); ?>
			</li>
		</ol>
	</section>
	<!-- Content Header End -->

	<!-- Content Start -->
	<section class="content">

		
    
		<div class="row">
		    <div class="col-xs-12">
		      	<div class="box box-info">
		      		<div class="box-header">
				        <h3 class="box-title">
				        	<?php echo $language->get('text_due_coll_report_sub_title'); ?>
				        </h3>
				     </div>
			      	<div class='box-body'>  
						<div class="table-responsive"> 

							<?php
					            $hide_colums = "";
					            if ($user->getGroupId() != 1) {
					              if (! $user->hasPermission('access', 'due_collection')) {
					                $hide_colums .= "4,";
					              }
					              if (! $user->hasPermission('access', 'view_invoice')) {
					                $hide_colums .= "5,";
					              }
					            }
					          ?>   

					          <!-- Invoice List Start -->
							<table id="invoice-invoice-list"  class="table table-bordered table-striped table-hover" data-hide-colums="<?php echo $hide_colums; ?>">
							    <thead>
							      <tr class="bg-gray">
							      	<th class="w-5 product-head text-center">
				                      <input type="checkbox" class="check-all" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
				                    </th>
							        <th class="w-10">
							        	<?php echo sprintf($language->get('label_id'), null); ?>
							        </th>
							        <th class="w-35">
							        	<?php echo $language->get('label_customer_name'); ?>
							        </th>
							        <th class="w-25">
							        	<?php echo $language->get('label_mobile'); ?>
							        </th>
							        <th class="w-10">
							        	<?php echo $language->get('label_invoice_due'); ?>
							        </th>
							        <th class="w-10">
							        	<?php echo $language->get('label_collection'); ?>
							        </th>
							        <th class="w-10">
							        	<?php echo $language->get('label_view'); ?>
							        </th>
							      </tr>
							    </thead>
							    <tfoot>
				               		<tr class="bg-gray">
					                    <th></th>
					                    <th></th>
					                    <th></th>
					                    <th></th>
					                    <th></th>
					                    <th></th>
					                    <th></th>
				                	</tr>
			            		</tfoot>
							</table>
							<!-- Invoice List End -->
						</div>  
			  		</div>
		      	</div>
		    </div>
	    </div>
	</section>
	<!-- Content End -->

</div>
<!-- Content wrapper End -->

<?php include ("footer.php"); ?>