<?php
ob_start();
session_start();
include ("../_init.php");

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_collection_report')) {
  redirect(root_url() . '/admin/dashboard.php');
}

// LOAD LANGUAGE FILE
$language->load('report_collection');

// SET DOCUMENT TITLE
$document->setTitle('Sell report');
$document->setBodyClass('sidebar-collapse');

// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/ReportCollectionController.js');
$document->addScript('../assets/wonderpillars/angular/modals/UserInvoiceDetailsModal.js');
$document->addScript('../assets/wonderpillars/angular/modals/DueCollectionDetailsModal.js');

// ADD BODY CLASS
$document->setBodyClass('sidebar-collapse');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php");
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper">

  <!--  Content Header Start -->
  <section class="content-header">
    <?php include ("../_inc/template/partials/apply_filter.php"); ?>
    <h1>
      <?php echo 'Sales Report'; ?>
      <small>
        <?php echo store('name'); ?>
      </small>
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="dashboard.php">
          <i class="fa fa-dashboard"></i> 
          <?php echo $language->get('text_dashboard'); ?>
        </a>
      </li>
      <li class="active">
        <?php echo 'Sales Report'; ?>
      </li>
    </ol>
  </section>
  <!-- Content Header End -->

  <!-- Content Start -->
  <section class="content">

   
    
    <div class="row">
      <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'read_collection_report')) : ?>
          <!-- Collection Report Start -->
          <div id="collection-report" class="col-md-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">
                  <?php echo 'View Report'; ?>
                </h3>
                            
            <div class="box-tools pull-right">
              
                <div class="btn-group">
                  <button type="button" class="btn btn-info">
                    <span class="fa fa-filter"></span> 
                    <?php if (current_nav() == 'report_sell_itemwise') : ?>
                      <?php echo $language->get('button_itemwise'); ?>
                    <?php elseif (current_nav() == 'report_sell_categorywise') : ?>
                      <?php echo $language->get('button_categorywise'); ?>
                    <?php elseif (current_nav() == 'report_sell_supplierwise') : ?>
                      <?php echo $language->get('button_supplierwise'); ?>
                    <?php elseif (current_nav() == 'report_sell_userwise') : ?>
                      <?php echo 'User wise'; ?>
                    <?php else: ?>
                      <?php echo $language->get('button_filter'); ?>
                    <?php endif; ?>
                  </button>
                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                      <li>
                        <a href="report_sell_itemwise.php">
                          <?php echo $language->get('button_itemwise'); ?>
                        </a>
                      </li>
                      <li>
                        <a href="report_sell_categorywise.php">
                          <?php echo $language->get('button_categorywise'); ?>
                        </a>
                      </li>
                      <li>
                        <a href="report_sell_supplierwise.php">
                          <?php echo $language->get('button_supplierwise'); ?>
                        </a>
                      </li>

                      <li>
                        <a href="report_sell_userwise.php">
                          <?php echo 'User wise'; ?>
                        </a>
                      </li>
                   </ul>
                </div>

            </div>
              </div>
              <!--filter form start -->
              <?php 
                //include('date_filter_form.php'); 
              ?>
               <!--filter form end -->    

              <div class="dashboard-widget box-body">
                <?php include('../_inc/template/partials/sell_report_userwise.php'); ?>
              </div>
            </div>
          </div>
          <!--Collection Report End -->
          <?php endif; ?>

      </div>
  </section>
  <!-- Content End -->

</div>
<!-- Content Wrapper End -->

<?php include ("footer.php"); ?>