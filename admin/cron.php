<?php
ob_start();
session_start();
include ("../_init.php");

if (DEMO) {
	die('Oops, You can not run cron in demo version.');
}

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'corn')) {
  redirect(root_url() . '/admin/dashboard.php');
}

// Load Cron Model
$cron_model = $registry->get('loader')->model('cron');

if ($m = $cron_model->run_cron()) {
    echo '<!doctype html><html><head><title>Cron Job</title><style>p{background:#F5F5F5;border:1px solid #EEE; padding:15px;}</style></head><body>';
    echo '<p>Corn job successfully run.</p>';
    foreach($m as $msg) {
        echo '<p>'.$msg.'</p>';
    }
    echo '</body></html>';
}

/*
----------------------------------------
| Usages
----------------------------------------
|   Cron Job (run at 1:00 AM daily):
|   0 1 * * * wget -qO- http://pos/admin/cron.php >/dev/null 2>&1
*/