<?php 
ob_start();
session_start();
include '../_init.php';

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}
// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_product')) {
	redirect(root_url() . '/admin/dashboard.php');
}
// LOAD LANGUAGE FILE
$language->load('management');
// LOAD PRODUCT MODEL
$product_model = $registry->get('loader')->model('discbycategory');
// FETCH PRODUCT INFO
$p_id = isset($request->get['id']) ? $request->get['id'] : '';
$data = $product_model->getDiscByCategory($p_id);
if (count($data) <= 1) {
	redirect(root_url() . '/admin/discount_by_category.php');
}
// SET DOCUMENT TITLE
$document->setTitle($language->get('title_product'));
// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php"); 
?>
<!-- Content Wrapper Start -->
<div class="content-wrapper">
  	<!-- Content Header Start -->
	<section class="content-header">
		<h1>
			<?php echo $language->get('text_dis_by_category'); ?> &raquo; <?php echo $data['title'];?>
		</h1>
		<ol class="breadcrumb">
			<li>
				<a href="dashboard.php">
					<i class="fa fa-dashboard"></i> 
					<?php echo $language->get('text_dashboard'); ?>
				</a>
			</li>
			<li>
				<a href="discount_by_category.php">
					<?php echo $language->get('text_dis_by_category'); ?>
				</a>
			</li>
			<li class="active">
				<?php echo $data['title'];?>
			</li>
		</ol>
	</section>
  	<!-- Content Header End -->

	<!-- Content Start -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">

			    <div class="nav-tabs-custom">
	                <div class="tab-content">
	                    <div class="tab-pane active" id="details">
	                        <div class="form-horizontal">
  <div class="box-body">

    <div class="form-group">
      <label for="title" class="col-sm-3 control-label">
        <?php echo $language->get('label_title'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="title" value="<?php echo $data['title']; ?>" name="title">
      </div>
    </div>

    <div class="form-group">
      <label for="description" class="col-sm-3 control-label">
        <?php echo $language->get('label_description'); ?>
      </label>
      <div class="col-sm-7">
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo $data['description']; ?></textarea>
      </div>
    </div>

     <div class="form-group">
      <label for="category_id" class="col-sm-3 control-label">
        <?php echo $language->get('label_category'); ?><i class="required">*</i>
      </label>
      <div class="col-sm-7">
        <select class="form-control select2" name="category_id" id="category_id">
           <option value="">
              <?php echo $language->get('text_select'); ?>
            </option>
            <?php foreach (get_categories_tree() as $key => $category) { ?>
              <option value="<?php echo $category['category_id'] ; ?>"
              	<?php echo $data['category_id']== $category['category_id'] ? "selected='selected'" : ''; ?>>
              	<?php echo $category['category_name'] ; ?>
              		
              	</option>
            <?php } ?>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="date_from" class="col-sm-3 control-label">
        <?php echo $language->get('label_from_date'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control pick_date" id="date_from" value="<?php echo date('d M Y', strtotime($data['date_from'])); ?>" name="date_from">
      </div>
    </div>

    <div class="form-group">
      <label for="date_to" class="col-sm-3 control-label">
        <?php echo $language->get('label_to_date'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" readonly="true" class="form-control pick_date" id="date_to" value="<?php echo date('d M Y', strtotime($data['date_to'])); ?>" name="date_to">
      </div>
    </div>

    <div class="form-group">
      <label for="discount_type" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount_type'); ?>
      </label>
      <div class="col-sm-7">
         <select class="form-control select2" name="discount_type" id="discount_type">
          <option <?php echo $data['discount_type']=='fixed' ? "selected='selected'" : ''; ?> value="fixed">Fixed</option>
          <option <?php echo $data['discount_type']=='precentage' ? "selected='selected'" : ''; ?> value="precentage">Percentage</option>
       </select>
      </div>
    </div>

    <div class="form-group">
      <label for="discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_discount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="discount" value="<?php echo $data['discount']; ?>" name="discount">
      </div>
    </div>

    <div class="form-group">
      <label for="max_discount" class="col-sm-3 control-label">
        <?php echo $language->get('label_max_discount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control decimal" id="max_discount" value="<?php echo $data['max_discount']; ?>" name="max_discount">
      </div>
    </div>

    
    <div class="form-group">
      <label for="min_purchase_amount" class="col-sm-3 control-label">
        <?php echo $language->get('label_min_purchase_amount'); ?>
      </label>
      <div class="col-sm-7">
        <input type="text" class="form-control number" id="min_purchase_amount" value="<?php echo $data['min_purchase_amount']; ?>" name="min_purchase_amount">
      </div>
    </div>
     


    

   
    
  </div>
</div>
	                    </div>
	                </div>
	            </div>

			</div>
		</div>
	</section>
  	<!-- Content End -->
</div>
<?php include ("footer.php"); ?>