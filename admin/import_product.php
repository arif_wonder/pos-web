<?php 
ob_start();
session_start();
include ("../_init.php");

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'import_product')) {
	redirect(root_url() . '/admin/dashboard.php');
}

// LOAD LANGUAGE FILE
$language->load('import');
$message = $language->get('text_product_import_alert');

// SET DOCUMENT TITLE
$document->setTitle($language->get('title_import_product'));

// INCLUDE EXEL READER
require('../_inc/vendor/spreadsheet-reader/php-excel-reader/excel_reader2.php');
require('../_inc/vendor/spreadsheet-reader/SpreadsheetReader.php');

// INCLUDE HEADER AND FOOTER
include("header.php");
include ("left_sidebar.php");
$product_model = $registry->get('loader')->model('product');

if (isset($request->post['submit'])) {

	$allowed =  array('xlsx','xlsm' ,'xlsb','xltx','xltm' ,'xls','xlt','xls' ,'xla','xlw','xlr','ods','ots');
	$ext = pathinfo($_FILES['filename']['name'], PATHINFO_EXTENSION);
		
	if (in_array($ext,$allowed)) {

		$dir = '../storage/';
		if(!is_dir($dir)) {
            mkdir($dir, 0777, true);            //create directory with specified path
            chmod($dir, 0777);
        }

		$target_file= $dir.basename($_FILES['filename']['name']);
		move_uploaded_file($_FILES['filename']['tmp_name'], $target_file);

		try {

			$Reader = new SpreadsheetReader($target_file);
			$Sheets = $Reader->Sheets();

			$insert_status = array();
			$update_status = array();
			$total_item_no = 0;

			foreach ($Sheets as $Index => $Name) {
				$Reader->ChangeSheet($Index);
				$skipData = array();
				$err         = '';
				$err_message = '';

				foreach ($Reader as $column) {
					if (strtolower(trim($column[0])) == 'productname' || !$column[0] ) continue;

						$pro_data['p_name']            = $column[0];
						$pro_data['p_discount']        = $column[1];
						$pro_data['p_discount_type']   = $column[2];
						$pro_data['p_brand']           = $column[3];
						$pro_data['p_unit']            = $column[4];
						$pro_data['p_taxrate']         = $column[5];
						$pro_data['p_gift_item']       = $column[6];
						$pro_data['p_code']            = $column[7];
						$pro_data['category_id']       = $column[8];
						$pro_data['box_id']            = $column[9];
						$pro_data['sup_id']            = $column[10];
						$pro_data['e_date']            = $column[11];

						$pro_data['product_store']     = explode(',',$column[12]);
						$pro_data['description']       = $column[13];
						$pro_data['status']            = $column[14];
						$pro_data['sort_order']        = $column[15];

						$pro_data['p_image']           = '';
						$pro_data['p_quantity']        = '';
						$pro_data['p_regular_price']   = '';
						$pro_data['p_special_price']   = '';
						$pro_data['p_purchage_price']  = '';
						$pro_data['p_sp_fromdate']     = '';
						$pro_data['p_sp_todate']       = '';

						if(empty($column[12])){
							$err = 'error';
							$err_message = 'Store id not found in xls';
						}

						if(empty($column[8])){
							$err = 'error';
							$err_message = 'Category id not found in xls';
						}

						if ($pro_data['category_id']) {
							$statement = $db->prepare("SELECT * FROM categorys WHERE category_id = ?");
							$statement->execute(array( ($pro_data['category_id']) ));

							if (!$statement->rowCount()) {
								$err = 'error';
								$err_message = 'Your enter category id not exist in the database.';
							}
						}

						if ($pro_data['p_name']) {

							$statement = $db->prepare("SELECT * FROM products WHERE LOWER(p_name) = ?");

							$statement->execute(array( strtolower($pro_data['product_name']) ));
							$product = $statement->fetch();

							if ($statement->rowCount()) {
								$err = 'error';
								$err_message = $pro_data['p_name'].' already exist.';
							}
						}
						else if (empty($pro_data['p_name'])) {
							$err = 'error';
						}

						if ($pro_data['p_brand']) {
							$statement = $db->prepare("SELECT * FROM product_brands WHERE brand_id = ?");
							$statement->execute(array( ($pro_data['p_brand']) ));

							if (!$statement->rowCount()) {
								$err = 'error';
								$err_message = 'Your enter brand id not exist in the database.';
							}
						}
						else if (empty($pro_data['p_brand'])) {
							$err = 'error';
							$err_message = 'Please enter brand for '.$pro_data['p_name'].'.';
						}

						if (empty($pro_data['p_code'])) {
							$pro_data['p_code'] = strtotime(date('Y-m-d H:i:s'));
						}
						else {
							$pstate = $db->prepare("SELECT * FROM products WHERE LOWER(p_code) = ?");

							$pstate->execute(array( strtolower($pro_data['p_code']) ));

							if ($pstate->rowCount()) {
								$err = 'error';
								$err_message = $pro_data['p_code'].' already exist.';
							}
						}

						// if (empty($pro_data['e_date'])) {
						// 	$err = 'error';
						// 	$err_message = 'Enter expiry date of '.$pro_data['p_name'].'.';
						// }
						// if (
						// 	!empty($pro_data['e_date']) && 
						// 	strtotime(date('Y-m-d')>= strtotime($pro_data['e_date']) ) ) {
						// 	$err = 'error';
						// 	$err_message = 'Enter expiry date of '.$pro_data['p_name'].' .';
						// }

						if ($err=='') {
							$product_id = $product_model->addProduct($pro_data);
							$total_item_no++;
						}
						
				}
				
			};

			$success = 0;
			$error = 0;
			$message = '';
			$message .= '<h4>Total Item: ' . $total_item_no . '</h4>';
			
				$message .= '<p><strong>Insert Status</strong></p>';
				$message .= '<ul>';
				$message .= '<li>Total Inserted: ' . $total_item_no . '</li>';
				$message .= '<li>Error in: ' . $err_message . '</li>';
				$message .= '</ul>';

			unlink($target_file);
		}
	    catch(Exception $e) {
	    	unlink($target_file);
			$error_message = $e->getMessage();
		}
	}

	//header('Content-Type: application/json');
	//echo json_encode(array('msg' => 'Product imported successfully'));
	$request->post = [];
	if ($err=='') {
		$_SESSION['success_message_import'] = 'Product imported successfully';
		$_SESSION['total_inserted'] = $total_item_no;
		$success_message = 'Product imported successfully';
		header("Location: import_product.php");
		exit;
	}
	
} 
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper">

	<!-- Content Header Start -->
	<section class="content-header">
		<h1>
		  <?php echo sprintf($language->get('text_import_title'), $language->get('text_product')); ?>
			<small>
			  	<?php echo store('name'); ?>
			</small>
		</h1>
		<ol class="breadcrumb">
			<li>
			  	<a href="dashboard.php">
			  		<i class="fa fa-dashboard"></i>
			  		<?php echo $language->get('text_dashboard'); ?>
			  	</a>
			</li>
			<li class="active">
			  	<?php echo sprintf($language->get('text_import_title'), $language->get('text_product')); ?>
			</li>
		</ol>
	</section>
	<!-- Content Header End -->

	<!-- Content Start -->
	<section class="content">

		
    
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-no-border">

					<div class="alert alert-info">
						<span class="fa fa-fw fa-info-circle"></span> <?php echo $message ; ?>
					</div>

					<?php if (isset($error_message)): ?>
					<div class="alert alert-danger">
					    <p>
					    	<span class="fa fa-warning"></span> 
					    	<?php echo $error_message ; ?>
					    </p>
					</div>
					<?php elseif (isset($success_message)): ?>
					<div class="alert alert-success">
					    <p>
					    	<span class="fa fa-check"></span> 
					    	<?php echo $success_message ; ?>
					    </p>
					</div>
					<?php endif; ?>

					<?php
						if(isset($_SESSION) && isset($_SESSION['success_message_import']) && $_SESSION['success_message_import'] !=''){?>
							<div class="alert alert-success">
					    <p>
					    	<span class="fa fa-check"></span> 
					    	<?php echo $_SESSION['success_message_import'] ;
					    		unset($_SESSION['success_message_import']);
					    	 ?>
					    </p>
					    <!-- <p>
					    	<span >Total inserted:</span> 
					    	<?php echo $_SESSION['total_inserted'] ;
					    		unset($_SESSION['total_inserted'])
					    	 ?>
					    </p> -->
					</div>
					
						<?php }
					?>

					<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
						<div class="box-body">

							<div class="form-group">
								<div class="col-sm-3">&nbsp;</div>
								<div class="col-sm-9">
							    	<?php echo $language->get('text_download'); ?>
								    <a href="../storage/samplefiles/product.xls" id="download_demo">
								    	<span class="fa fa-fw fa-download"></span> 
								    	<?php echo $language->get('button_download'); ?>
								    </a>
							 	</div>
							</div>

						  	<div class="form-group">
						    	<label for="filename" class="col-sm-3 control-label">
						    		<?php echo $language->get('text_select_xls_file'); ?>
						    	</label>
						        <div class="col-sm-5">	            
									<input type="file" class="form-control" name="filename" id="filename" accept=".xls" required>
						        </div>
						 	</div>
						    <div class="form-group">
						        <div class="col-sm-5 col-sm-offset-3">
							        <button type="submit" class="btn btn-success" name="submit">
							        	<span class="fa fa-fw fa-upload"></span> 
							          	<?php echo $language->get('button_import'); ?>
							        </button>
						        </div>
						    </div>
						</div>
				  	</form>
				</div>
			</div>
		</div>
	</section>
	<!-- Content End -->

</div>
<!-- Content Wrapper End -->

<?php include ("footer.php"); ?>