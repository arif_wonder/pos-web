<?php 
ob_start();
session_start();
include '../_init.php';

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}
// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_product')) {
	redirect(root_url() . '/admin/dashboard.php');
}
// LOAD LANGUAGE FILE
$language->load('management');
// LOAD PRODUCT MODEL
$promotions_model = $registry->get('loader')->model('promotions');
// FETCH PRODUCT INFO

$p_id = isset($request->get['id']) ? $request->get['id'] : '';
$promotions_type = isset($request->get['promotions_type']) ? $request->get['promotions_type'] : '';
$data = $promotions_model->getPromotions($promotions_type,$p_id);

if (count($data) <= 1) {
	redirect(root_url() . '/admin/promotions.php');
}
// SET DOCUMENT TITLE
$document->setTitle($language->get('title_product'));
// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php"); 
?>
<!-- Content Wrapper Start -->
<div class="content-wrapper">
  	<!-- Content Header Start -->
	<section class="content-header">
		<h1>
			<?php echo 
				//$language->get('text_dis_by_category'); 
			'Promotion';
				?> 
				&raquo; <?php echo $data['title'];?>
		</h1>
		<ol class="breadcrumb">
			<li>
				<a href="dashboard.php">
					<i class="fa fa-dashboard"></i> 
					<?php echo $language->get('text_dashboard'); ?>
				</a>
			</li>
			<li>
				<a href="discount_by_coupons.php">
					<?php echo $language->get('text_dis_by_category'); ?>
				</a>
			</li>
			<li class="active">
				<?php echo $data['title'];?>
			</li>
		</ol>
	</section>
  	<!-- Content Header End -->

	<!-- Content Start -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">

			    <div class="nav-tabs-custom">
	                <div class="tab-content">
	                    <div class="tab-pane active" id="details">
	                        <?php include '../_inc/template/promotions_view_form.php'; ?>
	                    </div>
	                </div>
	            </div>

			</div>
		</div>
	</section>
  	<!-- Content End -->
</div>
<?php include ("footer.php"); ?>