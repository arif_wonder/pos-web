<?php 
ob_start();
session_start();
include '../_init.php';
// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}
// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_damage_report')) {
	redirect(root_url() . '/admin/dashboard.php');
}
// LOAD LANGUAGE FILE
$language->load('management');
// SET DOCUMENT TITLE
$document->setTitle('Damage Product');
// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/DamageController.js');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php"); 
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper" ng-controller="DamageController">
  	<!-- Content Header Start -->
	<section class="content-header">
		<h1>
			<?php echo 'Damaged Product'; ?>
			<small>
				<?php echo store('name'); ?>	
			</small>
		</h1>
		<ol class="breadcrumb">
			<li>
				<a href="dashboard.php">
					<i class="fa fa-dashboard"></i> 
					<?php echo $language->get('text_dashboard'); ?>
				</a>
			</li>
			<li class="active">
				<?php echo 'Damage Product'; ?>	
			</li>
		</ol>
	</section>
  	<!-- Content Header End -->
	<!-- Content Start -->
	<section class="content">
		
	    


	    <div class="row">
		   
			    <div class="col-xs-12">
			        <div class="box box-success">
				        <div class="box-header">
				            <h3 class="box-title">
				            	<?php echo 'View All Damage Product'; ?>	
				            </h3>
				            <!--filter form start -->
				              <?php include('date_filter_form.php'); ?>
				               <!--filter form end --> 
				        </div>
						<div class="box-body">
							<div class="table-responsive">
								<?php
									$hide_colums = "";
									// if ($user->getGroupId() != 1) {
									// 	if (! $user->hasPermission('access', 'product_bulk_action')) {
									// 		$hide_colums .= "0,";
									// 	}
									// 	if (! $user->hasPermission('access', 'read_product')) {
									// 		$hide_colums .= "8,";
									// 	}
									// 	if (! $user->hasPermission('access', 'update_product')) {
									// 		$hide_colums .= "9,";
									// 	}
									// 	if (! $user->hasPermission('access', 'create_buying_invoice')) {
									// 		$hide_colums .= "10,";
									// 	}
									// 	if (! $user->hasPermission('access', 'product_return')) {
									// 		$hide_colums .= "11,";
									// 	}
									// 	if (! $user->hasPermission('access', 'print_barcode')) {
									// 		$hide_colums .= "12,";
									// 	}
									// 	if (! $user->hasPermission('access', 'delete_product')) {
									// 		$hide_colums .= "13,";
									// 	}
									// }
								?>  
								<table id="refund-list" class="table table-bordered table-striped table-hover" >
								    <thead>
								        <tr class="bg-gray">
								        	<th class="w-5 product-head text-center">
						                      <input type="checkbox" class="check-all" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
						                    </th>
								            <th class="w-5">
								            	Date
								            </th>
								            <th class="w-15">
								            	Product Name
								            </th>
								            <th class="w-15">
								            	Invoice Id
								            </th>
								            <th class="w-15">
								            	Image
								            </th>
								            <th class="w-15">
								            	Status
								            </th>
								            <th class="w-5">
								            	Customer
								            </th>
								            <th class="w-5">
								            	Sales person
								            </th>
								           
								        </tr>
								    </thead>
								    <tfoot>
										<tr class="bg-gray">
											<th class="w-5 product-head text-center">
						                      <input type="checkbox" class="check-all" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
						                    </th>
								            <th class="w-5">
								            	Date
								            </th>
								            <th class="w-15">
								            	Product Name
								            </th>
								            <th class="w-15">
								            	Invoice Id
								            </th>
								             <th class="w-15">
								            	Image
								            </th>
								            <th class="w-15">
								            	Status
								            </th>
								            <th class="w-5">
								            	Customer
								            </th>
								            <th class="w-5">
								            	Sales person
								            </th>
								           
								        </tr>
									</tfoot>
								</table>
							</div>
						</div>
			        </div>
			    </div>
			
	    </div>

	</section>
  	<!-- Content end -->

</div>
<!--  Content Wrapper End -->

<?php include ("footer.php"); ?>