<?php 
ob_start();
session_start();
include ("../_init.php");

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
// if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'import_product')) {
// 	redirect(root_url() . '/admin/dashboard.php');
// }

// LOAD LANGUAGE FILE
$language->load('import');
// LOAD LANGUAGE FILE
$language->load('brand');
$message = 'Please select a valid png,jpg,jpeg file';

// SET DOCUMENT TITLE
$document->setTitle('Product upload image');


// INCLUDE HEADER AND FOOTER
include("header.php");
include ("left_sidebar.php");


if(isset($_FILES["file"]))
{

	
	$files = $_FILES["file"]['name'];

	$validextensions = array("jpeg", "jpg", "png");
	$folder = $_SESSION['dbinfo']['db'];
	$pathfolder = ROOT . '/storage/products/'.$folder;
    if (!file_exists(ROOT . '/storage/products/'.$folder)) {
        mkdir(ROOT . '/storage/products/'.$folder, 0777, true);
        @chmod($pathfolder, 0777);
    }
	$uploadedfiles = [];
	foreach($files as $key => $file){
		$temporary = explode(".", $_FILES["file"]["name"][$key]);
		$file_extension = end($temporary);
		if ((($_FILES["file"]["type"][$key] == "image/png") || ($_FILES["file"]["type"][$key] == "image/jpg") || ($_FILES["file"]["type"][$key] == "image/jpeg")
		)
		&& in_array($file_extension, $validextensions)) {
			
			if ($_FILES["file"]["error"][$key] > 0) {
				
				echo "Return Code: " . $_FILES["file"]["error"][$key] . "<br/><br/>";

			} else {
				$rand = rand(rand(999, 111), rand(888, 222));
				$temp = explode(".", $_FILES["file"]["name"][$key]);
				//$newfilename = $_FILES["file"]["name"][$key];
				$newfilename = 'product_' .$rand. '.'.end($temp);
				$sourcePath = $_FILES["file"]["tmp_name"][$key]; // Storing source path of the file in a variable
				$targetPath = "../storage/products/".$folder."/".$newfilename; // Target path where file is to be stored

				if(move_uploaded_file($sourcePath,$targetPath)) {
					array_push($uploadedfiles, $newfilename);
				}; 
				
			}

		} else {

			echo "<span class='invalid'>***Invalid file Size or Type***<span>";
		}
	}
	 $_SESSION['import_pro_image_name'] = $uploadedfiles;
	// echo'File moved successfully';
	
	// header('location: export_image_name.php');
	$_SESSION['success_message_import_pro_image'] = 'Image moved successfully';
	header('location: upload_product_image.php');
	exit;
}

?>

<!-- Content Wrapper Start -->
<div class="content-wrapper">

	<!-- Content Header Start -->
	<section class="content-header">
		<h1>
		  <?php echo 'Upload Bulk Image for product'; ?>
			<small>
			  	<?php echo store('name'); ?>
			</small>
		</h1>
		<ol class="breadcrumb">
			<li>
			  	<a href="dashboard.php">
			  		<i class="fa fa-dashboard"></i>
			  		<?php echo $language->get('text_dashboard'); ?>
			  	</a>
			</li>
			<li class="active">
			  	<?php echo 'Upload image'; ?>
			</li>
		</ol>
	</section>
	<!-- Content Header End -->

	<!-- Content Start -->
	<section class="content">

		<div class="row">
			<div class="col-sm-12">
				<div class="box box-no-border">

					
						<?php 
						if(isset($message)){?>
							<div class="alert alert-info">
								<span class="fa fa-fw fa-info-circle"></span> 
								<?php echo $message ; ?>
							</div>
						<?php }?>

					<?php
						if(isset($_SESSION) && isset($_SESSION['success_message_import_pro_image']) && $_SESSION['success_message_import_pro_image'] !=''){?>
							<div class="alert alert-success">
					    <p>
					    	<span class="fa fa-check"></span> 
					    	<?php echo $_SESSION['success_message_import_pro_image'] ;
					    		unset($_SESSION['success_message_import_pro_image']);
					    	 ?>
					    </p>
					</div>
					
						<?php }
					?>

					<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
						<div class="box-body">

						  	<div class="form-group">
						    	<label for="filename" class="col-sm-3 control-label">
						    		<?php echo 'Select images(png,jpg,jpeg)'; ?>
						    	</label>
						        <div class="col-sm-5">	            
									<input type="file" class="form-control" name="file[]" id="filename" accept=".png, .jpg, .jpeg" required multiple>
						        </div>
						 	</div>
						    <div class="form-group">
						        <div class="col-sm-5 col-sm-offset-3">
							        <button type="submit" class="btn btn-success" name="submit">
							        	<span class="fa fa-fw fa-upload"></span> 
							          	<?php echo 'Upload'; ?>
							        </button>
						        </div>
						    </div>
						    <?php 
						    	if(isset($_SESSION['import_pro_image_name'])){?>
						    		<div class="form-group">
								        <div class="col-sm-5 col-sm-offset-3">
									        <a href="export_image_name.php"  >
									        	<span class="fa fa-fw fa-download"></span> 
									          	<?php echo 'Download Image Name'; ?>
									        </a>
								        </div>
								    </div>
						    	<?php }?>
						     
						</div>
				  	</form>
				</div>
			</div>
		</div>
	</section>
	<!-- Content End -->

</div>
<!-- Content Wrapper End -->

<?php include ("footer.php"); ?>