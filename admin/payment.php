<?php 
ob_start();
session_start();
include ("../_init.php");

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_payment_method')) {
  redirect(root_url() . '/admin/dashboard.php');
}

// LOAD LANGUAGE FILE
$language->load('payment');

// SET DOCUMENT TITLE
$document->setTitle($language->get('title_payment_method'));

// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/PaymentController.js');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php") ;
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper">

  <!-- Content Header Start -->
  <section class="content-header" ng-controller="PaymentController">
    <h1>
      <?php echo $language->get('text_payment_method_title'); ?>
      <small>
        <?php echo store('name'); ?>
      </small>
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="dashboard.php">
          <i class="fa fa-dashboard"></i> 
          <?php echo $language->get('text_dashboard'); ?>
        </a>
      </li>
      <li class="active">
        <?php echo $language->get('text_payment_method_title'); ?>
      </li>
    </ol>
  </section>
  <!-- Content Header End -->

  <!-- Content Start -->
  <section class="content">

   
    
    <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'create_payment_method')) : ?>
      <div class="box box-info<?php echo create_box_state(); ?>">
        <div class="box-header with-border">
          <h3 class="box-title">
            <span class="fa fa-fw fa-plus"></span> <?php echo $language->get('text_new_payment_method_title'); ?>
          </h3>
          <button type="button" class="btn btn-box-tool add-new-btn" data-widget="collapse" data-collapse="true">
            <i class="fa <?php echo !create_box_state() ? 'fa-minus' : 'fa-plus'; ?>"></i>
          </button>
        </div>
      
        <?php if (isset($error_message)): ?>
          <div class="alert alert-danger">
              <p>
                <span class="fa fa-warning"></span> 
                <?php echo $error_message ; ?>
              </p>
          </div>
        <?php elseif (isset($success_message)): ?>
          <div class="alert alert-success">
              <p>
                <span class="fa fa-check"></span> 
                <?php echo $success_message ; ?>
              </p>
          </div>
        <?php endif; ?>

        <!-- Include Payment Method Form -->
        <?php include('../_inc/template/payment_create_form.php'); ?>
      </div>
    <?php endif; ?>

    <div class="row">
      <form action="payment_bulk_action.php" method="post" enctype="multipart/form-data" id="payment-list-form">
      <div class="col-xs-12">
        <div class="box box-success">
          <div class="box-header">
            <h3 class="box-title">
              <?php echo $language->get('text_payment_method_list_title'); ?>
            </h3>
            <div class="box-tools pull-right">
              <!-- Filter Product Supplier Wise -->
               <?php //include('../_inc/template/partials/product_filter.php'); ?>
              <!-- Trash Box -->
                <!-- <div class="btn-group">
                  <a type="button" class="btn btn-danger" href="product.php?location=trash">
                      <span class="fa fa-trash"></span> 
                      <?php //echo $language->get('button_trash'); ?> 
                      <i class="badge badge-warning" id="total-trash">
                        <?php //echo total_trash_product(); ?>
                      </i>
                  </a>
                </div> -->
                <!-- Bulk Action -->
                <?php if ($user->getGroupId() == 1 || $user->hasPermission('access', 'product_bulk_action')) : ?>            
               <!--  <div class="btn-group">
                  <button type="button" class="btn btn-danger">
                      <?php echo $language->get('button_bulk'); ?>
                  </button>
                  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                      <li>
                        <a id="delete-all" href="#" data-form="#payment-list-form" data-loading-text="Deleting...">
                          <?php echo $language->get('button_delete_all'); ?>
                        </a>
                      </li>
                      <?php 
                      if(isset($request->get['location']) && $request->get['location'] == 'trash') : ?>
                      <li>
                        <a id="restore-all" href="#" data-form="#payment-list-form" data-datatable="payment-payment-list" data-loading-text="Restoring...">
                            <?php echo $language->get('button_restore_all'); ?>
                        </a>
                      </li>
                      <?php endif; ?>
                   </ul>
                </div> -->
              <?php endif; ?>
            </div>
          </div>
          <div class="box-body">
            <div class="table-responsive">  

                <?php
                  $hide_colums = "";
                  if ($user->getGroupId() != 1) {
                    if (! $user->hasPermission('access', 'update_payment_method')) {
                      $hide_colums .= "5,";
                    }
                    if (! $user->hasPermission('access', 'delete_payment_method')) {
                      $hide_colums .= "6,";
                    }
                  }
                ?> 
              
              <!-- Payment Method List Start -->
              <table id="payment-payment-list" class="table table-bordered table-striped table-hover" data-hide-colums="<?php echo $hide_colums; ?>">
                <thead>
                  <tr class="bg-gray">
                    <th class="w-5 product-head">
                      <input type="checkbox" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
                    </th>
                    <th class="w-5">
                      <?php echo sprintf($language->get('label_id'), null); ?>
                    </th>
                    <th class="w-35">
                      <?php echo sprintf($language->get('label_name'), null); ?>
                    </th>
                   <!--  <th class="w-5">
                      <?php echo $language->get('label_sort_order'); ?>
                    </th> -->
                    <th class="w-20">
                      <?php echo $language->get('label_details'); ?>
                    </th>
                    <th class="w-15">
                      <?php echo $language->get('label_status'); ?>
                    </th>
                    <th class="w-10">
                      <?php echo $language->get('label_edit'); ?>
                    </th>
                    <th class="w-10">
                      <?php echo $language->get('label_delete'); ?>
                    </th>
                  </tr>
                </thead>
                <tfoot>
                  <tr class="bg-gray">
                    <th class="w-5 product-head">
                      <input type="checkbox" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
                    </th>
                    <th class="w-5">
                      <?php echo sprintf($language->get('label_id'), null); ?>
                    </th>
                    <th class="w-35">
                      <?php echo sprintf($language->get('label_name'), null); ?>
                    </th>
                   <!--  <th class="w-5">
                      <?php echo $language->get('label_sort_order'); ?>
                    </th> -->
                    <th class="w-20">
                      <?php echo $language->get('label_details'); ?>
                    </th>
                    <th class="w-15">
                      <?php echo $language->get('label_status'); ?>
                    </th>
                    <th class="w-10">
                      <?php echo $language->get('label_edit'); ?>
                    </th>
                    <th class="w-10">
                      <?php echo $language->get('label_delete'); ?>
                    </th>
                  </tr>
                </tfoot>
              </table>
              <!-- Payment Method List End -->
            </div>
          </div>
        </div>
      </div>
      </form>
    </div>
  </section>
  <!-- Content End -->

</div>
<!-- Content Wrapper End -->

<?php include ("footer.php"); ?>