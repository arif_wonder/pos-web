<?php 
ob_start();
session_start();
include '../_init.php';
// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}
// REDIRECT, IF USER HAS NOT READ PERMISSION
// if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_product')) {
// 	redirect(root_url() . '/admin/dashboard.php');
// }
// LOAD LANGUAGE FILE
$language->load('management');
// SET DOCUMENT TITLE
$document->setTitle('Invoice');
// ADD SCRIPT

$body_class = $document->getBodyClass();
$title = $document->getTitle();


$description = $document->getDescription();
$keywords = $document->getKeywords();
$styles = $document->getStyles();
$scripts = $document->getScripts(); 

?>

<html xmlns="http://www.w3.org/1999/xhtml"  <?php echo !isset($angular_disabled) ? 'ng-app="angularApp"' : null; ?>>

<head>

    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!--[if !mso]><!-->

    <!--<![endif]-->
    <title>Invoice </title>
    <!--[if gte mso 9]>
	<style type="text/css" media="all">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->

    <style type="text/css" media="screen">
        /* Linked Styles */
        
        body {
            padding: 0 !important;
            margin: 0 !important;
            display: block !important;
            min-width: 100% !important;
            width: 100% !important;
            -webkit-text-size-adjust: none;
            font-family: arial;
        }
        
        a {
            color: #00349d;
            font-weight: bold;
            text-decoration: none
        }
        
        p {
            padding: 0 !important;
            margin: 0 !important;
            line-height: 22px;
            font-size: 13px;
        }
        
        img {
            -ms-interpolation-mode: bicubic;
            /* Allow smoother rendering of resized image in Internet Explorer */
        }
        
        .mcnPreviewText {
            display: none !important;
        }
        
        h4,
        h5,
        h3,
        h6 {
            padding: 0px;
            margin: 0px;
        }
        
        .qtd1 td {
            font-size: 12px;
            text-align: center;
        }

        .loading_img{
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
        }
    </style>

            <!-- jQuery JS  -->
        <script src="../assets/jquery/jquery.min.js" type="text/javascript"></script> 

        <!-- jQuery Ui JS -->
        <script src="../assets/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

        <!-- Bootstrap JS -->
        <script src="../assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

        <!-- Chart JS -->
        <script src="../assets/chartjs/Chart.min.js" type="text/javascript"></script>

        <!-- Jquery Sparkline JS -->
        <script src="../assets/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- Moment JS -->
        <script src="../assets/moment/moment.min.js" type="text/javascript"></script>
        <!-- Bootstrap Datepicker JS -->
        <script src="../assets/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

        <!-- Bootstrap Timepicker JS-->
        <script src="../assets/timepicker/bootstrap-timepicker.min.js" type="text/javascript" ></script>

        <!-- Bootstrap DateTimepicker JS-->
        <script src="../assets/datetimepicker/bootstrap-datetimepicker.js" type="text/javascript" ></script>
        <script src="../assets/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript" ></script>

        <!-- Bootstrap3 Wysihtml5 All JS -->
        <script src="../assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

        <!-- Select2 JS -->
        <script src="../assets/select2/select2.min.js" type="text/javascript"></script>

        <!-- Perfect Scrollbar JS -->
        <script src="../assets/perfectScroll/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>

        <!-- Sweetalert JS-->
        <script src="../assets/sweetalert/sweetalert.min.js" type="text/javascript"></script>

        <!-- Totastr JS -->
        <script src="../assets/toastr/toastr.min.js" type="text/javascript"></script>

        <!-- Accounting JS -->
        <script src="../assets/accounting/accounting.min.js" type="text/javascript"></script>

        <!-- IE JS -->
        <script src="../assets/wonderpillars/js/ie.js" type="text/javascript"></script>

        <!-- Theme JS -->
        <script src="../assets/wonderpillars/js/theme.js" type="text/javascript"></script>

        <!-- Common JS -->
        <script src="../assets/wonderpillars/js/common.js" type="text/javascript"></script>

        <!-- Main JS-->
        <script src="../assets/wonderpillars/js/main.js" type="text/javascript"></script>

        <!-- Angular JS -->
        <script src="../assets/wonderpillars/angularmin/angular.js" type="text/javascript"></script> 

        <!-- Angular App JS -->
        <script src="../assets/wonderpillars/angular/angularApp.js" type="text/javascript"></script>

        <!-- Angular Modal JS -->
        <script src="../assets/wonderpillars/angularmin/modal.js" type="text/javascript"></script>

        <!-- Anguar Filemanager JS -->
        <script src="../assets/wonderpillars/angularmin/filemanager.js" type="text/javascript"></script>
        <script src="../assets/wonderpillars/angular/controllers/InvoicePrintController.js" type="text/javascript"></script>
    
</head>

<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; font-family:arial; width:100% !important; background:#fff; -webkit-text-size-adjust:none;" ng-controller="InvoicePrintController">
<style type="text/css">
    img.img-fit {
    width: 150px;
    height: 100px;
    object-fit: contain;
}

</style>

    <img class="loading_img" ng-if="isLoading" src="../assets/loader.gif">
    <table ng-if="!isLoading" width="320px" border="0" cellspacing="0" cellpadding="0" style="margin:auto; margin-top:0px;">

        <tr>
            <td align="center" colspan="2">
               
                <?php if ($store->get('logo')): ?>
                  <img class="img-fit" src="<?php echo root_url(); ?>/assets/wonderpillars/img/logo-favicons/<?php echo $store->get('logo'); ?>">
                <?php else: ?>
                  <img class="img-fit" src="<?php echo root_url(); ?>/assets/wonderpillars/img/logo-favicons/nologo.jpeg">
                <?php endif; ?>
            </td>
        </tr>

        <tr>
            <td align="center" colspan="2" style="overflow: hidden;">
                <br>
                <h4 style="margin-bottom:8px; padding:0px; ">
                    <?php echo store('name'); ?> 
                </h4>
                <h6 style="margin-bottom:8px; padding:0px;"><?php echo store('address'); ?></h6>
                <h6 style="margin-bottom:8px; padding:0px; ">Invoice ID: {{ invoice_id }} </h6>
                <h6 style="margin-bottom:8px; padding:0px;">GST Number: <?php echo store('vat_reg_no'); ?> </h6>
            </td>
        </tr>

        <tr>
            <td align="center" colspan="2">
                <h5 style="margin-bottom:8px;">Date: {{ invoice_info.created_at }}</h5></td>
        </tr>

        <tr>
            <td style="text-align:left;">

                <h6>
								Cashier Name - {{ invoice_info.cashier }}
								<!-- <br> -->
								<!-- Order ID - 3452718 -->
							</h6>
            </td>

            <td style="text-align:right;">

                <h6>
								Customer Name - {{ invoice_info.customer_name }}
								<br>
								<!-- Mobile - {{ invoice_info.customer_mobile }} -->
							</h6>

            </td>
        </tr>

    </table>

    <table ng-if="!isLoading" width="320px" border="0" cellspacing="3" cellpadding="3" style="margin:auto; margin-top:10px; font-size:12px;">

        <tr style="border-bottom:dashed 1px #eee; border-top:dashed 1px #eee;">
            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;">S. No.</th>
            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;">
                Qty.
                <br> Unit
            </th>
            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;">
                MRP(Rs)
                <?php
                    if($store->get('gst_type') == 'SGSTCGST'){
                        echo '<br> CGST%';
                    }
                ?>
                
            </th>
            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;">
                Disc.(Rs)
                <?php
                    if($store->get('gst_type') == 'SGSTCGST'){
                            echo '<br> SGST%';
                    }
                    else{
                        echo '<br> IGST%';
                    }
                ?>
                <br> 
            </th >
            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c; text-align:right;">
                Amount(Rs)
                <br> (Incl. GST)
            </th>
        </tr>
      
            <tbody ng-repeat="item in invoice_items">
                <tr >
                    <td colspan="5">

                        <p style="text-align:left; font-weight:600;">
                           
                            {{ item.item_name }}
                        </p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p style="text-align:center; font-weight:600;">
                           {{ $index+1 }}
                        </p>
                    </td>
                    <td>
                        <p style="text-align:center; font-weight:600;"> 
                            
                            {{ item.item_quantity }}
                             <br>  {{ item.product.p_unit }}
                        </p>
                    </td>
                    <td>
                        <p style="text-align:center; font-weight:600;"> 
                            
                             
                            {{ item.product.buying_item.item_selling_price | formatDecimal:2 }}
                            <?php 
                            if($store->get('gst_type') == 'SGSTCGST')
                                {?>
                                    <br>
                                    {{ item.product.p_taxrate/2 | formatDecimal:2 }}
                                    <?php 
                                   
                                    ?>
                            <?php }?> 
                          </p>  
                    </td>
                    <td>
                        <p style="text-align:center; font-weight:600;"> 
                            <span ng-if="item.item_discount">
                                {{ item.item_discount | formatDecimal:2 }}
                            </span>

                            <span ng-if="!item.item_discount">
                                0.00
                            </span>

                          

                             <?php 
                               
                            
                            if($store->get('gst_type') == 'SGSTCGST'){?>
                                <br>  
                                {{ item.product.p_taxrate/2 | formatDecimal:2 }}
                                <?php 
                                // echo number_format(($product->product->p_taxrate/2),2); 
                                ?>
                            <?php }
                            else{?>
                                <br>  
                                 {{ item.product.p_taxrate | formatDecimal:2 }}
                                 <?php 
                                 
                                 ?>
                            <?php }?>
                            
                        </p>
                    </td>
                    <td>
                        <p style="text-align:right; font-weight:600;"> 
                            
                            {{ item.price_after_discount | formatDecimal:2 }}
                           
                              
                        </p>
                    </td>

                </tr>
            </tbody>

          
           
        

        

    </table>

    <table ng-if="!isLoading" width="320px" border="0" cellspacing="2" cellpadding="2" style="margin:auto; margin-top:10px; border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c; text-align:right; font-weight:600; font-size:12px;">
        <tbody>
            <tr>
                <td>
                    Gross Total
                </td>
                <td>
                  
                 
                   {{ invoice_info.gross_total | formatDecimal:2 }}
                </td>
            </tr>
            <tr>
                <td>
                    Total Discount(-)
                </td>
                <td>
                    {{ invoice_info.discount_amount | formatDecimal:2 }}
                    <?php 

                  
                    ?>
                </td>
            </tr>

           

            <tr>
                <td>
                    Previous Due(+)
                </td>
                <td>
                   
                    {{ invoice_info.previous_due | formatDecimal:2 }}
                </td>
            </tr>

             <tr>
                <td>
                    Credit Used(-)
                </td>
                <td>
                   
                    {{ invoice_info.paid_by_credit | formatDecimal:2 }}
                </td>
            </tr>

            <tr >
                <td style=" border-top:dashed 1px #3c3c3c;">
                    Payable Amount
                </td>
                <td style=" border-top:dashed 1px #3c3c3c;">
                   
                   
                    {{ calculatePayableAmount(invoice_info) | formatDecimal:2 }} 
                </td>
            </tr>
            <tr >
                <td >
                    Paid Amount
                </td>
                <td >
                   
                    
                    {{ paid_amount(invoice_info) | formatDecimal:2 }}
                </td>
            </tr>
            <tr>
                <td >
                    Present Dues
                </td>
                <td >
                   
                    {{ invoice_info.present_due | formatDecimal:2 }}
                </td>
            </tr>

            <tr >
                <td >
                    Payment Mode
                </td>

                <td >
                   
                    {{ invoice_info.payment_mode}}
                </td>
            </tr>
            

        </tbody>
    </table>
    <table ng-if="!isLoading" width="320px" border="0" cellspacing="3" cellpadding="2" style="margin:auto; margin-bottom:6px; margin-top:10px; border-bottom:dashed 1px #3c3c3c;  text-align:right; font-weight:600; font-size:12px;">
        <tbody>

            <tr>
                <td colspan="3">
                    <h5 style="font-size:16px; text-align:center; margin: 10px;">GST Details</h5></td>
            </tr>

            <tr>

                <tr>

                    <td>
                        <p style="text-align:center; font-weight:600;"> GST%
                        </p>
                    </td>
                    <td>
                        <p style="text-align:center; font-weight:600;"> Taxable </p>
                    </td>
                    <td>
                        <p style="text-align:center; font-weight:600;"> Total TAX </p>
                    </td>

                </tr>
              
                    <tbody ng-repeat="item in invoice_items">
                        <tr>
                            <td>
                                <p style="text-align:center; font-weight:600;"> 
                                   
                                    {{ item.product.p_taxrate | formatDecimal:2 }}
                                </p>
                            </td>
                            <td>
                            <p style="text-align:center; font-weight:600;"> 
                               

                                {{  calculateTaxPerItem(item) | formatDecimal:2 }}
                               
                            </p>
                            </td>
                            <td>
                                <p style="text-align:center; font-weight:600;">
                               
                                 {{ calculateTax(item) | formatDecimal:2 }}
                            </p>
                            </td>

                        </tr>
                    </tbody>
                  
            </tr>

            <tr>
                <td colspan="3" style="text-align:center; padding-bottom:2px; padding: 10px; border-top:dashed 1px #3c3c3c; ">
                  
                    <p style="font-size:18px;">
                      <?php
                              
                               
                                  if(unserialize($store->get('preference'))['invoice_footer_text']){
                                    echo unserialize($store->get('preference'))['invoice_footer_text'];
                                  }
                                  
                              ?>  
                     </p> 
                </td>
            </tr>

        </tbody>
    </table>
	<br>
	<div ng-if="!isLoading" align="center"><img src="../footer_logo.png" style="width:150px;"></div>
	<br>
    
    <script type="text/javascript">
        // $(document).ready(function(){
        //     var print = window.location.href.split('&');
        //     if(print[1] !== undefined && print[1] == 'print=true'){
        //         window.print();
        //     }
        // });
    </script>
</body>
</html>

