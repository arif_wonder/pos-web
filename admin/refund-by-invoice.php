<?php 
ob_start();
session_start();
include '../_init.php';
// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}
// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_product')) {
	redirect(root_url() . '/admin/dashboard.php');
}
// LOAD LANGUAGE FILE
$language->load('management');
// SET DOCUMENT TITLE
$document->setTitle($language->get('title_refund'));
// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/RefundByInvoiceController.js');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php"); 
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper" ng-controller="RefundByInvoiceController">
  	<!-- Content Header Start -->
	<section class="content-header">
		<h1>
			<?php echo $language->get('text_refund_product'); ?>
			<small>
				<?php echo store('name'); ?>	
			</small>
		</h1>
		<ol class="breadcrumb">
			<li>
				<a href="dashboard.php">
					<i class="fa fa-dashboard"></i> 
					<?php echo $language->get('text_dashboard'); ?>
				</a>
			</li>
			<li class="active">
				<?php echo $language->get('text_refund_product'); ?>	
			</li>
		</ol>
	</section>
  	<!-- Content Header End -->
	<!-- Content Start -->
	<section class="content">
		

	    <div class="row">
		    <form action="refund_by_invoice_bulk_action.php" method="post" enctype="multipart/form-data" id="refund-list-form">
			    <div class="col-xs-12">
			        <div class="box box-success">
				        <div class="box-header">
				            <h3 class="box-title">
				            	<?php echo sprintf($language->get('text_view_all'), $language->get('text_product_of_invoice_id').': '.$request->get['invoice_id']); ?>	
				            </h3>
							<?php if ($user->getGroupId() == 1) : ?>            
							<!-- <div class="btn-group">
							    <button type="button" class="btn btn-danger">
							      	<?php echo $language->get('button_bulk'); ?>
							    </button>
							    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
							        <span class="caret"></span>
							        <span class="sr-only">Toggle Dropdown</span>
							    </button>
							    <ul class="dropdown-menu" role="menu">
							        <li>
							        	<a id="delete-all-invoice" href="#" data-form="#refund-list-form" data-loading-text="Deleting...">
							        		<?php echo $language->get('button_return_all'); ?>
							        	</a>
							        </li>
							     </ul>
							</div> -->
							<?php endif; ?>
				        </div>
						<div class="box-body">
							<div class="table-responsive">
								<?php
									$hide_colums = "";
									if ($user->getGroupId() != 1) {
										if (! $user->hasPermission('access', 'product_bulk_action')) {
											$hide_colums .= "0,";
										}
										if (! $user->hasPermission('access', 'read_product')) {
											$hide_colums .= "8,";
										}
										if (! $user->hasPermission('access', 'update_product')) {
											$hide_colums .= "9,";
										}
										if (! $user->hasPermission('access', 'create_buying_invoice')) {
											$hide_colums .= "10,";
										}
										if (! $user->hasPermission('access', 'product_return')) {
											$hide_colums .= "11,";
										}
										if (! $user->hasPermission('access', 'print_barcode')) {
											$hide_colums .= "12,";
										}
										if (! $user->hasPermission('access', 'delete_product')) {
											$hide_colums .= "13,";
										}
									}
								?>  
								<table id="refund-by-invoice-list" class="table table-bordered table-striped table-hover" data-hide-colums="<?php echo $hide_colums; ?>">
								    <thead>
								        <tr class="bg-gray">
								           <!--  <th class="w-5 product-head text-center">
								            	<input type="checkbox" class="check-all" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
								            </th> -->
								            <th class="w-10">
								            	<?php echo $language->get('label_quantity'); ?>
								            </th>
								            <th class="w-40">
								            	<?php echo $language->get('label_product'); ?>
								            </th>
								            <th class="w-15">
								            	<?php echo $language->get('label_price'); ?>
								            </th>
								            <th class="w-10">
								            	<?php echo $language->get('label_discount'); ?>
								            </th>
								            <th class="w-10">
								            	<?php echo 'Tax'; ?>
								            </th>
								            <th class="w-10">
								            	<?php echo 'Return status'; ?>
								            </th>
								            <th class="w-10">
								            	<?php echo $language->get('label_action'); ?>
								            </th>

								        </tr>
								    </thead>
								    <tfoot>
										<tr class="bg-gray">
											<!-- <th class="w-5 product-head text-center">
									        	<input type="checkbox" class="check-all" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
									        </th> -->
									        <th class="w-10">
								            	<?php echo $language->get('label_quantity'); ?>
								            </th>
								            <th class="w-40">
								            	<?php echo $language->get('label_product'); ?>
								            </th>
								            <th class="w-15">
								            	<?php echo $language->get('label_price'); ?>
								            </th>
								            <th class="w-10">
								            	<?php echo $language->get('label_discount'); ?>
								            </th>
								            <th class="w-10">
								            	<?php echo 'Tax'; ?>
								            </th>
								            <th class="w-10">
								            	<?php echo 'Return status'; ?>
								            </th> 
								            <th class="w-10">
								            	<?php echo $language->get('label_action'); ?>
								            </th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
			        </div>
			    </div>
			</form>
	    </div>

	</section>
  	<!-- Content end -->

</div>
<!--  Content Wrapper End -->

<?php include ("footer.php"); ?>