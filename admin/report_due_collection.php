<?php
ob_start();
session_start();
include ("../_init.php");

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'read_due_collection_report')) {
  redirect(root_url() . '/admin/dashboard.php');
}

// LOAD LANGUAGE FILE
$language->load('due');

// SET DOCUMENT TITLE
$document->setTitle($language->get('title_due_report'));

// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/ReportDueCollectionController.js');

// ADD BODY CLASS
$document->setBodyClass('sidebar-collapse');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include ("left_sidebar.php");
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper" ng-controller="ReportDueCollectionController">

	<!-- Content Header Start -->
	<section class="content-header">
		<?php include ("../_inc/template/partials/apply_filter.php"); ?>
	  <h1>
	    <?php echo 'Outstanding payment report'; ?>
	    <small>
	    	<?php echo store('name'); ?>
	    </small>
	  </h1>
	  <ol class="breadcrumb">
	    <li>
	    	<a href="dashboard.php">
	    		<i class="fa fa-dashboard"></i> 
	    		<?php echo $language->get('text_dashboard'); ?>
	    	</a>
	    </li>
	    <li class="active">
	    	<?php echo 'Outstanding payment report'; ?>
	    </li>
	  </ol>
	</section>
	<!--Content Header End -->

	<!-- Content Start -->
	<section class="content">

		
	    
		<div class="row">
		    <div class="col-xs-12">
		      	<div class="box box-info">
		      		<div class="box-header">
				        <h3 class="box-title">
				        	<?php echo 'Outstanding payment report'; ?>
				        </h3>
					<!--filter form start -->
		              <?php include('date_filter_form.php'); ?>
		            <!--filter form end --> 
				     </div>
			      	<div class='box-body'>  
						<div class="table-responsive"> 

							<?php
					            $hide_colums = "";
					            if ($user->getGroupId() != 1) {
					              if (! $user->hasPermission('access', 'view_invoice')) {
					                $hide_colums .= "6,";
					              }
					            }
					          ?>   

					          <!-- Invoice List Start -->
							<table id="invoice-invoice-list"  class="table table-bordered table-striped table-hover" data-hide-colums="<?php echo $hide_colums; ?>">
							    <thead>
							      <tr class="bg-gray">
							        <!-- <th class="w-10">
							        	<?php //echo sprintf($language->get('label_id'), null); ?>
							        </th> -->
							        <th class="w-5 product-head text-center">
				                      <input type="checkbox" class="check-all" onclick="$('input[name*=\'select\']').prop('checked', this.checked);">
				                    </th>
							        <th class="w-30">
							        	<?php echo $language->get('label_customer_name'); ?>
							        </th>
							        <th class="w-20">
							        	<?php echo $language->get('label_mobile'); ?>
							        </th>
							        <th class="w-10">
							        	<?php echo $language->get('label_due_amount'); ?>
							        </th>
							        <th class="w-10">
							        	<?php echo $language->get('label_paid_amount'); ?>
							        </th>
							        <th class="w-10">
							        	<?php echo $language->get('label_present_due'); ?>
							        </th>
							        <th class="w-10">
							        	<?php echo $language->get('label_view'); ?>
							        </th>
							      </tr>
							    </thead>
							   
							</table>
							<!-- Invoice List End -->
						</div>  
			  		</div>
		      	</div>
		    </div>
	    </div>
	</section>
	<!-- Content End -->

</div>
<!-- Content Wrapper End -->

<?php include ("footer.php"); ?>