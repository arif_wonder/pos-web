<form action="" method="get" class="rep-for-pag">
  <div class="box-header with-border">
      <h3 class="box-title">
       From Date
       
       <input type="text" readonly="true" class="form-control pick_date" 
            id="date_from" name="from"
            value="<?php if(isset($_REQUEST['from'])) {echo $_REQUEST['from'];}else{echo '';}?>"
        >
      </h3>
    </div>

    <div class="box-header with-border">
      <h3 class="box-title">
       To Date
       
       <input type="text" readonly="true" class="form-control pick_date" id="date_to"  name="to"
        value="<?php if(isset($_REQUEST['to'])) { echo $_REQUEST['to'];}else{echo '';}  ?>"
       >
      </h3>
    </div>

    <div class="box-header with-border">
      <h3 class="box-title">
       <input type="submit"  class="form-control" name="submit" value="Search">
      </h3>
    </div>
</form>
<script type="text/javascript">
  $(function(){
  var year = new Date().getFullYear();
  var mindate = new Date();

  var maxdate = new Date();

  $("#date_from").datepicker({ 
    format: 'yyyy-mm-d', 
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,
    changeMonth: true,
    endDate : mindate,
  }).on('changeDate', function (ev) {
    if(ev.date){
       var minDate = new Date(ev.date.valueOf());
    }
   
    $('#date_to').datepicker('setStartDate', minDate);
  });

  $("#date_to").datepicker({ 
    format: 'yyyy-mm-d', 
    changeYear:true,
    clearBtn: true,
    todayHighlight: true,
    autoclose: true,   
    changeMonth: true,
    endDate : mindate,
  }).on('changeDate', function (ev) {
    if(ev.date){
          var minDate = new Date(ev.date.valueOf());
    }
    $('#date_from').datepicker('setEndDate', minDate);
  });
});
</script>
