<?php 
ob_start();
session_start();
include ("../_init.php");

// REDIRECT, IF USER IS NOT LOGGED IN
if (!$user->isLogged()) {
  redirect(root_url() . '/index.php?redirect_to=' . url());
}

// REDIRECT, IF USER HAS NOT READ PERMISSION
if (($user->getGroupId() != 1 && !$user->hasPermission('access', 'change_password')) || DEMO) {
  redirect(root_url() . '/admin/dashboard.php');;
}

// LOAD LANGUAGE FILE
$language->load('password');

// USER MODEL 
$user_model = $registry->get('loader')->model('user');

// FETCH ALL USER 
$users = $user_model->getUsers();

if(isset($request->post['form_change_password'])) 
{  
  
    try {

      // cCheck Update Permission
      if ($user->getGroupId() != 1 && !$user->hasPermission('access', 'change_password') || DEMO) {
        throw new Exception($language->get('error_password_permission'));
      }

      // Fetch User
      $the_user = $user_model->getUser($request->post['user_id']);
      if (!isset($the_user['id'])) {
        throw new Exception($language->get('error_password_user_found'));
      }

      if ($user->getGroupId() != 1) {

        // Old Passwod Validation
        if(empty($request->post['old'])) {
            throw new Exception($language->get('error_password_old'));
        }

        // Fetch User
        $old_password = md5($request->post['old']);

        // Check Old Passwrod
        if($old_password != $the_user['password']) {
            throw new Exception($language->get('error_password_old_wrong'));
        }     
      }
      
      // New Password Validation
      // if(!validateAlphanumeric($request->post['new1'])) {
      //     throw new Exception($language->get('error_password_new'));
      // }

      // Password  Length Check
      if(strlen($request->post['new1']) < 6) {
        throw new Exception($language->get('error_user_password_length'));
      }

      // Password  Length Check greater than 15
      if(strlen($request->post['new1']) > 15) {
        throw new Exception('Password should be less than or equal to 15 characters');
      }
      
      // Confirm Password Validation
      // if(!validateAlphanumeric($request->post['new2'])) {
      //     throw new Exception($language->get('error_password_old'));
      // }

      // Matching New and Confirm Password
      if($request->post['new1'] != $request->post['new2']) {
          throw new Exception($language->get('error_password_mismatch'));
      }
        
     
      $new_final_password = md5($request->post['new1']);
       
      // Updating Password
      $statement = $db->prepare("UPDATE `users` SET `password` = ?, `raw_password` = ? WHERE `id` = ?");
      $statement->execute(array($new_final_password, $request->post['new1'], $the_user['id']));
      $success_message = $language->get('text_success');
      $_SESSION['success_message_password_changed'] = 'Password changed successfully';
      header("Location: dashboard.php");
      exit;
    }
    catch(Exception $e) {
        $error_message = $e->getMessage();
    }
}

// SET DOCUMENT TITLE
$document->setTitle($language->get('title_password'));

// ADD BODY CLASS
$document->setBodyClass('password-change');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
include("left_sidebar.php");
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper">

  <!-- Content Header Start -->
  <section class="content-header">
    <h1>
      <?php echo $language->get('text_password_title'); ?>
      <small>
        <?php echo store('name'); ?>
      </small>
    </h1>
    <ol class="breadcrumb">
      <li>
        <a href="dashboard.php">
          <i class="fa fa-dashboard"></i> 
          <?php echo $language->get('text_dashboard'); ?>
        </a>
      </li>
      <li class="active">
        <?php echo $language->get('text_password_title'); ?>
      </li>
    </ol>
  </section>
  <!-- Content Header End -->

  <!-- Content Start -->
<section class="content">

  

  <!-- <?php if (isset($error_message)): ?>
    <div class="alert alert-danger">
        <p>
          <span class="fa fa-warning"></span>
           <?php echo $error_message ; ?>
        </p>
    </div>
  <?php elseif (isset($success_message)): ?>
    <div class="alert alert-success">
        <p>
          <span class="fa fa-check"></span>
           <?php echo $success_message ; ?>
        </p>
    </div>
  <?php endif; ?>  -->

 <!--  <div class="alert alert-success success-massage" style="display:none;">
        <p class="success-massage">
          <span class="fa fa-check id="success-massage"></span>
            password changed successfully.
        </p>
    </div> -->

  <div class="row">
    <div id="passChangeModalOpen" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" data-toggle="modal" data-target="#passChangeModal">
      <div class="panel panel-app password-panel">
        <div class="panel-body">
          <h2>
            <span class="icon">
              <svg class="svg-icon"><use href="#icon-btn-password"></svg>
            </span>
          </h2>
          <div class="small password-style">
            <?php echo $language->get('text_password_box_title'); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="passChangeModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">
              <span class="fa fa-fw fa-lock"></span> 
              <?php echo $language->get('text_password_title'); ?>
            </h4>
          </div>

          <div  class="alert alert-danger error-show" style="display:none;">
            <p class="error-show">
              <span id="error-show" class="fa fa-warning"></span>
              
            </p>
          </div>

          <div class="modal-body">   
            <form class="form-horizontal" id="password-form" action="" method="post" enctype="multipart/formdata">
              <input type="hidden" name="select_user" id="select_user">
              <div class="box-body">
                <?php if ($user->getGroupId() == 1) : ?>
                  <div class="form-group" id="own-password">
                    <label for="old" class="col-sm-4 control-label">
                      <?php echo $language->get('label_password_user'); ?>
                    </label>
                    <div class="col-sm-8">
                      <select name="user_id" id="select_user_id" class="form-control">
                        <?php foreach ($users as $the_user) : ?>
                          <option value="<?php echo $the_user['id']; ?>" <?php echo $the_user['id'] == $user->getId() ? 'selected' : null; ?>>
                            <?php echo $the_user['username'] . ' (' . $the_user['email'] . ')'; ?>
                          </option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                <?php else: ?>
                  <div class="form-group">
                    <div class="col-sm-8">
                        <?php foreach ($users as $the_user) : ?>
                          <?php if ($the_user['id'] == $user->getId()) : ?>
                            <input type="hidden" name="user_id" id="user_id" value="<?php echo $the_user['id']; ?>">
                          <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                  </div>
                <?php endif; ?>
                <?php if ($user->getGroupId() != 1) : ?>
                <div class="form-group">
                  <label for="old" class="col-sm-4 control-label">
                    <?php echo $language->get('label_password_old'); ?>
                  </label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="old" name="old" requied>
                  </div>
                </div>
                <?php endif; ?>
                <div class="form-group">
                  <label for="new1" class="col-sm-4 control-label">
                    <?php echo $language->get('label_password_new'); ?>
                  </label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="new1" name="new1" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="new2" class="col-sm-4 control-label">
                    <?php echo $language->get('label_password_confirm'); ?>
                  </label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="new2" name="new2" required>
                  </div>
                </div>
                <input type="hidden" name="form_change_password" value="form_change_password">
                <div class="form-group">
                  <label class="col-sm-4 control-label">&nbsp;</label>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-block btn-info pull-right" name="form_change_password" id="form_change_password">
                      <span class="fa fa-fw fa-pencil"></span> 
                      <?php echo $language->get('button_update'); ?>
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Content End -->
</div>
<!-- Content Wrapper End -->

<script type="text/javascript">
  $(document).ready(function(){
      // var url = window.location.href;
      // var params = url.split('?');
      var change_password = localStorage.getItem('change_password');
      var success_message = localStorage.getItem('password_success_message');
      //
      // if(params.length && params[1] !== undefined){
      //   var param = params[1].split('=');
      //   if(param.length && param[0] == 'change_password' && param[1] == 'true'){
      //     $('#passChangeModalOpen').click();
      //   }
      // }
      if(change_password){

         $('#passChangeModalOpen').click();
         $('#own-password').hide();
      }

      if(success_message){
         $('.success-massage').show();
         $('#success-massage').show();
         setTimeout(function(){ 
          localStorage.removeItem('password_success_message');
          $('.success-massage').hide();
          $('#success-massage').hide();
        }, 3000);
      }
      
  });

  $('#select_user_id').on('change', function() {
    $('#select_user').val(this.value);
  });



  $('#form_change_password').click(function(e){
    e.preventDefault();
    var user =  JSON.parse(localStorage.getItem('user_posapi'));
    var user_id = $('#select_user_id').val();
    // if(!user_id || user_id ==''){
    //   user_id = user.id;
    // }

    var password = $('#new1').val();
    var password_confirmation = $('#new2').val();
    
    var data = {
      password: password,
      password_confirmation: password_confirmation,
      user_id: user_id
    }



    if(password.length <6){
      $('.error-show').show();
      $('#error-show').show();
      $('#error-show').html('Password minimum six characters');
      return false;
    }
    else if(password.length >15){
      $('.error-show').show();
      $('#error-show').show();
      $('#error-show').html('Password should be less than or equal to 15 characters');
      return false;
    }
    else if(password != password_confirmation){
      $('.error-show').show();
      $('#error-show').show();
      $('#error-show').html("Confirm password didn't match");
      return false;
    }

    $('.error-show').hide();
    $('#error-show').hide();

    var admin_url = '<?php echo ADMIN_API ?>';
    
      $.ajax({
          beforeSend: function(request) {
            request.setRequestHeader("Authorization", "Bearer "+localStorage.getItem('token'));
          },
          url: admin_url+'/password/change',
          method: "POST",
          data: data,
          dataType: "json",
          success: function (response) {
            localStorage.removeItem('change_password');
            localStorage.setItem('password_success_message', true);
            localStorage.setItem('password', password);
            $('#password-form').submit();
          },
          error: function (response) {
            if(response.statusText == 'Unauthorized'){
              var login_data = {
                username: localStorage.getItem('email'),
                password: localStorage.getItem('password'),
              }
                $.ajax({
                  url: admin_url+'/login',
                  method: "POST",
                  data: login_data,
                  dataType: "json",
                  success: function (response) {
                    localStorage.setItem('token', response.data.token);
                    $('#form_change_password').click();
                  },
                  error: function (response) {
                    
                }});
            }
      }});
  });
</script>

<?php include ("footer.php"); ?>