<?php 
ob_start();
session_start();
include ("_init.php");

// LOAD LANGUAGE FILE
$language->load('invoice');

// VALIDATE INVOICE ID
if (!isset($request->get['invoice'])) { 
  redirect('/pos');
}

// INVOICE MODEL
$invoice_model = $registry->get('loader')->model('invoice');
$invoice_id = $request->get['invoice'];
$invoice_info = $invoice_model->getInvoiceInfo($invoice_id);

if (!$invoice_info) {
  redirect('/pos');
}

// FETCH INVOICE INFO
$invoice_info     = $invoice_model->getInvoiceInfo($invoice_id);
$inv_type         = $invoice_info['inv_type'];
$created_at     = format_date($invoice_info['created_at']);
$customer_id      = $invoice_info['customer_id'];
$customer_name    = $invoice_info['customer_name'];
$customer_contact = $invoice_info['customer_mobile'] 
                      ? $invoice_info['customer_mobile'] 
                      : $invoice_info['customer_email'];
$currency_code    = $invoice_info['currency_code'];
$payment_method   = get_the_payment_method($invoice_info['payment_method'], 'name');
$invoice_note     = $invoice_info['invoice_note'];

// FETCH INVOICE ITEMS
$invoice_items = $invoice_model->getInvoiceItems($invoice_id);

// FETCH INVOICE PRICE
$selling_price = $invoice_model->getSellingPrice($invoice_id);

// SET DOCUMENT TITLE
$document->setTitle($language->get('text_invoice') . ' - ' . $invoice_id);
 
// ADD SCRIPT
$document->addScript('../assets/wonderpillars/angular/controllers/InvoiceViewController.js');  

// ADD BODY CLASS
$document->setBodyClass('sidebar-collapse');

// INCLUDE HEADER AND FOOTER
include("header.php"); 
?>

<!-- Content Wrapper Start -->
<div class="content-wrapper" ng-controller="InvoiceViewController">

  <!-- Content Header Start -->
  <section class="content-header">
   
   
  </section>
  <!-- Content Header End -->

  <!-- Content Start -->
  <section class="content">

    
    <div class="row">
      <div class="col-xs-12">
        <div class="">
        	<div class='box-body'>    
            <div id="invoice" class="row">
              <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                <table class="table">
                  <tbody>
                    <tr class="invoice-header">
                      <td class="text-center" colspan="2">
                        <div class="invoice-header-info">
                          <div class="logo">
                           <!--  <?php echo $store->get('logo'); ?> -->
                            <?php if ($store->get('logo')): ?>
                              <img src="<?php echo root_url(); ?>/assets/wonderpillars/img/logo-favicons/1_logo.png">
                            <?php else: ?>
                              <img src="<?php echo root_url(); ?>/assets/wonderpillars/img/logo-favicons/nologo.png">
                            <?php endif; ?>
                          </div>
                          <h2 class="invoice-title"><?php echo store('name'); ?></h2>
                          <h4>
                            <strong>
                              <?php echo $language->get('label_invoice_id'); ?>: <?php echo $invoice_id; ?>
                            </strong>
                           <!--  <?php if (has_invoice_edit_permission($invoice_info['created_at'], $customer_id, $invoice_id)) : ?>
                                <a href="pos.php?customer_id=<?php echo $customer_id; ?>&invoice_id=<?php echo $invoice_id; ?>">
                                  &nbsp;<span class="fa fa-edit"></span>
                                </a>
                            <?php endif; ?> -->
                          </h4>
                          <div><?php echo $language->get('label_currency'); ?>: <?php echo $currency_code; ?></div>
                          <?php if (store('vat_reg_no')):?>
                            <p><?php echo $language->get('label_vat_reg_no'); ?>: <?php echo store('vat_reg_no'); ?></p>
                          <?php endif;?>
                          <h4><?php echo $language->get('label_date'); ?>: <?php echo $created_at; ?></h4>
                        </div>
                      </td>
                    </tr>
                    <tr class="invoice-address">
                      <td class="w-50 invoice-from">
                        <div>
                          <div class="com-details">
                            <p>
                              <strong>
                                <?php echo $language->get('text_from'); ?>
                              </strong></p>
                            <span class="invoice-address">
                              <?php echo store('address'); ?>
                            </span><br>
                            <span>
                              <?php echo $language->get('label_mobile'); ?>: <?php echo store('mobile'); ?>
                            </span><br>
                            <span>
                              <?php echo sprintf($language->get('label_email'), null); ?>: <?php echo store('email'); ?>
                            </span>
                          </div>
                        </div>
                      </td>
                      <td class="text-right invoice-to w-50">  
                        <p>
                          <strong>
                            <?php echo $language->get('text_to'); ?>
                          </strong>
                        </p>
                        <div>
                          <?php echo $language->get('label_customer_name'); ?>: <?php echo $customer_name ; ?> 
                          
                        </div>
                        <div>
                          <?php echo $language->get('label_mobile'); ?>/<?php echo sprintf($language->get('label_email'), null); ?>: <?php echo $customer_contact ; ?>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>

                <?php if ($inv_type != 'due_paid') : ?>
                <div class="table-responsive invoice-items">  
                  <table class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr class="active">
                				<th class="w-5 text-center">
                          #
                        </th>
                				<th class="w-40">
                          <?php echo $language->get('label_product_name'); ?>
                        </th>
                        <th class="w-10 text-center">
                          <?php echo $language->get('label_quantity'); ?>
                        </th>
                				<th class="text-right w-20">
                          <?php echo $language->get('label_price'); ?>
                        </th>
                				<th class="text-right w-20">
                          <?php echo $language->get('label_total'); ?>
                        </th>
                			</tr>
                    </thead>
                    <tbody>
                      <?php
                        $i=0;
                        foreach($invoice_items as $item) :
                          $i++; ?>
                          <tr>
                            <td class="text-center" data-title="#">
                              <?php echo $i ; ?>
                            </td>
                            <td data-title="<?php echo $language->get('label_product_name'); ?>">
                              <?php echo $item['item_name'] ; ?>
                            </td>
                            <td class="text-center" data-title="<?php echo $language->get('label_quantity'); ?>">
                              <?php echo $item['item_quantity']; ?>
                            </td>
                            <td class="text-right" data-title="<?php echo $language->get('label_price'); ?>">
                              <?php echo number_format($item['item_price'], 2); ?>
                            </td>
                            <td class="text-right" data-title='<?php echo $language->get('label_total'); ?>'>
                              <?php echo number_format($item['item_total'], 2); ?>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
                <?php endif; ?>

                <div class="table-responsive">
                  <table id="selling_bill" class="table">
                    <tbody>
                      <?php if ($inv_type != 'due_paid') : ?>
                      <tr class="active">
                      	<td class="w-80 text-right">
                          <?php echo $language->get('label_sub_total'); ?>
                        </td>
                      	<td class="w-20 text-right">
                          <?php echo number_format($selling_price['subtotal'],2); ?>
                        </td>
                      </tr>
                      <tr class="active">
                      	<td class="w-80 text-right">
                          <?php echo $language->get('label_discount_amount'); ?>
                        </td>
                      	<td class="w-20 text-right">
                          <?php echo $selling_price['discount_amount'] ? $selling_price['discount_amount'] : '0.00'; ?>
                        </td>
                      </tr>
                      <tr class="active">
                        <td class="w-80 text-right">
                          <?php echo $language->get('label_tax_amount'); ?>
                        </td>
                        <td class="w-20 text-right">
                          <?php echo $selling_price['tax_amount'] ? number_format($selling_price['tax_amount'], 2) : '0.00'; ?>
                        </td>
                      </tr>
                      <?php endif; ?>

                      <tr class="active">
                        <td class="w-80 text-right td-thick-border">
                          <?php 
                            echo $language->get('label_previous_due'); 
                            $due_amount = $selling_price['previous_due'];
                          ?>
                        </td>
                        <td class="w-20 text-right td-thick-border">
                          <?php echo number_format($due_amount, 2); ?>
                        </td>
                      </tr>

                      <?php if ($inv_type != 'due_paid') : ?>
                        <tr class="active">
                        	<td class="w-80 text-right">
                            <?php echo $language->get('label_payable_amount'); ?>
                          </td>
                        	<td class="w-20 text-right">
                            <?php echo number_format($selling_price['payable_amount'], 2); ?>
                          </td>
                        </tr>
                        <?php endif; ?>

                        <tr class="active">
                          <td class="w-80 text-right td-thick-border">
                            <?php echo $language->get('label_paid_amount'); ?> (<?php echo $payment_method; ?>) on <?php echo $created_at;?>
                          </td>
                          <?php $paid_amount = $selling_price['paid_amount']; ?>
                          <td class="w-20 text-right td-thick-border">
                            <?php echo number_format($paid_amount, 2); ?>
                          </td>
                        </tr>
                        <tr class="<?php echo $selling_price['present_due'] > 0 ? 'danger' : 'active';?>">
                          <td class="w-80 text-right">
                            <?php echo $language->get('label_due'); ?>
                          </td>
                          <td class="w-20 text-right">
                            <?php echo number_format($selling_price['present_due'], 2); ?>
                          </td>
                        </tr>
                        <?php if (!empty($invoice_model->getDuePaidRows($customer_id, $invoice_id))) : ?>
                          <?php 
                          $due_paid = 0;
                          foreach ($invoice_model->getDuePaidRows($customer_id, $invoice_id) as $row) : ?>
                            <tr class="success">
                              <td class="w-80 text-right">
                                Due paid on <?php echo $row['created_at'];?> by <?php echo get_the_user($row['created_by'], 'username');?>
                              </td>
                              <td class="w-20 text-right">
                                <?php 
                                $due_paid += $row['paid_amount'];
                                echo currency_format($row['paid_amount']); ?>
                              </td>
                            </tr>
                          <?php endforeach; ?>
                            <tr class="<?php echo $selling_price['present_due']-$due_paid > 0 ? 'danger' : 'active';?>">
                              <td class="w-80 text-right">
                                <?php echo $language->get('label_due'); ?>
                              </td>
                              <td class="w-20 text-right">
                                <?php echo number_format($selling_price['present_due']-$due_paid, 2); ?>
                              </td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($invoice_note) : ?>
                        <tr class="invoice-note">
                          <td colspan="2" class="warning text-center">
                            <br>
                            <p><?php echo $invoice_note; ?></p>
                          </td>
                        </tr>
                        <?php endif; ?>

                        <?php if (get_preference('invoice_footer_text')) : ?>
                        <tr>
                          <td colspan="2" class="text-center">
                            <br>
                            <h4>
                              <?php echo get_preference('invoice_footer_text'); ?>
                            </h4>
                          </td>
                        </tr>
                      <?php endif; ?>
                    </tbody>
                  </table>
                </div>

                <div class="invoice-header-info barcodes">
                  <?php
                  // $generator = barcode_generator();
                  // $symbology = barcode_symbology($generator, 'code_39');?>
                  <!-- <img src="data:image/png;base64,<?php //echo base64_encode($generator->getBarcode($invoice_id, $symbology, 1)); ?>" width="100"> -->
                  <?php
                    $qrcode_text = 'InvoiceID: ' . $invoice_id . ', Name: ' . $customer_name;
                    include(DIR_VENDOR.'/phpqrcode/qrlib.php');
                    QRcode::png($qrcode_text, ROOT.'/storage/qrcode.png', 'L', 3, 1);
                  ?>
                  <img src="../pos/storage/qrcode.png">
                </div>

                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr class="invoice-authority-cotainer">
                        <td class="w-50">
                          <div class="invoice_authority invoice_created_by">
                            <div class="name">
                              <?php echo $user->getUserName($invoice_info['created_by']); ?>
                            </div>
                            <div>
                              <?php echo $language->get('text_created_by'); ?>
                            </div>
                          </div>
                        </td>
                        <td class="w-50">
                          <div class="invoice_authority invoice_created_by">
                            <div class="name">
                              <?php echo $user->getUsername(store('cashier_id')); ?>
                            </div>
                            <div>
                              <?php echo $language->get('text_cashier'); ?>
                            </div>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>

                <div class="table-responsive footer-actions">
                  <table class="table">
                    <tbody>
                     
                    
                     
                      <tr class="text-center">
                        <td colspan="2">
                          <br>
                          <p class="powered-by">
                            <small>&copy; <?php echo store('name'); ?></small>
                          </p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>

              </div>
            </div> 
    		  </div> 
        </div>
      </div>
    </div>
  </section>
  <!-- Content End-->

</div>
<!-- Content Wrapper End -->

<?php include ("footer.php"); ?>